var jsonData ={
  "data": [
    {
      "section_name": "单选题",
      "question": "利用“牧运通”等信息化载体和手段，深入推行()，实现检疫监督全链条信息闭环管理。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "智慧检疫",
          "label": "A"
        },
        {
          "content": "智慧监管",
          "label": "B"
        },
        {
          "content": "智慧畜牧",
          "label": "C"
        },
        {
          "content": "智慧防疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家实行动物（     ）制度。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "疫病监测",
          "label": "A"
        },
        {
          "content": "疫情预警",
          "label": "B"
        },
        {
          "content": "动物检疫",
          "label": "C"
        },
        {
          "content": "疫病监测和疫情预警",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员参加旨在反对宪法、中国共产党领导和国家的集会、游行、示威等活动，并是活动的策划者、组织者和骨干分子，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，牛的畜禽种类代码是（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "1",
          "label": "A"
        },
        {
          "content": "2",
          "label": "B"
        },
        {
          "content": "3",
          "label": "C"
        },
        {
          "content": "4",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关作出准予行政许可的决定，应当自作出决定之日起（      ）内向申请人颁发、送达行政许可证件，或者加贴标签、加盖检验、检测、检疫印章。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "五日",
          "label": "A"
        },
        {
          "content": "七日",
          "label": "B"
        },
        {
          "content": "十日",
          "label": "C"
        },
        {
          "content": "十五日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（       ）负责组织制定国家生猪屠宰质量安全风险监测计划，对生猪屠宰环节的风险因素进行监测。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "国务院卫生健康主管部门",
          "label": "A"
        },
        {
          "content": "国务院农业农村主管部门",
          "label": "B"
        },
        {
          "content": "国务院农业农村主管部门和国务院卫生健康主管部门共同",
          "label": "C"
        },
        {
          "content": "农业农村部畜牧兽医局",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（     ）根据上级重大动物疫情应急预案和本地区的实际情况，制定本行政区域的重大动物疫情应急预案。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "县级以上地方人民政府",
          "label": "A"
        },
        {
          "content": "县级地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "县级地方人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》所称动物，是指（    ）、捕获的其他动物。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "家畜家禽",
          "label": "A"
        },
        {
          "content": "人工饲养",
          "label": "B"
        },
        {
          "content": "家畜家禽和人工喂养",
          "label": "C"
        },
        {
          "content": "家畜家禽和人工饲养",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，动物防疫实行预防为主，预防与（    ）相结合的方针。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "控制、净化",
          "label": "A"
        },
        {
          "content": "控制、消灭",
          "label": "B"
        },
        {
          "content": "净化、消灭",
          "label": "C"
        },
        {
          "content": "控制、净化、消灭",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，县级以上地方人民政府的动物卫生监督机构依照《动物防疫法》规定，负责（    ）工作。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "动物防疫",
          "label": "A"
        },
        {
          "content": "动物检疫",
          "label": "B"
        },
        {
          "content": "动物、动物产品的检疫",
          "label": "C"
        },
        {
          "content": "动物检疫与监督",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，（）应当在畜禽屠宰前，查验、登记畜禽标识。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "屠宰企业",
          "label": "B"
        },
        {
          "content": "农业农村主管部门",
          "label": "C"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "到2026年，全国畜间布病总体流行率有效降低，牛羊群体健康水平明显提高，个体阳性率控制在（）%以下。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "7",
          "label": "A"
        },
        {
          "content": "0.4",
          "label": "B"
        },
        {
          "content": "0.7",
          "label": "C"
        },
        {
          "content": "4",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政处罚决定书应当在宣告后当场交付当事人；当事人不在场的，行政机关应当在（      ）内依照《中华人民共和国民事诉讼法》的有关规定，将行政处罚决定书送达当事人。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "三日",
          "label": "A"
        },
        {
          "content": "五日",
          "label": "B"
        },
        {
          "content": "七日",
          "label": "C"
        },
        {
          "content": "十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，输入到无规定动物疫病区的相关易感动物，隔离检疫期为（    ）天。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "十五",
          "label": "A"
        },
        {
          "content": "二十一",
          "label": "B"
        },
        {
          "content": "三十",
          "label": "C"
        },
        {
          "content": "四十五",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "每名师资每年应承担不少于（）学时的官方兽医培训任务。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "6",
          "label": "A"
        },
        {
          "content": "7",
          "label": "B"
        },
        {
          "content": "8",
          "label": "C"
        },
        {
          "content": "4",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，（    ）的动物，以及用于科研、展示、演出和比赛等非食用性利用的动物，应当附有检疫证明。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "屠宰、经营",
          "label": "A"
        },
        {
          "content": "经营、运输",
          "label": "B"
        },
        {
          "content": "屠宰、运输",
          "label": "C"
        },
        {
          "content": "屠宰、经营、运输",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，　参与公职人员违法案件调查、处理的人员需要回避的，监察机关负责人的回避，由（）决定。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "上级监察机关",
          "label": "A"
        },
        {
          "content": "同级人民政府",
          "label": "B"
        },
        {
          "content": "监察机关负责人",
          "label": "C"
        },
        {
          "content": "被调查单位负责人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，政务处分决定自（）起生效。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "领导同意",
          "label": "A"
        },
        {
          "content": "党委批准",
          "label": "B"
        },
        {
          "content": "作出之日",
          "label": "C"
        },
        {
          "content": "行政拘留之日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，从事畜禽经营的销售者和购买者应当向所在地（）报告更新防疫档案相关内容。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "县级人民政府畜牧兽医行政主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府畜牧兽医主管部门",
          "label": "B"
        },
        {
          "content": "县级动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "县级动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，经营和运输的动物产品，应当附有（    ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "检疫证明",
          "label": "A"
        },
        {
          "content": "检疫标志",
          "label": "B"
        },
        {
          "content": "检疫证明、 检疫标志",
          "label": "C"
        },
        {
          "content": "检疫证明、检疫粘贴标志",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "官方兽医在屠宰过程中开展（    ）和必要的实验室疫病检测，并填写屠宰检疫记录。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "同步检疫",
          "label": "A"
        },
        {
          "content": "同步检验",
          "label": "B"
        },
        {
          "content": "同步检疫检验",
          "label": "C"
        },
        {
          "content": "三腺摘除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员受到降级以上政务处分的，应当由人事部门按照管理权限办理职务、工资及其他有关待遇等的变更手续，最长不得超过（）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "十五日",
          "label": "A"
        },
        {
          "content": "一个月",
          "label": "B"
        },
        {
          "content": "三个月",
          "label": "C"
        },
        {
          "content": "六个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，屠宰时收回的畜禽标识应当由（）保存、销毁。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "屠宰企业",
          "label": "B"
        },
        {
          "content": "农业农村主管部门",
          "label": "C"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "经屠宰检疫出具动物检疫证明，需要符合的条件不包括（    ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "待宰动物加施有畜禽标识",
          "label": "A"
        },
        {
          "content": "同步检疫合格",
          "label": "B"
        },
        {
          "content": "待宰动物临床检查健康",
          "label": "C"
        },
        {
          "content": "实验室疫病检测结果合格",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "防治牛结核病要求对感染牛及时进行（     ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "治疗",
          "label": "A"
        },
        {
          "content": "扑杀",
          "label": "B"
        },
        {
          "content": "出栏",
          "label": "C"
        },
        {
          "content": "隔离",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对于需使用抗菌药、抗病毒药、驱虫和杀虫剂、消毒剂等进行治疗的，要建立用药记录，并保存（     ）年以上。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "4",
          "label": "A"
        },
        {
          "content": "3",
          "label": "B"
        },
        {
          "content": "2",
          "label": "C"
        },
        {
          "content": "1",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《三类动物疫病防治规范》，患病水生动物养殖尾水应经（     ）后再行排放。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "阳光暴晒",
          "label": "A"
        },
        {
          "content": "过滤",
          "label": "B"
        },
        {
          "content": "澄清",
          "label": "C"
        },
        {
          "content": "无害化处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "农产品生产经营者对监督抽查检测结果有异议的，可以自收到检测结果之日起（ ）内，向实施农产品质量安全监督抽查的农业农村主管部门或者其上一级农业农村主管部门申请复检。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "三个工作日",
          "label": "A"
        },
        {
          "content": "五个工作日",
          "label": "B"
        },
        {
          "content": "十个工作日",
          "label": "C"
        },
        {
          "content": "十五个工作日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（     ）应当审核电子技术监控设备记录内容是否符合要求。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "执法人员",
          "label": "A"
        },
        {
          "content": "法制审核机构",
          "label": "B"
        },
        {
          "content": "行政机关",
          "label": "C"
        },
        {
          "content": "委托的技术鉴定机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "制定行政许可的目的是维护（     ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "公共利益和社会秩序",
          "label": "A"
        },
        {
          "content": "公共利益和经济秩序",
          "label": "B"
        },
        {
          "content": "国家利益和社会秩序",
          "label": "C"
        },
        {
          "content": "国家利益和经济秩序",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，经检疫合格的动物应当按照动物检疫证明载明的（    ）运输，并在（   ）内到达，运输途中发生疫情的应当按有关规定报告并处置。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "路径",
          "label": "A"
        },
        {
          "content": "目的地",
          "label": "B"
        },
        {
          "content": "5日",
          "label": "C"
        },
        {
          "content": "规定时间",
          "label": "D"
        },
        {
          "content": "当日",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对布鲁氏菌病实施强制免疫的，（）养殖场(户)应逐级报省级农业农村部门同意后实施。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "免疫区内不实施免疫的",
          "label": "A"
        },
        {
          "content": "非免疫区实施免疫的",
          "label": "B"
        },
        {
          "content": "免疫区内实施免疫的",
          "label": "C"
        },
        {
          "content": "非免疫区不实施免疫的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽标识制度坚持的原则是（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "统一规划",
          "label": "A"
        },
        {
          "content": "分类指导",
          "label": "B"
        },
        {
          "content": "分步实施",
          "label": "C"
        },
        {
          "content": "稳步推进",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "张三从外省引进10头种猪，到达后应当在（          ）进行隔离观察。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "隔离场",
          "label": "A"
        },
        {
          "content": "饲养场内的隔离舍",
          "label": "B"
        },
        {
          "content": "指定地点",
          "label": "C"
        },
        {
          "content": "独立的饲养场",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《中华人民共和国公职人员政务处分法》规定的人员的有（    )。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "人民政府的公务员",
          "label": "A"
        },
        {
          "content": "国有企业管理人员",
          "label": "B"
        },
        {
          "content": "法律授权管理公共事务组织中从事公务的人员",
          "label": "C"
        },
        {
          "content": "基层群众性组织中的长期聘用人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "定期集中组织开展牛羊（）等关键场所和环节“大清洗、大消毒”专项行动，有效消灭传染源。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "养殖",
          "label": "A"
        },
        {
          "content": "运输",
          "label": "B"
        },
        {
          "content": "屠宰",
          "label": "C"
        },
        {
          "content": "无害化处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，（　　）的撤销和疫区封锁的解除，按照国务院农业农村主管部门规定的标准和程序评估后，由原决定机关决定并宣布。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "疫点",
          "label": "A"
        },
        {
          "content": "疫区",
          "label": "B"
        },
        {
          "content": "受威胁区",
          "label": "C"
        },
        {
          "content": "疫源",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员泄露国家秘密、工作秘密，或者泄露因履行职责掌握的商业秘密、个人隐私，造成不良后果或者影响，情节较重的，予以（）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "记大过",
          "label": "A"
        },
        {
          "content": "降级",
          "label": "B"
        },
        {
          "content": "撤职",
          "label": "C"
        },
        {
          "content": "开除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，应当检疫出证的畜禽胴体包括畜禽经宰杀、放血后除去（   ）的躯体部分，也包括家畜躯体部分的（   ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "毛、头、尾及四肢后",
          "label": "A"
        },
        {
          "content": "二分体",
          "label": "B"
        },
        {
          "content": "毛、内脏、头、尾及四肢（腕及关节以下）后",
          "label": "C"
        },
        {
          "content": "四分体",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "违法《动物防疫法》规定，禁止屠宰、经营、运输动物和生产、经营、加工、贮藏、运输动物产品的，（    ）。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "由县级以上地方人民政府农业农村主管部门责令改正、采取补救措施",
          "label": "A"
        },
        {
          "content": "并处同类检疫合格动物、动物产品货值金额五倍以上三十倍以下罚款",
          "label": "B"
        },
        {
          "content": "并处同类检疫合格动物、动物产品货值金额十五倍以上三十倍以下罚款",
          "label": "C"
        },
        {
          "content": "没收违法所得动物和动物产品",
          "label": "D"
        },
        {
          "content": "并处同类检疫合格动物、动物产品货值金额十倍以上三十倍以下罚款",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "发生动物疫情时，生猪定点屠宰厂（场）未按照规定开展动物疫病检测的，下列处理、处罚正确的有（       ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "给予警告",
          "label": "A"
        },
        {
          "content": "由农业农村主管部门责令停业整顿，并处5000元以上5万元以下的罚款",
          "label": "B"
        },
        {
          "content": "由农业农村主管部门对其直接负责的主管人员和其他直接责任人员处2万元以上5万元以下的罚款",
          "label": "C"
        },
        {
          "content": "情节严重的，由设区的市级人民政府吊销生猪定点屠宰证书，收回生猪定点屠宰标志牌",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，县级以上地方人民政府农业农村主管部门执行监督检查任务，对染疫或者疑似染疫的动物、动物产品及相关物品进行（　　）措施，有关单位和个人不得拒绝或者阻碍。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "隔离",
          "label": "A"
        },
        {
          "content": "查封",
          "label": "B"
        },
        {
          "content": "扣押",
          "label": "C"
        },
        {
          "content": "处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽标识编码由（）组成。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "畜禽种类代码",
          "label": "A"
        },
        {
          "content": "县级行政区域代码",
          "label": "B"
        },
        {
          "content": "养殖场代码",
          "label": "C"
        },
        {
          "content": "标识顺序号",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽标识包括经农业农村部批准使用的（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "耳标",
          "label": "A"
        },
        {
          "content": "电子标签",
          "label": "B"
        },
        {
          "content": "脚环",
          "label": "C"
        },
        {
          "content": "其他承载畜禽信息的标识物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《2022—2025年全国官方兽医培训计划》总体目标，以提高官方兽医业务能力和综合素质为重点，加强官方兽医培训考核，强化培训条件保障，加快培养一支（）的官方兽医队伍，为有效开展动物检疫工作、保障养殖业生产安全和公共卫生安全提供有力支撑。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "业务精湛",
          "label": "A"
        },
        {
          "content": "作风优良",
          "label": "B"
        },
        {
          "content": "纪律严明",
          "label": "C"
        },
        {
          "content": "保障有力",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关违法实施行政许可，可能承担的法律责任有（       ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "民事赔偿",
          "label": "A"
        },
        {
          "content": "国家赔偿",
          "label": "B"
        },
        {
          "content": "刑事责任",
          "label": "C"
        },
        {
          "content": "行政责任",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各省级农业农村部门要对辖区内官方兽医统一进行编号管理，尽快完善动物检疫电子出证系统，实现（）关联，及时准确掌握官方兽医检疫出证情况。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "工作单位",
          "label": "A"
        },
        {
          "content": "官方兽医 ",
          "label": "B"
        },
        {
          "content": "检疫出证信息",
          "label": "C"
        },
        {
          "content": "检疫申报点",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物防疫法》规定，禁止屠宰、经营、运输下列（    ）动物及动物产品。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "疫区内易感染的",
          "label": "A"
        },
        {
          "content": "病死或者死因不明的",
          "label": "B"
        },
        {
          "content": "染疫或者疑似染疫的",
          "label": "C"
        },
        {
          "content": "依法应当检疫而未经检疫或检疫不合格的",
          "label": "D"
        },
        {
          "content": "封锁区内与发生动物疫病有关的",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "规范检疫出证，强化检疫监管，监督养殖主体和贩运人严格落实动物检疫申报、落地报告制度，实现动物调运（）全程可追溯。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "启运地",
          "label": "A"
        },
        {
          "content": "途经地",
          "label": "B"
        },
        {
          "content": "目的地",
          "label": "C"
        },
        {
          "content": "备案地",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政许可的基本特点是（      ）。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "依申请行为",
          "label": "A"
        },
        {
          "content": "强制性行政管理行为",
          "label": "B"
        },
        {
          "content": "外部行政行为",
          "label": "C"
        },
        {
          "content": "准予从事特定活动的行为",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，实施检疫的官方兽医应当在检疫证明、检疫标志上签字或者盖章，并对检疫结论负责。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "畜禽养殖场的选址、建设应当符合国土空间规划，不得违反法律法规的规定，在禁养区域建设畜禽养殖场。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽标识在清洗消毒后可以有条件重复使用。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，屠宰、经营、运输的动物产品，以及用于科研、展示、演出和比赛等非食用性利用的动物，应当附有检疫证明。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "申请取得种畜禽生产经营许可证，应当有与生产经营规模相适应的畜牧兽医技术人员。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "开展农产品质量安全监督检查，有权采取查封、扣押用于违法生产经营农产品的设施、设备、场所以及运输工具等措施。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "国务院农业农村主管部门根据行政区划、养殖屠宰产业布局、风险评估情况等对动物疫病实施分区防控，可以采取禁止或者限制特定动物、动物产品跨区域调运等措施。（   ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村部组织制定官方兽医培训大纲，审定培训教材和考试题库。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《国家畜禽遗传资源品种名录》规定，阿拉善双峰驼作为传统畜禽，是我国重要的地方品种。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "复检机构应当自收到复检样品之日起七个工作日内出具检测报告。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "养殖场户可根据本地区疫病流行情况，合理制定免疫程序，对危害严重的疫病实施免疫。\r\n",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》的规定，动物疫病净化、消灭的技术工作由动物疫病预防控制机构承担。（   ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村部规定，牛羊肉检疫验讫标志扎带总长300mm，厚1mm，宽5mm，扎带最下端的手牵部分长70mm。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《国家畜禽遗传资源品种名录》规定，吉林梅花鹿作为传统畜禽，在我国已有多年的饲养历史。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "拒绝或者阻碍官方兽医依法履行职责的；发现动物染疫、疑似染疫未报告，或者未采取隔离等控制措施的，由县级以上地方人民政府农业农村主管部门责令改正，可以处一万元以下罚款；拒不改正的，处一万元以上五万元以下罚款，并可以责令停业整顿。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "县级以上人民政府农业农村主管部门应当定期或者不定期组织对可能危及农产品质量安全的农药、兽药、饲料和饲料添加剂、肥料等农业投入品进行监督抽查，并公布抽查结果。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "国务院农业农村主管部门、市场监督管理部门依照本法和规定的职责，对农产品质量安全实施监督管理。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，销售者或购买者属于养殖场的，应及时在畜禽养殖档案中登记畜禽标识编码及相关信息变化情况。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）在“四、强化工作条件保障，提升动物运输监管能力”中指出，可以安排官方兽医、特聘动物协检员等参与相关工作，满足48小时值守需求。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "有序推进牛羊集中或定点屠宰，强化检疫检验，建立牛羊及其产品进出台账，记录来源和流向，确保可追溯。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》明确常规防治病种是弓形虫病、钩端螺旋体病、沙门氏菌病、日本脑炎(流行性乙型脑炎) 、猪链球菌Ⅱ型感染、旋毛虫病、囊尾蚴病、李氏杆菌病、类鼻疽、片形吸虫病、鹦鹉热、Q 热、利什曼原虫病、华支睾吸虫病。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，县级人民政府农业农村主管部门制定官方兽医培训计划，提供培训条件，定期对官方兽医进行培训和考核。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》所称动物产品，是指动物的肉、生皮、原毛、绒、脏器、脂、血液、精液、卵、胚胎、骨、蹄、头、角、筋以及可能传播动物疫病的奶、蛋等。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，尚未实施畜禽产品无纸化出证并实现电子证照互通互认的地区，对在本屠宰企业内分割加工的畜禽产品不再继续出证。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对到达目的地后分销的畜禽产品，不再重复出证。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）是为了深入贯彻落实《中华人民共和国动物防疫法》、《国务院办公厅关于加强非洲猪瘟防控工作的意见》（国办发〔2019〕31号）、《动物检疫管理办法》有关要求。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，经航空、铁路、道路、水路运输动物和动物产品的，托运人托运时应当提供检疫证明；没有检疫证明的，承运人不得承运。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《中华人民共和国畜牧法》的规定，有关部门对运输中的畜禽进行检查，应当有法律、行政法规的依据。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村部办公厅关于进一步加强官方兽医管理工作的通知（农办牧〔2023〕10号）要求各地要将从事动物检疫官方兽医的信息与所在动物检疫申报点关联。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，新出生畜禽，在出生后30天内离开饲养地的，应当在到达地加施畜禽标识。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫证明填写中，以下针对货主填写不正确的是（）",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "张三",
          "label": "A"
        },
        {
          "content": "某某公司",
          "label": "B"
        },
        {
          "content": "某某公司李四",
          "label": "C"
        },
        {
          "content": "李四",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，新出生畜禽，在出生后（）天内加施畜禽标识",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "10",
          "label": "A"
        },
        {
          "content": "20",
          "label": "B"
        },
        {
          "content": "30",
          "label": "C"
        },
        {
          "content": "60",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员篡改、伪造本人档案资料的，予以（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，从国外引进畜禽，在畜禽到达目的地（）内加施畜禽标识。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "10",
          "label": "A"
        },
        {
          "content": "20",
          "label": "B"
        },
        {
          "content": "30",
          "label": "C"
        },
        {
          "content": "60",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，（    ）应当按照所在地人民政府或农业农村主管部门的要求，参加动物疫病预防、控制和动物疫情扑灭等活动。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "执业兽医",
          "label": "A"
        },
        {
          "content": "乡村兽医",
          "label": "B"
        },
        {
          "content": "官方兽医",
          "label": "C"
        },
        {
          "content": "执业兽医、乡村兽医",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "高致病性禽流感、口蹄疫和小反刍兽疫免疫抗体合格率常年保持在()%以上。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "80",
          "label": "A"
        },
        {
          "content": "70",
          "label": "B"
        },
        {
          "content": "90",
          "label": "C"
        },
        {
          "content": "100",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对行政处罚不服的，有权依法申请（     ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "行政诉讼",
          "label": "A"
        },
        {
          "content": "行政复议",
          "label": "B"
        },
        {
          "content": "行政复议或者提起行政诉讼",
          "label": "C"
        },
        {
          "content": "听证、行政复议或者提起行政诉讼",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对(    )的畜禽产品，不再重复出证。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "到达目的地后贮存",
          "label": "A"
        },
        {
          "content": "到达目的地后分销",
          "label": "B"
        },
        {
          "content": "到达目的地后屠宰",
          "label": "C"
        },
        {
          "content": "已取得动物检疫证明",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "罚款、没收的违法所得或者没收非法财物拍卖的款项，必须全部上缴（     ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "行政机关",
          "label": "A"
        },
        {
          "content": "司法部门",
          "label": "B"
        },
        {
          "content": "税务部门",
          "label": "C"
        },
        {
          "content": "国库",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列哪种情形，行政机关可以撤销行政许可。\r\n（       ）",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "因不可抗力导致行政许可事项无法实施的",
          "label": "A"
        },
        {
          "content": "法人或者其他组织依法终止的",
          "label": "B"
        },
        {
          "content": "行政许可有效期届满未延续的",
          "label": "C"
        },
        {
          "content": "行政机关工作人员滥用职权、玩忽职守作出准予行政许可决定的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "布病强制免疫工作有效开展，免疫地区免疫密度常年保持在（）%以上。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "70",
          "label": "A"
        },
        {
          "content": "80",
          "label": "B"
        },
        {
          "content": "90",
          "label": "C"
        },
        {
          "content": "100",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，发生二类动物疫情后，疫点、疫区、受威胁区的划定机关是所在地（　　）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "B"
        },
        {
          "content": "县级以上动物卫生监督机构",
          "label": "C"
        },
        {
          "content": "县级防治重大动物疫病指挥部",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政处罚法》，执法人员当场收缴的罚款，应当自收缴罚款之日起（    ）内，交至行政机关",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "二日",
          "label": "A"
        },
        {
          "content": "三日",
          "label": "B"
        },
        {
          "content": "五日",
          "label": "C"
        },
        {
          "content": "七日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，养殖档案和防疫档案的保存时间，禽为（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "2年",
          "label": "A"
        },
        {
          "content": "10年",
          "label": "B"
        },
        {
          "content": "20年",
          "label": "C"
        },
        {
          "content": "长期",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "(      )组织协调居民委员会、村民委员会，做好本辖区流浪犬、猫的控制和处置，防止疫病传播。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "县级以上地方人民政府",
          "label": "A"
        },
        {
          "content": "县级人民政府 ",
          "label": "B"
        },
        {
          "content": "街道办事处、乡级人民政府",
          "label": "C"
        },
        {
          "content": "县级人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国务院农业农村主管部门应当设立（ ），对可能影响农产品质量安全的潜在危害进行风险分析和评估。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "农产品质量安全专家委员会",
          "label": "A"
        },
        {
          "content": "农产品质量安全风险管理专家委员会",
          "label": "B"
        },
        {
          "content": "农产品质量安全风险评估专家委员会",
          "label": "C"
        },
        {
          "content": "农产品质量安全风险监测专家委员会",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "布病净化和无疫小区建设工作扎实推进，内蒙古20%以上，辽宁、四川、陕西、甘肃、新疆50%以上，其他省份()%以上的牛羊种畜场（站）建成省级或国家级布病净化场、无疫小区；",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "60",
          "label": "A"
        },
        {
          "content": "80",
          "label": "B"
        },
        {
          "content": "40",
          "label": "C"
        },
        {
          "content": "50",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，出售、运输的生皮、原毛、绒、血液、角等产品，供体动物符合各项检疫合格条件，且（    )，出具动物检疫证明。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "动物疫病检测合格",
          "label": "A"
        },
        {
          "content": "按规定消毒合格",
          "label": "B"
        },
        {
          "content": "实验室检测合格",
          "label": "C"
        },
        {
          "content": "按规定消毒",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，动物疫病预防控制机构，承担动物疫病的监测、检测、诊断、流行病学调查、疫情报告以及其他预防、控制、（　　）等技术工作。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "免疫、消毒",
          "label": "A"
        },
        {
          "content": "净化、消灭",
          "label": "B"
        },
        {
          "content": "净化、消毒",
          "label": "C"
        },
        {
          "content": "免疫、消灭",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家生物安全总体情况、重大生物安全风险警示信息、重大生物安全事件及其调查处理信息等重大生物安全信息，由（ ）根据职责分工发布；其他生物安全信息由（ ）根据职责权限发布。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "国务院有关部门和县级以上地方人民政府及其有关部门；国家生物安全工作协调机制成员单位",
          "label": "A"
        },
        {
          "content": "国务院及其有关部门；县级以上地方人民政府及其有关部门",
          "label": "B"
        },
        {
          "content": "国家生物安全工作协调机制成员单位；其他相关单位",
          "label": "C"
        },
        {
          "content": "国家生物安全工作协调机制成员单位；国务院有关部门和县级以上地方人民政府及其有关部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》所称动物产品，是指动物的（    ）、生皮、原毛、绒、脏器、脂、血液、精液、卵、胚胎、骨、蹄、头、角、筋以及可能传播动物疫病的奶、蛋等。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "肉",
          "label": "A"
        },
        {
          "content": "分割肉",
          "label": "B"
        },
        {
          "content": "肉品",
          "label": "C"
        },
        {
          "content": "胴体",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，（ 　）组织群众做好本辖区的动物疫病预防与控制工作。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "县级以上人民政府",
          "label": "A"
        },
        {
          "content": "县级以上农业农村主管部门",
          "label": "B"
        },
        {
          "content": "县级人民政府",
          "label": "C"
        },
        {
          "content": "乡级人民政府、街道办事处",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，国家对严重危害养殖业生产和人体健康的动物疫病实施（    ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "追溯",
          "label": "A"
        },
        {
          "content": "检疫",
          "label": "B"
        },
        {
          "content": "监测",
          "label": "C"
        },
        {
          "content": "强制免疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，本行政区域内畜禽标识和养殖档案的监督管理工作由（）负责。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级畜牧兽医行政主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "县级以上地方政府畜牧兽医行政主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "按照《动物检疫管理办法》规定，申报检疫时不可以通过（   ）方式申报。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "传真",
          "label": "A"
        },
        {
          "content": "电子数据交换",
          "label": "B"
        },
        {
          "content": "申报点填报",
          "label": "C"
        },
        {
          "content": "电话",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪定点屠宰证书、生猪定点屠宰标志牌以及肉品品质检验合格验讫印章和肉品品质检验合格证的式样，由（       ）统一规定。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "国务院农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "设区的市级人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "省级以上人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "在每年3—5月、9—11月春秋两季集中免疫期间，对免疫进展实行（）制度。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "月报告",
          "label": "A"
        },
        {
          "content": "周报告",
          "label": "B"
        },
        {
          "content": "日报告",
          "label": "C"
        },
        {
          "content": "季报告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，出售或者运输的动物、动物产品取得官方兽医出具的（　　）后，方可离开产地。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "《动物检疫合格证明》和车辆消毒证明",
          "label": "A"
        },
        {
          "content": "《动物检疫合格证明》和无疫区证明",
          "label": "B"
        },
        {
          "content": "处理通知书",
          "label": "C"
        },
        {
          "content": "动物检疫证明",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "实施有限自然资源开发利用、公共资源配置以及直接关系公共利益的特定行业的市场准入等事项的行政许可的，行政机关应当通过（     ）等公平竞争的方式作出决定。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "协商",
          "label": "A"
        },
        {
          "content": "拍卖",
          "label": "B"
        },
        {
          "content": "招标",
          "label": "C"
        },
        {
          "content": "招标、拍卖",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，以下属于参与公职人员违法案件调查人员需要回避的情形有（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "被调查人的近亲属",
          "label": "A"
        },
        {
          "content": "本案的证人",
          "label": "B"
        },
        {
          "content": "与调查的案件有利害关系",
          "label": "C"
        },
        {
          "content": "曾经的同事",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，以下（）情形，应当对畜禽、畜禽产品实施追溯。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "标识与畜禽、畜禽产品不符",
          "label": "A"
        },
        {
          "content": "畜禽、畜禽产品染疫",
          "label": "B"
        },
        {
          "content": "畜禽、畜禽产品没有检疫证明",
          "label": "C"
        },
        {
          "content": "违规使用兽药",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，以下（）情形，应当对畜禽、畜禽产品实施追溯。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "违规使用有毒、有害物质",
          "label": "A"
        },
        {
          "content": "发生重大动物卫生安全事件",
          "label": "B"
        },
        {
          "content": "检疫证明过期",
          "label": "C"
        },
        {
          "content": "其他应当实施追溯的情形",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，以下属于政务处分的种类的有（）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "警告、记过",
          "label": "A"
        },
        {
          "content": "记大过、降级",
          "label": "B"
        },
        {
          "content": "行政拘留",
          "label": "C"
        },
        {
          "content": "撤职、开除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国畜牧法》规定，畜牧业生产经营者应当依法履行以下哪些义务，并接受有关主管部门依法实施的监督检查。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "动物防疫义务",
          "label": "A"
        },
        {
          "content": "动物福利义务",
          "label": "B"
        },
        {
          "content": "生态环境保护义务",
          "label": "C"
        },
        {
          "content": "畜禽产品安全",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物检疫管理办法》规定，动物检疫证章标志包括（    ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "耳标",
          "label": "A"
        },
        {
          "content": "动物检疫印章",
          "label": "B"
        },
        {
          "content": "动物检疫标志",
          "label": "C"
        },
        {
          "content": "动物检疫证明",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽养殖代码由（）组成，作为养殖档案编号。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "畜禽种类代码",
          "label": "A"
        },
        {
          "content": "县级行政区域代码",
          "label": "B"
        },
        {
          "content": "养殖场代码",
          "label": "C"
        },
        {
          "content": "顺序号",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，发生一类动物疫病时，当地县级以上人民政府农业农村主管部门应当立即派人到现场，划定（　　）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "疫点",
          "label": "A"
        },
        {
          "content": "疫区",
          "label": "B"
        },
        {
          "content": "受威胁区",
          "label": "C"
        },
        {
          "content": "疫源",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）应当建立生猪产品出厂（场）记录制度，以下属于应记录的内容的有（     ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "名称、规格、数量",
          "label": "A"
        },
        {
          "content": "检疫证明号、肉品品质检验合格证号",
          "label": "B"
        },
        {
          "content": "屠宰日期",
          "label": "C"
        },
        {
          "content": "出厂（场）日期",
          "label": "D"
        },
        {
          "content": "购货者名称、地址、联系方式",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，县级以上人民政府按照国务院的规定，根据（    ）原则建立动物疫病预防控制机构。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "统筹规划",
          "label": "A"
        },
        {
          "content": "合理布局",
          "label": "B"
        },
        {
          "content": "协同管理",
          "label": "C"
        },
        {
          "content": "综合设置",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫证明填写中，以下针对产品名称填写方式正确的有（）。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "猪肉",
          "label": "A"
        },
        {
          "content": "皮张",
          "label": "B"
        },
        {
          "content": "鸭血",
          "label": "C"
        },
        {
          "content": "脂肪",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "定期集中组织开展牛羊养殖、运输、屠宰、无害化处理等关键场所和环节“（）”专项行动，有效消灭传染源。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "大清洗",
          "label": "A"
        },
        {
          "content": "大消毒",
          "label": "B"
        },
        {
          "content": "大监测",
          "label": "C"
        },
        {
          "content": "大排查",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对散养动物，采取（）与（）相结合的方式进行强制免疫。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "春秋冬三季集中免疫",
          "label": "A"
        },
        {
          "content": "春秋两季集中免疫",
          "label": "B"
        },
        {
          "content": "定期补免",
          "label": "C"
        },
        {
          "content": "程序化免疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国家动物疫病强制免疫立足维护（）大局，坚持防疫优先，扎实开展动物疫病强制免疫，切实筑牢动物防疫屏障。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "养殖业发展安全",
          "label": "A"
        },
        {
          "content": "公共卫生安全",
          "label": "B"
        },
        {
          "content": "生物安全",
          "label": "C"
        },
        {
          "content": "动物安全",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公务员以及参照《中华人民共和国公务员法》管理的人员被撤职的，按照规定降低（  ），同时降低工资和待遇。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "职务",
          "label": "A"
        },
        {
          "content": "职级",
          "label": "B"
        },
        {
          "content": "衔级",
          "label": "C"
        },
        {
          "content": "级别",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，任何单位和个人不得（    )病死动物和病害动物产品。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "买卖",
          "label": "A"
        },
        {
          "content": "加工",
          "label": "B"
        },
        {
          "content": "随意弃置",
          "label": "C"
        },
        {
          "content": "处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "严格官方兽医管理，严禁（）官方兽医证。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "伪造",
          "label": "A"
        },
        {
          "content": "变造",
          "label": "B"
        },
        {
          "content": "转借",
          "label": "C"
        },
        {
          "content": "以其他方式违法使用",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，县级以上人民政府农业农村主管部门及其工作人员有（　　）行为之一的，由本级人民政府责令改正，通报批评。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "未及时采取预防、控制、扑灭等措施的",
          "label": "A"
        },
        {
          "content": "对不符合条件的颁发动物防疫条件合格证、动物诊疗许可证",
          "label": "B"
        },
        {
          "content": "对符合条件的拒不颁发动物防疫条件合格证、动物诊疗许可证的",
          "label": "C"
        },
        {
          "content": "从事与动物防疫有关的经营性活动",
          "label": "D"
        },
        {
          "content": "违法收取费用的",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列哪些情形，可以或不予行政处罚。（     ）",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "违法行为轻微并及时改正的",
          "label": "A"
        },
        {
          "content": "没有造成危害后果的",
          "label": "B"
        },
        {
          "content": "初次违法且危害后果轻微并及时改正的",
          "label": "C"
        },
        {
          "content": "当事人有证据足以证明没有主观过错的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，人畜共患传染病名录由国务院卫生健康部门会同国务院农业农村、野生动物保护等主管部门制定并公布。（   ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "坚持干什么学什么、缺什么补什么，聚焦主责主业，紧密结合官方兽医岗位要求和基层动物检疫监督工作实际确定培训内容，增强培训工作针对性和实效性。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，动物卫生监督机构的官方兽医和协检人员具体实施动物、动物产品检疫。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，动物卫生监督机构实施产地检疫时，应当查验畜禽标识。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "为尽快确认结果，进行农产品质量安全监督抽查检测复检时应采用快速检测方法。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村主管部门开展农产品质量安全风险监测和风险评估工作时，采集样品应当按照市场价格支付费用。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "布鲁氏菌病免疫指标：免疫地区免疫密度常年保持在90%以上，免疫建档率100%，免疫奶牛场备案率100%，免疫评价工作开展率100%。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，种用、乳用动物应当符合国务院农业农村主管部门规定的健康标准。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农户饲养的种畜禽用于自繁自养和有少量剩余仔畜、雏禽出售的，农户饲养种公畜进行互助配种的，不需要办理种畜禽生产经营许可证。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，当畜禽养殖场饲养多种动物时，可以有多个畜禽养殖代码。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "官方兽医岗前培训，每人参加培训时间不得少于30学时（含线上培训）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "经营动物、动物产品的集贸市场应当取得动物防疫条件合格证。（   ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》提出有条件的地区，可根据高致病性禽流感监测评估结果和防治实际探索建立免疫退出机制。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽标识实行一畜一标，编码在特殊情况下可以重复使用。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《中华人民共和国畜牧法》规定，销售种畜禽时可以不附具动物卫生监督机构出具的检疫证明，由购买者向动物卫生监督机构申报检疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《中华人民共和国畜牧法》规定，国家鼓励畜禽屠宰经营者直接从畜禽贩运者收购畜禽，建立稳定收购渠道，降低动物疫病和质量安全风险。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "为进一步规范畜禽屠宰检疫工作，方便畜禽产品流通，就新修订的《动物检疫管理办法》施行后屠宰检疫以及出具动物检疫证明有关事项，农业农村部办公厅印发了《关于进一步规范畜禽屠宰检疫有关工作的通知（农办牧〔2022〕31号）》。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》明确常规防治病种的数量是16种。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，进出口动物和动物产品，承运人凭进口报关单证或者海关签发的检疫单证运递。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "销售、收购国务院农业农村主管部门规定应当加施标识而没有标识的畜禽，或者重复使用畜禽标识的，由县级以上地方人民政府农业农村主管部门和市场监督管理部门分别责令改正，并处二千元以下罚款。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农产品质量安全监督抽查检测所产生的费用由被抽查人承担。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《中华人民共和国畜牧法》规定，畜禽遗传资源保护以国家为主、多元参与，坚持保护优先、高效利用的原则，实行分类分级保护。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "狂犬病是我国法定报告传染病中病死率最高的人兽共患病。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，病害动物产品，是指来源于病死动物的产品，或者经检验检疫可能危害人体或者动物健康的动物产品。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据动物疫病对养殖业生产和人体健康的危害程度，将《动物防疫法》规定的动物疫病分为四类。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "官方兽医参加线下培训的，应发放培训证书（证明），明确培训学时。对参加线上培训的，可自动累积学时。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "原则上以县为单位确定本省份的布鲁氏菌病免疫区和非免疫区。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，国家实行官方兽医（    ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "审核制度",
          "label": "A"
        },
        {
          "content": "考评制度",
          "label": "B"
        },
        {
          "content": "考核制度",
          "label": "C"
        },
        {
          "content": "任命制度",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "违法事实确凿并有法定依据，对法人或者其他组织处以（      ）罚款或者警告的行政处罚的，可以当场作出行政处罚决定。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "一千元以下",
          "label": "A"
        },
        {
          "content": "二千元以下",
          "label": "B"
        },
        {
          "content": "三千元以下",
          "label": "C"
        },
        {
          "content": "五千元以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（      ）按照动物疫病净化、消灭规划、计划，对动物疫病净化效果进行监测、评估。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "省级动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "省级动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "为解决地方人民政府两个以上部门分别实施的行政许可事项，规定了三种可供选择的办理方式，下列（    ）项不是。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "集中办理",
          "label": "A"
        },
        {
          "content": "统一办理",
          "label": "B"
        },
        {
          "content": "联合办理",
          "label": "C"
        },
        {
          "content": "一个窗口对外",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关作出行政许可决定，依法需要听证、招标、拍卖、检验、检测、检疫、鉴定和专家评审的，行政机关应当将所需时间（     ）告知申请人。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "书面",
          "label": "A"
        },
        {
          "content": "口头",
          "label": "B"
        },
        {
          "content": "书面或口头",
          "label": "C"
        },
        {
          "content": "以任何可行的方式",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，养殖档案和防疫档案的保存时间，牛为（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "2年",
          "label": "A"
        },
        {
          "content": "10年",
          "label": "B"
        },
        {
          "content": "20年",
          "label": "C"
        },
        {
          "content": "长期",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "专业从事病死畜禽和病害畜禽产品收集的单位和个人，应当配备专用运输车辆，并向承运人所在地（     ）备案。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "县级人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级动物卫生监督机构",
          "label": "B"
        },
        {
          "content": "县级动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "县级人民政府交通运输主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "跨省、自治区、直辖市引进的（    ）到达输入地后，应当在隔离场或者饲养场内的隔离舍进行隔离观察，隔离期为三十天。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "继续饲养的动物",
          "label": "A"
        },
        {
          "content": "乳用、种用动物",
          "label": "B"
        },
        {
          "content": "乳用动物",
          "label": "C"
        },
        {
          "content": "种用动物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《国家畜禽遗传资源品种名录》规定，以下属于规定的传统畜禽的是（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "羊驼",
          "label": "A"
        },
        {
          "content": "东北马鹿",
          "label": "B"
        },
        {
          "content": "莱茵鹅",
          "label": "C"
        },
        {
          "content": "珍珠鸡",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽养殖场养殖档案及种畜个体养殖档案格式由（）统一制定。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级畜牧兽医行政主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "县级以上地方政府畜牧兽医行政主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "强制免疫动物疫病的应免畜禽免疫密度应达到（）%。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "80",
          "label": "A"
        },
        {
          "content": "90",
          "label": "B"
        },
        {
          "content": "100",
          "label": "C"
        },
        {
          "content": "70",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "到2025年，国家、省、市、县四级官方兽医培训体系进一步健全完善，官方兽医知识结构持续更新优化，专业能力和综合素质明显提升，省、市、县三级动物卫生监督机构官方兽医完训率达到100%，考试合格率达到（）％以上。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "100",
          "label": "A"
        },
        {
          "content": "90",
          "label": "B"
        },
        {
          "content": "95",
          "label": "C"
        },
        {
          "content": "80",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国畜牧法》规定，进行交易的畜禽应当（       ）和国务院有关部门规定的技术要求。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "符合动物健康标准",
          "label": "A"
        },
        {
          "content": "符合动物防疫标准",
          "label": "B"
        },
        {
          "content": "符合动物福利标准",
          "label": "C"
        },
        {
          "content": "符合农产品质量安全标准",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，拒不按照规定纠正特定关系人违规任职、兼职或者从事经营活动，且不服从职务调整的，予以（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "开除",
          "label": "A"
        },
        {
          "content": "撤职",
          "label": "B"
        },
        {
          "content": "留党察看",
          "label": "C"
        },
        {
          "content": "党内警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（     ）应当定期组织开展行政执法评议、考核，加强对行政处罚的监督检查，规范和保障行政处罚的实施。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "省级以上人民政府",
          "label": "A"
        },
        {
          "content": "县级以上人民政府",
          "label": "B"
        },
        {
          "content": "省级以上人民代表大会",
          "label": "C"
        },
        {
          "content": "县级以上人民代表大会",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽养殖代码由（）按照备案顺序统一编号。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "县级人民政府畜牧兽医行政主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府畜牧兽医主管部门",
          "label": "B"
        },
        {
          "content": "县级动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "县级动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，动物饲养场、屠宰企业的执业兽医或者动物防疫技术人员，应当（    ）官方兽医实施检疫。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "配合",
          "label": "A"
        },
        {
          "content": "辅助",
          "label": "B"
        },
        {
          "content": "协助",
          "label": "C"
        },
        {
          "content": "指导",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员对监察机关作出的涉及本人的政务处分决定不服的，可以依法向作出决定的监察机关申请（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "复审",
          "label": "A"
        },
        {
          "content": "复议",
          "label": "B"
        },
        {
          "content": "申诉",
          "label": "C"
        },
        {
          "content": "复核",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（    ）根据动物疫病净化、消灭规划，制定并组织实施本行政区域的动物疫病净化、消灭计划。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "县级以上人民政府",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "B"
        },
        {
          "content": "省、自治区、直辖市人民政府",
          "label": "C"
        },
        {
          "content": "省、自治区、直辖市人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，国家实行官方兽医（    ）制度。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "指派",
          "label": "A"
        },
        {
          "content": "考核",
          "label": "B"
        },
        {
          "content": "指定",
          "label": "C"
        },
        {
          "content": "任命",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，跨省、自治区、直辖市引进的种用、乳用动物到达输入地后，货主应当按照国务院农业农村主管部门的规定对引进的种用、乳用动物进行（    ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "隔离",
          "label": "A"
        },
        {
          "content": "观察",
          "label": "B"
        },
        {
          "content": "隔离观察",
          "label": "C"
        },
        {
          "content": "防疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对直接关系当事人或者第三人重大权益，在行政机关负责人作出行政处罚的决定之前，应当由（      ）进行法制审核。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "司法机关",
          "label": "A"
        },
        {
          "content": "行政机关负责人",
          "label": "B"
        },
        {
          "content": "执法人员",
          "label": "C"
        },
        {
          "content": "法制审核人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "官方兽医实施动物检疫工作时，应当持有（     ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "工作证",
          "label": "A"
        },
        {
          "content": "动物检疫证",
          "label": "B"
        },
        {
          "content": "农业行政执法证",
          "label": "C"
        },
        {
          "content": "官方兽医证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列选项属于可以不设立行政许可的事项是（    ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "直接关系公共安全、人身健康、生命财产安全的重要设备、设施、产品、物品，需要按照技术标准、技术规范，通过检验、检测、检疫等方式进行审定的事项",
          "label": "A"
        },
        {
          "content": "企业或者其他组织的设立等，需要确定主体资格的事项",
          "label": "B"
        },
        {
          "content": "通过市场竞争机制能够有效调节的事项",
          "label": "C"
        },
        {
          "content": "提供公众服务并且直接关系公共利益的职业、行业，需要确定具备特殊信誉、特殊条件或者特殊技能等资格、资质的事项",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "中央和省级财政养殖环节病死畜禽无害化处理补助资金下达后，市县财政应在（     ）内将补助资金给付到位。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "六个月",
          "label": "A"
        },
        {
          "content": "四个月",
          "label": "B"
        },
        {
          "content": "二个月",
          "label": "C"
        },
        {
          "content": "三个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，养殖档案和防疫档案的保存时间，种畜禽为（）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "2年",
          "label": "A"
        },
        {
          "content": "10年",
          "label": "B"
        },
        {
          "content": "20年",
          "label": "C"
        },
        {
          "content": "长期",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《全国畜间人兽共患病防治规划2022—2030年）》提出的日本血吸虫病的重点防治措施的是（     ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "开展家畜疫情监测",
          "label": "A"
        },
        {
          "content": "加强家畜传染源管理",
          "label": "B"
        },
        {
          "content": "实施农业灭螺工程",
          "label": "C"
        },
        {
          "content": "禁止易感动物调运",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰场应当严格执行动物入场查验登记、待宰巡查等制度，查验进场生猪的（      ）。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "运输车辆备案情况",
          "label": "A"
        },
        {
          "content": "动物检疫证明",
          "label": "B"
        },
        {
          "content": "运输车辆消毒情况",
          "label": "C"
        },
        {
          "content": "畜禽标识",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "违反生物安全法规定，从事国家禁止的生物技术研究、开发与应用活动的，由县级以上人民政府（ ）主管部门根据职责分工，责令停止违法行为，没收违法所得、技术资料和用于违法行为的工具、设备、原材料等物品，处罚款，并可以依法禁止一定期限内从事相应的生物技术研究、开发与应用活动，吊销相关许可证件。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "卫生健康",
          "label": "A"
        },
        {
          "content": "科学技术",
          "label": "B"
        },
        {
          "content": "农业农村",
          "label": "C"
        },
        {
          "content": "应急管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）、其他单位和个人对生猪、生猪产品注水或者注入其他物质的，由农业农村主管部门（       ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "没收注水或者注入其他物质的生猪、生猪产品、注水工具和设备以及违法所得",
          "label": "A"
        },
        {
          "content": "货值金额不足1万元的，并处5万元以上10万元以下的罚款",
          "label": "B"
        },
        {
          "content": "货值金额1万元以上的，并处货值金额10倍以上20倍以下的罚款",
          "label": "C"
        },
        {
          "content": "对生猪定点屠宰厂（场）或者其他单位的直接负责的主管人员和其他直接责任人员处5万元以上10万元以下的罚款",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列哪些事项可以不设定行政许可？（      ）",
      "standard_answer": "A,B,D,E",
      "option_list": [
        {
          "content": "公民、法人或者其他组织能够自主决定的",
          "label": "A"
        },
        {
          "content": "市场竞争机制能够有效调节的",
          "label": "B"
        },
        {
          "content": "企业或者其他组织的设立等，需要确定主体资格的事项",
          "label": "C"
        },
        {
          "content": "行业组织或者中介机构能够自律管理的",
          "label": "D"
        },
        {
          "content": "行政机关采用事后监督等其他行政管理方式能够解决的",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《国家畜禽遗传资源品种名录》规定，以下属于规定的传统畜禽的有（）。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "文昌鸡",
          "label": "A"
        },
        {
          "content": "矮脚鸡",
          "label": "B"
        },
        {
          "content": "中国山鸡",
          "label": "C"
        },
        {
          "content": "珍珠鸡",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员接受、提供可能影响公正行使公权力的宴请、旅游、健身、娱乐等活动安排，情节严重的，予以（）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "记大过",
          "label": "A"
        },
        {
          "content": "降级",
          "label": "B"
        },
        {
          "content": "撤职",
          "label": "C"
        },
        {
          "content": "开除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "患有人畜共患传染病的人员不得直接从事动物疫病监测、检测、检验检疫、诊疗以及易感染动物的（      ）等活动。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "饲养",
          "label": "A"
        },
        {
          "content": "屠宰",
          "label": "B"
        },
        {
          "content": "经营",
          "label": "C"
        },
        {
          "content": "隔离",
          "label": "D"
        },
        {
          "content": "运输",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对（）的家禽，有关养殖场(户)逐级报省级农业农村部门同意后，可不实施免疫。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "养殖户不愿免疫的",
          "label": "A"
        },
        {
          "content": "进口国(地区)明确要求不得实施高致病性禽流感免疫的出口家禽",
          "label": "B"
        },
        {
          "content": "供研究和疫苗生产用的",
          "label": "C"
        },
        {
          "content": "其他特殊原因不免疫的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，适于封锁的情况是（　　）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "发生非洲猪瘟时",
          "label": "A"
        },
        {
          "content": "狂犬病呈暴发性流行时",
          "label": "B"
        },
        {
          "content": "发生高致病性禽流感时",
          "label": "C"
        },
        {
          "content": "鳖腮腺炎病呈暴发性流行时",
          "label": "D"
        },
        {
          "content": "新城疫发生时",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，以下属于畜禽养殖场的养殖档案登记内容的有（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "畜禽的品种、数量",
          "label": "A"
        },
        {
          "content": "投入品和兽药的来源",
          "label": "B"
        },
        {
          "content": "监测、消毒情况",
          "label": "C"
        },
        {
          "content": "畜禽养殖代码",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列关于动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所审查发证说法正确的是：（ ）。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "场所建设竣工后，应当向所在地县级人民政府农业农村主管部门提出申请，并提交相应材料。",
          "label": "A"
        },
        {
          "content": "申请材料不齐全或者不符合规定条件的，县级人民政府农业农村主管部门应当自收到申请材料之日起十个工作日内，一次性告知申请人需补正的内容。",
          "label": "B"
        },
        {
          "content": "县级人民政府农业农村主管部门应当自受理申请之日起十五个工作日内完成材料审核，并结合选址综合评估结果完成现场核查，审查合格的，颁发动物防疫条件合格证。",
          "label": "C"
        },
        {
          "content": "审查不合格的，应当书面通知申请人，并说明理由。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物防疫法》，下面叙述正确的是（　　）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "国家支持地方建立无规定动物疫病区",
          "label": "A"
        },
        {
          "content": "国家鼓励动物饲养场建设无规定动物疫病生物安全隔离区",
          "label": "B"
        },
        {
          "content": "国务院农业农村主管部门对动物疫病实施分区防控",
          "label": "C"
        },
        {
          "content": "无规定动物疫病区需经国务院农业农村主管部门验收合格予以公布",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国家鼓励社会力量参与动物防疫工作。各级人民政府采取措施，支持单位和个人参与动物防疫的（     ）等活动。",
      "standard_answer": "A,B,D,E",
      "option_list": [
        {
          "content": "宣传教育",
          "label": "A"
        },
        {
          "content": "疫情报告",
          "label": "B"
        },
        {
          "content": "疫情诊断",
          "label": "C"
        },
        {
          "content": "捐赠",
          "label": "D"
        },
        {
          "content": "志愿服务",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽防疫档案中，以下属于畜禽养殖场需要登记的信息有（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "畜禽种类",
          "label": "A"
        },
        {
          "content": "数量",
          "label": "B"
        },
        {
          "content": "免疫日期",
          "label": "C"
        },
        {
          "content": "畜禽养殖代码",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《三类动物疫病防治规范》，从事动物饲养、屠宰、经营、隔离、运输等活动的单位和个人应当建立并执行动物防疫消毒制度，科学规范开展消毒工作，及时对（     ）等进行无害化处理。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "病死动物",
          "label": "A"
        },
        {
          "content": "病死动物的排泄物",
          "label": "B"
        },
        {
          "content": "被污染的饲料",
          "label": "C"
        },
        {
          "content": "垫料",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "省级农业农村部门、财政部门要在科学测算无害化处理成本基础上，充分考虑（     ），合理确定本省（区、市）无害化处理补助对象，按照处理方式、病死畜禽大小等因素分类确定补助标准，",
      "standard_answer": "B,C,E",
      "option_list": [
        {
          "content": "无害化处理企业覆盖范围",
          "label": "A"
        },
        {
          "content": "无害化处理企业建设成本",
          "label": "B"
        },
        {
          "content": "无害化处理企业运营成本",
          "label": "C"
        },
        {
          "content": "无害化处理企业规模大小",
          "label": "D"
        },
        {
          "content": "无害化处理企业合理收益",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号），是为了加强动物运输活动防疫管理，降低非洲猪瘟等重大动物疫病跨区域传播风险。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，动物诊疗许可证应当载明诊疗机构名称、诊疗活动范围、从业地点和法定代表人（负责人）等事项。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "要按照“早、快、严、小” 原则做好炭疽疫情处置，对病畜进行彻底放血扑杀和无害化处理，掩埋点设立永久性警示标志，疫源地周边禁止放牧。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "建立日常排查制度，对调出牛羊、引起人感染布病和高流产率畜群以及其他可疑情况，及时开展排查、隔离、采样、检测和报告等工作。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，国务院农业农村主管部门制定并组织实施动物疫病净化、消灭规划。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》要求，在农业农村部指导下加快动物检疫信息化进程，在2027年底前全面实施无纸化出证，推动实现动物检疫证明电子证照全国互通互认。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《三类动物疫病防治规范》，经临床诊断、流行病学调查或实验室检测，综合研判认定为三类动物疫病的，不可以对患病动物进行治疗。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《国家畜禽遗传资源品种名录》规定，比利时兔作为传统畜禽，我国在多年前引进。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "高致病性禽流感、口蹄疫和小反刍兽疫免疫抗体合格率常年保持在90%以上。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，动物B检疫证明到达地点的填写规范是，填写到达地的省、市、县名，以及饲养场（养殖小区）、屠宰场、交易市场或乡镇、村名。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "人工捕获的野生动物，应当按照国家有关规定报所在地动物卫生监督机构检疫，检疫合格的，方可饲养、经营和运输。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》中所称的动物，是指家畜家禽和人工饲养、合法捕获的其他动物。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "对全国所有牛、羊、骆驼、鹿进行O型和A型口蹄疫免疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，国家实行动物防疫条件审核制度。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "对所有的牛羊进行布鲁氏菌病免疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，从事动物诊疗活动的机构，应当向县级以上地方人民政府农业农村主管部门申请动物诊疗许可证。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫处理通知单的编号规则是，年号+8位数字顺序号，以县为单位自行编制。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，跨省、自治区、直辖市通过道路运输动物的，应当经省、自治区、直辖市人民政府设立的指定通道入省境或者过省境。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "违反《动物防疫法》规定，动物、动物产品的运载工具不符合国务院农业农村主管部门规定的动物防疫要求的，由县级以上地方人民政府农业农村主管部门责令改正，可以处五千元以下罚款。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，病死动物，是指染疫死亡、因病死亡、死因不明的死亡动物。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "县级人民政府对本县病死畜禽无害化处理负总责。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定， 饲养种畜应当建立个体养殖档案，注明标识编码、性别、出生日期、父系和母系品种类型、母本的标识编码等信息。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽躯体及生皮、原毛、绒、脏器、血液、蹄、头、角等直接从屠宰线生产的畜禽产品出证。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "为了（ ），保障公共卫生安全和人体健康，根据《中华人民共和国动物防疫法》，制定动物防疫条件审查办法。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "规范动物防疫条件审查",
          "label": "A"
        },
        {
          "content": "有效预防、控制、净化、消灭动物疫病",
          "label": "B"
        },
        {
          "content": "防控人畜共患传染病",
          "label": "C"
        },
        {
          "content": "以上都是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国畜牧法》规定，畜牧业生产经营者（       ）成立行业协会，为成员提供信息、技术、营销、培训等服务，加强行业自律，维护成员和行业利益。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "应当",
          "label": "A"
        },
        {
          "content": "可以依法自愿",
          "label": "B"
        },
        {
          "content": "必须",
          "label": "C"
        },
        {
          "content": "协商",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家鼓励生猪养殖、屠宰、加工、配送、销售一体化发展，推行（       ）屠宰，支持建设冷链流通和配送体系。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "现代化",
          "label": "A"
        },
        {
          "content": "规模化",
          "label": "B"
        },
        {
          "content": "品牌化",
          "label": "C"
        },
        {
          "content": "标准化",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "以下行为不属于违反生物安全法规定的是：（ ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "个人购买或者持有列入管控清单的重要设备或者特殊生物因子",
          "label": "A"
        },
        {
          "content": "个人设立病原微生物实验室或者从事病原微生物实验活动",
          "label": "B"
        },
        {
          "content": "经实验室负责人批准进入高等级病原微生物实验室",
          "label": "C"
        },
        {
          "content": "购买或者引进列入管控清单的重要设备、特殊生物因子未进行登记，或者未报国务院有关部门备案",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，执业兽医取得资格证书以后，还应当向所在地县级人民政府（　　）备案，方可从事动物诊疗等经营活动。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "农业农村主管部门",
          "label": "B"
        },
        {
          "content": "市场监督管理部门",
          "label": "C"
        },
        {
          "content": "兽医协会",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列哪种动物传染病不是一类动物疫病。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "口蹄疫",
          "label": "A"
        },
        {
          "content": "狂犬病",
          "label": "B"
        },
        {
          "content": "高致病性禽流感",
          "label": "C"
        },
        {
          "content": "非洲猪瘟",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列说法错误的是：（ ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "农产品是指来源于种植业、林业、畜牧业和渔业等的初级产品。",
          "label": "A"
        },
        {
          "content": "农产品质量安全是指农产品质量达到农产品质量安全标准，符合保障人的健康、安全的要求。",
          "label": "B"
        },
        {
          "content": "国家加强农产品质量安全工作，建立了科学、严格的监督管理制度。",
          "label": "C"
        },
        {
          "content": "对农产品质量安全实施监督管理工作由农业农村主管部门全权负责。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，对饲养的动物未按照动物疫病强制免疫计划或者免疫技术规范实施免疫接种，逾期不改正的，由县级上地方人民政府农业农村主管部门委托代为处理，所需费用由（　　）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "农业农村主管部门承担，处一千元以下罚款",
          "label": "A"
        },
        {
          "content": "违法行为人承担，处一千元以上五千元以下罚款",
          "label": "B"
        },
        {
          "content": "农业农村主管部门承担，处一千元以上五千元以下罚款",
          "label": "C"
        },
        {
          "content": "违法行为人承担，处一千元以下罚款",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员有违法行为，有关机关依照规定给予组织处理的，（）可以同时给予政务处分。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "检查机关",
          "label": "A"
        },
        {
          "content": "机关党委",
          "label": "B"
        },
        {
          "content": "监察机关",
          "label": "C"
        },
        {
          "content": "机管纪委",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "在证据可能灭失或者以后难以取得的情况下，经行政机关负责人批准，可以先行登记保存，并应当在(      ) 内及时作出处理决定。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "三日",
          "label": "A"
        },
        {
          "content": "五日",
          "label": "B"
        },
        {
          "content": "七日",
          "label": "C"
        },
        {
          "content": "十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（     ）应当制定本行政区域病死畜禽和病害畜禽产品无害化处理生物安全风险调查评估方案并组织实施。 ",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "县级以上动物疫病预防控制机构",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "省级动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，（）统一采购畜禽标识，逐级供应。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省级动物疫病预防控制机构",
          "label": "B"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "省级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫证明填写中，以下针对数量及单位填写方式正确的是（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "壹佰头",
          "label": "A"
        },
        {
          "content": "三十只",
          "label": "B"
        },
        {
          "content": "陆千羽",
          "label": "C"
        },
        {
          "content": "10000枚",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家鼓励和支持（ ）建立农产品质量安全信息员工作制度，协助开展有关工作。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "农产品生产企业",
          "label": "A"
        },
        {
          "content": "农民专业合作社",
          "label": "B"
        },
        {
          "content": "农业社会化服务组织",
          "label": "C"
        },
        {
          "content": "基层群众性自治组织",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，已经确定指定通道的省份，要定期开展（   ）动物疫病、路网布局、畜禽流通等情况的分析研判，",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "本省份",
          "label": "A"
        },
        {
          "content": "本省份及周边区域",
          "label": "B"
        },
        {
          "content": "周边区域",
          "label": "C"
        },
        {
          "content": "本辖区",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，（）应当根据畜禽标识、养殖档案等信息对畜禽及畜禽产品实施追溯和处理。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级畜牧兽医行政主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "县级以上地方政府畜牧兽医行政主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "被许可人需要延续依法取得的行政许可的有效期的，应当在该行政许可有效期届满（     ）前向作出行政许可决定的行政机关提出申请。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "十日",
          "label": "A"
        },
        {
          "content": "二十日",
          "label": "B"
        },
        {
          "content": "三十日",
          "label": "C"
        },
        {
          "content": "六十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员纵容、默许特定关系人利用本人职权或者职务上的影响谋取私利，情节较重的，予以（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，猪、牛、羊首次加施畜禽标识应当在（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "右耳中部",
          "label": "A"
        },
        {
          "content": "左耳中部",
          "label": "B"
        },
        {
          "content": "左右耳均可",
          "label": "C"
        },
        {
          "content": "颈部",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "(       )应当根据本地情况，决定在城市特定区域禁止家畜家禽活体交易。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "县级以上人民政府",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "B"
        },
        {
          "content": "省、自治区、直辖市人民政府",
          "label": "C"
        },
        {
          "content": "省、自治区、直辖市人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，县级以上地方人民政府的（  ）负责本行政区域内动物检疫工作。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "农业农村主管部门",
          "label": "B"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "农业综合执法机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（ ）对本行政区域内生物安全工作负责。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "国务院有关部门",
          "label": "A"
        },
        {
          "content": "地方各级人民政府",
          "label": "B"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "C"
        },
        {
          "content": "乡镇人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "为深入贯彻《中华人民共和国动物防疫法》，落实《“十四五”全国畜牧兽医行业发展规划》要求，加强官方兽医队伍建设，强化官方兽医培训，提升官方兽医队伍（），制定本计划。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "依法履职能力和水平",
          "label": "A"
        },
        {
          "content": "职业素养",
          "label": "B"
        },
        {
          "content": "政治能力",
          "label": "C"
        },
        {
          "content": "创新能力",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国畜牧法》规定，畜牧业生产经营者应当依法履行（       ）和生态环境保护义务，接受有关主管部门依法实施的监督检查。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "畜禽遗传资源保护",
          "label": "A"
        },
        {
          "content": "健康养殖",
          "label": "B"
        },
        {
          "content": "动物防疫",
          "label": "C"
        },
        {
          "content": "动物检疫申报",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "各（）级农业农村部门要按照规定格式统一制作官方兽医证，加强证件发放使用管理。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "县",
          "label": "A"
        },
        {
          "content": "省 ",
          "label": "B"
        },
        {
          "content": "市",
          "label": "C"
        },
        {
          "content": "地",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "违法事实确凿并有法定依据，对公民处以（   ）罚款或者警告的行政处罚的，可以当场作出行政处罚决定。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "五十元以下",
          "label": "A"
        },
        {
          "content": "一百元以下",
          "label": "B"
        },
        {
          "content": "二百元以下",
          "label": "C"
        },
        {
          "content": "三百元以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员在管理服务活动中态度恶劣粗暴，造成不良后果或者影响，情节严重的，予以（）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "记大过",
          "label": "A"
        },
        {
          "content": "降级",
          "label": "B"
        },
        {
          "content": "撤职",
          "label": "C"
        },
        {
          "content": "开除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "县级以上地方人民政府要根据本地区（     ）等情况，统筹规划和合理布局病死畜禽无害化收集处理体系。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "畜禽养殖",
          "label": "A"
        },
        {
          "content": "疫病发生",
          "label": "B"
        },
        {
          "content": "土地消纳能力",
          "label": "C"
        },
        {
          "content": "畜禽死亡",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "县级以上地方人民政府农业农村主管部门应当定期对本行政区域的强制免疫（     ）进行评估，并向社会公布评估结果。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "计划实施情况",
          "label": "A"
        },
        {
          "content": "效果",
          "label": "B"
        },
        {
          "content": "组织情况",
          "label": "C"
        },
        {
          "content": "效价",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽（    ）等直接从屠宰线生产的畜禽产品出证。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "肉",
          "label": "A"
        },
        {
          "content": "胴体及生皮、原毛、绒",
          "label": "B"
        },
        {
          "content": "蹄、头、角、",
          "label": "C"
        },
        {
          "content": "脏器、血液、",
          "label": "D"
        },
        {
          "content": "脂、筋、骨",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》提出，除（     ）外，跨省调运活畜时，禁止布病易感动物从高风险区域向低风险区域调运。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "布病净化场",
          "label": "A"
        },
        {
          "content": "布病无疫小区",
          "label": "B"
        },
        {
          "content": "布病无疫区",
          "label": "C"
        },
        {
          "content": "用于屠宰和种用乳用",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "当事人同意并签订确认书的，行政机关可以采用\r\n（    ）等方式，将行政处罚决定书等送达当事人。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "微信",
          "label": "A"
        },
        {
          "content": "传真",
          "label": "B"
        },
        {
          "content": "电子邮件",
          "label": "C"
        },
        {
          "content": "拍照",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，禁止（     ）伪造或者变造的检疫证明、检疫标志或者畜禽标识。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "处理",
          "label": "A"
        },
        {
          "content": "持有",
          "label": "B"
        },
        {
          "content": "发现",
          "label": "C"
        },
        {
          "content": "使用",
          "label": "D"
        },
        {
          "content": "报告",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽防疫档案中，以下属于畜禽散养户需要登记的信息有（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "畜禽种类",
          "label": "A"
        },
        {
          "content": "数量",
          "label": "B"
        },
        {
          "content": "免疫日期",
          "label": "C"
        },
        {
          "content": "畜禽养殖代码",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列关于农产品产地规定正确的是：（ ）。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "县级以上人民政府应当采取措施，加强农产品基地建设，推进农业标准化示范建设，改善农产品的生产条件。",
          "label": "A"
        },
        {
          "content": "废水、废气、固体废物或者其他有毒有害物质经处理后可以向农产品产地排放或者倾倒。",
          "label": "B"
        },
        {
          "content": "农业生产用水和用作肥料的固体废物，应当符合法律、法规和国家有关强制性标准的要求。",
          "label": "C"
        },
        {
          "content": "农药、肥料、农用薄膜等农业投入品应当按照国家有关规定回收并妥善处置包装物和废弃物。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列关于动物防疫条件审查监督管理说法正确的是：（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "取得动物防疫条件合格证后，变更场址或者经营范围的，应当重新申请办理，同时交回原动物防疫条件合格证，由原发证机关予以注销。",
          "label": "A"
        },
        {
          "content": "变更布局、设施设备和制度，可能引起动物防疫条件发生变化的，应当提前三十日向原发证机关报告。",
          "label": "B"
        },
        {
          "content": "变更布局、设施设备和制度，可能引起动物防疫条件发生变化的，向原发证机关报告后，发证机关应当在十五日内完成审查，并将审查结果通知申请人。",
          "label": "C"
        },
        {
          "content": "变更单位名称或者法定代表人（负责人）的，应当在变更后三十日内持有效证明申请变更动物防疫条件合格证。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "（     ）行政许可证件，或者以其他形式非法转让行政许可的，行政机关应当依法给予行政处罚。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "涂改",
          "label": "A"
        },
        {
          "content": "倒卖",
          "label": "B"
        },
        {
          "content": "出租",
          "label": "C"
        },
        {
          "content": "出借",
          "label": "D"
        },
        {
          "content": "丢失",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "作出行政执法后，下列哪些信息应当进行公示\r\n（      ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "实施机关",
          "label": "A"
        },
        {
          "content": "立案依据",
          "label": "B"
        },
        {
          "content": "实施程序",
          "label": "C"
        },
        {
          "content": "救济渠道",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）对其生产的生猪产品质量安全负责，发现其生产的生猪产品（     ）的，应当立即停止屠宰，报告农业农村主管部门，通知销售者或者委托人，召回已经销售的生猪产品，并记录通知和召回情况。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "不符合食品安全标准",
          "label": "A"
        },
        {
          "content": "有证据证明可能危害人体健康",
          "label": "B"
        },
        {
          "content": "染疫或者疑似染疫",
          "label": "C"
        },
        {
          "content": "检疫不合格",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《全国畜间人兽共患病防治规划2022—2030年）》提出的布病的重点防治措施的是（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "实施专项防控行动",
          "label": "A"
        },
        {
          "content": "推进区域化管理",
          "label": "B"
        },
        {
          "content": "强化牛羊调运监管",
          "label": "C"
        },
        {
          "content": "加强奶畜风险监测",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "依据《动物防疫法》规定，用于（    ）的动物未附有检疫证明的，由县级以上地方人民政府农业农村主管部门责令改正,处三千元以上一万元以下罚款。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "展示",
          "label": "A"
        },
        {
          "content": "演出",
          "label": "B"
        },
        {
          "content": "比赛",
          "label": "C"
        },
        {
          "content": "科研",
          "label": "D"
        },
        {
          "content": "饲养",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，动物A检疫证明启运地点的填写规范是，饲养场（养殖小区）、交易市场的动物填写生产地的省、市、县名和饲养场（养殖小区）、交易市场名称；散养动物填写生产地的省、市、县、乡、村名。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "对全国所有羊进行小反刍兽疫免疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽胴体及生皮、原毛、绒、脏器、血液、蹄、头、角等直接从屠宰线生产的畜禽产品出证。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，官方兽医可以从事与动物防疫有关的经营性活动。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "动物疫病预防控制机构负责监督检查养殖场(户)履行强制免疫义务情况。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，屠宰染疫或者疑似染疫、病死或者死因不明的畜禽的行为人及其法定代表人（负责人）、直接负责的主管人员和其他直接责任人员，自处罚决定作出之日起五年内不得从事相关活动。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，从事动物、动物产品运输的单位和个人，应当配合做好病死动物和病害动物产品的无害化处理，不得在途中擅自弃置和处理有关动物和动物产品。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，任何单位和个人不得销售、收购、运输、屠宰应当加施标识而没有标识的畜禽。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，屠宰、出售或者运输动物以及出售或者运输动物产品前，货主应当按照国务院农业农村主管部门的规定向所在地动物卫生监督机构申报检疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "新闻媒体应当开展农产品质量安全法律、法规和农产品质量安全知识的公益宣传，对违法行为进行舆论监督。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村部规定，牛羊肉检疫验讫标志由塑料扎带、锁扣、标牌和动物产品检疫验讫标签组成。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "完善官方兽医师资轮训制度，官方兽医师资每年轮训一次。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "何单位和个人不得违反有关环境保护法律、法规的规定向农产品产地排放或者倾倒废水、废气、固体废物或者其他有毒有害物质。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "村民委员会、居民委员会组织本辖区饲养动物的单位和个人做好强制免疫，协助做好监督检查。（   ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，要着力创新监管机制，织密动物运输监管信息化网络，实现指定通道基础信息便捷查询和监管信息启运地、途径地、目的地全程共享，为加强动物调运监管提供支撑。  ",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要定期开展本省份及周边区域动物疫病、路网布局、畜禽流通等情况的分析研判，并综合考虑已有站点资源、监管力量配备、工作条件保障等因素，适时调整完善指定通道布局。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村部规定，生猪屠宰检疫验讫印章由验讫印章和无害化处理印章“高温”“销毁”组成。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "县级以上地方人民政府农业农村主管部门应当定期对本行政区域的强制免疫计划实施情况和效果进行评估，并向社会公布评估结果。（   ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，县级以上地方人民政府农业农村主管部门负责组织实施动物疫病强制免疫计划，并对饲养动物的单位和个人履行强制免疫义务的情况进行监督检查。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "国务院农业农村主管部门规定应当加施标识而没有标识的畜禽，不得销售、收购。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（     ）负责组织实施动物疫病强制免疫计划，并对饲养动物的单位和个人履行强制免疫义务的情况进行监督检查。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "县级以上人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "C"
        },
        {
          "content": "县级人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "到2026年，全国畜间布病总体流行率有效降低，牛羊群体健康水平明显提高，个体阳性率控制在0.4%以下，群体阳性率控制在（）%以下。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "7",
          "label": "A"
        },
        {
          "content": "0.4",
          "label": "B"
        },
        {
          "content": "0.7",
          "label": "C"
        },
        {
          "content": "4",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，符合不再重复出证条件的畜禽产品，需要附具（     ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物检疫证明电子证照加注件",
          "label": "A"
        },
        {
          "content": "动物检疫证明电子证照",
          "label": "B"
        },
        {
          "content": "无纸化动物检疫证明",
          "label": "C"
        },
        {
          "content": "动物检疫证明电子证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，给与降级处分的，政务处分期为（）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "六个月",
          "label": "A"
        },
        {
          "content": "十二个月",
          "label": "B"
        },
        {
          "content": "十八个月",
          "label": "C"
        },
        {
          "content": "二十四个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，养殖档案和防疫档案的保存时间，商品猪为（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "2年",
          "label": "A"
        },
        {
          "content": "10年",
          "label": "B"
        },
        {
          "content": "20年",
          "label": "C"
        },
        {
          "content": "长期",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定的实施日期是（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "2002年5月24日起",
          "label": "A"
        },
        {
          "content": "2008年7月1日起",
          "label": "B"
        },
        {
          "content": "2006年7月1日起",
          "label": "C"
        },
        {
          "content": "2012年5月24日起",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，动物卫生监督机构应当及时向（    ）报告检疫不合格情况。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "上级动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "上级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "同级农业综合执法机构",
          "label": "C"
        },
        {
          "content": "同级农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《三类动物疫病防治规范》，饲养种用、乳用动物的单位和个人，应按照相应动物健康标准等规定，定期开展（     ）检测；检测不合格的，应当按照国家有关规定处理。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "动物疾病",
          "label": "A"
        },
        {
          "content": "动物疫病",
          "label": "B"
        },
        {
          "content": "流行性疫病",
          "label": "C"
        },
        {
          "content": "重大动物疫病",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "布病净化和无疫小区建设工作扎实推进，内蒙古20%以上，辽宁、四川、陕西、甘肃、新疆（）%以上，其他省份80%以上的牛羊种畜场（站）建成省级或国家级布病净化场、无疫小区；",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "20",
          "label": "A"
        },
        {
          "content": "30",
          "label": "B"
        },
        {
          "content": "40",
          "label": "C"
        },
        {
          "content": "50",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，饲养种用、乳用动物的单位和个人，应当按照国务院农业农村主管部门的要求，定期开展（    ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物疫病检测",
          "label": "A"
        },
        {
          "content": "动物疫病监测",
          "label": "B"
        },
        {
          "content": "动物疫病检疫",
          "label": "C"
        },
        {
          "content": "动物检疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（    ）应当检查待宰动物健康状况，在屠宰过程中开展同步检疫和必要的实验室疫病检测，并填写屠宰检疫记录。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "兽医卫生人员",
          "label": "A"
        },
        {
          "content": "协检人员",
          "label": "B"
        },
        {
          "content": "官方兽医或协检人员",
          "label": "C"
        },
        {
          "content": "官方兽医",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员被开除的，自（）起，应当解除其与所在机关、单位的人事关系或者劳动关系。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "政务处分决定生效之日",
          "label": "A"
        },
        {
          "content": "领导同意之日",
          "label": "B"
        },
        {
          "content": "党委批准之日",
          "label": "C"
        },
        {
          "content": "违纪行为发生之日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》适用于（  ）对违法的公职人员给与政务处分的活动。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "公安机关",
          "label": "A"
        },
        {
          "content": "监察机关",
          "label": "B"
        },
        {
          "content": "检察机关",
          "label": "C"
        },
        {
          "content": "机关党委",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，动物卫生监督机构受理申报后，可以安排（    ）协助官方兽医到现场或指定地点核实信息，开展临床健康检查。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "协检人员",
          "label": "A"
        },
        {
          "content": "动物防疫人员",
          "label": "B"
        },
        {
          "content": "执业兽医",
          "label": "C"
        },
        {
          "content": "动物防疫技术人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员诬告陷害，意图使他人受到名誉损害或者责任追究等不良影响，情节严重的，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，实施检疫的官方兽医应当在检疫证明、检疫标志上签字或者盖章，并对（    ）负责。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "检疫结果",
          "label": "A"
        },
        {
          "content": "检疫结论",
          "label": "B"
        },
        {
          "content": "出证结果",
          "label": "C"
        },
        {
          "content": "出证结论",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "(    )根据国家动物疫病监测计划，制定本行政区域的动物疫病监测计划。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "B"
        },
        {
          "content": "省、自治区、直辖市人民政府",
          "label": "C"
        },
        {
          "content": "省、自治区、直辖市人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "发生重大新发突发传染病，（ ）履行疫情防控职责，加强组织领导，开展群防群控、医疗救治，动员和鼓励社会力量依法有序参与疫情防控工作。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "卫生健康部门",
          "label": "A"
        },
        {
          "content": "　疾病预防控制机构",
          "label": "B"
        },
        {
          "content": "地方各级人民政府",
          "label": "C"
        },
        {
          "content": "应急管理部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员拒不承担赡养、抚养、扶养义务，情节较重的，予以（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，（　　）应当建立健全动物疫病监测网络，加强动物疫病监测。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "县级以上人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上人民政府",
          "label": "B"
        },
        {
          "content": "县级以上动物卫生监督机构",
          "label": "C"
        },
        {
          "content": "县级以上动物疫病预防控制机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "委托行政机关对受委托组织实施行政处罚的行为应当负责监督，（     ）对该行为的后果承担法律责任。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "受委托组织",
          "label": "A"
        },
        {
          "content": "委托行政机关",
          "label": "B"
        },
        {
          "content": "实施行政处罚的执法人员",
          "label": "C"
        },
        {
          "content": "当地人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定， 国外引进的畜禽在国内发生重大动物疫情，由（）会同有关部门进行追溯。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级畜牧兽医行政主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "县级以上地方政府畜牧兽医行政主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，出售或者运输的动物取得动物检疫证明，需要符合的条件不包括（   ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "来自符合风险分级管理有关规定的饲养场（户）",
          "label": "A"
        },
        {
          "content": "有原始动物检疫证明和完整的进出场记录",
          "label": "B"
        },
        {
          "content": "按照规定进行了强制免疫，并在有效保护期内",
          "label": "C"
        },
        {
          "content": "畜禽标识符合规定",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "县级以上地方人民政府农业农村主管部门会同同级生态环境、自然资源等部门制定农产品产地监测计划，加强农产品产地安全调查、监测和评价工作属于农产品（ ）制度。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "质量安全风险监测",
          "label": "A"
        },
        {
          "content": "产地监测",
          "label": "B"
        },
        {
          "content": "风险管理",
          "label": "C"
        },
        {
          "content": "质量安全风险评估",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "拒绝、阻挠依法开展的农产品质量安全监督检查、事故调查处理、抽样检测和风险评估的，应（ ）。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "责令停产停业",
          "label": "A"
        },
        {
          "content": "没收生产经营的工具、设备、原料等物品",
          "label": "B"
        },
        {
          "content": "处二千元以上五万元以下罚款",
          "label": "C"
        },
        {
          "content": "构成违反治安管理行为的，由公安机关依法给予治安管理处罚",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，动物卫生监督机构的官方兽医实施检疫，（     ），并对检疫结论负责。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "出具无疫区证明",
          "label": "A"
        },
        {
          "content": "加施畜禽标识",
          "label": "B"
        },
        {
          "content": "出具动物检疫证明",
          "label": "C"
        },
        {
          "content": "加施检疫标志",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "依据《动物防疫法》规定，（    ），由县级以上人民政府农业农村主管部门没收检疫证明、检疫标志、畜禽标识和对应的动物、动物产品，并处三千元以上三万元以下罚款。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "持有伪造或者变造的检疫证明、检疫标志",
          "label": "A"
        },
        {
          "content": "使用伪造或者变造的检疫证明、检疫标志",
          "label": "B"
        },
        {
          "content": "持有伪造或者变造的畜禽标识",
          "label": "C"
        },
        {
          "content": "使用伪造或者变造的畜禽标识",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "加强典型案例警示教育，教育指导广大官方兽医以案为戒，认真履职尽责，严格遵守(),严格按照动物检疫规程规定实施检疫。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "法律",
          "label": "A"
        },
        {
          "content": "职业道德",
          "label": "B"
        },
        {
          "content": "纪律规矩",
          "label": "C"
        },
        {
          "content": "法规",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关作出行政许可决定，依法需要办理下列哪些事项所需时间不计算在自申请之日起应当作出行政许可决定期限内？（      ）",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "听证",
          "label": "A"
        },
        {
          "content": "检测",
          "label": "B"
        },
        {
          "content": "检疫",
          "label": "C"
        },
        {
          "content": "专家评审",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，动物卫生监督机构工作人员违反本法规定，对未经检疫或者检疫不合格的动物、动物产品出具检疫证明的，由本级人民政府或者农业农村主管部门（    ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "罚款",
          "label": "A"
        },
        {
          "content": "责令改正",
          "label": "B"
        },
        {
          "content": "依法给予处分",
          "label": "C"
        },
        {
          "content": "通报批评",
          "label": "D"
        },
        {
          "content": "依法给予处罚",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，动物屠宰检疫时，符合下列（　　）条件，方可出具动物检疫证明。",
      "standard_answer": "B,C,D,E",
      "option_list": [
        {
          "content": "动物来自非封锁区",
          "label": "A"
        },
        {
          "content": "待宰动物临床检查健康",
          "label": "B"
        },
        {
          "content": "需要进行实验室疫病检测的，检测结果符合要求",
          "label": "C"
        },
        {
          "content": "同步检疫合格",
          "label": "D"
        },
        {
          "content": "申报材料符合检疫规程规定",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，官方兽医应当回收进入屠宰加工场所待宰动物附有的（   ），且回收后保存期限不得少于（   ）个月。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "动物申报处理单",
          "label": "A"
        },
        {
          "content": "动物检疫证明",
          "label": "B"
        },
        {
          "content": "12个月",
          "label": "C"
        },
        {
          "content": "24个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对全国所有牛、羊、骆驼、鹿进行（）口蹄疫免疫.",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "O型",
          "label": "A"
        },
        {
          "content": "A型",
          "label": "B"
        },
        {
          "content": "C型",
          "label": "C"
        },
        {
          "content": "SAT1",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "中央财政养殖环节病死畜禽无害化处理补助资金不得用于下列哪些范围的无害化处理补助（     ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "养殖环节患病死亡的未断奶仔猪",
          "label": "A"
        },
        {
          "content": "因非洲猪瘟被扑杀的猪",
          "label": "B"
        },
        {
          "content": "屠宰环节病死畜禽",
          "label": "C"
        },
        {
          "content": "屠宰环节病害畜禽产品",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，已经确定指定通道的省份，要定期开展分析研判，并综合考虑（   ）等因素，适时调整完善指定通道布局。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "动物疫病",
          "label": "A"
        },
        {
          "content": "已有站点资源",
          "label": "B"
        },
        {
          "content": "监管力量配备",
          "label": "C"
        },
        {
          "content": "工作条件保障",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关作出行政许可决定，依法需要办理下列哪些事项所需时间不计算在自申请之日起应当作出行政许可决定期限内？（      ）",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "招标",
          "label": "A"
        },
        {
          "content": "检验",
          "label": "B"
        },
        {
          "content": "鉴定",
          "label": "C"
        },
        {
          "content": "拍卖",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》明确了重点防范的外来疫病的数量是3种。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "一类疫病是指口蹄疫、非洲猪瘟、高致病性禽流感、布鲁氏菌病等对人、动物构成特别严重危害，可能造成造成重大经济损失和社会影响，需要采取紧急、严厉的强制预防、控制等措施的疫病。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，经营和运输的动物产品，应当附有检疫证明、检疫标志。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "国家对畜禽遗传资源享有主权。经批准向境外输出畜禽遗传资源的，还应当依照《中华人民共和国动物防疫法》的规定办理相关手续并实施检疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农产品生产经营者对监督抽查检测结果有异议的，可以向初检机构申请复检。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽标识严重磨损、破损、脱落后，应当及时加施新的标识，并在养殖档案中记录新标识编码。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "各地动物卫生监督机构要切实加强对官方兽医培训工作的组织领导，保障培训经费，提供培训条件，配齐配强师资人员，加强督促指导和总结评估，统筹做好本地官方兽医培训工作，不断提高培训质量。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "从事畜禽经营、运输的单位和个人应当委托就近的畜禽养殖场对经营、运输过程中的病死畜禽进行处理，所需费用由货主承担。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "布鲁氏菌病是由布鲁氏菌属细菌感染引起的非人畜共患传染病。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，官方兽医依法履行动物、动物产品检疫职责，任何单位和个人不得拒绝或者阻碍。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，从事动物运输的单位、个人以及车辆，应当向县级动物卫生监督机构备案。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "官方兽医培训的总体目标：以提高官方兽医业务能力和综合素质为重点，加强官方兽医培训考核，强化培训条件保障，加快培养一支业务精湛、作风优良、纪律严明、保障有力的官方兽医队伍，为有效开展动物检疫工作、保障养殖业生产安全和公共卫生安全提供有力支撑。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "坚持一地一策，根据各地布病流行形势，以乡镇为基本单位连片推进布病防控。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，发生重大动物疫情时，国务院农业农村主管部门负责划定动物疫病风险区，禁止或者限制特定动物、动物产品由高风险区向低风险区调运。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《三类动物疫病防治规范》，对患三类动物疫病的畜禽无需隔离饲养，必要时对患病动物的同群动物采取给药、免疫等预防性措施。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《国家畜禽遗传资源品种名录》规定，关中奶山羊作为传统畜禽，是我国重点培育的奶山羊品种。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "加施了畜禽标识的畜禽死亡后，其畜禽标识可以取下重复使用，除此之外，禁止其他任何情况下重复使用畜禽标识。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，动物A检疫证明用途可以填写饲养、屠宰、种用、宠用、出国等内容。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，饲养动物的单位和个人应当履行动物疫病强制免疫义务，按照强制免疫计划和技术规范，对动物实施免疫接种，并按照国家有关规定建立（    ）、加施（    ），保证可追溯。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "免疫档案、耳标",
          "label": "A"
        },
        {
          "content": "防疫档案、畜禽标识",
          "label": "B"
        },
        {
          "content": "免疫档案、畜禽标识",
          "label": "C"
        },
        {
          "content": "养殖档案、防疫耳标",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "省域内跨市（地）、县（市）流入的病死畜禽，由（     ）责令有关地方和部门调查。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "共同的上一级政府",
          "label": "A"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "省级政府",
          "label": "C"
        },
        {
          "content": "省级政府督察部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员应当给予两种以上政务处分的，执行（）的政务处分。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "其中较轻",
          "label": "A"
        },
        {
          "content": "其中最重",
          "label": "B"
        },
        {
          "content": "选择其一",
          "label": "C"
        },
        {
          "content": "两种合并",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，跨省、自治区、直辖市通过道路运输动物的，应当经省、自治区、直辖市人民政府设立的（    ）入省境或者过省境。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "指定通道",
          "label": "A"
        },
        {
          "content": "通道",
          "label": "B"
        },
        {
          "content": "指定道路",
          "label": "C"
        },
        {
          "content": "动物防疫检查站",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》对动物疫病进行分类的根据是（　　）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "动物的发病率和死亡率",
          "label": "A"
        },
        {
          "content": "对养殖业生产和人体健康的危害程度",
          "label": "B"
        },
        {
          "content": "对公共卫生的影响",
          "label": "C"
        },
        {
          "content": "对养殖业生产和个人造成的经济损失",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "从事养殖、运输、屠宰、加工等相关重点职业人群的布病防治知识知晓率达90% 以上，基层动物防疫检疫人员的布病防治知识普及覆盖面达（）% 以上。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "70",
          "label": "A"
        },
        {
          "content": "95",
          "label": "B"
        },
        {
          "content": "90",
          "label": "C"
        },
        {
          "content": "100",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "布病净化和无疫小区建设工作扎实推进，内蒙古（）%以上，辽宁、四川、陕西、甘肃、新疆50%以上，其他省份80%以上的牛羊种畜场（站）建成省级或国家级布病净化场、无疫小区；",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "20",
          "label": "A"
        },
        {
          "content": "30",
          "label": "B"
        },
        {
          "content": "40",
          "label": "C"
        },
        {
          "content": "50",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪定点屠宰厂（场）被吊销生猪定点屠宰证书的，其法定代表人（负责人）、直接负责的主管人员和其他直接责任人员自处罚决定作出之日起（       ）不得申请生猪定点屠宰证书或者从事生猪屠宰管理活动。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "10年内",
          "label": "A"
        },
        {
          "content": "8年内",
          "label": "B"
        },
        {
          "content": "5年内",
          "label": "C"
        },
        {
          "content": "3年内",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽养殖者应当向（）申领畜禽标识。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "县级农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级动物卫生监督机构",
          "label": "B"
        },
        {
          "content": "当地县级动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "乡镇动物防疫分站",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政许可法》，下列说法不正确的是（    ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "行政机关实施行政许可，不得向申请人提出购买指定商品、接受有偿服务等不正当要求",
          "label": "A"
        },
        {
          "content": "申请事项依法不需要取得行政许可的，应当即时告知申请人不受理",
          "label": "B"
        },
        {
          "content": "申请事项依法不属于本行政机关职权范围的，应当即时作出不予受理的决定，并告知申请人向有关行政机关申请",
          "label": "C"
        },
        {
          "content": "上级行政机关可以要求申请人重复提供申请材料。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "中央国家安全领导机构负责国家生物安全工作的（ ），研究制定、指导实施国家生物安全战略和有关重大方针政策，统筹协调国家生物安全的重大事项和重要工作，建立国家生物安全工作协调机制。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "决策",
          "label": "A"
        },
        {
          "content": "议事",
          "label": "B"
        },
        {
          "content": "协调",
          "label": "C"
        },
        {
          "content": "决策和议事协调",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "官方兽医应于每年（）月底前登录兽医卫生综合信息平台官方兽医培训考试模块或“牧运通”APP，提交当年参加培训相关证明材料。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "1",
          "label": "A"
        },
        {
          "content": "11",
          "label": "B"
        },
        {
          "content": "12",
          "label": "C"
        },
        {
          "content": "10",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "申报动物检疫隐瞒有关情况或者提供虚假材料的，或者以欺骗、贿赂等不正当手段取得动物检疫证明的，依照（   ）有关规定予以处罚。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "《中华人民共和国动物防疫法》",
          "label": "A"
        },
        {
          "content": "《中华人民共和国行政许可法》",
          "label": "B"
        },
        {
          "content": "《中华人民共和国行政处罚法》",
          "label": "C"
        },
        {
          "content": "《中华人民共和国行政强制法》",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政许可申请人隐瞒有关情况或者提供虚假材料申请行政许可的，行政机关（       ）",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "不予受理",
          "label": "A"
        },
        {
          "content": "不予行政许可",
          "label": "B"
        },
        {
          "content": "不予受理或者不予行政许可，并给予警告",
          "label": "C"
        },
        {
          "content": "不予受理或者不予行政许可",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "在证据可能灭失或者以后难以取得的情况下，经（      ）批准，可以先行登记保存，并应当在七日内及时作出处理决定。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "行政执法机构负责人",
          "label": "A"
        },
        {
          "content": "法制审核机构负责人",
          "label": "B"
        },
        {
          "content": "行政机关负责人",
          "label": "C"
        },
        {
          "content": "司法机关负责人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，发现未经指定通道跨省份道路运输动物的，以及违反有关规定接收动物的，要（    ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "依法依规进行处理",
          "label": "A"
        },
        {
          "content": "及时报告",
          "label": "B"
        },
        {
          "content": "及时处理",
          "label": "C"
        },
        {
          "content": "从严处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政许可申请人隐瞒有关情况或者提供虚假材料申请行政许可，且属于直接关系公共安全、人身健康、生命财产安全事项的，申请人在（     ）内不得再次申请该行政许可。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "一年内",
          "label": "A"
        },
        {
          "content": "二年内",
          "label": "B"
        },
        {
          "content": "三年内",
          "label": "C"
        },
        {
          "content": "五年内",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列那种情况，由此给公民、法人或者其他组织造成财产损失时，行政机关应当予以补偿？\r\n（     ）",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "王某在不符合条件情况下，为了取得生猪定点屠宰许可而贿赂行政机关工作人员",
          "label": "A"
        },
        {
          "content": "陈某为了取得动物检疫证明而送财物给实施检疫的官方兽医",
          "label": "B"
        },
        {
          "content": "官方兽医发现申报人提供虚假材料，拒不出具检疫证明，造成申报人损失。",
          "label": "C"
        },
        {
          "content": "官方兽医李某无正当理由，对符合规定条件的畜禽，拒不出具检疫证明，造成申报人损失。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪定点屠宰厂（场）应当建立严格的（       ）检验管理制度。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "传染病以外的疫病",
          "label": "A"
        },
        {
          "content": "瘦肉精",
          "label": "B"
        },
        {
          "content": "肉品品质",
          "label": "C"
        },
        {
          "content": "有害腺体",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "违反《动物防疫法》规定，对染疫动物及其排泄物、染疫动物产品或者被染疫动物、动物产品污染的运载工具、垫料、包装物、容器等未按照规定处置，造成环境污染或者生态破坏的，依照（    ）有关法律法规进行处罚。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "生物安全",
          "label": "A"
        },
        {
          "content": "环境污染",
          "label": "B"
        },
        {
          "content": "公共卫生",
          "label": "C"
        },
        {
          "content": "环境保护",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，养殖档案和防疫档案的保存时间，羊为（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "2年",
          "label": "A"
        },
        {
          "content": "10年",
          "label": "B"
        },
        {
          "content": "20年",
          "label": "C"
        },
        {
          "content": "长期",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "张三有一批已经取得产地检疫证明的生猪，要从动物的集贸市场继续出售，按照《动物检疫管理办法》规定，需要符合下列（   ）条件。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "有原始动物检疫证明",
          "label": "A"
        },
        {
          "content": "畜禽标识符合规定",
          "label": "B"
        },
        {
          "content": "临床检查健康",
          "label": "C"
        },
        {
          "content": "有完整的进出场记录",
          "label": "D"
        },
        {
          "content": "原始动物检疫证明超过调运有效期，按规定需要进行实验室疫病检测的，检测结果合格。",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "地方性法规和省、自治区、直辖市人民政府规章，不得设定（     ）",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "应当由国家统一确定的公民、法人或者其他组织的资格、资质的行政许可",
          "label": "A"
        },
        {
          "content": "企业或者其他组织的设立登记及其前置性行政许可",
          "label": "B"
        },
        {
          "content": "直接涉及国家安全、公共安全、经济宏观调控、生态环境保护以及直接关系人身健康、生命财产安全等特定活动，需要按照法定条件予以批准的事项",
          "label": "C"
        },
        {
          "content": "有限自然资源开发利用、公共资源配置以及直接关系公共利益的特定行业的市场准入等，需要赋予特定权利的事项",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《行政处罚法》，下列说法正确的是（   ）。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "当事人对行政处罚决定不服，申请行政复议或者提起行政诉讼的，行政处罚不停止执行，法律另有规定的除外。",
          "label": "A"
        },
        {
          "content": "当事人对行政处罚决定不服，申请行政复议或者提起行政诉讼的，行政处罚可以停止执行，法律另有规定的除外。",
          "label": "B"
        },
        {
          "content": "以非法手段取得的证据，也可以作为认定案件事实的根据。",
          "label": "C"
        },
        {
          "content": "以非法手段取得的证据，不得作为认定案件事实的根据。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "设定和实施行政许可，应当遵循（     ）的原则。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "公开",
          "label": "A"
        },
        {
          "content": "公平",
          "label": "B"
        },
        {
          "content": "公正",
          "label": "C"
        },
        {
          "content": "非歧视",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各省份要加强免疫效果监测评价，坚持()与()相结合，对畜禽群体抗体合格率未达到规定要求的，及时组织开展补免。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "固定监测",
          "label": "A"
        },
        {
          "content": "随机抽检",
          "label": "B"
        },
        {
          "content": "专项监测",
          "label": "C"
        },
        {
          "content": "常规监测",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，病害动物产品，是指来源于（    ）的动物产品。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "病死动物的产品",
          "label": "A"
        },
        {
          "content": "经检验检疫可能危害人体健康",
          "label": "B"
        },
        {
          "content": "经检验检疫可能危害环境",
          "label": "C"
        },
        {
          "content": "经检验检疫可能危害动物健康",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，有（   ）的，由县级以上地方人民政府农业农村主管部门责令限期改正，可以处一千元以下罚款。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "对饲养的动物未按照动物疫病强制免疫计划或者免疫技术规范实施免疫接种的",
          "label": "A"
        },
        {
          "content": "对饲养的种用、乳用动物未按照国务院农业农村主管部门的要求定期开展疫病检测，或者经检测不合格而未按照规定处理的",
          "label": "B"
        },
        {
          "content": "对饲养的犬只未按规定定期进行狂犬病免疫接种的",
          "label": "C"
        },
        {
          "content": "动物、动物产品的运载工具在装载前和卸载后未按照规定及时清洗、消毒的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，补检的动物具备下列（   ）条件的，补检合格，出具动物检疫证明。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "畜禽标识符合规定",
          "label": "A"
        },
        {
          "content": "检疫申报需要提供的材料齐全、符合要求",
          "label": "B"
        },
        {
          "content": "临床检查健康",
          "label": "C"
        },
        {
          "content": "补检的动物未佩戴耳标，货主于七日内提供检疫规程规定的实验室疫病检测报告，检测结果合格",
          "label": "D"
        },
        {
          "content": "货主于五日内提供检疫规程规定的实验室疫病检测报告，检测结果合格",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "要切实加强养殖环节病死畜禽无害化处理补助资金使用监管，对（     ）的单位和个人，一经查实，全额追回补助资金并依法依规从严处理。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "未规范处理",
          "label": "A"
        },
        {
          "content": "提供虚假资料",
          "label": "B"
        },
        {
          "content": "骗取套取补助资金",
          "label": "C"
        },
        {
          "content": "挪用补助资金",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，以下属于检疫处理通知单需要填写的内容有（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "编号",
          "label": "A"
        },
        {
          "content": "货主的姓名",
          "label": "B"
        },
        {
          "content": "动物或动物产品的种类",
          "label": "C"
        },
        {
          "content": "无害化处理方法",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，经强制免疫的动物，应当按照国务院农业农村主管部门规定（　　）。",
      "standard_answer": "B,C,E",
      "option_list": [
        {
          "content": "保证可溯源",
          "label": "A"
        },
        {
          "content": "加施畜禽标识",
          "label": "B"
        },
        {
          "content": "保证可追溯",
          "label": "C"
        },
        {
          "content": "建立养殖档案",
          "label": "D"
        },
        {
          "content": "建立免疫档案",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，省级人民政府畜牧兽医行政主管部门应当建立畜禽标识及所需配套设备的采购、保管、（）、销毁等制度。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "发放",
          "label": "A"
        },
        {
          "content": "使用",
          "label": "B"
        },
        {
          "content": "登记",
          "label": "C"
        },
        {
          "content": "回收",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "病死畜禽和病害畜禽产品无害化处理场所应当安装视频监控设备，对病死畜禽和病害畜禽产品（     ）等进行全程监控。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "进（出）场",
          "label": "A"
        },
        {
          "content": "交接",
          "label": "B"
        },
        {
          "content": "处理",
          "label": "C"
        },
        {
          "content": "处理产物存放",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对布病阳性率较高的场群，应及时（）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "转场",
          "label": "A"
        },
        {
          "content": "整群淘汰",
          "label": "B"
        },
        {
          "content": "开展免疫",
          "label": "C"
        },
        {
          "content": "剔除阳性动物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，货主应当履行动物疫病强制免疫义务，按照免疫计划和技术规范，对动物实施免疫接种，并按照国家有关规定建立免疫档案、加施畜禽标识，保证可追溯。（   ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，动物饲养场、屠宰企业的执业兽医或者动物防疫技术人员，应当协助官方兽医实施检疫。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "违反《动物防疫法》规定，造成人畜共患传染病传播、流行的，从重给予处分、处罚。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "执业兽医开具兽医处方应当亲自诊断，并对诊断结论负责。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "国家实行动物疫病检测和疫情预警制度。（   ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "强制免疫动物疫病的群体免疫密度应常年保持在100%以上。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，种畜调运时应当在个体养殖档案上注明调出和调入地，个体养殖档案应当留场备查。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "我国牛结核病在奶牛群体中仍有一定程度流行，防控形势不容乐观。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "地方各级农业农村主管部门对辖区内动物防疫工作负总责，组织有关部门按照职责分工，落实强制免疫工作任务。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽标识编码共有18位数字。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村部规定，生猪屠宰检疫验讫印章，上面承载省份、检疫验讫、地市代码、屠宰场编码及日期等信息。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "违反《动物防疫法》规定，持有、使用伪造或者变造的检疫证明、检疫标志或者畜禽标识的，由县级以上人民政府农业农村主管部门没收检疫证明、检疫标志、畜禽标识和对应的动物、动物产品，并处三千元以上三万元以下罚款。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，官方兽医应当具备国务院农业农村主管部门规定的条件，由省、自治区、直辖市人民政府农业农村主管部门按程序确认、任命。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "官方兽医要做好免疫记录、按时报告，确保免疫记录与畜禽标识相符。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "饲养动物的有关单位和个人应自行开展免疫或向第三方服务主体购买免疫服务，对饲养动物实施免疫接种，并按有关规定建立免疫档案、加施畜禽标识，确保可追溯。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要按照科学规划、合理布局、安全便民、管理可行的原则，尽快确定并公布指定通道。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，（）应当建立畜禽标识及所需配套设备的采购等制度。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "省级人民政府畜牧兽医行政主管部门",
          "label": "A"
        },
        {
          "content": "省级动物疫病预防控制机构",
          "label": "B"
        },
        {
          "content": "县级以上地方人民政府畜牧兽医主管部门",
          "label": "C"
        },
        {
          "content": "省级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "某市人大常委会制定了一部地方性法规，规定公民如果要经营某种商品，不但要得到市场监督管理部门的许可，而且要得到文化和卫生行政管理部门的许可，但是相应行政法规并没有规定要得到文化和卫生行政管理部门的许可。则关于这部地方性法规的正确说法是（       ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "该地方性法规是违法的，因为地方性法规没有设定行政许可的权力",
          "label": "A"
        },
        {
          "content": "该地方性法规是违法的，因为对于该行政许可事项，行政法规已作出了规定",
          "label": "B"
        },
        {
          "content": "该地方性法规是违法的，地方性法规无权对行政法规设定的行政许可增设行政许可的条件",
          "label": "C"
        },
        {
          "content": "该地方性法规是合法的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "农产品质量安全标准由（ ）主管部门商有关部门推进实施。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "农业农村",
          "label": "A"
        },
        {
          "content": "市场监管",
          "label": "B"
        },
        {
          "content": "卫生健康",
          "label": "C"
        },
        {
          "content": "以上都不是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，因科研、药用、展示等特殊情形需要（   ）的野生动物，应当按照国家有关规定报动物卫生监督机构检疫，检疫合格的，方可利用。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "非食用性利用",
          "label": "A"
        },
        {
          "content": "屠宰",
          "label": "B"
        },
        {
          "content": "养殖",
          "label": "C"
        },
        {
          "content": "利用",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "布病强制免疫工作有效开展，免疫地区免疫密度常年保持在90%以上，免疫建档率100%，免疫奶牛场备案率100%，免疫评价工作开展率()%。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "70",
          "label": "A"
        },
        {
          "content": "80",
          "label": "B"
        },
        {
          "content": "90",
          "label": "C"
        },
        {
          "content": "100",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪定点屠宰证书和生猪定点屠宰标志牌由（       ）颁发。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "省级人民政府",
          "label": "A"
        },
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "设区的市级人民政府",
          "label": "C"
        },
        {
          "content": "设区的市级人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，以下畜禽防疫档案内容中，属于畜禽养殖场需要填写但散养户不需要填写的内容是（）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "畜禽种类",
          "label": "A"
        },
        {
          "content": "数量",
          "label": "B"
        },
        {
          "content": "免疫日期",
          "label": "C"
        },
        {
          "content": "畜禽养殖代码",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "到2025年，国家、省、市、县四级官方兽医培训体系进一步健全完善，官方兽医知识结构持续更新优化，专业能力和综合素质明显提升，省、市、县三级动物卫生监督机构官方兽医完训率达到（）%，考试合格率达到90％以上。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "100",
          "label": "A"
        },
        {
          "content": "90",
          "label": "B"
        },
        {
          "content": "95",
          "label": "C"
        },
        {
          "content": "80",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政许可采取统一办理或者联合办理、集中办理的，在规定时间内不能办结的，经本级人民政府负责人批准，可以延长（      ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "五日",
          "label": "A"
        },
        {
          "content": "十日",
          "label": "B"
        },
        {
          "content": "十五日",
          "label": "C"
        },
        {
          "content": "三十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员收受可能影响公正行使公权力的礼品、礼金、有价证券等财物情节较重的，予以（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "官方兽医岗前培训，每人参加培训时间不得少于（）学时（含线上培训）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "20",
          "label": "A"
        },
        {
          "content": "30",
          "label": "B"
        },
        {
          "content": "40",
          "label": "C"
        },
        {
          "content": "50",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关实施行政处罚时，应当责令当事人\r\n（      ）违法行为。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "停止或限期停止",
          "label": "A"
        },
        {
          "content": "中止或立即终止",
          "label": "B"
        },
        {
          "content": "改正或者限期改正",
          "label": "C"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，（　　）应当加强动物检疫申报点的建设。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "县级以上地方人民政府动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "所在地乡镇人民政府",
          "label": "C"
        },
        {
          "content": "县级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "设定行政许可，应当促进（      ）协调发展。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "经济、文化和生态环境",
          "label": "A"
        },
        {
          "content": "经济、社会和生态环境",
          "label": "B"
        },
        {
          "content": "社会、文化和生态环境",
          "label": "C"
        },
        {
          "content": "社会、文化和自然环境",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）在“五、加强法律宣传贯彻，推动指定通道制度有效落实”中要求，要重点在（  ）等场所开展宣传，指导生产经营主体利用信息化手段查询、了解指定通道基础信息，规范开展动物运输活动。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "检疫申报点",
          "label": "A"
        },
        {
          "content": "监督检查站",
          "label": "B"
        },
        {
          "content": "指定通道",
          "label": "C"
        },
        {
          "content": "屠宰厂（场）",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《中华人民共和国畜牧法》的规定，畜禽批发市场选址，应当符合法律、行政法规和国务院农业农村主管部门规定的动物防疫条件，并距离种畜禽场和大型畜禽养殖场（       ）以外。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "七公里",
          "label": "A"
        },
        {
          "content": "五公里",
          "label": "B"
        },
        {
          "content": "三公里",
          "label": "C"
        },
        {
          "content": "十公里",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，动物饲养场和动物屠宰加工场所，应当有与其规模相适应的（    ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "执业兽医",
          "label": "A"
        },
        {
          "content": "动物防疫技术人员",
          "label": "B"
        },
        {
          "content": "执业兽医或者动物防疫技术人员",
          "label": "C"
        },
        {
          "content": "执业兽医和动物防疫技术人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家鼓励（ ）对农产品质量安全进行社会监督，对农产品质量安全监督管理工作提出意见和建议。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "消费者协会",
          "label": "A"
        },
        {
          "content": "单位",
          "label": "B"
        },
        {
          "content": "个人",
          "label": "C"
        },
        {
          "content": "以上都是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，县级以上（　　）应当储备动物疫情应急处置所需的防疫物资。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "动物疫病预防控制机构",
          "label": "A"
        },
        {
          "content": "动物卫生监督机构",
          "label": "B"
        },
        {
          "content": "人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各地农业农村部门要健全完善官方兽医培训考核评价机制，将官方兽医（）和（）作为官方兽医任职考核、职务职称晋升的重要内容和依据。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "参加学习培训",
          "label": "A"
        },
        {
          "content": "考核情况",
          "label": "B"
        },
        {
          "content": "获奖情况",
          "label": "C"
        },
        {
          "content": "履职情况",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，养殖场发现动物染疫或者疑似染疫的，应当立即向（　　）报告。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "所在地动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "所在地动物疫病预防控制机构",
          "label": "B"
        },
        {
          "content": "所在地农业农村主管部门",
          "label": "C"
        },
        {
          "content": "所在地农业综合执法机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《全国畜间人兽共患病防治规划2022—2030年）》提出的炭疽的重点防治措施的是（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "做好监测报告",
          "label": "A"
        },
        {
          "content": "严格规范处置疫情",
          "label": "B"
        },
        {
          "content": "做好针对性免疫",
          "label": "C"
        },
        {
          "content": "加强动物卫生监管",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下行为中属于《生猪屠宰管理条例》禁止的有（       ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "生猪定点屠宰厂（场）以及其他任何单位和个人对生猪、生猪产品注水或者注入其他物质",
          "label": "A"
        },
        {
          "content": "生猪定点屠宰厂（场）屠宰注水或者注入其他物质的生猪",
          "label": "B"
        },
        {
          "content": "任何单位和个人为未经定点违法从事生猪屠宰活动的单位和个人提供生猪屠宰场所或者生猪产品储存设施",
          "label": "C"
        },
        {
          "content": "为对生猪、生猪产品注水或者注入其他物质的单位和个人提供场所",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "全面推进“先打后补”工作，在()年年底前实现规模养殖场(户)全覆盖，在()年年底前逐步全面停止政府招标采购强制免疫疫苗。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "2022",
          "label": "A"
        },
        {
          "content": "2023",
          "label": "B"
        },
        {
          "content": "2025",
          "label": "C"
        },
        {
          "content": "2024",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "直接关系（     ）的设备、设施、产品、物品的检验、检测、检疫，除法律、行政法规规定由行政机关实施的外，应当逐步由符合法定条件的专业技术组织实施。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "国家安全",
          "label": "A"
        },
        {
          "content": "公共安全",
          "label": "B"
        },
        {
          "content": "人身健康安全",
          "label": "C"
        },
        {
          "content": "生命财产安全",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "官方兽医岗前培训和考核由省级农业农村部门负责，（）农业农村部门具体组织实施。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "县",
          "label": "A"
        },
        {
          "content": "乡",
          "label": "B"
        },
        {
          "content": "省",
          "label": "C"
        },
        {
          "content": "市",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，动物饲养场、屠宰加工场所的(    )或者(    ），应当协助官方兽医实施动物检疫。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "协检人员",
          "label": "A"
        },
        {
          "content": "执业兽医",
          "label": "B"
        },
        {
          "content": "动物防疫技术人员",
          "label": "C"
        },
        {
          "content": "兽医专业人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "受行政机关委托组织实施行政处罚的组织必须符合（     ）等条件",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "机构编制部门同意",
          "label": "A"
        },
        {
          "content": "依法成立并具有管理公共事务职能",
          "label": "B"
        },
        {
          "content": "有熟悉有关法律、法规、规章和业务并取得行政执法资格的工作人员",
          "label": "C"
        },
        {
          "content": "有条件组织进行相应的技术检查或者技术鉴定",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，动物疫病预防控制机构承担动物疫病的（    ）以及其他预防、控制技术工作。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "诊断",
          "label": "A"
        },
        {
          "content": "流行病学调查",
          "label": "B"
        },
        {
          "content": "监测",
          "label": "C"
        },
        {
          "content": "检测",
          "label": "D"
        },
        {
          "content": "疫情报告",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《病死畜禽和病害畜禽产品无害化处理管理办法》，病死畜禽和病害畜禽产品无害化处理场所销售无害化处理产物的，应做到（     ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "严控无害化处理产物流向",
          "label": "A"
        },
        {
          "content": "查验购买方资质并留存相关材料",
          "label": "B"
        },
        {
          "content": "定期对购买方使用产物情况进行核查",
          "label": "C"
        },
        {
          "content": "与购买方签订销售合同",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国畜牧法》的规定，以下违法行为中要依照《中华人民共和国动物防疫法》的有关规定追究法律责任的有（       ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "销售的种畜禽未附具检疫证明的",
          "label": "A"
        },
        {
          "content": "伪造、变造畜禽标识的",
          "label": "B"
        },
        {
          "content": "持有、使用伪造、变造的畜禽标识的",
          "label": "C"
        },
        {
          "content": "销售、收购国务院农业农村主管部门规定应当加施标识而没有标识的畜禽的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员因违法行为获得的（）等利益，监察机关应当建议有关机关、单位、组织按规定予以纠正。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "职务",
          "label": "A"
        },
        {
          "content": "衔级",
          "label": "B"
        },
        {
          "content": "职级",
          "label": "C"
        },
        {
          "content": "级别",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "从事动物（    ）的单位和个人，应当按照国家有关规定做好病死动物、病害动物产品的无害化处理，或者委托动物和动物产品无害化处理场所处理。",
      "standard_answer": "A,B,D,E",
      "option_list": [
        {
          "content": "饲养",
          "label": "A"
        },
        {
          "content": "屠宰",
          "label": "B"
        },
        {
          "content": "运输",
          "label": "C"
        },
        {
          "content": "隔离",
          "label": "D"
        },
        {
          "content": "经营",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，动物诊疗许可证应当载明（    ）等事项。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "诊疗机构名称",
          "label": "A"
        },
        {
          "content": "诊疗活动范围",
          "label": "B"
        },
        {
          "content": "从业地点",
          "label": "C"
        },
        {
          "content": "法定代表人（负责人）",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，经营动物、动物产品的集贸市场应当具备国务院农业农村主管部门规定的动物防疫条件，并接受农业农村主管部门的监督检查。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，经指定通道运输动物的监督检查中，发现动物临床健康状况异常的，要及时采取隔离观察、采样检测等措施。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，要根据本地区动物疫病防控需要，配备必要的防疫消毒设施设备，指导承运人做好动物运输车辆的防疫消毒。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，发生一类动物疫病时，在封锁期间，禁止非疫区的所有动物进入疫区。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》提出的重点防范的外来疫病的防治目标是传入和扩散风险有效降低。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《中华人民共和国畜牧法》规定，在非洲猪瘟等重大动物疫情应急期间，从事家畜养殖不得使用未经高温处理的餐馆、食堂的泔水饲喂家畜，非应急期间可以使用。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "违反《动物防疫法》规定，屠宰、经营、运输的动物未附有检疫证明，经营和运输的动物产品未附有检疫证明、检疫标志的，对货主或者承运人处运输费用三倍以上五倍以下罚款，情节严重的，处五倍以上十倍以下罚款。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，国家对严重危害养殖业生产和人体健康的动物疫病实施强制免疫。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "省、自治区、直辖市人民政府农业农村主管部门制定本行政区域的强制免疫计划;根据本行政区域动物疫病流行情况调整实施强制免疫的动物疫病病种和区域，报本级人民政府批准后执行，并报国务院农业农村主管部门备案。（   ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "要充分发挥兽医卫生综合信息平台官方兽医培训考试模块和“牧运通”APP作用，督促指导官方兽医加强自主学习，全面开展官方兽医线下培训和线下考试。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "销售商品代仔畜、雏禽的，应当附具动物卫生监督机构出具的检疫证明。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，**县动物卫生监督机构官方兽医张三对未经检疫的动物出具检疫证明，应当由上一级市级农业农村主管部门责令改正，通报批评；并对直接负责的主管人员和其他直接责任人员依法给予处分。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）是为了深入贯彻落实《中华人民共和国动物防疫法》、《国务院办公厅关于加强非洲猪瘟防控工作的意见》（国办发〔2019〕32号）、《国务院办公厅关于促进畜牧业高质量发展的意见》（国办发〔2020〕31号）有关要求。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，县级以上地方人民政府设立的动物卫生监督机构负责动物、动物产品的检疫工作。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所，应当符合本办法规定的动物防疫条件，并取得（ ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物防疫条件合格证",
          "label": "A"
        },
        {
          "content": "动物防疫场所合格证",
          "label": "B"
        },
        {
          "content": "动物防疫安全合格证",
          "label": "C"
        },
        {
          "content": "动物防疫条件许可证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "省、自治区、直辖市人民政府规章可以设定临时性的行政许可。临时性的行政许可实施满（   ）需要继续实施的，应当提请本级人民代表大会及其常务委员会制定地方性法规。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "三个月",
          "label": "A"
        },
        {
          "content": "六个月",
          "label": "B"
        },
        {
          "content": "一年",
          "label": "C"
        },
        {
          "content": "两年",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "××县发生三类动物疫病时，（　　）应当按照国务院农业农村主管部门的规定组织防治。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "××县农牧局、发生疫情的有关乡镇畜牧兽医站",
          "label": "A"
        },
        {
          "content": "××县动物疫病预防控制中心、发生疫情有关的乡镇畜牧兽医站",
          "label": "B"
        },
        {
          "content": "××县农牧局、发生疫情有关的乡镇人民政府",
          "label": "C"
        },
        {
          "content": "××县人民政府、发生疫情有关的乡镇人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，（）建立本行政区域畜禽标识信息数据库，并成为国家畜禽标识信息中央数据库的子数据库。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级畜牧兽医行政主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "县级以上地方政府畜牧兽医行政主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国畜牧法》规定，国务院农业农村主管部门应当制定畜禽标识和养殖档案管理办法，采取措施落实（       ）和责任追究制度。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "新发现的畜禽遗传资源保护",
          "label": "A"
        },
        {
          "content": "畜禽遗传资源保护",
          "label": "B"
        },
        {
          "content": "畜禽遗传资源保护名录",
          "label": "C"
        },
        {
          "content": "畜禽产品质量安全追溯",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物卫生监督机构依照《动物防疫法》和国务院农业农村主管部门的规定对（    ）实施检疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "动物、产品",
          "label": "A"
        },
        {
          "content": "动物、动物产品",
          "label": "B"
        },
        {
          "content": "动物",
          "label": "C"
        },
        {
          "content": "产品",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员对监察机关作出的涉及本人的政务处分决定不服，对作出决定的监察机关决定仍不服的，可以向上一级监察机关申请（）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "复审",
          "label": "A"
        },
        {
          "content": "复议",
          "label": "B"
        },
        {
          "content": "申诉",
          "label": "C"
        },
        {
          "content": "复核",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，国家实行执业兽医（　　）制度。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "长期聘任",
          "label": "A"
        },
        {
          "content": "定期鉴定",
          "label": "B"
        },
        {
          "content": "资格考试",
          "label": "C"
        },
        {
          "content": "临时评估",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "关于行政许可收费表述不正确的是（      ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "行政机关实施行政许可，应当按照公布的法定项目和标准收费",
          "label": "A"
        },
        {
          "content": "行政机关所收取的费用，除去必要的办公费用后，必须全部上缴国库",
          "label": "B"
        },
        {
          "content": "财政部门不得以任何形式向行政机关返还或者变相返还实施行政许可所收取的费用",
          "label": "C"
        },
        {
          "content": "行政机关所收取的费用，任何机关或者个人不得以任何形式截留、挪用、私分或者变相私分",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，（）建立包括国家畜禽标识信息中央数据库在内的国家畜禽标识信息管理系统。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级畜牧兽医行政主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "县级以上地方政府畜牧兽医行政主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "从事养殖、运输、屠宰、加工等相关重点职业人群的布病防治知识知晓率达()% 以上，基层动物防疫检疫人员的布病防治知识普及覆盖面达95% 以上。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "70",
          "label": "A"
        },
        {
          "content": "95",
          "label": "B"
        },
        {
          "content": "90",
          "label": "C"
        },
        {
          "content": "100",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家实行生猪屠宰（       ）风险监测制度。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "质量安全",
          "label": "A"
        },
        {
          "content": "产品品质",
          "label": "B"
        },
        {
          "content": "违法添加",
          "label": "C"
        },
        {
          "content": "食源性致病微生物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（    ）负责本行政区域内动物检疫证章标志的管理工作，建立动物检疫证章标志管理制度，严格按照程序订购、保管、发放。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "县级以上动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "县级以上农业综合执法机构",
          "label": "C"
        },
        {
          "content": "动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物疫病诊疗过程中，相关人员应做好（     ）。治疗期间所使用的用具应严格消毒，产生的医疗废弃物等应进行无害化处理。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "个人防护",
          "label": "A"
        },
        {
          "content": "个人卫生",
          "label": "B"
        },
        {
          "content": "个人清洁",
          "label": "C"
        },
        {
          "content": "个人安全",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员有两个以上违法行为的，应当（）政务处分。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "分别确定",
          "label": "A"
        },
        {
          "content": "合并确定",
          "label": "B"
        },
        {
          "content": "选取其中之一",
          "label": "C"
        },
        {
          "content": "选取较重的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物检疫管理办法》，官方兽医符合的条件不包括（    )。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "从事动物防疫等相关工作满五年以上",
          "label": "A"
        },
        {
          "content": "从事动物检疫工作",
          "label": "B"
        },
        {
          "content": "接受岗前培训，并经考核合格",
          "label": "C"
        },
        {
          "content": "具有畜牧兽医水产初级以上职称",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "官方兽医编号由（）位阿拉伯数字组成，包括工作单位对应的行政区划代码+顺序号。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "8",
          "label": "A"
        },
        {
          "content": "9",
          "label": "B"
        },
        {
          "content": "11",
          "label": "C"
        },
        {
          "content": "10",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽养殖场、养殖小区应当依法向（）备案，取得畜禽养殖代码。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "所在地县级人民政府畜牧兽医行政主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府畜牧兽医主管部门",
          "label": "B"
        },
        {
          "content": "县级动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "县级动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政处罚应当由具有行政执法资格的执法人员实施。执法人员不得（     ），法律另有规定的除外。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "多于五人",
          "label": "A"
        },
        {
          "content": "少于两人",
          "label": "B"
        },
        {
          "content": "少于两人、多于五人",
          "label": "C"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对于不满（    ）周岁的未成年人有违法行为的，不予行政处罚",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "十二",
          "label": "A"
        },
        {
          "content": "十四",
          "label": "B"
        },
        {
          "content": "十六",
          "label": "C"
        },
        {
          "content": "十八",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，（   ）制定并组织实施动物疫病防治规划。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "县级以上人民政府",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "B"
        },
        {
          "content": "县级以上农业农村主管部门",
          "label": "C"
        },
        {
          "content": "县级以上动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "到（）年，全国畜间布病总体流行率有效降低，牛羊群体健康水平明显提高，个体阳性率控制在0.4%以下，群体阳性率控制在7%以下。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "2023",
          "label": "A"
        },
        {
          "content": "2024",
          "label": "B"
        },
        {
          "content": "2025",
          "label": "C"
        },
        {
          "content": "2026",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "从事动物（     ）等活动的单位和个人，依照《动物防疫法》和国务院农业农村主管部门的规定，做好动物疫病防疫工作，承担动物防疫责任。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "饲养、屠宰",
          "label": "A"
        },
        {
          "content": "经营、隔离",
          "label": "B"
        },
        {
          "content": "运输",
          "label": "C"
        },
        {
          "content": "交易",
          "label": "D"
        },
        {
          "content": "贩运",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "贯彻习近平总书记关于加强国家生物安全风险防控和治理体系建设指示精神，坚持人民至上、生命至上，实行积极防御、系统治理，有效控制传染源、阻断传播途径、提高抗病能力，切实做好布病源头防控工作，维护()安全。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "畜牧业生产",
          "label": "A"
        },
        {
          "content": "公共卫生",
          "label": "B"
        },
        {
          "content": "生物",
          "label": "C"
        },
        {
          "content": "动物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，法律、法规授权或者受国家机关依法委托管理公共事务的组织中从事公务的人员，在政务处分期内，不得晋升（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "职务",
          "label": "A"
        },
        {
          "content": "岗位",
          "label": "B"
        },
        {
          "content": "职员等级",
          "label": "C"
        },
        {
          "content": "职称",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "A县农牧局的工作人员对不符合动物防疫条件的养殖场颁发了《动物防疫条件合格证》，应当（    ）。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "由A县人民政府责令改正，通报批评",
          "label": "A"
        },
        {
          "content": "由A县农牧局责令改正，通报批评",
          "label": "B"
        },
        {
          "content": "由A县人民政府对直接负责的主管人员和其他直接责任人员依法给予处分",
          "label": "C"
        },
        {
          "content": "由A县农牧局对直接负责的主管人员和其他直接责任人员依法给予处分",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰证书应当载明屠宰厂（场）（     ）等事项。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "名称",
          "label": "A"
        },
        {
          "content": "生产地址",
          "label": "B"
        },
        {
          "content": "经营范围",
          "label": "C"
        },
        {
          "content": "法定代表人（负责人）",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各地要强化指定通道的条件保障，配齐配强工作力量，安排足够的（   ）人员。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "监督执法",
          "label": "A"
        },
        {
          "content": "监督管理",
          "label": "B"
        },
        {
          "content": "动物防疫及辅助",
          "label": "C"
        },
        {
          "content": "官方兽医",
          "label": "D"
        },
        {
          "content": "行政执法",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "关于行政处罚，下列说法正确的是（      ）",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "国务院部门规章可以在法律、行政法规规定的给予行政处罚的行为、种类和幅度的范围内作出具体规定",
          "label": "A"
        },
        {
          "content": "地方政府规章可以在法律、法规规定的给予行政处罚的行为、种类和幅度的范围内作出具体规定",
          "label": "B"
        },
        {
          "content": "尚未制定法律、行政法规的，国务院部门规章对违反行政管理秩序的行为，可以设定警告、通报批评或者一定数额罚款的行政处罚",
          "label": "C"
        },
        {
          "content": "法律、行政法规对违法行为已经作出行政处罚规定，地方性法规需要作出具体规定的，必须在法律、行政法规规定的给予行政处罚的行为、种类和幅度的范围内规定",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "优先支持()等开展布病净化。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "国家级牛羊养殖标准化示范场",
          "label": "A"
        },
        {
          "content": "休闲观光牧场",
          "label": "B"
        },
        {
          "content": "养殖合作社",
          "label": "C"
        },
        {
          "content": "规模场",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "建立病死畜禽无害化处理机制，要按照推进生态文明建设的总体要求，以（     ）为目标。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "及时处理",
          "label": "A"
        },
        {
          "content": "不留隐患",
          "label": "B"
        },
        {
          "content": "清洁环保",
          "label": "C"
        },
        {
          "content": "合理利用",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "饲养动物的单位和个人应当履行动物疫病强制免疫义务，按照（    ),对动物实施免疫接种，并按照国家有关规定建立免疫档案、加施畜禽标识，保证可追溯。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "强制免疫计划",
          "label": "A"
        },
        {
          "content": "强制免疫规划",
          "label": "B"
        },
        {
          "content": "技术规范",
          "label": "C"
        },
        {
          "content": "技术标准",
          "label": "D"
        },
        {
          "content": "免疫规范",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，复审机关认为政务处分所依据的事实不清，应当（）作出决定。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "重新",
          "label": "A"
        },
        {
          "content": "责令原作出决定的监察机关重新",
          "label": "B"
        },
        {
          "content": "指定监察机关",
          "label": "C"
        },
        {
          "content": "由共同管辖的上级监察机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下应当取得种畜禽生产经营许可证的有（     ）。",
      "standard_answer": "A,B,D,E",
      "option_list": [
        {
          "content": "从事种畜禽生产经营的单位",
          "label": "A"
        },
        {
          "content": "从事种畜禽生产经营的个人",
          "label": "B"
        },
        {
          "content": "饲养种公畜进行互助配种的农户",
          "label": "C"
        },
        {
          "content": "生产经营商品代仔畜、雏禽的单位",
          "label": "D"
        },
        {
          "content": "生产经营商品代仔畜、雏禽的个人",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《生猪屠宰管理条例》的立法目的是（     ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "加强生猪屠宰管理",
          "label": "A"
        },
        {
          "content": "保证生猪产品质量安全",
          "label": "B"
        },
        {
          "content": "防控人畜共患传染病",
          "label": "C"
        },
        {
          "content": "保障人民身体健康",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "饲养动物的单位和个人是免疫主体，承担免疫责任。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，县级以上地方人民政府农业农村主管部门应当根据本地情况，决定在城市特定区域禁止家畜家禽活体交易。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "国家推进动物疫病消灭，鼓励和支持饲养动物的单位和个人开展动物疫病净化。饲养动物的单位和个人达到国务院农业农村主管部门规定的净化标准的，由省级人民政府农业农村主管部门予以公布。（   ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "对全国所有鸡、鸭、鹅、鹌鹑等人工饲养的禽类，根据当地实际情况，在科学评估的基础上选择适宜疫苗，进行新城疫、高致病性禽流感免疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "乡镇人民政府对本行政区域的农产品质量安全工作负责，统一领导、组织、协调本行政区域的农产品质量安全工作。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《国家畜禽遗传资源品种名录》规定，西双版纳斗鸡作为特种畜禽，在我国云南很多地区有饲养传统。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "畜禽养殖场的规模标准和备案管理办法，由省、自治区、直辖市人民政府农业农村主管部门制定。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "县级农业农村部门要定期对辖区内布病防控效果进行评估，根据评估结果及时调整防控策略和措施。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村主管部门提供畜禽标识不得收费。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，尚未实施畜禽产品无纸化出证并实现电子证照互通互认的地区，暂时对在本屠宰企业内分割加工的畜禽产品继续出证。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "官方兽医证的格式由（     ）统一规定。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "省级动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对畜禽群体抗体合格率未达到规定要求的，及时组织开展（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "扑杀",
          "label": "A"
        },
        {
          "content": "补免",
          "label": "B"
        },
        {
          "content": "上报",
          "label": "C"
        },
        {
          "content": "监测",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "违反《动物防疫法》规定，对经强制免疫的动物未按照规定建立免疫档案，或者未按照规定加施畜禽标识的，依照（　　）的有关规定处罚。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "《动物检疫管理办法》",
          "label": "A"
        },
        {
          "content": "《畜禽标识和养殖档案管理办法》",
          "label": "B"
        },
        {
          "content": "《中华人民共和国畜牧法》",
          "label": "C"
        },
        {
          "content": "《畜禽标识管理办法》",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对开展强制免疫“先打后补”的养殖场(户)，要组织开展（），确保免疫效果。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "普查",
          "label": "A"
        },
        {
          "content": "排查",
          "label": "B"
        },
        {
          "content": "抽查",
          "label": "C"
        },
        {
          "content": "监测",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "××县张某要设立宠物门诊，应当向（　　）申请动物诊疗许可证。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "××县农业农村局",
          "label": "A"
        },
        {
          "content": "××县动物卫生监督所",
          "label": "B"
        },
        {
          "content": "××县动物疫病预防控制中心",
          "label": "C"
        },
        {
          "content": "××县市场监督管理局",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政处罚法》，下列哪种情形，执法人员可以当场收缴罚款。（       ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "依法给予一百元以下罚款的",
          "label": "A"
        },
        {
          "content": "依法给予两百元以下罚款的",
          "label": "B"
        },
        {
          "content": "依法给予三百元以下罚款的",
          "label": "C"
        },
        {
          "content": "当事人提出申请的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》明确了重点防范的外来疫病是尼帕病毒性脑炎和（     ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "蓝舌病",
          "label": "A"
        },
        {
          "content": "日本脑炎",
          "label": "B"
        },
        {
          "content": "痒病",
          "label": "C"
        },
        {
          "content": "牛海绵状脑病",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "全面推进“先打后补”工作，在2022年年底前实现（）全覆盖。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "种畜场",
          "label": "A"
        },
        {
          "content": "散养户",
          "label": "B"
        },
        {
          "content": "奶畜场",
          "label": "C"
        },
        {
          "content": "规模养殖场（户）",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违法取得的财物和用于违法行为的本人财物，应当退还原所有人或者原持有人的，依法（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "予以退还",
          "label": "A"
        },
        {
          "content": "上缴国库",
          "label": "B"
        },
        {
          "content": "经法院审理后确定",
          "label": "C"
        },
        {
          "content": "由监察机关处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，经检疫不合格的动物、动物产品，由官方兽医出具（　　）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "无害化处理通知书",
          "label": "A"
        },
        {
          "content": "检疫处理通知书",
          "label": "B"
        },
        {
          "content": "检疫处理通知单",
          "label": "C"
        },
        {
          "content": "无害化处理通知单",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，动物卫生监督机构接到检疫申报后，应当及时指派官方兽医对动物、动物产品实施检疫；检疫合格的，（    ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "出具检疫证明",
          "label": "A"
        },
        {
          "content": "加施检疫标志",
          "label": "B"
        },
        {
          "content": "出具检疫证明、加施检疫标志",
          "label": "C"
        },
        {
          "content": "出具证明、加施标志",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》第十三届全国人民代表大会常务委员会第十九次会议通过，于2020年（）实施。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "1月1日",
          "label": "A"
        },
        {
          "content": "5月1日",
          "label": "B"
        },
        {
          "content": "7月1日",
          "label": "C"
        },
        {
          "content": "9月1日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员拒不执行或者变相不执行、拖延执行上级依法作出的决定、命令情节严重的，予以（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "农业农村部规定，牛羊肉塑料卡环式检疫验讫标签规格为（）毫米。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "9×10",
          "label": "A"
        },
        {
          "content": "10×10",
          "label": "B"
        },
        {
          "content": "34×38",
          "label": "C"
        },
        {
          "content": "32×32",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，（　　）制定、调整并公布检疫规程，明确动物检疫的范围、对象和程序。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "农业农村部",
          "label": "B"
        },
        {
          "content": "农业农村部畜牧兽医局",
          "label": "C"
        },
        {
          "content": "中国动物疫病预防控制中心",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员对依法行使批评、申诉、控告、检举等权利的行为进行压制或者打击报复的，予以（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "警告、记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列哪种情形，不予行政处罚。（     ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "精神病人、智力残疾人在不能辨认或者不能控制自己行为时有违法行为的",
          "label": "A"
        },
        {
          "content": "间歇性精神病人在精神正常时有违法行为的",
          "label": "B"
        },
        {
          "content": "尚未完全丧失辨认或者控制自己行为能力的精神病人、智力残疾人有违法行为的",
          "label": "C"
        },
        {
          "content": "以上都不是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，向无规定动物疫病区输入相关易感动物的，货主应当向输入地（   ）动物卫生监督机构申报。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "隔离场所在地",
          "label": "A"
        },
        {
          "content": "省级",
          "label": "B"
        },
        {
          "content": "县级",
          "label": "C"
        },
        {
          "content": "无疫区域",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国畜牧法》的规定，县级以上人民政府应当组织农业农村主管部门和其他有关部门，依照《中华人民共和国畜牧法》和有关法律、行政法规的规定，加强对（       ）的监督管理。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "畜禽饲养环境",
          "label": "A"
        },
        {
          "content": "种畜禽质量",
          "label": "B"
        },
        {
          "content": "畜禽交易与运输",
          "label": "C"
        },
        {
          "content": "畜禽屠宰",
          "label": "D"
        },
        {
          "content": "饲料、饲料添加剂、兽药等投入品的生产、经营、使用",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "官方兽医跨县（区）工作调动，应当（）。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "注销原证",
          "label": "A"
        },
        {
          "content": "重新任命发证",
          "label": "B"
        },
        {
          "content": "重新培训上岗",
          "label": "C"
        },
        {
          "content": "携带原证上岗",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《生猪屠宰管理条例》，生猪定点屠宰厂（场）屠宰生猪，应当遵守（     ），并严格执行消毒技术规范。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "安全生产规定",
          "label": "A"
        },
        {
          "content": "国家规定的操作规程",
          "label": "B"
        },
        {
          "content": "国家规定的技术要求",
          "label": "C"
        },
        {
          "content": "生猪屠宰质量管理规范",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员篡改、伪造本人档案资料情节严重的，予以（）。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "记过",
          "label": "A"
        },
        {
          "content": "记大过",
          "label": "B"
        },
        {
          "content": "降级",
          "label": "C"
        },
        {
          "content": "撤职",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，已经离职或者死亡的公职人员在履职期间有违法行为的，（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "不再给予政务处分",
          "label": "A"
        },
        {
          "content": "可以对其立案调查",
          "label": "B"
        },
        {
          "content": "违法所得依法没收、追缴",
          "label": "C"
        },
        {
          "content": "国家财产上缴国库",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "相关科研院校、医疗机构以及其他企业事业单位应当将生物安全法律法规和生物安全知识纳入教育培训内容，加强（ ）生物安全意识和伦理意识的培养。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "学生",
          "label": "A"
        },
        {
          "content": "从业人员",
          "label": "B"
        },
        {
          "content": "领导干部",
          "label": "C"
        },
        {
          "content": "单位负责人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列定义符合农产品质量安全法的是（ ）。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "农产品是指在农业活动中获得的植物、动物、微生物及其产品。",
          "label": "A"
        },
        {
          "content": "农产品是指所有与农业相关的初级产品。",
          "label": "B"
        },
        {
          "content": "农产品质量安全是指农产品质量达到农产品质量安全标准，符合保障人的健康、安全的要求。",
          "label": "C"
        },
        {
          "content": "农产品质量安全是指农产品质量对人体健康不能产生明显的或潜在的危害。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违反规定公款消费，情节较重的，予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，屠宰检疫合格后，可对动物的（    ）出具动物检疫证明，加盖验讫印章或者其他检疫标志。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "胴体及生皮、原毛、绒",
          "label": "A"
        },
        {
          "content": "脏器、血液",
          "label": "B"
        },
        {
          "content": "蹄、头、角",
          "label": "C"
        },
        {
          "content": "肉、脂、骨",
          "label": "D"
        },
        {
          "content": "分割肉、筋",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "县级以上人民政府农业农村主管部门和市场监督管理部门应当加强（ ）过程中农产品质量安全监督管理的协调配合和执法衔接，及时通报和共享农产品质量安全监督管理信息，并按照职责权限，发布有关农产品质量安全日常监督管理信息。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "生产",
          "label": "A"
        },
        {
          "content": "收购",
          "label": "B"
        },
        {
          "content": "储存",
          "label": "C"
        },
        {
          "content": "运输",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "张三是A省B县的协检人员，按照《动物检疫管理办法》的规定，他可以协助官方兽医 （    ）。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "核实信息",
          "label": "A"
        },
        {
          "content": "开展临床健康检查",
          "label": "B"
        },
        {
          "content": "开展同步检疫",
          "label": "C"
        },
        {
          "content": "出具动物检疫证明",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列（）是免疫主体，承担免疫责任。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "动物疫病预防控制机构",
          "label": "A"
        },
        {
          "content": "饲养动物的个人",
          "label": "B"
        },
        {
          "content": "兽医站",
          "label": "C"
        },
        {
          "content": "饲养动物的单位",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "运载工具在应当在（     ）及时清洗、消毒。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "装载前",
          "label": "A"
        },
        {
          "content": "装载后",
          "label": "B"
        },
        {
          "content": "卸载后",
          "label": "C"
        },
        {
          "content": "卸载前",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，发生一类动物疫病时，在封锁期间，禁止（    ）的动物、动物产品流出疫区。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "染疫",
          "label": "A"
        },
        {
          "content": "疑似染疫",
          "label": "B"
        },
        {
          "content": "易感染",
          "label": "C"
        },
        {
          "content": "所有",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，禁止持有、使用伪造或者变造的检疫证明、检疫标志或者畜禽标识。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "严格按照《家畜布鲁氏菌病防治技术规范》要求处置布鲁氏菌病疫情，对发病和监测阳性动物及其同群动物全部扑杀。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，运载工具在装载前和装载后应当及时清洗、消毒。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，对未经检疫的动物产品出具检疫证明、加施检疫标志行为的动物卫生监督机构及其工作人员，由本级人民政府或者农业农村主管部门责令改正；对直接负责的主管人员和其他直接责任人员依法给予处分。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽肉及生皮、原毛、绒、脏器、脂、血液、骨、蹄、头、角、筋等直接从屠宰场生产的畜禽产品出证。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "全面推进“先打后补”工作，在2022年年底前实现规模养殖场(户)全覆盖，在2025年年底前逐步全面停止政府招标采购强制免疫疫苗。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "经上级农业农村主管部门监督抽查的同批次农产品，下级农业农村主管部门可以重复抽查进行确认。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫申报单用于动物、动物产品的产地检疫、屠宰检疫和分销换证的申报。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要按照科学规划、合理布局、安全建设、管理方便的原则，尽快确定并公布指定通道。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，为控制动物疫病，必要时，经省、自治区、直辖市人民政府农业农村主管部门批准，可以设立临时性的动物防疫检查站，执行监督检查任务。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "到2026年，全国畜间布病总体流行率有效降低，牛羊群体健康水平明显提高，群体阳性率控制在0.4%以下。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "各县级农业农村部门要按照规定格式统一制作官方兽医证，加强证件发放使用管理。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，重大动物疫情由县级以上地方人民政府农业农村主管部门认定。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "输入到无规定动物疫病区的动物、动物产品，货主应当按照国务院农业农村主管部门的规定向无规定动物疫病区省级动物卫生监督机构申报检疫，经检疫合格的，方可进入。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "在每年3—5月、9—11月春秋两季集中免疫期间，对免疫进展实行月报告制度。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽经屠宰检疫合格后，动物卫生监督机构应当在畜禽产品检疫标志中注明畜禽标识编码。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫证明填写中，动物A到达时效除特殊情况外，一般最长不得超过（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "5天",
          "label": "A"
        },
        {
          "content": "7天",
          "label": "B"
        },
        {
          "content": "当日",
          "label": "C"
        },
        {
          "content": "6个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列哪种情形，行政机关应当依法办理有关行政许可的注销手续。（       ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "违反法定程序作出准予行政许可决定的",
          "label": "A"
        },
        {
          "content": "法人或者其他组织依法终止的",
          "label": "B"
        },
        {
          "content": "超越法定职权作出准予行政许可决定的",
          "label": "C"
        },
        {
          "content": "对不具备申请资格或者不符合法定条件的申请人准予行政许可的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，县级以上人民政府农业农村主管部门制定官方兽医培训计划，提供培训条件，定期对官方兽医进行（    ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "培训和考核",
          "label": "A"
        },
        {
          "content": "培训和考试",
          "label": "B"
        },
        {
          "content": "培训",
          "label": "C"
        },
        {
          "content": "考试",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》中，补检的动物补检合格条件不包括（    ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "畜禽标识符合规定",
          "label": "A"
        },
        {
          "content": "临床检查健康",
          "label": "B"
        },
        {
          "content": "货主于五日内提供检疫规程规定的实验室疫病检测报告，检测结果合格",
          "label": "C"
        },
        {
          "content": "检疫申报需要提供的材料齐全、符合要求",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对种畜以外的牛羊进行()免疫。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "小反刍兽疫",
          "label": "A"
        },
        {
          "content": "羊痘",
          "label": "B"
        },
        {
          "content": "布鲁氏菌病",
          "label": "C"
        },
        {
          "content": "口蹄疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "各省份按（）报告疫苗采购及免疫情况。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "月",
          "label": "A"
        },
        {
          "content": "年",
          "label": "B"
        },
        {
          "content": "季",
          "label": "C"
        },
        {
          "content": "周",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "已满十四周岁不满十八周岁的未成年人有违法行为的，应当（     ）行政处罚。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "不予",
          "label": "A"
        },
        {
          "content": "从轻",
          "label": "B"
        },
        {
          "content": "减轻",
          "label": "C"
        },
        {
          "content": "从轻或者减轻",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "变更单位名称或者法定代表人（负责人）的，应当在变更后（ ）持有效证明申请变更动物防疫条件合格证。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "七日内",
          "label": "A"
        },
        {
          "content": "十五日内",
          "label": "B"
        },
        {
          "content": "一个月内",
          "label": "C"
        },
        {
          "content": "三个月内",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "实施行政处罚，纠正违法行为，应当坚持（   ）相结合，教育公民法人或者其他组织自觉守法。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "教育和指导",
          "label": "A"
        },
        {
          "content": "治病与救人",
          "label": "B"
        },
        {
          "content": "处罚与教育",
          "label": "C"
        },
        {
          "content": "教育和纠正",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，在重大动物疫情报告期间，必要时，所在地（　　）可以作出封锁决定并采取扑杀、销毁等措施。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "县级以上人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "B"
        },
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "县级地方人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（      ）可以设定临时性行政许可。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "国务院部门规章",
          "label": "A"
        },
        {
          "content": "国务院决定",
          "label": "B"
        },
        {
          "content": "省、自治区、直辖市人民政府规章",
          "label": "C"
        },
        {
          "content": "较大的市的人民政府规章",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "以下不属于生物安全应急制度的是：（ ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "国务院有关部门组织制定相关领域、行业生物安全事件应急预案",
          "label": "A"
        },
        {
          "content": "地方人民政府加强应急准备、人员培训和应急演练",
          "label": "B"
        },
        {
          "content": "解放军、警察部队依法参加生物安全事件应急处置和应急救援工作",
          "label": "C"
        },
        {
          "content": "国家生物安全工作协调机制对重大新发突发传染病组织开展调查溯源",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，（   ）制定本行政区域官方兽医培训计划，提供必要的培训条件。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "县级以上动物疫病预防控制中心",
          "label": "C"
        },
        {
          "content": "省级动物疫病预防控制中心",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，从事动物运输的单位、个人以及车辆，应当向所在地县级人民政府农业农村主管部门（    ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "登记",
          "label": "A"
        },
        {
          "content": "备案 ",
          "label": "B"
        },
        {
          "content": "注册",
          "label": "C"
        },
        {
          "content": "申请",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "为深入贯彻（），落实《“十四五”全国畜牧兽医行业发展规划》要求，加强官方兽医队伍建设，强化官方兽医培训，提升官方兽医队伍依法履职能力和水平，制定《2022—2025年全国官方兽医培训计划》。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "《中华人民共和国动物防疫法》",
          "label": "A"
        },
        {
          "content": "《中华人民共和国畜牧法》",
          "label": "B"
        },
        {
          "content": "《中华人民共和国生物安全法》",
          "label": "C"
        },
        {
          "content": "《中华人民共和国农产品质量安全法》",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "陆生野生动物检疫办法，由（     ）另行制定。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "农业农村部会同国家林业和草原局",
          "label": "A"
        },
        {
          "content": "国家林业和草原局会同农业农村部",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "国家林业和草原局",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "农业农村主管部门制定国家农产品质量安全风险监测计划，对重点区域、重点农产品品种进行质量安全风险监测属于农产品质量安全（ ）制度。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "风险监测",
          "label": "A"
        },
        {
          "content": "风险评估",
          "label": "B"
        },
        {
          "content": "产地监测",
          "label": "C"
        },
        {
          "content": "风险管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "（）要做好免疫记录、按时报告，确保免疫记录与畜禽标识相符。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "官方兽医",
          "label": "A"
        },
        {
          "content": "村级防疫员",
          "label": "B"
        },
        {
          "content": "乡镇动物防疫机构",
          "label": "C"
        },
        {
          "content": "动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，已经实施畜禽产品（   ）并实现（   ）的地区，对取得动物检疫证明的畜禽产品，继续在本屠宰企业内分割加工的，不再重复出证。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "双轨并行出证",
          "label": "A"
        },
        {
          "content": "无纸化出证",
          "label": "B"
        },
        {
          "content": "电子证照互通互认",
          "label": "C"
        },
        {
          "content": "检疫检验信息互通互认",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，任何单位和个人不得藏匿、转移、盗掘已被（　　）的动物和动物产品。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "隔离",
          "label": "A"
        },
        {
          "content": "封存",
          "label": "B"
        },
        {
          "content": "处理",
          "label": "C"
        },
        {
          "content": "消毒",
          "label": "D"
        },
        {
          "content": "封锁",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "张三的饲养场要出售一批肉鸡，需要符合下列（     ）条件。",
      "standard_answer": "B,C,D,E",
      "option_list": [
        {
          "content": "饲养场在非封锁区或发生相关动物疫情",
          "label": "A"
        },
        {
          "content": "饲养场符合风险分级管理有关规定",
          "label": "B"
        },
        {
          "content": "申报材料符合检疫规程规定",
          "label": "C"
        },
        {
          "content": "按照规定进行了强制免疫，并在有效保护期内",
          "label": "D"
        },
        {
          "content": "临床检查健康",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对种畜以外的（）进行布鲁氏菌病免疫，种畜禁止免疫。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "鹿",
          "label": "A"
        },
        {
          "content": "骆驼",
          "label": "B"
        },
        {
          "content": "牛",
          "label": "C"
        },
        {
          "content": "羊",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员在选拔任用、录用、聘用、考核、晋升、评选等干部人事工作中违反有关规定的，予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，官方兽医应当检查待宰动物健康状况，在屠宰过程中开展（       ），并填写屠宰检疫记录。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "三腺摘除",
          "label": "A"
        },
        {
          "content": "同步检验",
          "label": "B"
        },
        {
          "content": "必要的实验室疫病检测",
          "label": "C"
        },
        {
          "content": "同步检疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对于从事病原微生物实验活动未在相应等级的实验室进行，或者高等级病原微生物实验室未经批准从事高致病性、疑似高致病性病原微生物实验活动的行为，应进行（ ）处罚。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "责令停止违法行为",
          "label": "A"
        },
        {
          "content": "监督其将用于实验活动的病原微生物销毁或者送交保藏机构",
          "label": "B"
        },
        {
          "content": "给予警告",
          "label": "C"
        },
        {
          "content": "处以相应罚款",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《三类动物疫病防治规范》要求从事动物饲养、屠宰、经营、隔离、运输等活动的单位和个人应当加强管理，确保水生动物养殖场所（     ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "具有合格水源",
          "label": "A"
        },
        {
          "content": "具有独立进排水系统",
          "label": "B"
        },
        {
          "content": "保持适宜的养殖水环境",
          "label": "C"
        },
        {
          "content": "溶氧量保持在合理区间",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "按照农产品质量安全法，农产品质量安全检测机构、检测人员出具虚假检测报告的，应（ ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "没收该机构所收取的检测费用并处以相应罚款",
          "label": "A"
        },
        {
          "content": "相关负责人处以相应罚款",
          "label": "B"
        },
        {
          "content": "吊销该检测机构的资质证书",
          "label": "C"
        },
        {
          "content": "使消费者的合法权益受到损害的，应当承担连带责任",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关发现公民、法人或者其他组织有依法应当给予行政处罚的行为的，必须（    ）地调查。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "全面",
          "label": "A"
        },
        {
          "content": "客观",
          "label": "B"
        },
        {
          "content": "公正",
          "label": "C"
        },
        {
          "content": "公开",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "统筹抓好()等其他易感动物的布病防控，切实降低流行率，有效防范传播风险。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "肉牛 ",
          "label": "A"
        },
        {
          "content": "猪",
          "label": "B"
        },
        {
          "content": "鹿",
          "label": "C"
        },
        {
          "content": "骆驼",
          "label": "D"
        },
        {
          "content": "犬",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "违反《动物防疫法》规定，将禁止或者限制调运的特定动物、动物产品由动物疫病高风险区调入低风险区的，由县级以上地方人民政府农业农村主管部门没收运输费用、违法运输的动物和动物产品，并处运输费用一倍以上五倍以下罚款。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽标识生产企业不得向省级动物疫病预防控制机构以外的单位和个人提供畜禽标识。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各地要强化指定通道的条件保障，配齐配强工作力量，安排足够的监督执法、动物防疫及辅助人员。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，进出境动物、动物产品的检疫，适用《中华人民共和国进出境动植物检疫法》。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "布鲁氏菌病免疫区以实施持续免疫为主，非免疫区以实施净化为主。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "目前我国布病防控形势不严峻。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，（）根据数据采集要求，组织畜禽养殖相关信息的录入、上传和更新工作。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级畜牧兽医行政主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "县级以上地方政府畜牧兽医行政主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违反规定出境或者办理因私出境证件情节严重的，予以（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员受到降级以上政务处分的，应当由人事部门按照管理权限在作出政务处分决定后（）内办理职务、工资及其他有关待遇等的变更手续。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "十五日",
          "label": "A"
        },
        {
          "content": "一个月",
          "label": "B"
        },
        {
          "content": "三个月",
          "label": "C"
        },
        {
          "content": "六个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，担任领导职务的公职人员有违法行为，被罢免、撤销、免去或者辞去领导职务的，（）可以同时给予政务处分。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "检查机关",
          "label": "A"
        },
        {
          "content": "机关党委",
          "label": "B"
        },
        {
          "content": "监察机关",
          "label": "C"
        },
        {
          "content": "机管纪委",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员是非法组织、非法活动的策划者、组织者和骨干分子，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（     ）应当结合本地实际，做好农村地区饲养犬只的防疫管理工作。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "县级人民政府",
          "label": "A"
        },
        {
          "content": "县级人民政府和乡级人民政府",
          "label": "B"
        },
        {
          "content": "乡级人民政府、街道办事处",
          "label": "C"
        },
        {
          "content": "县级人民政府和乡级人民政府、街道办事处",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，运载工具在（    ）应当及时清洗、消毒 。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "装载前",
          "label": "A"
        },
        {
          "content": "卸载后",
          "label": "B"
        },
        {
          "content": "装载前和装载后",
          "label": "C"
        },
        {
          "content": "装载前和卸载后",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《三类动物疫病防治规范》，从事动物饲养、屠宰、经营、隔离、运输等活动的单位和个人应当建立并执行（     ），科学规范开展消毒工作。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物防疫消毒制度",
          "label": "A"
        },
        {
          "content": "消毒设施设备检修制度",
          "label": "B"
        },
        {
          "content": "人员防护制度",
          "label": "C"
        },
        {
          "content": "生物安全制度",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，申报检疫的，应当提交动物检疫申报单以及农业农村部规定的其他材料，并对申报材料的（    ）负责。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "真实性",
          "label": "A"
        },
        {
          "content": "可靠性",
          "label": "B"
        },
        {
          "content": "规范性",
          "label": "C"
        },
        {
          "content": "合法性",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列哪种情形，可以从轻或者减轻行政处罚。\r\n（     ）",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "精神病人、智力残疾人在不能辨认或者不能控制自己行为时有违法行为的",
          "label": "A"
        },
        {
          "content": "间歇性精神病人在精神正常时有违法行为的",
          "label": "B"
        },
        {
          "content": "尚未完全丧失辨认或者控制自己行为能力的精神病人、智力残疾人有违法行为的",
          "label": "C"
        },
        {
          "content": "以上都不是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，猪的畜禽种类代码是（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "1",
          "label": "A"
        },
        {
          "content": "2",
          "label": "B"
        },
        {
          "content": "3",
          "label": "C"
        },
        {
          "content": "4",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "无规定动物疫病区和无规定动物疫病生物安全隔离区由（    ）验收合格予以公布，并对其维持情况进行监督检查。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "国务院农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省级动物疫病预防控制机构",
          "label": "B"
        },
        {
          "content": "省、自治区、直辖市人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "省、自治区、直辖市人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "违法事实确凿并有法定依据，对公民处以（   ）罚款或者警告的行政处罚的，对法人或者其他组织处以（      ）罚款或者警告的行政处罚的，可以当场作出行政处罚决定。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "二百元以下、三千元以下",
          "label": "A"
        },
        {
          "content": "二百元以下、一千元以下",
          "label": "B"
        },
        {
          "content": "三百元以下、三千元以下",
          "label": "C"
        },
        {
          "content": "二百元以下、一千元以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对新任命的官方兽医进行动物检疫监督、畜禽屠宰管理、动物产品质量安全、官方兽医职业道德规范和行风建设、动物卫生法学理论等方面培训，经（）合格的方可上岗。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "考核",
          "label": "A"
        },
        {
          "content": "考试",
          "label": "B"
        },
        {
          "content": "培训",
          "label": "C"
        },
        {
          "content": "考察",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》明确了重点防治病种的防治目标是（     ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "得到有效控制",
          "label": "A"
        },
        {
          "content": "流行率稳定控制在较低水平",
          "label": "B"
        },
        {
          "content": "传入和扩散风险有效降低",
          "label": "C"
        },
        {
          "content": "实现全部消灭",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，县级以上动物卫生监督机构负责本行政区域内动物检疫证章标志的管理工作，建立动物检疫证章标志管理制度，严格按照程序（    ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "购买",
          "label": "A"
        },
        {
          "content": "订购",
          "label": "B"
        },
        {
          "content": "保管",
          "label": "C"
        },
        {
          "content": "发放",
          "label": "D"
        },
        {
          "content": "回收",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "农业农村部办公厅关于进一步加强官方兽医管理工作的通知（农办牧〔2023〕10号）要求各地要强化（）的官方兽医任命管理工作机制,全面梳理审核辖区内官方兽医情况。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "动态管理",
          "label": "A"
        },
        {
          "content": "严格审核",
          "label": "B"
        },
        {
          "content": "省级确认",
          "label": "C"
        },
        {
          "content": "所在地任命和注销",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，要重点在检疫申报点等场所开展宣传，指导生产经营主体(      )。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "利用信息化手段查询指定通道基础信息",
          "label": "A"
        },
        {
          "content": "了解指定通道基础信息",
          "label": "B"
        },
        {
          "content": "规范开展指定通道运输管理",
          "label": "C"
        },
        {
          "content": "规范开展动物运输活动",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公务员以及参照《中华人民共和国公务员法》管理的人员在政务处分期内，不得晋升（  ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "职务",
          "label": "A"
        },
        {
          "content": "职级",
          "label": "B"
        },
        {
          "content": "衔级",
          "label": "C"
        },
        {
          "content": "级别",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公务员以及参照《中华人民共和国公务员法》管理的人员被撤职的，按照规定降低职务、职级、衔级和级别，同时降低（）。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "信用等级",
          "label": "A"
        },
        {
          "content": "工资",
          "label": "B"
        },
        {
          "content": "医疗报销比例",
          "label": "C"
        },
        {
          "content": "待遇",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》中列举的二类动物疫病包括（     ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "狂犬病",
          "label": "A"
        },
        {
          "content": "布鲁氏菌病",
          "label": "B"
        },
        {
          "content": "草鱼出血病",
          "label": "C"
        },
        {
          "content": "牛海绵状脑病",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，国有企业管理人员在政务处分期内，受到（）处分的，不得晋升薪酬待遇等级。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "记过",
          "label": "A"
        },
        {
          "content": "记大过",
          "label": "B"
        },
        {
          "content": "降级",
          "label": "C"
        },
        {
          "content": "撤职",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜间布鲁氏菌病防控五年行动方案》指导思想是：贯彻习近平总书记关于加强国家生物安全风险防控和治理体系建设指示精神，坚持（），实行（），有效控制传染源、阻断传播途径、提高抗病能力，切实做好布病源头防控工作，维护畜牧业生产安全、公共卫生安全和生物安全。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "人民至上",
          "label": "A"
        },
        {
          "content": "生命至上",
          "label": "B"
        },
        {
          "content": "积极防御",
          "label": "C"
        },
        {
          "content": "系统治理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "设定行政许可，应当维护（      ），促进经济、社会和生态环境协调发展。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "经济利益",
          "label": "A"
        },
        {
          "content": "公共利益",
          "label": "B"
        },
        {
          "content": "生态环境",
          "label": "C"
        },
        {
          "content": "社会秩序",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对阳性场群，有针对性地持续开展全群跟踪监测，确保覆盖区域内所有()的场群。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "饲养种畜",
          "label": "A"
        },
        {
          "content": "存在阳性个体",
          "label": "B"
        },
        {
          "content": "造成人感染情况",
          "label": "C"
        },
        {
          "content": "饲养奶畜",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，用于（    ）等非食用性利用的动物，应当附有检疫证明。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "展示",
          "label": "A"
        },
        {
          "content": "科研",
          "label": "B"
        },
        {
          "content": "比赛",
          "label": "C"
        },
        {
          "content": "演出",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，跨省、自治区、直辖市引进的种用、乳用动物到达输入地后，货主应当按照国务院农业农村主管部门的规定对引进的种用、乳用动物进行隔离观察。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "国家鼓励畜禽就地屠宰，引导畜禽屠宰企业向养殖主产区转移，支持畜禽产品加工、储存、运输冷链体系建设。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村部具体承担培训教材课件制作、考试题库完善、培训考试平台运维等工作。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫处理通知单用于产地检疫、屠宰检疫发现不合格动物和动物产品的处理。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "动物饲养场和隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所，生产经营条件发生变化，不再符合《动物防疫法》规定的动物防疫条件继续从事相关活动的，由县级以上地方人民政府农业农村主管部门给予警告，责令限期改正。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要完善动物检疫证明电子出证系统，及时采集、更新指定通道及有关检查站名称、位置坐标等基础信息，建立指定通道监管信息实时采集、上传功能，并将数据共享至兽医卫生综合信息平台。    （    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，发现违法违规行为的，要依法加重处理。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，患有人畜共患传染病的人员不得从事动物疫病监测、检测、检验检疫、诊疗以及易感动物的饲养、屠宰、经营、隔离、运输等活动。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《中华人民共和国畜牧法》所称种畜禽，是指经过选育、具有种用价值、适于繁殖后代的畜禽及其卵子（蛋）、精液、胚胎等。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，经指定通道运输动物的监督检查中，对于无违法违规情况且动物临床健康的，出具动物检疫证明。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "奶畜确需免疫布鲁氏菌病的，养殖场(户)应逐级报（）农业农村部门同意后实施。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "省级",
          "label": "A"
        },
        {
          "content": "县级",
          "label": "B"
        },
        {
          "content": "国务院",
          "label": "C"
        },
        {
          "content": "市级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，国家实行动物疫情(    )制度。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "公布",
          "label": "A"
        },
        {
          "content": "报告",
          "label": "B"
        },
        {
          "content": "通告",
          "label": "C"
        },
        {
          "content": "通报",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "病死畜禽和病害畜禽产品无害化处理场所销售无害化处理产物的，应当严控无害化处理产物流向，查验（     ）并留存相关材料，签订销售合同。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "购买方生产计划",
          "label": "A"
        },
        {
          "content": "购买方设施设备目录",
          "label": "B"
        },
        {
          "content": "购买方资质",
          "label": "C"
        },
        {
          "content": "购买方技术方案",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "进入屠宰加工场所的待宰动物应当附有动物检疫证明并加施有（     ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "畜禽标识",
          "label": "A"
        },
        {
          "content": "符合规定的畜禽标识",
          "label": "B"
        },
        {
          "content": "牲畜耳标",
          "label": "C"
        },
        {
          "content": "符合规定的耳标",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "县级以上地方人民政府（ ）主管部门应当加强对农产品质量安全执法人员的专业技术培训并组织考核。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "市场监管",
          "label": "A"
        },
        {
          "content": "农业农村",
          "label": "B"
        },
        {
          "content": "卫生健康",
          "label": "C"
        },
        {
          "content": "科学技术",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违反规定向管理服务对象收取、摊派财物，情节特别严重的，予以（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "开除",
          "label": "A"
        },
        {
          "content": "撤职",
          "label": "B"
        },
        {
          "content": "留党察看",
          "label": "C"
        },
        {
          "content": "党内警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽在屠宰时，（）应当回收畜禽标识。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "屠宰企业",
          "label": "B"
        },
        {
          "content": "农业农村主管部门",
          "label": "C"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "规模养殖场(户)及有条件的地方实施（）免疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "程序化",
          "label": "A"
        },
        {
          "content": "集中 ",
          "label": "B"
        },
        {
          "content": "定期",
          "label": "C"
        },
        {
          "content": "自主",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违法取得的财物和用于违法行为的本人财物，除依法应当由其他机关没收、追缴或者责令退赔的，由（）没收、追缴或者责令退赔。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "检查机关",
          "label": "A"
        },
        {
          "content": "监察机关",
          "label": "B"
        },
        {
          "content": "机关党委",
          "label": "C"
        },
        {
          "content": "机管纪委",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，我国动物疫病分为（    ）类。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "一",
          "label": "A"
        },
        {
          "content": "二",
          "label": "B"
        },
        {
          "content": "三",
          "label": "C"
        },
        {
          "content": "四",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "公开的行政处罚决定被依法变更、撤销、确认违法或者确认无效的，行政机关应当在（       ）内撤回行政处罚决定信息并公开说明理由。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "三日",
          "label": "A"
        },
        {
          "content": "五日",
          "label": "B"
        },
        {
          "content": "七日",
          "label": "C"
        },
        {
          "content": "十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（   ）应当提供与检疫工作相适应的官方兽医驻场检疫室、工作室和检疫操作台等设施。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "屠宰加工场所",
          "label": "A"
        },
        {
          "content": "县级动物卫生监督机构",
          "label": "B"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "县级以上地方人民政府设立的动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "违反《动物防疫法》第二十九条规定，屠宰、经营、运输动物或者生产、经营、加工、贮藏、运输动物产品的，违法行为人及其法定代表人（负责人）、直接负责的主管人员和其他直接责任人员，自处罚决定作出之日起(    )年内不得从事相关活动。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "三",
          "label": "A"
        },
        {
          "content": "五",
          "label": "B"
        },
        {
          "content": "二",
          "label": "C"
        },
        {
          "content": "十",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所变更单位名称或者法定代表人（负责人）未办理变更手续的，由县级以上地方人民政府农业农村主管部门（ ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "给予警告",
          "label": "A"
        },
        {
          "content": "处以罚款",
          "label": "B"
        },
        {
          "content": "责令停业整顿",
          "label": "C"
        },
        {
          "content": "责令限期改正",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政处罚决定依法作出后，当事人确有经济困难，需要延期或者分期缴纳罚款的，经当事人申请和（    ）批准，可以暂缓或者分期缴纳。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "执法人员",
          "label": "A"
        },
        {
          "content": "人民法院",
          "label": "B"
        },
        {
          "content": "司法机关",
          "label": "C"
        },
        {
          "content": "行政机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，地方各级人民政府、有关部门及其工作人员（    ），由上级人民政府或者有关部门责令改正，通报批评；对直接负责的主管人员和其他直接责任人员依法给予处分。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "瞒报、谎报动物疫情的",
          "label": "A"
        },
        {
          "content": "授意他人瞒报、谎报、迟报动物疫情的",
          "label": "B"
        },
        {
          "content": "阻碍他人报告动物疫情的",
          "label": "C"
        },
        {
          "content": "迟报、漏报动物疫情的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，依法应当检疫而未经检疫的胴体、肉（     ）等动物产品，不予补检，予以收缴销毁。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "脏器、脂、血液",
          "label": "A"
        },
        {
          "content": "精液、卵、胚胎",
          "label": "B"
        },
        {
          "content": "蹄、头",
          "label": "C"
        },
        {
          "content": "种蛋",
          "label": "D"
        },
        {
          "content": "骨、筋",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要完善动物检疫证明电子出证系统，实现指定通道基础信息便捷查询和监管信息（   ）全程共享，为加强动物调运监管提供支撑。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "动物健康状况",
          "label": "A"
        },
        {
          "content": "启运地",
          "label": "B"
        },
        {
          "content": "途径地",
          "label": "C"
        },
        {
          "content": "目的地",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《全国畜间人兽共患病防治规划2022—2030年）》提出的高致病性禽流感的重点防治措施的是（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "做好强制免疫",
          "label": "A"
        },
        {
          "content": "加强监测预警",
          "label": "B"
        },
        {
          "content": "严格检疫监管和市场准入",
          "label": "C"
        },
        {
          "content": "推进区域化管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《动物防疫法》所称的动物产品有（    ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "肉、脏器、脂、血液",
          "label": "A"
        },
        {
          "content": "生皮、原毛、绒",
          "label": "B"
        },
        {
          "content": "精液、卵、胚胎",
          "label": "C"
        },
        {
          "content": "骨、蹄、头、角、筋",
          "label": "D"
        },
        {
          "content": "可能传播动物疫病的奶、蛋",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "屠宰加工场所应当严格执行(    )等制度，查验进场待宰动物的动物检疫证明和畜禽标识。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "入场监督查验",
          "label": "A"
        },
        {
          "content": "动物入场查验登记",
          "label": "B"
        },
        {
          "content": "待宰巡查",
          "label": "C"
        },
        {
          "content": "入场动物车辆登记",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各省要按照（     ）的原则，结合生猪养殖、动物疫病防控和生猪产品消费实际情况制订生猪屠宰行业发展规划。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "科学布局",
          "label": "A"
        },
        {
          "content": "集中屠宰",
          "label": "B"
        },
        {
          "content": "有利流通",
          "label": "C"
        },
        {
          "content": "方便群众",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政许可依法由地方人民政府两个以上部门分别实施的，本级人民政府可以确定一个部门受理行政许可申请并转告有关部门分别提出意见后统一办理，或者组织有关部门（        ）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "分别办理",
          "label": "A"
        },
        {
          "content": "联合办理",
          "label": "B"
        },
        {
          "content": "集中办理",
          "label": "C"
        },
        {
          "content": "统一送达",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违反个人有关事项报告规定，隐瞒不报，情节较重的，予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各地要强化指定通道条件保障，配齐配强工作力量，可以安排（   ）等参与相关工作，满足值守需求。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "执业兽医",
          "label": "A"
        },
        {
          "content": "官方兽医",
          "label": "B"
        },
        {
          "content": "特聘动物防疫专家",
          "label": "C"
        },
        {
          "content": "特聘动物防疫专员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各省份按月报告疫苗采购及免疫情况。在每年()月集中免疫期间，对免疫进展实行周报告制度。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "3—5月",
          "label": "A"
        },
        {
          "content": "7-8月",
          "label": "B"
        },
        {
          "content": "9—11月",
          "label": "C"
        },
        {
          "content": "2-4月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "每名师资每年应承担不少于5学时的官方兽医培训任务。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "病死畜禽无害化处理补助要按照“谁养殖、补给谁”的原则，建立与养殖量、无害化处理率相挂钩的财政补助机制。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村部规定，牛羊肉检疫验讫标志标签左边为中国动物卫生监督标志图徽，右边为二维码，其下为各省简称后加8位数字的流水号码。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "乡镇兽医站负责开展养殖环节强制免疫效果评价。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》明确重点防治病种的数量是8种。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《国家畜禽遗传资源品种名录》规定，水貂作为传统畜禽，已成为我国重要的皮毛动物。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，要规范设置指定通道引导标志，在显要位置公示设立依据、工作职责、工作程序、动物运输管理要求、监督检查人员和监督电话等信息。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "地方各级人民政府对本地区病死畜禽无害化处理负第一责任人的责任。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "坚持一地多策，根据各地布病流行形势，以县为基本单位连片推进布病防控，免疫区以实施持续免疫为主，非免疫区以实施持续监测剔除为主，有效落实各项基础性、综合性防控措施。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，参与公职人员违法案件调查、处理的人员需要回避的，除监察机关负责人外的其他参与违法案件调查、处理人员的回避，由（）决定。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "上级监察机关",
          "label": "A"
        },
        {
          "content": "同级人民政府",
          "label": "B"
        },
        {
          "content": "监察机关负责人",
          "label": "C"
        },
        {
          "content": "被调查单位负责人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，（）应当建立畜禽防疫档案",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "省级人民政府畜牧兽医行政主管部门",
          "label": "A"
        },
        {
          "content": "省级动物疫病预防控制机构",
          "label": "B"
        },
        {
          "content": "县级以上地方人民政府畜牧兽医主管部门",
          "label": "C"
        },
        {
          "content": "县级动物疫病预防控制机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "各省份可采用养殖场(户)自行免疫、第三方服务主体免疫、政府购买服务等多种形式，全面推进（）工作。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "集中免疫",
          "label": "A"
        },
        {
          "content": "自主免疫",
          "label": "B"
        },
        {
          "content": "先打后补",
          "label": "C"
        },
        {
          "content": "先补后打",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列选项不属于行政处罚措施的是",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "责任约谈",
          "label": "B"
        },
        {
          "content": "通报批评",
          "label": "C"
        },
        {
          "content": "降低资质等级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "被许可人在作出行政许可决定的行政机关管辖区域外违法从事行政许可事项活动的，违法行为发生地的行政机关应当依法将被许可人的违法事实、处理结果抄告（      ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "当地司法机关",
          "label": "A"
        },
        {
          "content": "当地人民政府",
          "label": "B"
        },
        {
          "content": "上级行政机关",
          "label": "C"
        },
        {
          "content": "作出行政许可决定的行政机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》提出要完善监测预警体系，健全以国家兽医实验室和（     ）为主体的畜间人兽共患病监测预警网络。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "省级动物疫病预防控制机构",
          "label": "A"
        },
        {
          "content": "省市县三级动物疫病预防控制机构",
          "label": "B"
        },
        {
          "content": "市级以上动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "第三方兽医检测机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，畜禽标识所需经费列入（）财政预算。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级人民政府",
          "label": "B"
        },
        {
          "content": "市级人民政府",
          "label": "C"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关对被许可人实施监督检查时，被许可人进行正常的生产经营活动，行政机关应当（      ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "可以责令被许可人暂停生产经营活动",
          "label": "A"
        },
        {
          "content": "不得妨碍被许可人正常的生产经营活动",
          "label": "B"
        },
        {
          "content": "可以允许被许可人在缴纳保证金后不停止生产经营活动",
          "label": "C"
        },
        {
          "content": "检查后准许被许可人恢复生产经营活动",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "承运人张三从A省B县F养殖场拉一车肉鸡到B省D县E屠宰场，到达目的地后，（   ）应当向D县动物卫生监督机构报告。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "张三",
          "label": "A"
        },
        {
          "content": "E屠宰场",
          "label": "B"
        },
        {
          "content": "张三或E屠宰场",
          "label": "C"
        },
        {
          "content": "F养殖场",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "四川、西藏、青海等省份可使用（）倍剂量的羊棘球蚴病基因工程亚单位疫苗开展牦牛包虫病免疫，免疫范围由各省份自行确定。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "4",
          "label": "A"
        },
        {
          "content": "3",
          "label": "B"
        },
        {
          "content": "5",
          "label": "C"
        },
        {
          "content": "2",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，输入到无规定动物疫病区的动物、动物产品，货主应当按照国务院农业农村主管部门的规定向无规定动物疫病区（    ）申报检疫，经检疫合格的，方可进入。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "所在地动物卫生监督机构",
          "label": "B"
        },
        {
          "content": "农业农村主管部门",
          "label": "C"
        },
        {
          "content": "所在地农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国畜牧法》规定，（       ）应当根据农产品批发市场发展规划，对在畜禽集散地建立畜禽批发市场给予扶持。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "B"
        },
        {
          "content": "市级以上地方人民政府",
          "label": "C"
        },
        {
          "content": "省级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定,动物检疫遵循过程监管、风险控制、区域化和（   ）管理相结合的原则。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "去向可查",
          "label": "A"
        },
        {
          "content": "可追溯",
          "label": "B"
        },
        {
          "content": "可追踪",
          "label": "C"
        },
        {
          "content": "可识别",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，发放动物防疫条件合格证的行政主体是县级以上地方人民政府（　　）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "农业农村主管部门",
          "label": "B"
        },
        {
          "content": "动物疫病预防控制中心",
          "label": "C"
        },
        {
          "content": "行政审批机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关依法对被许可人从事行政许可事项的活动进行监督检查时，公众对行政机关的监督检查记录\r\n（   ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "有权查阅",
          "label": "A"
        },
        {
          "content": "无权查阅",
          "label": "B"
        },
        {
          "content": "经过行政机关负责人同意后可以查阅",
          "label": "C"
        },
        {
          "content": "经过监督检查人员同意后可以查阅",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，给与记大过处分的，政务处分期为（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "六个月",
          "label": "A"
        },
        {
          "content": "十二个月",
          "label": "B"
        },
        {
          "content": "十八个月",
          "label": "C"
        },
        {
          "content": "二十四个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物检疫管理办法》规定，运输用于继续饲养或屠宰的畜禽到达目的地后，（    ）应当在三日内向启运地县级动物卫生监督机构报告。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "申报人",
          "label": "A"
        },
        {
          "content": "货主",
          "label": "B"
        },
        {
          "content": "贩运人",
          "label": "C"
        },
        {
          "content": "承运人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》强调，畜间人兽共患病防控要坚持的基本原则是（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "源头防治，突出重点",
          "label": "A"
        },
        {
          "content": "政府主导，多方参与",
          "label": "B"
        },
        {
          "content": "因地制宜，因病施策",
          "label": "C"
        },
        {
          "content": "协调配合，统筹推进",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列哪些事项可以设定行政许可？（      ）",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "直接关系公共安全、人身健康、生命财产安全的重要设备、设施、产品、物品，需要按照技术标准、技术规范，通过检验、检测、检疫等方式进行审定的事项",
          "label": "A"
        },
        {
          "content": "企业或者其他组织的设立等，需要确定主体资格的事项",
          "label": "B"
        },
        {
          "content": "有限自然资源开发利用、公共资源配置以及直接关系公共利益的特定行业的市场准入等，需要赋予特定权利的事项",
          "label": "C"
        },
        {
          "content": "直接涉及国家安全、公共安全、经济宏观调控、生态环境保护以及直接关系人身健康、生命财产安全等特定活动",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，以下（       ）由国务院农业农村主管部门验收合格予以公布，并对其维持情况进行监督检查。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "无规定动物疫病区",
          "label": "A"
        },
        {
          "content": "动物疫病净化场",
          "label": "B"
        },
        {
          "content": "无规定动物疫病生物安全隔离区",
          "label": "C"
        },
        {
          "content": "动物疫病分区",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "有序推进牛羊集中或定点屠宰，强化检疫检验，建立牛羊及其产品进出台账，记录（），确保可追溯。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "无害化 ",
          "label": "A"
        },
        {
          "content": "来源",
          "label": "B"
        },
        {
          "content": "流向",
          "label": "C"
        },
        {
          "content": "抽检",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《三类动物疫病防治规范》，应使用营养全面、品质良好的饲料。畜禽养殖应使用清洁饮水，鼓励采取（     ）的饲养方式。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "立体笼养",
          "label": "A"
        },
        {
          "content": "全进全出",
          "label": "B"
        },
        {
          "content": "自繁自养",
          "label": "C"
        },
        {
          "content": "动静结合",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关根据法律、行政法规的规定，对（   ）进行定期检验。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "直接关系公共安全的重要设备、设施",
          "label": "A"
        },
        {
          "content": "直接关系人身健康的重要设备、设施",
          "label": "B"
        },
        {
          "content": "直接关系生命财产安全的重要设备、设施",
          "label": "C"
        },
        {
          "content": "价值昂贵稀有的重要设备、设施",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《全国畜间人兽共患病防治规划2022—2030年）》提出的包虫病的重点防治措施的是（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "做好家犬驱虫",
          "label": "A"
        },
        {
          "content": "加强流浪犬管控",
          "label": "B"
        },
        {
          "content": "加强综合防疫管理",
          "label": "C"
        },
        {
          "content": "加强流行区宣传教育",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》中列举的一类疫病有（    ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "狂犬病",
          "label": "A"
        },
        {
          "content": "非洲猪瘟",
          "label": "B"
        },
        {
          "content": "口蹄疫",
          "label": "C"
        },
        {
          "content": "高致病性禽流感",
          "label": "D"
        },
        {
          "content": "布鲁氏菌病",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "官方兽医师资培训分别由（）和（）组织实施。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "农业农村部",
          "label": "A"
        },
        {
          "content": "中国动物疫病预防控制中心",
          "label": "B"
        },
        {
          "content": "省级农业农村部门",
          "label": "C"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列选项属于行政处罚措施的是（      ）",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "通报批评",
          "label": "A"
        },
        {
          "content": "没收非法财物",
          "label": "B"
        },
        {
          "content": "冻结存款、汇款",
          "label": "C"
        },
        {
          "content": "责令改正",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "官方兽医依法履行（     ）职责，任何单位和个人不得拒绝或者阻碍。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "动物检疫",
          "label": "A"
        },
        {
          "content": "无害化处理监督",
          "label": "B"
        },
        {
          "content": "动物产品检疫",
          "label": "C"
        },
        {
          "content": "行政处罚",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "官方兽医在岗培训由（）两级农业农村部门负责组织实施。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "国家",
          "label": "A"
        },
        {
          "content": "省",
          "label": "B"
        },
        {
          "content": "市",
          "label": "C"
        },
        {
          "content": "县 ",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "动物检疫证明的动物检疫证章标志的（    ）等要求，由农业农村部统一规定。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "内容",
          "label": "A"
        },
        {
          "content": "格式",
          "label": "B"
        },
        {
          "content": "规格",
          "label": "C"
        },
        {
          "content": "编码",
          "label": "D"
        },
        {
          "content": "制作",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《病死畜禽和病害畜禽产品无害化处理管理办法》，下列畜禽和畜禽产品属于应当进行无害化处理的是（     ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "染疫或者疑似染疫死亡、因病死亡或者死因不明的",
          "label": "A"
        },
        {
          "content": "经检疫、检验可能危害人体或者动物健康的",
          "label": "B"
        },
        {
          "content": "因自然灾害、应激反应、物理挤压等因素死亡的",
          "label": "C"
        },
        {
          "content": "死胎、木乃伊胎等",
          "label": "D"
        },
        {
          "content": "因动物疫病防控需要被扑杀或销毁的",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "开展生猪养殖保险要建立生猪养殖保险转让的便捷办理通道，避免出现销售仔猪、二次育肥猪等重复投保现象。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对到达目的地后分销的畜禽产品，可以结合本地实际，确定是否出证。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "对全国所有猪进行O型口蹄疫免疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "擅自发布动物疫情的，由县级以上地方人民政府农业农村主管部门责令改正，处一千元以上一万元以下罚款。（    ） ",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "对开展强制免疫“先打后补”的养殖场(户)，要组织开展抽查，确保免疫效果。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要完善动物检疫证明电子出证系统，定期采集、更新指定通道及有关检查站名称、位置坐标等基础信息，建立指定通道监管信息实时采集、上传功能，并将数据定期上传至动物卫生综合信息平台。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国畜牧法》规定，（       ）应当指导畜牧业生产经营者改善畜禽繁育、饲养、运输、屠宰的条件和环境。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "国务院农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "市级农业农村主管部门",
          "label": "C"
        },
        {
          "content": "县级以上农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "农业农村主管部门应当依照本条例的规定严格履行职责，加强对生猪屠宰活动的（       ），建立健全随机抽查机制。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "突击检查",
          "label": "A"
        },
        {
          "content": "飞行检查",
          "label": "B"
        },
        {
          "content": "日常管理",
          "label": "C"
        },
        {
          "content": "日常监督检查",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "某市畜产品贸易公司输入到无规定动物疫病区的相关易感动物，应当在（　　）进行隔离检疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "隔离场所",
          "label": "A"
        },
        {
          "content": "在输入地省级动物卫生监督机构指定的隔离场所",
          "label": "B"
        },
        {
          "content": "输入目的地的隔离场所",
          "label": "C"
        },
        {
          "content": "在输入地动物卫生监督机构指定的隔离场所",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，屠宰、出售或者运输动物以及出售或者运输 动物产品前，（    ）应当按照国务院农业农村主管部门的规定向所在地动物卫生监督机构申报检疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "货主",
          "label": "A"
        },
        {
          "content": "动物屠宰场",
          "label": "B"
        },
        {
          "content": "承运人",
          "label": "C"
        },
        {
          "content": "动物饲养场（户）",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政许可的实施机关可以对已设定的行政许可的实施情况及存在的必要性适时进行评价，并将意见报告该（        ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "行政许可的设定机关",
          "label": "A"
        },
        {
          "content": "全国人民代表大会",
          "label": "B"
        },
        {
          "content": "全国人大常委会",
          "label": "C"
        },
        {
          "content": "国务院",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《国家畜禽遗传资源品种名录》规定，以下属于规定的特种畜禽的是（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "敖鲁古雅驯鹿",
          "label": "A"
        },
        {
          "content": "卡奴鸽",
          "label": "B"
        },
        {
          "content": "尼克肉鸡",
          "label": "C"
        },
        {
          "content": "太行驴",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，官方兽医应当具备国务院农业农村主管部门规定的条件，由省、自治区、直辖市人民政府农业农村主管部门按程序确认，由所在地（    ）任命。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "省级动物卫生监督机构",
          "label": "C"
        },
        {
          "content": "县级以上动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，受理申报后，动物卫生监督机构应当（    ）官方兽医实施检疫。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "委托",
          "label": "A"
        },
        {
          "content": "委派",
          "label": "B"
        },
        {
          "content": "指定",
          "label": "C"
        },
        {
          "content": "指派",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "未按照规定向当事人告知拟作出的行政处罚内容及事实、理由、依据，或者拒绝听取当事人的陈述、申辩，（      ）作出行政处罚决定。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "不再",
          "label": "A"
        },
        {
          "content": "延迟",
          "label": "B"
        },
        {
          "content": "不得",
          "label": "C"
        },
        {
          "content": "停止",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "经批准从境外引进畜禽遗传资源的，依照（       ）的规定办理相关手续并实施检疫。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "《中华人民共和国动物防疫法》",
          "label": "A"
        },
        {
          "content": "《动物检疫管理办法》",
          "label": "B"
        },
        {
          "content": "《中华人民共和国农产品质量安全法》",
          "label": "C"
        },
        {
          "content": "《中华人民共和国进出境动植物检疫法》",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政许可的设立机关应当定期对其设定的行政许可进行（     ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "检查",
          "label": "A"
        },
        {
          "content": "评价",
          "label": "B"
        },
        {
          "content": "调查",
          "label": "C"
        },
        {
          "content": "修改",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政处罚法》，关于听证以下说法正确的是（      ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "当事人要求听证的，应当在行政机关告知后十日内提出",
          "label": "A"
        },
        {
          "content": "行政机关应当在举行听证的七日前，通知当事人及有关人员听证的时间、地点",
          "label": "B"
        },
        {
          "content": "当事人必须亲自参加听证",
          "label": "C"
        },
        {
          "content": "听证必须公开举行",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，动物卫生监督机构及其工作人员不得从事与动物防疫有关的（　　）活动。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "宣传性",
          "label": "A"
        },
        {
          "content": "公益性",
          "label": "B"
        },
        {
          "content": "经营性",
          "label": "C"
        },
        {
          "content": "技术性",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列关于农产品销售说法错误的是：（ ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "农业技术推广等机构应当为农户等农产品生产经营者提供农产品检测技术服务。",
          "label": "A"
        },
        {
          "content": "禁止将农产品与有毒有害物质一同储存、运输，防止污染农产品。",
          "label": "B"
        },
        {
          "content": "农药、兽药等化学物质残留或者含有的重金属等有毒有害物质不符合农产品质量安全标准的农产品经重新检测后可进行销售。",
          "label": "C"
        },
        {
          "content": "含有国家禁止使用的农药、兽药或者其他化合物的农产品不得销售。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，（　　）应当根据动物检疫工作需要，合理设置动物检疫申报点，并向社会公布。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "动物卫生监督机构",
          "label": "C"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政许可采取统一办理或者联合办理、集中办理的，办理的时间不得超过（     ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "二十日",
          "label": "A"
        },
        {
          "content": "三十日",
          "label": "B"
        },
        {
          "content": "四十五日",
          "label": "C"
        },
        {
          "content": "六十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，（    ）的动物，应当附有检疫证明。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "经营",
          "label": "A"
        },
        {
          "content": "饲养",
          "label": "B"
        },
        {
          "content": "运输",
          "label": "C"
        },
        {
          "content": "屠宰",
          "label": "D"
        },
        {
          "content": "免疫",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，以下属于检疫申报单需要填写的内容有（）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "编号",
          "label": "A"
        },
        {
          "content": "货主的姓名",
          "label": "B"
        },
        {
          "content": "动物或动物产品的种类",
          "label": "C"
        },
        {
          "content": "数量及单位",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物检疫管理办法》规定，（     ）不得接收未附有有效动物检疫证明的动物。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "饲养场",
          "label": "A"
        },
        {
          "content": "饲养户",
          "label": "B"
        },
        {
          "content": "承运人",
          "label": "C"
        },
        {
          "content": "屠宰加工场所",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列说法正确的是：（ ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "省级人民政府对本行政区域的农产品质量安全工作负责。",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府确定本级各部门的农产品质量安全监督管理工作职责。",
          "label": "B"
        },
        {
          "content": "各有关部门在职责范围内负责本行政区域的农产品质量安全监督管理工作。",
          "label": "C"
        },
        {
          "content": "乡镇人民政府协助上级人民政府及其有关部门做好农产品质量安全监督管理工作。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《三类动物疫病防治规范》要求从事动物饲养、屠宰、经营、隔离、运输等活动的单位和个人应当加强管理，保持畜禽养殖环境（     ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "不受外界干扰",
          "label": "A"
        },
        {
          "content": "卫生清洁",
          "label": "B"
        },
        {
          "content": "通风良好",
          "label": "C"
        },
        {
          "content": "合理的环境温度和湿度",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物检疫管理办法》，下列关于检疫申报制度表述正确的是（　　）。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "出售或者运输动物、动物产品的，货主应当提前三天申报检疫",
          "label": "A"
        },
        {
          "content": "屠宰动物的，应当提前六小时申报检疫",
          "label": "B"
        },
        {
          "content": "向无规定动物疫病区输入相关易感动物的，应当在起运5天前申报检疫",
          "label": "C"
        },
        {
          "content": "合法捕获野生动物的，应当在捕获后15天内申报检疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对官方兽医师资进行（）等方面培训",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "动物检疫监督",
          "label": "A"
        },
        {
          "content": "动物卫生法学理论",
          "label": "B"
        },
        {
          "content": "重大动物疫情应急处置",
          "label": "C"
        },
        {
          "content": "职业道德规范 ",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "饲养动物的单位和个人要按有关规定（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "建立免疫档案",
          "label": "A"
        },
        {
          "content": "加施畜禽标识",
          "label": "B"
        },
        {
          "content": "确保可追溯",
          "label": "C"
        },
        {
          "content": "定期监测",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，基层群众性自治组织中从事管理的人员有违法行为的，监察机关可以予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）在“五、加强法律宣传贯彻，推动指定通道制度有效落实”中要求，要重点在监督检查站等场所开展宣传，指导生产经营主体利用信息化手段查询、了解指定通道基础信息，规范开展动物运输活动。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "动物卫生监督机构的官方兽医按照培训计划要求，全员参加培训。协检人员培训参照官方兽医培训计划执行。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农业农村部负责组建国家官方兽医培训师资库，加强师资选拔、培训、考核和动态管理。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政处罚法》，当事人要求听证的，行政机关应当在举行听证的（     ）前，通知当事人及有关人员听证的时间、地点",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "三日",
          "label": "A"
        },
        {
          "content": "五日",
          "label": "B"
        },
        {
          "content": "七日",
          "label": "C"
        },
        {
          "content": "十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "水产苗种产地检疫，由从事水生动物检疫的县级以上（   ）实施。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "农业农村主管部门",
          "label": "A"
        },
        {
          "content": "动物卫生监督机构",
          "label": "B"
        },
        {
          "content": "水生动物主管部门",
          "label": "C"
        },
        {
          "content": "水生动物检疫机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，政务处分期自（）起计算。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "政务处分决定生效之日",
          "label": "A"
        },
        {
          "content": "领导同意之日",
          "label": "B"
        },
        {
          "content": "党委批准之日",
          "label": "C"
        },
        {
          "content": "违纪行为发生之日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "饲养动物的单位和个人达到国务院农业农村主管部门规定的净化标准的，由(     )予以公布。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "国务院农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省、自治区、直辖市人民政府",
          "label": "B"
        },
        {
          "content": "省级以上人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "省、自治区、直辖市人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，动物卫生监督机构的（    ）具体实施动物、动物产品检疫。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "兽医人员",
          "label": "A"
        },
        {
          "content": "执业兽医",
          "label": "B"
        },
        {
          "content": "官方兽医及协检人员",
          "label": "C"
        },
        {
          "content": "官方兽医",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "官方兽医跨（）工作调动，应当注销原证，重新任命发证。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "省",
          "label": "A"
        },
        {
          "content": "市",
          "label": "B"
        },
        {
          "content": "乡镇",
          "label": "C"
        },
        {
          "content": "县（区）",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，县级以上人民政府建立的(    )应当为动物检疫及其监督管理工作提供技术支撑。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物疫病预防控制机构",
          "label": "A"
        },
        {
          "content": "农业综合执法机构",
          "label": "B"
        },
        {
          "content": "动物疫病检测机构",
          "label": "C"
        },
        {
          "content": "农业综合执法大队",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，官方兽医依法履行动物、动物产品检疫职责，任何单位和个人不得（    ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "拒绝",
          "label": "A"
        },
        {
          "content": "妨碍",
          "label": "B"
        },
        {
          "content": "拒绝或者阻碍",
          "label": "C"
        },
        {
          "content": "拒绝或者妨碍",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "按照农产品质量安全法，农产品生产经营者应当（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "依照法律、法规和农产品质量安全标准从事生产经营活动",
          "label": "A"
        },
        {
          "content": "接受社会监督，承担社会责任",
          "label": "B"
        },
        {
          "content": "对自己生产经营的农产品质量安全负责",
          "label": "C"
        },
        {
          "content": "协助政府部门做好农产品质量安全监督管理工作",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下关于人兽共患病防治工作的叙述正确的有（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "事关畜牧业高质量发展和人民群众身体健康",
          "label": "A"
        },
        {
          "content": "事关公共卫生安全和国家生物安全",
          "label": "B"
        },
        {
          "content": "是贯彻落实乡村振兴战略和健康中国战略的重要内容",
          "label": "C"
        },
        {
          "content": "是政府社会管理和公共服务的重要职责",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《生猪屠宰管理条例》，以下属于生猪定点屠宰厂（场）应当具备的条件的是（     ）。",
      "standard_answer": "A,C,D,E",
      "option_list": [
        {
          "content": "有与屠宰规模相适应、水质符合国家规定标准的水源条件",
          "label": "A"
        },
        {
          "content": "有依法派驻的官方兽医",
          "label": "B"
        },
        {
          "content": "有经考核合格的兽医卫生检验人员",
          "label": "C"
        },
        {
          "content": "有依法取得健康证明的屠宰技术人员",
          "label": "D"
        },
        {
          "content": "依法取得动物防疫条件合格证",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，染疫动物及其排泄物、染疫动物产品，运载工具中的（     ）等被污染的物品，应当按照国家有关规定处理，不得随意处置。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "动物排泄物",
          "label": "A"
        },
        {
          "content": "垫料",
          "label": "B"
        },
        {
          "content": "容器",
          "label": "C"
        },
        {
          "content": "包装物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "跨省际或省域内跨市（地）、县（市）流入的病死畜禽，在完成调查并按法定程序作出处理决定后，要及时将调查结果和对（     ）的处理意见向社会公布。重要情况及时向国务院报告。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "病死畜禽",
          "label": "A"
        },
        {
          "content": "生产经营者",
          "label": "B"
        },
        {
          "content": "监管部门",
          "label": "C"
        },
        {
          "content": "地方政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "要按照“（     ）”的原则，充分发挥财政资金的引导作用，引导社会资本投资，跨行政区域建设无害化处理场，合理配套建设收集站点，提高处理能力和开工率，最大限度发挥规模效益，加快无害化处理体系建设。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "统筹规划、属地负责",
          "label": "A"
        },
        {
          "content": "严格准入、规范操作",
          "label": "B"
        },
        {
          "content": "政府监管、市场运作",
          "label": "C"
        },
        {
          "content": "财政补助、保险联动",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员利用职权或者职务上的影响为本人或者他人谋取私利，情节较重的，予以（）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "记大过",
          "label": "A"
        },
        {
          "content": "降级",
          "label": "B"
        },
        {
          "content": "撤职",
          "label": "C"
        },
        {
          "content": "开除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关及其执法人员当场收缴罚款的，必须向当事人出具（     ）统一制发的专用票据",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "国务院财政部门",
          "label": "A"
        },
        {
          "content": "国务院税务部门",
          "label": "B"
        },
        {
          "content": "省、自治区、直辖市人民政府财政部门",
          "label": "C"
        },
        {
          "content": "省、自治区、直辖市人民政府税务部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "倡导养成健康消费习惯，不食用未加热成熟的牛羊肉，不饮用未消毒杀菌的生奶。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "全面实施畜禽运输车辆和人员备案制度，充分发挥动物防疫指定通道作用，加强活畜跨区域调运监管，防止畜间布病跨区域传播。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，国家实行官方兽医考核制度。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列关于农产品生产说法错误的是：（ ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "农产品生产企业、农民专业合作社、农业社会化服务组织应当加强农产品质量安全管理。",
          "label": "A"
        },
        {
          "content": "农产品生产企业应委托具有专业技术知识的人员进行农产品质量安全指导。",
          "label": "B"
        },
        {
          "content": "农业技术推广机构应当加强对农产品生产经营者质量安全知识和技能的培训。",
          "label": "C"
        },
        {
          "content": "农业农村主管部门应当建立农产品质量安全管理制度",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物卫生监督机构的官方兽医按照培训计划要求，（）参加培训。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "分批",
          "label": "A"
        },
        {
          "content": "自主 ",
          "label": "B"
        },
        {
          "content": "全员",
          "label": "C"
        },
        {
          "content": "自愿",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对全国有关畜种，根据当地实际情况，在科学评估的基础上选择适宜口蹄疫疫苗，进行（）免疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "O型和(或)A型口蹄疫",
          "label": "A"
        },
        {
          "content": "0型",
          "label": "B"
        },
        {
          "content": "A型",
          "label": "C"
        },
        {
          "content": "C型",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家生物安全工作协调机制组织建立统一的国家生物安全信息平台，有关部门将生物安全数据、资料等信息汇交国家生物安全信息平台，实现信息共享，属于生物安全（ ）制度。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "信息发布",
          "label": "A"
        },
        {
          "content": "信息共享",
          "label": "B"
        },
        {
          "content": "信息汇总",
          "label": "C"
        },
        {
          "content": "信息收集",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "农业农村部规定，牛羊肉塑料卡环式检疫验讫标志标牌规格为（）毫米。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "9×10",
          "label": "A"
        },
        {
          "content": "10×10",
          "label": "B"
        },
        {
          "content": "34×38",
          "label": "C"
        },
        {
          "content": "32×32",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "省级人民政府根据精简、统一、效能的原则，决定一个行政机关行使有关行政机关的行政许可权之前，必须经（     ）批准。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "省级人民代表大会",
          "label": "A"
        },
        {
          "content": "省级人大常委会",
          "label": "B"
        },
        {
          "content": "国务院",
          "label": "C"
        },
        {
          "content": "国务院主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家生物安全工作协调机制组织建立国家生物安全（ ）体系，提高生物安全风险识别和分析能力。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "风险监测预警",
          "label": "A"
        },
        {
          "content": "风险调查评估",
          "label": "B"
        },
        {
          "content": "风险防控措施",
          "label": "C"
        },
        {
          "content": "应急处置",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（      ）是指处于同一生物安全管理体系下，在一定期限内没有发生规定的一种或者几种动物疫病的若干动物饲养场及其辅助生产场所构成的，并经验收合格的特定小型区域。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "无规定动物疫病区",
          "label": "A"
        },
        {
          "content": "无规定动物疫病生物安全隔离区",
          "label": "B"
        },
        {
          "content": "无规定动物疫病场所",
          "label": "C"
        },
        {
          "content": "无规定动物疫病区或无规定动物疫病生物安全隔离区",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "从事畜禽饲养、屠宰、经营、隔离以及病死畜禽和病害畜禽产品收集、无害化处理的单位和个人，应当建立病死畜禽和病害畜禽产品无害化处理台账，相关台账记录保存期不少于（     ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "二年",
          "label": "A"
        },
        {
          "content": "一年",
          "label": "B"
        },
        {
          "content": "五年",
          "label": "C"
        },
        {
          "content": "十年",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违法行为情节轻微，且具有从轻或者减轻情形的，可以对其进行谈话提醒、批评教育、责令检查或者予以诫勉，（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "并作出警告的政务处分",
          "label": "A"
        },
        {
          "content": "免予或者不予政务处分",
          "label": "B"
        },
        {
          "content": "并作出党内行政处分",
          "label": "C"
        },
        {
          "content": "并作出记大过以下的行政处分",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "违反《生猪屠宰管理条例》规定，生猪定点屠宰厂（场）出厂（场）未经肉品品质检验或者经肉品品质检验不合格的生猪产品的，下列处罚正确的有（       ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "由农业农村主管部门责令停业整顿，没收生猪产品和违法所得",
          "label": "A"
        },
        {
          "content": "货值金额不足1万元的，由农业农村主管部门并处10万元以上15万元以下的罚款",
          "label": "B"
        },
        {
          "content": "货值金额1万元以上的，由农业农村主管部门并处货值金额15倍以上30倍以下的罚款",
          "label": "C"
        },
        {
          "content": "由农业农村主管部门对其直接负责的主管人员和其他直接责任人员处5万元以上10万元以下的罚款",
          "label": "D"
        },
        {
          "content": "情节严重的，由设区的市级人民政府吊销生猪定点屠宰证书，收回生猪定点屠宰标志牌，并可以由公安机关依照《中华人民共和国食品安全法》的规定，对其直接负责的主管人员和其他直接责任人员处5日以上15日以下拘留",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "发生（ ）时，有关部门应当及时开展生物安全风险调查评估，依法采取必要的风险防控措施。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "通过风险监测或者接到举报发现可能存在生物安全风险",
          "label": "A"
        },
        {
          "content": "为确定监督管理的重点领域、重点项目，制定、调整生物安全相关名录或者清单",
          "label": "B"
        },
        {
          "content": "发生重大新发突发传染病、动植物疫情等危害生物安全的事件",
          "label": "C"
        },
        {
          "content": "需要调查评估的其他情形",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员在管理服务活动中故意刁难、吃拿卡要，情节较重的，予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "设定和实施行政许可，应当依照法定的（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "权限",
          "label": "A"
        },
        {
          "content": "范围",
          "label": "B"
        },
        {
          "content": "条件",
          "label": "C"
        },
        {
          "content": "程序",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国家动物疫病强制免疫目标要求：强制免疫动物疫病的群体免疫密度应常年保持在90%以上，应免畜禽免疫密度应达到100%，（）免疫抗体合格率常年保持在70%以上。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "新城疫",
          "label": "A"
        },
        {
          "content": "口蹄疫",
          "label": "B"
        },
        {
          "content": "小反刍兽疫",
          "label": "C"
        },
        {
          "content": "高致病性禽流感",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，国务院农业农村主管部门确定强制免疫的（     ）。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "范围",
          "label": "A"
        },
        {
          "content": "动物疫病种类",
          "label": "B"
        },
        {
          "content": "动物疫病病种",
          "label": "C"
        },
        {
          "content": "区域",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "从事动物饲养、屠宰、经营、隔离、运输以及动物产品生产、经营、加工、贮藏等活动的单位和个人，依照《动物防疫法》和国务院农业农村主管部门的规定，做好（　　）等动物防疫工作，承担动物防疫责任。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "净化、消灭",
          "label": "A"
        },
        {
          "content": "检测、隔离",
          "label": "B"
        },
        {
          "content": "无害化处理",
          "label": "C"
        },
        {
          "content": "免疫、消毒",
          "label": "D"
        },
        {
          "content": "检测、检疫",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "XX县农业农村局监督检查时发现，该县胡某养猪场生猪佩戴的耳标为假耳标，共2500枚，已使用300枚，经调查该批耳标为本县赵某伪造，经营该批耳标共获违法收入500元。县农业农村局依法对胡某立案查处，下列处罚（    ）是符合法律规定的。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "责令当事人立即改正违法行为",
          "label": "A"
        },
        {
          "content": "没收违法所得500元",
          "label": "B"
        },
        {
          "content": "没收耳标",
          "label": "C"
        },
        {
          "content": "罚款10000元",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，任何单位和个人不得（    ）动物检疫证章标志。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "伪造",
          "label": "A"
        },
        {
          "content": "变造",
          "label": "B"
        },
        {
          "content": "转借",
          "label": "C"
        },
        {
          "content": "转让",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《全国畜间人兽共患病防治规划2022—2030年）》明确的常规防治病种的是（     ）。",
      "standard_answer": "B,C,D,E",
      "option_list": [
        {
          "content": "血矛线虫病",
          "label": "A"
        },
        {
          "content": "沙门氏菌病",
          "label": "B"
        },
        {
          "content": "李氏杆菌病",
          "label": "C"
        },
        {
          "content": "类鼻疽",
          "label": "D"
        },
        {
          "content": "片形吸虫病",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，经检疫不合格的动物、动物产品，货主应当在动物卫生监督机构的监督下按照国家有关规定处理，处理费用由货主承担。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "农产品生产企业、农民专业合作社、农业社会化服务组织应当建立农产品生产记录，农产品生产记录应当至少保存一年。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "对散养动物，采取春秋两季集中免疫与定期补免相结合的方式进行，对规模养殖场(户)及有条件的地方实施程序化免疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要根据本地区实际，探索建设过省境运输动物指定通道制度，创新监管方式，推动过省境运输动物规范便捷通行。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "中国动物疫病预防控制中心负责承担全国官方兽医培训计划具体实施工作，指导各地开展官方兽医培训。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各有关地区要加强经指定通道运输动物的监督检查，查验动物检疫证明是否有效、牲畜耳标是否佩戴齐全、运输车辆是否按规定备案、动物健康状况有无异常以及证物是否相符等。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "经肉品品质检验不合格的生猪产品，应当在（       ）的监督下，按照国家有关规定处理，并如实记录处理情况。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "兽医卫生检验人员",
          "label": "A"
        },
        {
          "content": "官方兽医",
          "label": "B"
        },
        {
          "content": "生猪定点屠宰厂（场）指定的专门人员",
          "label": "C"
        },
        {
          "content": "农业农村主管部门指定的专门人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，全国畜禽标识和养殖档案的监督管理工作由（）负责。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级畜牧兽医行政主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "县级以上地方政府畜牧兽医行政主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对于已满（    ）周岁不满（   ）周岁的未成年人有违法行为的，应当从轻或者减轻行政处罚。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "十四，十八",
          "label": "A"
        },
        {
          "content": "十六，十八",
          "label": "B"
        },
        {
          "content": "十四，十六",
          "label": "C"
        },
        {
          "content": "十二，十八",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪定点屠宰厂（场）被吊销生猪定点屠宰证书的，其法定代表人（负责人）、直接负责的主管人员和其他直接责任人员因食品安全犯罪被判处有期徒刑以上刑罚的，（       ）不得从事生猪屠宰管理活动。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "10年内",
          "label": "A"
        },
        {
          "content": "5年内",
          "label": "B"
        },
        {
          "content": "3年内",
          "label": "C"
        },
        {
          "content": "终身",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，未按照动物检疫证明载明的目的地运输的，情节严重的，处以（   ）罚款。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "三千元以上五千元以下",
          "label": "A"
        },
        {
          "content": "五千元以上一万元以下",
          "label": "B"
        },
        {
          "content": "三千元以上三万元以下",
          "label": "C"
        },
        {
          "content": "五千元以上三万元以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列哪种情形，应当给予行政处罚。（     ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "精神病人、智力残疾人在不能辨认或者不能控制自己行为时有违法行为的",
          "label": "A"
        },
        {
          "content": "间歇性精神病人在精神正常时有违法行为的",
          "label": "B"
        },
        {
          "content": "尚未完全丧失辨认或者控制自己行为能力的精神病人、智力残疾人有违法行为的",
          "label": "C"
        },
        {
          "content": "以上都不是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "承运人张三从A省B县拉一车肉鸡到B省D屠宰场，到达目的地后，张三应当在（   ）内向启运地动物卫生监督机构报告。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "二十四小时",
          "label": "A"
        },
        {
          "content": "一日",
          "label": "B"
        },
        {
          "content": "三日",
          "label": "C"
        },
        {
          "content": "十二小时",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关应当自行政处罚案件立案之日起（   ）内作出行政处罚决定。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "六十日",
          "label": "A"
        },
        {
          "content": "九十日",
          "label": "B"
        },
        {
          "content": "一百日",
          "label": "C"
        },
        {
          "content": "一百二十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，指导（   ）做好动物运输车辆的防疫消毒，特别是要做好（   ）时节车辆防疫消毒工作。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "承运人；冬季严寒",
          "label": "A"
        },
        {
          "content": "货主；冬季严寒",
          "label": "B"
        },
        {
          "content": "承运人；夏季酷暑",
          "label": "C"
        },
        {
          "content": "货主；夏季酷暑",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，(     )应当符合国务院农业农村主管部门规定的健康标准。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "种用、乳用动物",
          "label": "A"
        },
        {
          "content": "种用动物",
          "label": "B"
        },
        {
          "content": "乳用动物",
          "label": "C"
        },
        {
          "content": "动物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列哪些级别的政府可以设定行政许可？（   ）",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级人民政府",
          "label": "B"
        },
        {
          "content": "设区的市人民政府",
          "label": "C"
        },
        {
          "content": "县级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "人畜共患传染病名录由（    ）会同（   ）等主管部门制定并公布。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "国务院农业农村主管部门",
          "label": "A"
        },
        {
          "content": "林业草原部门",
          "label": "B"
        },
        {
          "content": "国务院卫生健康",
          "label": "C"
        },
        {
          "content": "野生动物保护",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，受理检疫申报后，动物卫生监督机构可以安排协检人员协助官方兽医到（       ）核实信息，开展临床健康检查。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "现场",
          "label": "A"
        },
        {
          "content": "就近地点",
          "label": "B"
        },
        {
          "content": "指定地点",
          "label": "C"
        },
        {
          "content": "约定的地点",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，农业农村部制定、调整并公布检疫规程，明确动物检疫的（         ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "范围",
          "label": "A"
        },
        {
          "content": "对象",
          "label": "B"
        },
        {
          "content": "程序",
          "label": "C"
        },
        {
          "content": "流程",
          "label": "D"
        },
        {
          "content": "结果处理",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列属于行政机关颁发的行政许可证件的是\r\n（       ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "许可证",
          "label": "A"
        },
        {
          "content": "资格证",
          "label": "B"
        },
        {
          "content": "执照",
          "label": "C"
        },
        {
          "content": "检疫证明",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "习近平总书记在十九届中央政治局第三十三次集体学习时对畜间人兽共患病防治提出明确要求，强调要坚持（     ），从源头前端阻断人兽共患病的传播途径。 ",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "预防为主",
          "label": "A"
        },
        {
          "content": "人病兽防",
          "label": "B"
        },
        {
          "content": "关口前移",
          "label": "C"
        },
        {
          "content": "市场准入",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员不按照规定请示、报告重大事项，情节严重的，予以（）。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "记过",
          "label": "A"
        },
        {
          "content": "记大过",
          "label": "B"
        },
        {
          "content": "降级",
          "label": "C"
        },
        {
          "content": "撤职",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列属于动物隔离场所应当符合的防疫条件的是：（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "饲养区内设置配备疫苗冷藏冷冻设备、消毒和诊疗等防疫设备的兽医室。",
          "label": "A"
        },
        {
          "content": "饲养区内清洁道、污染道分设。",
          "label": "B"
        },
        {
          "content": "配备符合国家规定的病死动物和病害动物产品无害化处理设施设备或者冷藏冷冻等暂存设施设备。",
          "label": "C"
        },
        {
          "content": "建立免疫、用药、检疫申报、疫情报告、无害化处理、畜禽标识及养殖档案管理等动物防疫制度。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《行政处罚法》，下列哪些情形，执法人员可以当场收缴罚款。（       ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "在边远、水上、交通不便地区，当事人到指定的银行或者通过电子支付系统缴纳罚款确有困难、经当事人提出的",
          "label": "A"
        },
        {
          "content": "依法给予一百元以下罚款的",
          "label": "B"
        },
        {
          "content": "依法给予两百元以下罚款的",
          "label": "C"
        },
        {
          "content": "不当场收缴事后难以执行的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，发现未经指定通道跨省份道路运输动物的，以及违反有关规定接收动物的，要予以退回重新检疫。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "以种畜场、奶畜场和规模牛羊场为重点，全面开展布病净化场和无疫小区建设，每年建成评估一批高水平的布病净化场和无疫小区。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "经批准从境外引进畜禽遗传资源的，依照《中华人民共和国进出境动植物检疫法》的规定办理相关手续并实施检疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "农业农村部规定，新型动物产品检疫粘贴标志背面，中央图案为（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "防伪微缩文字“中国农业农村部监制”",
          "label": "A"
        },
        {
          "content": "各省份监督所公章",
          "label": "B"
        },
        {
          "content": "“检疫专用，仿冒必究”字样",
          "label": "C"
        },
        {
          "content": "团花版纹防伪",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，官方兽医应当回收进入屠宰加工场所待宰动物附有的动物检疫证明保存期限不得少于（    ）个月。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "三",
          "label": "A"
        },
        {
          "content": "六",
          "label": "B"
        },
        {
          "content": "十二",
          "label": "C"
        },
        {
          "content": "二十四",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家生物安全工作协调机制组织有关部门加强不同领域生物安全标准的协调和衔接，建立和完善生物安全（ ）体系。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "标准",
          "label": "A"
        },
        {
          "content": "审查",
          "label": "B"
        },
        {
          "content": "名录和清单",
          "label": "C"
        },
        {
          "content": "应急",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "强制免疫动物疫病的群体免疫密度应常年保持在（）%以上。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "80",
          "label": "A"
        },
        {
          "content": "90",
          "label": "B"
        },
        {
          "content": "70",
          "label": "C"
        },
        {
          "content": "100",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "携带犬只出户的，应当按照规定(         )，防止犬只伤人、疫病传播。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "佩戴犬牌并采取系犬绳等措施",
          "label": "A"
        },
        {
          "content": "佩戴犬牌",
          "label": "B"
        },
        {
          "content": "系犬绳",
          "label": "C"
        },
        {
          "content": "佩戴犬证并采取系犬绳等措施",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，给与撤职处分的，政务处分期为（）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "六个月",
          "label": "A"
        },
        {
          "content": "十二个月",
          "label": "B"
        },
        {
          "content": "十八个月",
          "label": "C"
        },
        {
          "content": "二十四个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "违反《动物防疫法》规定,造成人畜共患传染病传播、流行的，(    )给予处分、处罚。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "依法从重",
          "label": "A"
        },
        {
          "content": "从重",
          "label": "B"
        },
        {
          "content": "可以从重",
          "label": "C"
        },
        {
          "content": "应当从重",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "省、市、县三级农业农村部门要按照人员管理权限，及时公布官方兽医名单，颁发官方兽医证，指导官方兽医（）上岗。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "考核",
          "label": "A"
        },
        {
          "content": "培训",
          "label": "B"
        },
        {
          "content": "持证",
          "label": "C"
        },
        {
          "content": "任命",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "审核通过的官方兽医的编号由（     ）确认。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "上级农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "市级农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，发现动物疫病的，要（    ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "及时处置",
          "label": "A"
        },
        {
          "content": "立即进行无害化处理",
          "label": "B"
        },
        {
          "content": "规范报告并处置",
          "label": "C"
        },
        {
          "content": "及时通知出具动物检疫证明的机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政处罚法》，涉及公民生命健康安全、金融安全且有危害后果的违法行为，在（    ）年内未被发现的，不再给予行政处罚。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "三",
          "label": "A"
        },
        {
          "content": "五",
          "label": "B"
        },
        {
          "content": "十",
          "label": "C"
        },
        {
          "content": "十五",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国务院卫生健康、农业农村、林业草原、海关、生态环境主管部门应当建立（ ）监测网络，组织监测站点布局、建设，完善监测信息报告系统，开展主动监测和病原检测，并纳入国家生物安全风险监测预警体系。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "新发突发传染病",
          "label": "A"
        },
        {
          "content": "动植物疫情",
          "label": "B"
        },
        {
          "content": "进出境检疫",
          "label": "C"
        },
        {
          "content": "生物技术环境安全",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》提出要深入推进畜间人兽共患病强制免疫先打后补改革，完善以养殖场户为责任主体，以（     ）等社会化服务队伍为技术依托的强制免疫网络。",
      "standard_answer": "A,B,D,E",
      "option_list": [
        {
          "content": "企业执业兽医",
          "label": "A"
        },
        {
          "content": "乡村兽医",
          "label": "B"
        },
        {
          "content": "官方兽医",
          "label": "C"
        },
        {
          "content": "村级防疫员",
          "label": "D"
        },
        {
          "content": "特聘防疫专员",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，在监督检查中发现动物临床健康状况异常的，要及时采取（      ）等措施。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "隔离观察",
          "label": "A"
        },
        {
          "content": "无害化处理",
          "label": "B"
        },
        {
          "content": "实验室检测",
          "label": "C"
        },
        {
          "content": "采样检测",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《行政处罚法》，当场作出行政处罚决定，下列哪些情形，执法人员可以当场收缴罚款。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "依法给予一百元以下罚款的",
          "label": "A"
        },
        {
          "content": "依法给予两百元以下罚款的",
          "label": "B"
        },
        {
          "content": "不当场收缴事后难以执行的",
          "label": "C"
        },
        {
          "content": "当事人提出申请的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物防疫法》规定，禁止转让、伪造或者变造（    ）。",
      "standard_answer": "A,C,E",
      "option_list": [
        {
          "content": "畜禽标识",
          "label": "A"
        },
        {
          "content": "检疫标识",
          "label": "B"
        },
        {
          "content": "检疫证明",
          "label": "C"
        },
        {
          "content": "畜禽标志",
          "label": "D"
        },
        {
          "content": "检疫标志",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国家加强农产品质量安全工作，实行（ ），建立科学、严格的监督管理制度，构建协同、高效的社会共治体系。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "源头治理",
          "label": "A"
        },
        {
          "content": "风险管理",
          "label": "B"
        },
        {
          "content": "流程管控",
          "label": "C"
        },
        {
          "content": "全程控制",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国畜牧法》规定，从事畜禽养殖，不得有下列哪些行为。（     ）",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "违反法律、行政法规和国家有关强制性标准、国务院农业农村主管部门的规定使用饲料、饲料添加剂、兽药",
          "label": "A"
        },
        {
          "content": "使用未经高温处理的餐馆、食堂的泔水饲喂家畜",
          "label": "B"
        },
        {
          "content": "在垃圾场或者使用垃圾场中的物质饲养畜禽",
          "label": "C"
        },
        {
          "content": "随意弃置和处理病死畜禽",
          "label": "D"
        },
        {
          "content": "法律、行政法规和国务院农业农村主管部门规定的危害人和畜禽健康的其他行为",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "在屠宰检疫中，官方兽医的职责是（      ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "检查待宰动物健康状况",
          "label": "A"
        },
        {
          "content": "开展同步检疫",
          "label": "B"
        },
        {
          "content": "开展必要的实验室检测",
          "label": "C"
        },
        {
          "content": "填写屠宰检疫记录",
          "label": "D"
        },
        {
          "content": "入场监督查验",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "深入开展动物检疫监督信息数据质量提升年活动，进一步规范数据填报审核，防范不法分子利用信息化技术违规出证，对（）等情况，要及时核查处理。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "频繁出证",
          "label": "A"
        },
        {
          "content": "检疫出证数量异常波动",
          "label": "B"
        },
        {
          "content": "委托申报",
          "label": "C"
        },
        {
          "content": "频繁作废",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《2022—2025年全国官方兽医培训计划》培训目标，以提高官方兽医（）为重点，加强官方兽医培训考核，强化培训条件保障，加快培养一支业务精湛、作风优良、纪律严明、保障有力的官方兽医队伍。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "业务能力",
          "label": "A"
        },
        {
          "content": "综合素质",
          "label": "B"
        },
        {
          "content": "政治能力",
          "label": "C"
        },
        {
          "content": "创新能力",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "对供研究和疫苗生产用的家禽、进口国(地区)明确要求不得实施高致病性禽流感免疫的出口家禽，以及因其他特殊原因不免疫的，有关养殖场(户)逐级报市级农业农村部门同意后，可不实施免疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，动物卫生监督机构依照《动物防疫法》和国务院农业农村主管部门的规定对动物、动物产品实施检疫。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "防范公共卫生风险不是《中华人民共和国畜牧法》的立法目的之一。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《动物防疫法》规定，禁止转让、伪造或者变造检疫证明、检疫标志或者畜禽标识。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "到2025年，国家、省、市、县四级官方兽医培训体系进一步健全完善，官方兽医知识结构持续更新优化，专业能力和综合素质明显提升，省、市、县三级动物卫生监督机构官方兽医完训率达到90%，考试合格率达到90％以上。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "根据《中华人民共和国畜牧法》规定，畜禽养殖户的防疫条件、畜禽粪污无害化处理和资源化利用要求，由省、自治区、直辖市人民政府农业农村主管部门会同有关部门规定。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，已经实施畜禽产品无纸化出证的地区，对取得动物检疫证明的畜禽产品，继续在本屠宰企业内分割加工的，不再重复出证。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员因犯罪被单处罚金，或者犯罪情节轻微，人民检察院依法作出不起诉决定或者人民法院依法免予刑事处罚的，予以（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "开除",
          "label": "A"
        },
        {
          "content": "撤职",
          "label": "B"
        },
        {
          "content": "留党察看",
          "label": "C"
        },
        {
          "content": "党内警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对情节复杂或者重大违法行为给予行政处罚，在行政机关负责人作出行政处罚的决定之前，\r\n（   ）应当集体讨论决定。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "执法人员",
          "label": "A"
        },
        {
          "content": "法制审核机构",
          "label": "B"
        },
        {
          "content": "司法机关",
          "label": "C"
        },
        {
          "content": "行政机关负责人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，对于无违法违规情况且动物临床健康的，要（    ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "对动物检疫证明进行上传",
          "label": "A"
        },
        {
          "content": "在动物检疫证明上加盖专用印章",
          "label": "B"
        },
        {
          "content": "不予以通行",
          "label": "C"
        },
        {
          "content": "在动物处理单上加盖专用印章",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "赵某欲将饲养的20头生猪出售，按照《动物检疫管理办法》规定，他在申报检疫时可以采取或者通过（    ）等方式申报。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "传真",
          "label": "A"
        },
        {
          "content": "申报点填报",
          "label": "B"
        },
        {
          "content": "电子数据交换",
          "label": "C"
        },
        {
          "content": "以上都可以",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员参加非法组织、非法活动情节严重的，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，饲养种用、乳用动物的单位和个人应当按照（　　）的要求，定期开展动物疫病检测。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "县级以上农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省级动物疫病预防控制机构",
          "label": "B"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "C"
        },
        {
          "content": "国务院农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违反规定，在公务接待、公务交通、会议活动、办公用房以及其他工作生活保障等方面超标准、超范围，情节严重的，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "养殖场(户)要详细记录畜禽（）等情况。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "存栏",
          "label": "A"
        },
        {
          "content": "出栏",
          "label": "B"
        },
        {
          "content": "免疫",
          "label": "C"
        },
        {
          "content": "收入",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对从事（）等重点人群开展专门宣传，不断强化防范意识，指导做好消毒、隔离等防护措施。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "养殖 ",
          "label": "A"
        },
        {
          "content": "运输",
          "label": "B"
        },
        {
          "content": "屠宰",
          "label": "C"
        },
        {
          "content": "加工",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，以下哪些场所（     ），应当符合动物防疫条件，取得动物防疫条件合格证。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "动物饲养场",
          "label": "A"
        },
        {
          "content": "动物隔离场所",
          "label": "B"
        },
        {
          "content": "动物屠宰加工场所",
          "label": "C"
        },
        {
          "content": "动物和动物产品无害化处理场所",
          "label": "D"
        },
        {
          "content": "动物集贸市场",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，补检的生皮、原毛、绒、角等动物产品具备下列(    )条件的，补检合格，可出具动物检疫证明。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "经外观检查无腐烂变质；",
          "label": "A"
        },
        {
          "content": "申报材料符合检疫规程规定",
          "label": "B"
        },
        {
          "content": "按规定进行消毒",
          "label": "C"
        },
        {
          "content": "货主于七日内提供检疫规程规定的实验室疫病检测报告，检测结果合格",
          "label": "D"
        },
        {
          "content": "货主于五日内提供检疫规程规定的实验室疫病检测报告，检测结果合格",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "某动物批发市场生猪贩运户张某在卸完生猪后将车上排泄物、垫料等在市场外随处丢弃，县级以上地方人民政府农业农村主管部门应当（    ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "给予警告",
          "label": "A"
        },
        {
          "content": "责令张某限期处理",
          "label": "B"
        },
        {
          "content": "逾期不处理，罚款6000元",
          "label": "C"
        },
        {
          "content": "逾期不处理，委托有关单位代为处理，费用由张某承担",
          "label": "D"
        },
        {
          "content": "逾期不处理，罚款3000元",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员收受可能影响公正行使公权力的礼品、礼金、有价证券等财物的，予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《中华人民共和国畜牧法》的立法目的的有（     ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "规范畜牧业生产经营行为",
          "label": "A"
        },
        {
          "content": "保障畜禽产品供给和质量安全",
          "label": "B"
        },
        {
          "content": "维护畜牧业生产经营者的合法权益",
          "label": "C"
        },
        {
          "content": "促进畜牧业高质量发展",
          "label": "D"
        },
        {
          "content": "防范公共卫生风险",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "对种畜以外的牛羊进行布鲁氏菌病免疫，种畜禁止免疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "国务院农业农村主管部门确定强制免疫的动物疫病病种和区域。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各地要把宣传解读（   ）作为动物防疫法宣贯的一项重要内容，广泛开展多种形式的宣传活动。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "指定运输制度",
          "label": "A"
        },
        {
          "content": "指定通道制度",
          "label": "B"
        },
        {
          "content": "指定检疫制度",
          "label": "C"
        },
        {
          "content": "运输通道制度",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "农业农村部加强信息化建设，建立动物防疫条件审查（ ）系统。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "信息处理",
          "label": "A"
        },
        {
          "content": "信息监督",
          "label": "B"
        },
        {
          "content": "信息办公",
          "label": "C"
        },
        {
          "content": "信息管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "受委托行政机关在委托范围内，以（     ）名义实施行政许可",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "自己",
          "label": "A"
        },
        {
          "content": "委托行政机关",
          "label": "B"
        },
        {
          "content": "授权机关",
          "label": "C"
        },
        {
          "content": "特定行政机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员公开发表反对宪法确立的国家指导思想，反对中国共产党领导的文章、演说、宣言、声明等的，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "执法人员当场作出行政处罚决定的，应当向当事人出示（      ），填写预定格式、编有号码的行政处罚决定书。行政处罚决定书应当当场交付当事人。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "身份证件",
          "label": "A"
        },
        {
          "content": "执法证件",
          "label": "B"
        },
        {
          "content": "身份证件或执法证件",
          "label": "C"
        },
        {
          "content": "身份证件和执法证件",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违反规定从事或者参与营利性活动，或者违反规定兼任职务、领取报酬情节严重的，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "相关科研院校、医疗机构以及其他企业事业单位应当将生物安全法律法规和生物安全知识纳入（ ），加强学生、从业人员生物安全意识和伦理意识的培养。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "日常管理",
          "label": "A"
        },
        {
          "content": "制度化管理",
          "label": "B"
        },
        {
          "content": "教育培训内容",
          "label": "C"
        },
        {
          "content": "安全规划",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，运输用于继续饲养或者屠宰的畜禽到达目的地后，未向启运地动物卫生监督机构报告的，县级以上地方人民政府农业农村主管部门处以（   ）罚款。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "一千元以上三千元以下",
          "label": "A"
        },
        {
          "content": "两千元以上三千元以下",
          "label": "B"
        },
        {
          "content": "两千元以上五千元以下",
          "label": "C"
        },
        {
          "content": "处三千元以上五千元以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，要根据本地区实际，探索建设(    )制度，创新监管方式，推动过省境运输动物规范便捷通行。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "过省境运输动物指定通道",
          "label": "A"
        },
        {
          "content": "运输动物指定通道",
          "label": "B"
        },
        {
          "content": "过省境运输动物运输管理",
          "label": "C"
        },
        {
          "content": "入省境运输动物制定通道",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所变更单位名称或者法定代表人（负责人）未办理变更手续的，由县级以上地方人民政府（ ）主管部门责令限期改正。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "农业农村",
          "label": "A"
        },
        {
          "content": "市场监督",
          "label": "B"
        },
        {
          "content": "兽医",
          "label": "C"
        },
        {
          "content": "以上都不是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "违反《中华人民共和国畜牧法》的规定，畜禽养殖场未建立养殖档案或者未按照规定保存养殖档案的，县级以上地方人民政府农业农村主管部门采取的处理、处罚正确的有（       ）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "责令限期改正",
          "label": "B"
        },
        {
          "content": "可以处一万元以下罚款",
          "label": "C"
        },
        {
          "content": "情节严重的，处一万元以上五万元以下罚款",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，县级人民政府农业农村主管部门可以根据动物检疫工作需要，向（     ）派驻动物卫生监督机构或者官方兽医。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "乡",
          "label": "A"
        },
        {
          "content": "镇",
          "label": "B"
        },
        {
          "content": "或者特定区域",
          "label": "C"
        },
        {
          "content": "火车站",
          "label": "D"
        },
        {
          "content": "或者飞机场",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，有下列（   ）情形之一的，按照依法应当检疫而未经检疫处理处罚。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "动物种类、动物产品名称、畜禽标识号与动物检疫证明不符的",
          "label": "A"
        },
        {
          "content": "动物、动物产品数量超出动物检疫证明载明部分的",
          "label": "B"
        },
        {
          "content": "使用转让的动物检疫证明的",
          "label": "C"
        },
        {
          "content": "超出检疫范围的动物、动物产品持有检疫证明的",
          "label": "D"
        },
        {
          "content": "动物、动物产品数量与动物检疫证明不相符的",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《生猪屠宰管理条例》规定，农业农村主管部门应当建立举报制度，公布（       ），受理对违反条例规定行为的举报，并及时依法处理。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "举报电话",
          "label": "A"
        },
        {
          "content": "举报信箱或者电子邮箱",
          "label": "B"
        },
        {
          "content": "联系人",
          "label": "C"
        },
        {
          "content": "办公地址",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列哪些是三类动物疫病。（     ）",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "猪丹毒",
          "label": "A"
        },
        {
          "content": "大肠杆菌病",
          "label": "B"
        },
        {
          "content": "禽结核病",
          "label": "C"
        },
        {
          "content": "鳖腮腺炎病",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列哪些机关可以设定行政许可？（   ）",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "全国人民代表大会",
          "label": "A"
        },
        {
          "content": "省人民代表大会",
          "label": "B"
        },
        {
          "content": "较大的市的人民代表大会",
          "label": "C"
        },
        {
          "content": "县人民代表大会",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "从事下列（ ）活动，适用《生物安全法》。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "防控重大新发突发传染病、动植物疫情",
          "label": "A"
        },
        {
          "content": "生物技术研究、开发与应用",
          "label": "B"
        },
        {
          "content": "病原微生物实验室生物安全管理",
          "label": "C"
        },
        {
          "content": "防范外来物种入侵与保护生物多样性",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》，以下属于目前我国炭疽疫情的特点的有（     ）。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "有明显的季节性、区域性",
          "label": "A"
        },
        {
          "content": "大部分地区存在集中发生的情况",
          "label": "B"
        },
        {
          "content": "个别呈点状发生态势",
          "label": "C"
        },
        {
          "content": "以老疫点和疫源地为高发地区",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，经（    ）运输动物和动物产品的，托运人托运时应当提供检疫证明；没有检疫证明的，承运人不得承运。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "航空",
          "label": "A"
        },
        {
          "content": "道路",
          "label": "B"
        },
        {
          "content": "铁路",
          "label": "C"
        },
        {
          "content": "水路",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "执法人员与案件有直接利害关系或者有其他关系可能影响公正执法的，应当（      ）",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "报告",
          "label": "A"
        },
        {
          "content": "备案",
          "label": "B"
        },
        {
          "content": "回避",
          "label": "C"
        },
        {
          "content": "记录",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，对在动物疫病预防、控制、净化、消灭过程中强制扑杀的动物、销毁的动物产品和相关物品，县级以上以上人民政府（    ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "给予补偿",
          "label": "A"
        },
        {
          "content": "登记备案",
          "label": "B"
        },
        {
          "content": "资金支持",
          "label": "C"
        },
        {
          "content": "给予补助",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "被许可人以欺骗、贿赂等不正当手段取得行政许可按照规定被撤销的，被许可人基于行政许可取得的利益（    ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "受法律保护",
          "label": "A"
        },
        {
          "content": "不受法律保护",
          "label": "B"
        },
        {
          "content": "附条件的受法律保护",
          "label": "C"
        },
        {
          "content": "无条件的受法律保护",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员实施家庭暴力，虐待、遗弃家庭成员，情节严重的，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定,动物检疫遵循(   )、风险控制、区域化和可追溯管理相结合的原则。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "运输监管",
          "label": "A"
        },
        {
          "content": "现场核验",
          "label": "B"
        },
        {
          "content": "过程监管",
          "label": "C"
        },
        {
          "content": "过程管控",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "××县发生一类动物疫病时，（　　）应当立即派人到现场，划定疫点、疫区、受威胁区，调查疫源，及时报请××县人民政府对疫区实行封锁。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "××县动物卫生监督所",
          "label": "A"
        },
        {
          "content": "××县动物疫病预防控制中心",
          "label": "B"
        },
        {
          "content": "××县农业农村主管部门",
          "label": "C"
        },
        {
          "content": "××县防治重大动物疫病指挥部",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪定点屠宰厂（场）变更生产地址的，应当依照《生猪屠宰管理条例》的规定，（       ）生猪定点屠宰证书。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "换发",
          "label": "A"
        },
        {
          "content": "续展",
          "label": "B"
        },
        {
          "content": "申请变更",
          "label": "C"
        },
        {
          "content": "重新申请",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，确定强制免疫的动物疫病病种和区域的是（    ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "国务院农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省、自治区、直辖市人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "中国动物疫病预防控制中心",
          "label": "C"
        },
        {
          "content": "省级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "农业农村主管部门应当根据农产品质量安全风险监测、风险评估结果采取相应的管理措施，并将结果及时通报给（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "国务院市场监督管理部门",
          "label": "A"
        },
        {
          "content": "国务院卫生健康部门",
          "label": "B"
        },
        {
          "content": "有关省、自治区、直辖市人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "县级以上人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": " 《动物检疫管理办法》规定，禁止（     ）依法应当检疫而未经检疫或者检疫不合格的动物。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "加工",
          "label": "A"
        },
        {
          "content": "经营",
          "label": "B"
        },
        {
          "content": "运输",
          "label": "C"
        },
        {
          "content": "屠宰",
          "label": "D"
        },
        {
          "content": "出售",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "病死猪无害化处理与保险联动机制建设试点工作的主要任务是（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "开展全覆盖式生猪养殖保险",
          "label": "A"
        },
        {
          "content": "开展全覆盖式无害化处理",
          "label": "B"
        },
        {
          "content": "开展信息化联动管理",
          "label": "C"
        },
        {
          "content": "开展政策打包支持",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，官方兽医禁止（     ）或者以其他方式违法使用官方兽医证。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "伪造",
          "label": "A"
        },
        {
          "content": "变造",
          "label": "B"
        },
        {
          "content": "转借",
          "label": "C"
        },
        {
          "content": "非工作时间携带",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物防疫法》规定，应当向所在地县级人民政府农业农村主管部门备案的有（    ）。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "从事动物运输的单位",
          "label": "A"
        },
        {
          "content": "从事动物饲养的人员",
          "label": "B"
        },
        {
          "content": "从事动物运输的个人",
          "label": "C"
        },
        {
          "content": "动物运输车辆 ",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各地农业农村部门要充分发挥兽医卫生综合信息平台官方兽医培训考试模块和“牧运通”APP作用，督促指导官方兽医加强自主学习，全面开展官方兽医（）和（）。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "线下培训",
          "label": "A"
        },
        {
          "content": "线下考试",
          "label": "B"
        },
        {
          "content": "线上培训",
          "label": "C"
        },
        {
          "content": "线上考试",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以（）为重点，全面开展布病净化场和无疫小区建设，每年建成评估一批高水平的布病净化场和无疫小区。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "散养户",
          "label": "A"
        },
        {
          "content": "奶畜场",
          "label": "B"
        },
        {
          "content": "规模牛羊场",
          "label": "C"
        },
        {
          "content": "种畜场",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各地区要综合考虑（     ）等因素，制定病死畜禽无害化处理财政补助、收费等政策，确保无害化处理场所能够实现正常运营。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "病死畜禽收集成本",
          "label": "A"
        },
        {
          "content": "设施建设成本",
          "label": "B"
        },
        {
          "content": "实际处理成本",
          "label": "C"
        },
        {
          "content": "病死畜禽死亡数",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员有下列（）行为情节较重的，予以降级或者撤职。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "散布有损中国共产党领导和国家声誉的言论的",
          "label": "A"
        },
        {
          "content": "参加旨在反对宪法的集会、游行、示威等活动的",
          "label": "B"
        },
        {
          "content": "参加民族分裂活动的",
          "label": "C"
        },
        {
          "content": "利用宗教活动破坏民族团结和社会稳定的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "农业农村部办公厅关于进一步加强官方兽医管理工作的通知（农办牧〔2023〕10号）要求加强（）管理。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "官方兽医任命",
          "label": "A"
        },
        {
          "content": "官方兽医行风建设和履职",
          "label": "B"
        },
        {
          "content": "官方兽医证制作使用",
          "label": "C"
        },
        {
          "content": "官方兽医信息",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《中华人民共和国畜牧法》只适用于在中华人民共和国境内从事畜禽的遗传资源保护利用、繁育、饲养、经营、运输、屠宰等活动，不包括蜂、蚕的资源保护利用和生产经营。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "养殖场户应根据国家和本地区的动物疫病防治要求，主动开展疫病净化工作。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，已经实施畜禽产品无纸化出证并实现电子证照互通互认的地区，对取得动物检疫证明的畜禽产品，继续在本屠宰企业内分割加工的，不再重复出证，附具动物检疫证明电子证照加注件。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政许可法》，除可以当场作出行政许可决定的外，行政机关应当自受理行政许可申请之日起（      ）内作出行政许可决定。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "三日",
          "label": "A"
        },
        {
          "content": "五日",
          "label": "B"
        },
        {
          "content": "十日",
          "label": "C"
        },
        {
          "content": "二十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（）组织制定官方兽医培训大纲，审定培训教材和考试题库",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "农业农村部",
          "label": "A"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "动物卫生监督",
          "label": "C"
        },
        {
          "content": "中国动物疫病预防控制中心",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，发生重大动物疫情时，国务院农业农村主管部门负责划定动物疫病风险区，禁止或者限制特定动物、动物产品由（    ）调运。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "低风险区向高风险区",
          "label": "A"
        },
        {
          "content": "高风险区向低风险区",
          "label": "B"
        },
        {
          "content": "中风险区向高风险区",
          "label": "C"
        },
        {
          "content": "高风险区向中风险区",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关作出的准予行政许可决定（      ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "应当予以公开，但公众无权查阅",
          "label": "A"
        },
        {
          "content": "应当予以公开，公众有权查阅",
          "label": "B"
        },
        {
          "content": "不予公开，但公众有权查阅",
          "label": "C"
        },
        {
          "content": "不予公开，且公众无权查阅",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政处罚法》，在水上当场收缴的罚款，应当自抵岸之日起（     ）内交至行政机关；行政机关应当在（     ）内将罚款缴付指定的银行。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "二日、二日",
          "label": "A"
        },
        {
          "content": "二日、三日",
          "label": "B"
        },
        {
          "content": "三日、三日",
          "label": "C"
        },
        {
          "content": "三日、五日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（）要详细记录畜禽存栏、出栏、免疫等情况，特别是疫苗种类、生产厂家、生产批号等信息。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "养殖场(户)",
          "label": "A"
        },
        {
          "content": "官方兽医",
          "label": "B"
        },
        {
          "content": "协管员",
          "label": "C"
        },
        {
          "content": "村级防疫员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "按照《动物检疫管理办法》规定，货主申报检疫时，应当提交（    ）以及农业农村部规定的其他材料，并对申报材料的真实性负责。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "免疫证明",
          "label": "A"
        },
        {
          "content": "养殖档案",
          "label": "B"
        },
        {
          "content": "检疫申报单",
          "label": "C"
        },
        {
          "content": "动物疫病检测报告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，跨省、自治区、直辖市引进的乳用、种用动物到达输入地后，应当在隔离场或者饲养场内的隔离舍进行（  ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "隔离",
          "label": "A"
        },
        {
          "content": "观察",
          "label": "B"
        },
        {
          "content": "饲养",
          "label": "C"
        },
        {
          "content": "隔离观察",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政处罚法》，关于听证以下说法错误的是（      ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "除涉及国家秘密、商业秘密或者个人隐私依法予以保密外，听证公开举行",
          "label": "A"
        },
        {
          "content": "当事人必须亲自参加听证",
          "label": "B"
        },
        {
          "content": "听证应当制作笔录",
          "label": "C"
        },
        {
          "content": "当事人要求听证的，应当在行政机关告知后五日内提出",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家对畜禽遗传资源享有主权。经批准向境外输出畜禽遗传资源的，还应当依照（       ）的规定办理相关手续并实施检疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "《中华人民共和国进出境动植物检疫法》",
          "label": "A"
        },
        {
          "content": "《中华人民共和国动物防疫法》",
          "label": "B"
        },
        {
          "content": "《动物检疫管理办法》",
          "label": "C"
        },
        {
          "content": "《中华人民共和国畜牧法》",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪定点屠宰厂（场）、其他单位和个人对生猪、生猪产品注入其他物质的，除了由农业农村主管部门按规定进行处罚外，还可以由公安机关依照《中华人民共和国食品安全法》的规定，对其直接负责的主管人员和其他直接责任人员处（       ）拘留。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "5日以上10日以下",
          "label": "A"
        },
        {
          "content": "5日以上15日以下",
          "label": "B"
        },
        {
          "content": "10日以上15日以下",
          "label": "C"
        },
        {
          "content": "7日以上15日以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "动物防疫条件审查应当遵循（ ）的原则。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "公开",
          "label": "A"
        },
        {
          "content": "公平",
          "label": "B"
        },
        {
          "content": "公正",
          "label": "C"
        },
        {
          "content": "便民",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，出售或者运输水生动物的亲本、稚体、幼体、受精卵、发眼卵及其他遗传育种材料等水产苗种的，经检疫符合下列（    ）条件的，出具动物检疫证明。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "来自未发生相关水生动物疫情的苗种生产场",
          "label": "A"
        },
        {
          "content": "申报材料符合检疫规程规定",
          "label": "B"
        },
        {
          "content": "临床检查健康",
          "label": "C"
        },
        {
          "content": "需要进行实验室疫病检测的，检测结果合格",
          "label": "D"
        },
        {
          "content": "来自非封锁区及未发生相关水生动物疫情的苗种生产场",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "为（），制定《畜间布鲁氏菌病防控五年行动方案（2022-2026年）》。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "加强畜间布病防控",
          "label": "A"
        },
        {
          "content": "降低流行率和传播风险",
          "label": "B"
        },
        {
          "content": "促进畜牧业高质量发展",
          "label": "C"
        },
        {
          "content": "维护人民群众身体健康",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，监察机关在调查终结后，决定给予政务处分的，应当制作政务处分决定书，以下属于政务处分决定书应当载明的事项有（）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "违法事实和证据",
          "label": "A"
        },
        {
          "content": "政务处分的种类",
          "label": "B"
        },
        {
          "content": "被处分人的认罪态度",
          "label": "C"
        },
        {
          "content": "作出处分决定的机关名称",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《三类动物疫病防治规范》，三类动物疫病（     ）出现异常升高等情况，或呈暴发性流行时，应当按照动物疫情快报要求进行报告。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "发病率",
          "label": "A"
        },
        {
          "content": "死亡率",
          "label": "B"
        },
        {
          "content": "流行率",
          "label": "C"
        },
        {
          "content": "传播速度",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，作出政务处分决定前，监察机关应当将调查认定的违法事实及拟给予政务处分的依据告知被调查人，听取被调查人的（），并对事实、理由和证据进行核实，记录在案。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "陈述",
          "label": "A"
        },
        {
          "content": "诉讼",
          "label": "B"
        },
        {
          "content": "申辩",
          "label": "C"
        },
        {
          "content": "申诉",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列选项不属于行政处罚措施的是（      ）",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "通报批评",
          "label": "A"
        },
        {
          "content": "责任约谈",
          "label": "B"
        },
        {
          "content": "责令改正",
          "label": "C"
        },
        {
          "content": "扣押财物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "全国官方兽医培训的基本原则是（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "分级实施，全员培训",
          "label": "A"
        },
        {
          "content": "面向基层，务实管用",
          "label": "B"
        },
        {
          "content": "自学为主，学时管理",
          "label": "C"
        },
        {
          "content": "自愿培训，线下实施",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，我国跨省流通使用的检疫证明包括（）。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "《动物检疫合格证明》（动物A）",
          "label": "A"
        },
        {
          "content": "《动物检疫合格证明》（动物B）",
          "label": "B"
        },
        {
          "content": "《动物检疫合格证明》（产品A）",
          "label": "C"
        },
        {
          "content": "《动物检疫合格证明》（产品B）",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，发生一类动物疫病时，县级以上地方人民政府应当立即组织有关部门和单位采取封锁、隔离、（    ）等强制性措施。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "销毁",
          "label": "A"
        },
        {
          "content": "消毒",
          "label": "B"
        },
        {
          "content": "无害化处理",
          "label": "C"
        },
        {
          "content": "紧急免疫接种",
          "label": "D"
        },
        {
          "content": "扑杀",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，任何单位和个人不得（　　）动物疫情。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "瞒报",
          "label": "A"
        },
        {
          "content": "谎报",
          "label": "B"
        },
        {
          "content": "迟报",
          "label": "C"
        },
        {
          "content": "漏报",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "从事畜禽饲养、屠宰、经营、运输的单位和个人是病死畜禽无害化处理的第一责任人。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国畜牧法》规定，国家实行生猪定点屠宰制度，对生猪以外的其他畜禽（       ）实行定点屠宰。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "不得",
          "label": "A"
        },
        {
          "content": "可以",
          "label": "B"
        },
        {
          "content": "应当",
          "label": "C"
        },
        {
          "content": "适时",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，农业农村部制定、调整并公布（    ），明确动物检疫的范围、对象和程序。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "检疫办法",
          "label": "A"
        },
        {
          "content": "动物检疫规程",
          "label": "B"
        },
        {
          "content": "检疫规程",
          "label": "C"
        },
        {
          "content": "动物检疫规范",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对在生物安全工作中做出突出贡献的单位和个人，县级以上人民政府及其有关部门按照国家规定予以( )。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "表彰",
          "label": "A"
        },
        {
          "content": "奖励",
          "label": "B"
        },
        {
          "content": "表彰和奖励",
          "label": "C"
        },
        {
          "content": "以上都不是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，经航空、铁路、道路、水路运输动物和动物产品的，（    ）托运时应当提供检疫证明；没有检疫证明的，承运人不得承运。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "货主",
          "label": "A"
        },
        {
          "content": "贩运人",
          "label": "B"
        },
        {
          "content": "承运人",
          "label": "C"
        },
        {
          "content": "托运人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》明确了重点防范的外来疫病是牛海绵状脑病和（     ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "牛赤羽病",
          "label": "A"
        },
        {
          "content": "梅迪-维斯纳病",
          "label": "B"
        },
        {
          "content": "尼帕病毒性脑炎",
          "label": "C"
        },
        {
          "content": "猪丁型冠状病毒感染",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》提出要建立完善兽医社会化服务相关制度和标准，强化监督管理，加快构建（     ）主导的公益性兽医社会化服务与市场主导的经营性兽医社会化服务深度融合的长效机制。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "企业",
          "label": "A"
        },
        {
          "content": "兽医行业协会",
          "label": "B"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，运输用于饲养的动物到达目的后，目的地饲养场（户）应当在接收畜禽后（   ）内向所在地县级动物卫生监督机构报告。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "三日",
          "label": "A"
        },
        {
          "content": "二十四小时",
          "label": "B"
        },
        {
          "content": "二日",
          "label": "C"
        },
        {
          "content": "十二小时",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关对申请人提出的行政许可的申请的处理，说法不正确的是（      ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "出售、运输某种动物不需要检疫，但赵某不知，仍然向动物卫生监督机构申报检疫，动物卫生监督机构应即时告知赵某不受理",
          "label": "A"
        },
        {
          "content": "张某向某动物卫生监督机构提出某项申请，但是该事项属于市场监督管理部门的职权 ，于是动物卫生监督机构即时作出了不受理的决定，并告知李某向市场监督管理部门提出申请",
          "label": "B"
        },
        {
          "content": "李某向动物卫生监督机构提出的申请材料有错误，可以当场更正，但动物卫生监督机构不允许李某当场更正",
          "label": "C"
        },
        {
          "content": "田某向某动物卫生监督机构申报检疫，但提交的申报材料不符合法定形式、内容不齐全，动物卫生监督机构在三日内告知其需要补正的内容后，则应当在田某补正后予以受理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》提出要推进智慧防治，建立以（     ）为核心的全链条智慧监管体系，建成覆盖养殖场户、屠宰企业、指定通道、无害化处理场、交易市场的智能监控信息系统。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "动物饲养监管",
          "label": "A"
        },
        {
          "content": "动物屠宰监管",
          "label": "B"
        },
        {
          "content": "动物移动监管",
          "label": "C"
        },
        {
          "content": "上市动物产品监管",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所建设竣工后，应当向所在地（ ）提出申请，并提交相应材料。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "县级人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级人民政府兽医主管部门",
          "label": "B"
        },
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "省级人民政府兽医主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "(       )制定并组织实施动物疫病净化、消灭规划。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "国务院农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省、自治区、直辖市人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "省、自治区、直辖市人民政府",
          "label": "C"
        },
        {
          "content": "国务院",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政许可法》，省、自治区、直辖市人民政府对行政法规设定的有关经济事务的行政许可，根据本行政区域经济和社会发展情况，认为通过公民、法人或者其他组织能够自主决定的，\r\n或市场竞争机制能够有效调节的，或行业组织或者中介机构能够自律管理的，或行政机关采用事后监督等其他行政管理方式能够解决的，报\r\n（     ）批准后，可以在本行政区域内停止实施该行政许可。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "省、自治区、直辖市人民代表大会",
          "label": "A"
        },
        {
          "content": "国务院",
          "label": "B"
        },
        {
          "content": "全国人民代表大会",
          "label": "C"
        },
        {
          "content": "国务院各部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列有关国家建立农产品质量安全风险监测制度说法正确的是：（ ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "国务院农业农村主管部门应当制定国家农产品质量安全风险监测计划，并对重点区域、重点农产品品种进行质量安全风险监测。",
          "label": "A"
        },
        {
          "content": "省、自治区、直辖市人民政府农业农村主管部门制定本行政区域的农产品质量安全风险监测实施方案，并报国务院农业农村主管部门备案。",
          "label": "B"
        },
        {
          "content": "省、自治区、直辖市人民政府农业农村主管部门负责组织实施本行政区域的农产品质量安全风险监测。",
          "label": "C"
        },
        {
          "content": "县级以上人民政府市场监督管理部门和其他有关部门获知有关农产品质量安全风险信息后，应当立即核实并向同级农业农村主管部门通报。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "农业农村部负责组建国家官方兽医培训师资库，加强师资（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "选拔",
          "label": "A"
        },
        {
          "content": "培训",
          "label": "B"
        },
        {
          "content": "考核",
          "label": "C"
        },
        {
          "content": " 动态管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列违反农产品质量安全法有关规定处罚正确的是：（ ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "冒用农产品质量标志的，应责令改正，没收违法所得，并按货值金额处罚款",
          "label": "A"
        },
        {
          "content": "未按照规定开具承诺达标合格证的，应给予批评教育，责令限期改正",
          "label": "B"
        },
        {
          "content": "销售其他不符合农产品质量安全标准的农产品的，应按货值金额处罚款",
          "label": "C"
        },
        {
          "content": "伪造、变造农产品生产记录的，应责令限期改正",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，动物卫生监督机构接到申报后，有下列(    )情形之一的，可以不予受理，并说明理由。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "申报材料不齐全，告知后申报人拒不补正的",
          "label": "A"
        },
        {
          "content": "申报的动物、动物产品不属于本行政区域的",
          "label": "B"
        },
        {
          "content": "申报的动物、动物产品不属于动物检疫范围的",
          "label": "C"
        },
        {
          "content": "农业农村部规定不应当检疫的动物、动物产品",
          "label": "D"
        },
        {
          "content": "法律法规规定的其他不予受理的情形",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员参与或者支持迷信活动，造成不良影响的，予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "畜禽养殖者应当按照国家关于畜禽标识管理的规定，在应当加施标识的畜禽的任意部位加施标识。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《生猪屠宰管理条例》规定，生猪定点屠宰厂（场）应当有（       ）的屠宰技术人员。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "取得职业资格",
          "label": "A"
        },
        {
          "content": "经过岗前培训",
          "label": "B"
        },
        {
          "content": "签订正式劳动合同",
          "label": "C"
        },
        {
          "content": "依法取得健康证明",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "省、自治区、直辖市根据当地实际情况，可以决定将基层管理迫切需要的县级人民政府部门的行政处罚权交由能够有效承接的（    ）行使，并定期组织评估。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "具有公共事务职能的组织",
          "label": "A"
        },
        {
          "content": "受委托组织",
          "label": "B"
        },
        {
          "content": "乡镇人民政府、街道办事处",
          "label": "C"
        },
        {
          "content": "具有行政执法资格的人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽胴体及生皮、原毛、绒、脏器、血液、蹄、头、角等直接从（    ）的畜禽产品出证。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "屠宰场生产",
          "label": "A"
        },
        {
          "content": "屠宰场销售",
          "label": "B"
        },
        {
          "content": "屠宰线生产",
          "label": "C"
        },
        {
          "content": "屠宰线分割",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "进出口动物和动物产品，承运人凭（    ）运递。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "进口报关单证或者海关签发的检疫单证",
          "label": "A"
        },
        {
          "content": "进口报关单证",
          "label": "B"
        },
        {
          "content": "海关签发的检疫单证",
          "label": "C"
        },
        {
          "content": "动物检验单证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（      ）应当定期对本行政区域的强制免疫计划实施情况和效果进行评估，并向社会公布评估结果。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "县级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "病死畜禽和病害畜禽产品无害化处理场所应未安装视频监控设备，对病死畜禽和病害畜禽产品进（出）场、交接、处理和处理产物存放等进行全程监控的，由县级以上地方人民政府农业农村主管部门责令改正；拒不改正或者情节严重的，处（     ）罚款。 ",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "一千元以上五千元以下",
          "label": "A"
        },
        {
          "content": "二千元以上一万元以下",
          "label": "B"
        },
        {
          "content": "五千元以上二万元以下",
          "label": "C"
        },
        {
          "content": "二千元以上二万元以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "地方各级（）对辖区内动物防疫工作负总责，组织有关部门按照职责分工，落实强制免疫工作任务。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "人民政府",
          "label": "A"
        },
        {
          "content": "农业农村局",
          "label": "B"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "畜牧局",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "省级农业农村部门要定期开展畜间布病流行病学调查，掌握本地区布病疫情发生规律、流行趋势和风险因素，对（）进行汇总分析，及时对疫情进行预警。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "免疫状况 ",
          "label": "A"
        },
        {
          "content": "调运情况",
          "label": "B"
        },
        {
          "content": "流行动态",
          "label": "C"
        },
        {
          "content": "监测结果",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国畜牧法》提出，县级以上人民政府应当将畜牧业发展纳入国民经济和社会发展规划，加强畜牧业基础设施建设，鼓励和扶持发展（     ）养殖。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "现代化",
          "label": "A"
        },
        {
          "content": "规模化",
          "label": "B"
        },
        {
          "content": "标准化",
          "label": "C"
        },
        {
          "content": "智能化",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下哪些属于编制省级病死畜禽和病害畜禽产品集中无害化处理场所建设规划时要考虑的因素（     ）。",
      "standard_answer": "A,C,D,E",
      "option_list": [
        {
          "content": "本行政区域畜牧业发展规划",
          "label": "A"
        },
        {
          "content": "年度畜禽产品调入情况",
          "label": "B"
        },
        {
          "content": "畜禽养殖",
          "label": "C"
        },
        {
          "content": "疫病发生",
          "label": "D"
        },
        {
          "content": "畜禽死亡",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所未按规定报告动物防疫条件情况和防疫制度执行情况的，应（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "县级以上地方人民政府农业农村主管部门责令改正",
          "label": "A"
        },
        {
          "content": "可以处一万元以下罚款",
          "label": "B"
        },
        {
          "content": "拒不改正的，处一万元以上五万元以下罚款，并可以责令停业整顿",
          "label": "C"
        },
        {
          "content": "相关负责人员自处罚决定作出之日起五年内不得从事相关活动",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，农业农村部（    ）检疫规程，明确动物检疫的范围、对象和程序。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "制定",
          "label": "A"
        },
        {
          "content": "修改",
          "label": "B"
        },
        {
          "content": "公布",
          "label": "C"
        },
        {
          "content": "调整",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国畜牧法》规定，经过驯化和选育而成的驯养动物符合下列哪些条件的，可以列入畜禽遗传资源目录。（     ）",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "遗传性状稳定",
          "label": "A"
        },
        {
          "content": "有成熟的品种",
          "label": "B"
        },
        {
          "content": "有一定的种群规模",
          "label": "C"
        },
        {
          "content": "能够不依赖于野生种群而独立繁衍",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "从事动物产品（     ）等活动的单位和个人，依照《动物防疫法》和国务院农业农村主管部门的规定，做好动物疫病防疫工作，承担动物防疫责任。",
      "standard_answer": "B,C,D,E",
      "option_list": [
        {
          "content": "分销",
          "label": "A"
        },
        {
          "content": "生产",
          "label": "B"
        },
        {
          "content": "经营",
          "label": "C"
        },
        {
          "content": "加工",
          "label": "D"
        },
        {
          "content": "贮藏",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "官方兽医跨县（区）工作调动，应当携原证上岗。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政处罚决定依法作出后，当事人应当自收到行政处罚决定书之日起（    ）内，到指定的银行或者通过电子支付系统缴纳罚款。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "十日",
          "label": "A"
        },
        {
          "content": "六十日",
          "label": "B"
        },
        {
          "content": "三十日",
          "label": "C"
        },
        {
          "content": "十五日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，监察机关应当按照（    ），加强对公职人员的监督，依法给予违法的公职人员政务处分。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "管理权限",
          "label": "A"
        },
        {
          "content": "法律规定",
          "label": "B"
        },
        {
          "content": "监督职责",
          "label": "C"
        },
        {
          "content": "部门规定",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "(     )制定并组织实施本行政区域的无规定动物疫病区建设方案。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "县级以上地方人民政府",
          "label": "A"
        },
        {
          "content": "省、自治区、直辖市人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "省、自治区、直辖市人民政府",
          "label": "C"
        },
        {
          "content": "县级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "跨县级以上行政区域运输病死畜禽和病害畜禽产品的，相关区域（     ）应当加强协作配合，及时通报紧急情况，落实监管责任。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "市级以上地方人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "共同的上一级人民政府农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，国家实行（    ）审查制度。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物防疫条件",
          "label": "A"
        },
        {
          "content": "动物检疫证明",
          "label": "B"
        },
        {
          "content": "动物防疫条件合格证",
          "label": "C"
        },
        {
          "content": "动物检疫条件",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪定点屠宰厂（场）变更屠宰厂（场）名称、法定代表人（负责人）的，应当在市场监督管理部门办理变更登记手续后（       ），向原发证机关办理变更生猪定点屠宰证书。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "15个工作日内",
          "label": "A"
        },
        {
          "content": "14个工作日内",
          "label": "B"
        },
        {
          "content": "10个工作日内",
          "label": "C"
        },
        {
          "content": "7个工作日内",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（       ）应当加强生猪定点屠宰的宣传教育，协助做好生猪屠宰监督管理工作。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "省级人民政府",
          "label": "A"
        },
        {
          "content": "市级以上地方人民政府",
          "label": "B"
        },
        {
          "content": "乡镇人民政府、街道办事处",
          "label": "C"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "坚持科技引领，推动畜间布病快速鉴别诊断技术和防控模式创新应用，加强基层防控能力建设，统筹利用（）等技术力量，不断提高布病防控技术支撑水平。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "国家兽医实验室",
          "label": "A"
        },
        {
          "content": "疫控机构",
          "label": "B"
        },
        {
          "content": "科研教学单位",
          "label": "C"
        },
        {
          "content": "龙头企业",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各省级农业农村部门、财政部门要结合（     ）等因素，会同相关部门制定本省（区、市）无害化处理场布局规划，实行总量控制，留足发展空间。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "养殖量",
          "label": "A"
        },
        {
          "content": "养殖布局",
          "label": "B"
        },
        {
          "content": "畜禽产品消费量",
          "label": "C"
        },
        {
          "content": "畜禽死亡数",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "（     ）可以决定一个行政机关行使有关行政机关的行政处罚权。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省、自治区、直辖市人民政府",
          "label": "B"
        },
        {
          "content": "设区的市级人民政府",
          "label": "C"
        },
        {
          "content": "县级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下生猪屠宰后未经加工的产品中，属于《生猪屠宰管理条例》所称生猪产品的有（       ）。",
      "standard_answer": "A,B,C,E",
      "option_list": [
        {
          "content": "胴体、肉、脂、脏器",
          "label": "A"
        },
        {
          "content": "骨、头、蹄",
          "label": "B"
        },
        {
          "content": "血液",
          "label": "C"
        },
        {
          "content": "毛",
          "label": "D"
        },
        {
          "content": "皮",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员有下列（）行为的，予以记过或者记大过。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "散布有损宪法权威言论的",
          "label": "A"
        },
        {
          "content": "参加旨在反对中国共产党领导和国家的集会、游行活动的",
          "label": "B"
        },
        {
          "content": "拒不执行国家的路线方针政策的",
          "label": "C"
        },
        {
          "content": "参加非法组织、非法活动的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列关于违反生物安全法规定需承担的相关法律责任正确的是：（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "瞒报、谎报、缓报、漏报，授意他人瞒报、谎报、缓报，或者阻碍他人报告传染病、动植物疫病或者不明原因的聚集性疾病的，由县级以上人民政府有关部门责令改正，给予警告。",
          "label": "A"
        },
        {
          "content": "编造、散布虚假的生物安全信息，构成违反治安管理行为的，由公安机关依法给予治安管理处罚。",
          "label": "B"
        },
        {
          "content": "从事生物技术研究、开发活动未遵守相关规范，经警告后拒不改正或者造成严重后果的，责令停止研究、开发活动，并处以罚款。",
          "label": "C"
        },
        {
          "content": "将使用后的实验动物流入市场，情节严重的，由主管部门责令改正，没收违法所得，并处以罚款。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "关于行政处罚，两个以上行政机关都有管辖权，但对管辖发生争议的，协商不成的，（    ）指定管辖。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "同级人民政府指定的行政机关",
          "label": "A"
        },
        {
          "content": "报请共同的上一级行政机关",
          "label": "B"
        },
        {
          "content": "直接由共同的上一级行政机关",
          "label": "C"
        },
        {
          "content": "报请共同的上一级行政机关或直接由共同的上一级行政机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对涉及重大公共利益的违法行为给予行政处罚，在行政机关负责人作出行政处罚的决定之前，\r\n应当由（      ）进行法制审核。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "行政机关负责人",
          "label": "A"
        },
        {
          "content": "司法机关",
          "label": "B"
        },
        {
          "content": "法制审核人员",
          "label": "C"
        },
        {
          "content": "执法人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，屠宰动物的，应当提前（    ）向所在地动物卫生监督机构申报检疫。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "3小时",
          "label": "A"
        },
        {
          "content": "8小时",
          "label": "B"
        },
        {
          "content": "12小时",
          "label": "C"
        },
        {
          "content": "6小时",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，基层群众性自治组织中从事管理的人员有违法行为的，（）可以予以警告、记过、记大过。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "检查机关",
          "label": "A"
        },
        {
          "content": "监察机关",
          "label": "B"
        },
        {
          "content": "机关党委",
          "label": "C"
        },
        {
          "content": "机管纪委",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员不按照规定公开工作信息，侵犯管理服务对象知情权，造成不良后果或者影响，情节严重的，予以（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "违反《动物防疫法》规定,违反本法规定，给他人人身、财产造成损害的，依法承担（     ）责任。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "刑事",
          "label": "A"
        },
        {
          "content": "赔偿",
          "label": "B"
        },
        {
          "content": "民事",
          "label": "C"
        },
        {
          "content": "补偿",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》适用于中华人民共和国领域内的（  ）的检疫及其监督管理活动。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "畜禽、畜禽产品",
          "label": "A"
        },
        {
          "content": "动物及其产品",
          "label": "B"
        },
        {
          "content": "动物、动物产品",
          "label": "C"
        },
        {
          "content": "畜禽及其产品",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员散布有损宪法权威、中国共产党领导和国家声誉的言论的，予以（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》中，从专门经营动物的集贸市场继续出售或者运输的已经取得产地检疫证明的动物，出具动物检疫证明，需要符合的检疫条件不包括（    )。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "申报材料符合检疫规程规定",
          "label": "A"
        },
        {
          "content": "临床检查健康",
          "label": "B"
        },
        {
          "content": "按规定需要进行实验室疫病检测的，检测结果合格",
          "label": "C"
        },
        {
          "content": "有原始动物检疫证明和完整的进出场记录",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，任何单位和个人不得持有或者使用（    ）的动物检疫证章标志。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "伪造",
          "label": "A"
        },
        {
          "content": "电子",
          "label": "B"
        },
        {
          "content": "变造",
          "label": "C"
        },
        {
          "content": "转让",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《全国畜间人兽共患病防治规划2022—2030年）》明确的重点防治病种的是（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "布病",
          "label": "A"
        },
        {
          "content": "牛结核病",
          "label": "B"
        },
        {
          "content": "狂犬病",
          "label": "C"
        },
        {
          "content": "日本血吸虫病",
          "label": "D"
        },
        {
          "content": "沙门氏菌病",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，要求，各有关地区要加强对经指定通道运输动物的监督检查，需要查验的内容包括（   ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "动物检疫证明是否有效",
          "label": "A"
        },
        {
          "content": "牲畜耳标是否佩戴齐全",
          "label": "B"
        },
        {
          "content": "运输车辆是否按规定备案",
          "label": "C"
        },
        {
          "content": "动物健康状况有无异常",
          "label": "D"
        },
        {
          "content": "证物是否相符",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "出现下列哪些情形，作出行政许可决定的行政机关或者其上级行政机关，根据利害关系人的请求或者依据职权，可以撤销行政许可。（      ）\r\n    ",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "行政机关工作人员滥用职权、玩忽职守作出准予行政许可决定的\r\n    ",
          "label": "A"
        },
        {
          "content": "超越法定职权作出准予行政许可决定的\r\n    ",
          "label": "B"
        },
        {
          "content": "违反法定程序作出准予行政许可决定的",
          "label": "C"
        },
        {
          "content": "对不具备申请资格或者不符合法定条件的申请人准予行政许可的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "运输用于继续饲养的畜禽到达目的地后，未向启运地动物卫生监督机构报告的，由县级以上地方人民政府农业农村主管部门处(   )罚款；情节严重的，处(   )罚款。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "一千元以上五千元以下",
          "label": "A"
        },
        {
          "content": "一千元以上三千元以下",
          "label": "B"
        },
        {
          "content": "三千元以上三万元以下",
          "label": "C"
        },
        {
          "content": "五千元以上五万元以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "按照农产品质量安全法，隐瞒、谎报、缓报农产品质量安全事故或者隐匿、伪造、毁灭有关证据的，应对直接负责的主管人员和其他直接责任人员（ ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "给予记大过处分",
          "label": "A"
        },
        {
          "content": "情节较重的，给予降级或者撤职处分",
          "label": "B"
        },
        {
          "content": "情节严重的，给予开除处分",
          "label": "C"
        },
        {
          "content": "造成严重后果的，其主要负责人还应当引咎辞职",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《行政处罚法》，哪些行政处罚应当告知当事人有要求听证的权利。（       ）",
      "standard_answer": "A,B,C,D,E,F",
      "option_list": [
        {
          "content": "较大数额罚款",
          "label": "A"
        },
        {
          "content": "没收较大数额违法所得、没收较大价值非法财物",
          "label": "B"
        },
        {
          "content": "降低资质等级、吊销许可证件",
          "label": "C"
        },
        {
          "content": "责令停产停业、责令关闭、限制从业",
          "label": "D"
        },
        {
          "content": "其他较重的行政处罚",
          "label": "E"
        },
        {
          "content": "法律、法规、规章规定的其他情形",
          "label": "F"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物防疫法》规定，动物卫生监督机构及其工作人员违反本法规定，有下列行为之一的，由本级人民政府或者农业农村主管部门责令改正，通报批评；对直接负责的主管人员和其他直接责任人员依法给予处分：（    ）",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "对未经检疫或者检疫不合格的动物、动物产品出具检疫证明、加施检疫标志",
          "label": "A"
        },
        {
          "content": "对检疫合格的动物、动物产品拒不出具检疫证明、加施检疫标志的",
          "label": "B"
        },
        {
          "content": "对附有检疫证明、检疫标志的动物、动物产品重复检疫的",
          "label": "C"
        },
        {
          "content": "从事与动物防疫有关的经营性活动，或者违法收取费用的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "到2026年，全国畜间布病总体流行率有效降低，牛羊群体健康水平明显提高，个体阳性率控制在7%以下。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "违法事实不清、证据不足的，（     ）行政处罚。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "应当从轻",
          "label": "A"
        },
        {
          "content": "应当减轻",
          "label": "B"
        },
        {
          "content": "应当从轻或减轻",
          "label": "C"
        },
        {
          "content": "不得给予",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫证明填写中，以下关于产品A到达时效最长不得超过（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "5天",
          "label": "A"
        },
        {
          "content": "7天",
          "label": "B"
        },
        {
          "content": "当日",
          "label": "C"
        },
        {
          "content": "6个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，给与警告处分的，政务处分期为（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "六个月",
          "label": "A"
        },
        {
          "content": "十二个月",
          "label": "B"
        },
        {
          "content": "十八个月",
          "label": "C"
        },
        {
          "content": "二十四个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（       ）应当将其确定的生猪定点屠宰厂（场）名单及时向社会公布。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "设区的市级人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "设区的市级人民政府",
          "label": "B"
        },
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "省级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员利用宗族或者黑恶势力等欺压群众，或者纵容、包庇黑恶势力活动，情节严重的，予以（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "开除",
          "label": "A"
        },
        {
          "content": "撤职",
          "label": "B"
        },
        {
          "content": "留党察看",
          "label": "C"
        },
        {
          "content": "党内警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政处罚法》，违法行为在（    ）年内未被发现的，不再给予行政处罚。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "二",
          "label": "A"
        },
        {
          "content": "三",
          "label": "B"
        },
        {
          "content": "五",
          "label": "C"
        },
        {
          "content": "十",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员因犯罪被单处罚金，或者犯罪情节轻微，人民检察院依法作出不起诉决定或者人民法院依法免予刑事处罚，造成不良影响的，予以（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "开除",
          "label": "A"
        },
        {
          "content": "撤职",
          "label": "B"
        },
        {
          "content": "留党察看",
          "label": "C"
        },
        {
          "content": "党内警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "要严格按照相关技术规范对病死畜禽进行无害化处理，逐步减少（     ）等处理方式，确保有效杀灭病原体，清洁安全，不污染环境。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "深埋",
          "label": "A"
        },
        {
          "content": "炭化焚烧法",
          "label": "B"
        },
        {
          "content": "化尸窖",
          "label": "C"
        },
        {
          "content": "堆肥",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "坚持人病兽防、关口前移，重点抓好（）的布病防控，",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "种牛",
          "label": "A"
        },
        {
          "content": "种羊",
          "label": "B"
        },
        {
          "content": "奶牛",
          "label": "C"
        },
        {
          "content": "奶山羊",
          "label": "D"
        },
        {
          "content": "肉羊",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "坚持夯实基础，不断强化基层动物防疫体系建设，压实（）责任，注重布病防控与各项支持政策相衔接，构建系统化、规范化、长效化政策制度和工作推进机制。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "属地管理",
          "label": "A"
        },
        {
          "content": "部门监管",
          "label": "B"
        },
        {
          "content": "生产经营者主体",
          "label": "C"
        },
        {
          "content": "科研教学单位",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国家根据生猪定点屠宰厂（场）的（     ），推行生猪定点屠宰厂（场）分级管理制度。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "规模",
          "label": "A"
        },
        {
          "content": "自然屏障",
          "label": "B"
        },
        {
          "content": "生产和技术条件",
          "label": "C"
        },
        {
          "content": "质量安全管理状况",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公办的教育、科研、文化、医疗卫生、体育等单位中从事管理的人员，受到撤职政务处分的，降低（），同时降低薪酬待遇。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "职务",
          "label": "A"
        },
        {
          "content": "岗位",
          "label": "B"
        },
        {
          "content": "职员等级",
          "label": "C"
        },
        {
          "content": "信用等级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生物安全是国家安全的重要组成部分。维护生物安全应当贯彻总体国家安全观，统筹发展和安全，坚持（ ）的原则。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "以人为本",
          "label": "A"
        },
        {
          "content": "风险预防",
          "label": "B"
        },
        {
          "content": "分类管理",
          "label": "C"
        },
        {
          "content": "协同配合",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，禁止（    ）检疫证明、检疫标志或者畜禽标识。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "转让",
          "label": "A"
        },
        {
          "content": "伪造",
          "label": "B"
        },
        {
          "content": "查验",
          "label": "C"
        },
        {
          "content": "变造",
          "label": "D"
        },
        {
          "content": "使用",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关在作出行政处罚决定之前，应当告知当事人拟作出的（     ）",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "行政处罚内容",
          "label": "A"
        },
        {
          "content": "事实",
          "label": "B"
        },
        {
          "content": "理由",
          "label": "C"
        },
        {
          "content": "依据",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，动物卫生监督机构接到检疫申报后，应当及时指派官方兽医对动物、动物产品实施检疫；检疫合格的，出具检疫证明、加施检疫标志。（　　）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）通知要求，已经确定指定通道的，要定期开展本省份及周边区域动物疫病、路网布局、畜禽流通等情况的分析研判，并综合考虑已有站点资源、监管力量配备、工作条件保障等因素，适时调整完善指定通道布局。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，发生一类动物疫病时，当地县级以上地方人民政府农业农村主管部门应当立即派人到现场，划定疫点、疫区、受威胁区，调查疫源，及时对疫区实行封锁。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "养蜂生产者在国内转地放蜂，凭国务院农业农村主管部门统一格式印制的检疫证明运输蜂群，转地放蜂时都应当申报检疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》明确了常规防治病种的防治目标是（     ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "个别病种实现消灭",
          "label": "A"
        },
        {
          "content": "流行率稳定控制在较低水平",
          "label": "B"
        },
        {
          "content": "得到有效控制",
          "label": "C"
        },
        {
          "content": "传入和扩散风险有效降低",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，（    ）由县级以上人民政府农业农村主管部门认定。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "重大动物疫情",
          "label": "A"
        },
        {
          "content": "动物疫情",
          "label": "B"
        },
        {
          "content": "重大动物疫病",
          "label": "C"
        },
        {
          "content": "动物疫病",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "违反《中华人民共和国畜牧法》规定，畜禽养殖场未建立养殖档案的，由县级以上地方人民政府农业农村主管部门责令限期改正，可以处（       ）罚款。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "二万元以下",
          "label": "A"
        },
        {
          "content": "一万元以下",
          "label": "B"
        },
        {
          "content": "五千元以下",
          "label": "C"
        },
        {
          "content": "二千元以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对全国所有羊进行（）免疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "小反刍兽疫",
          "label": "A"
        },
        {
          "content": "羊痘",
          "label": "B"
        },
        {
          "content": "布鲁氏菌病",
          "label": "C"
        },
        {
          "content": "蓝舌病",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，（   ）加强信息化建设，建立全国统一的动物检疫管理信息化系统，实现动物检疫信息的可追溯。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "省级农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，（   ）以外的其他水生动物及其产品不实施检疫。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "苗种",
          "label": "A"
        },
        {
          "content": "除遗传育种材料",
          "label": "B"
        },
        {
          "content": "除水生动物的亲本",
          "label": "C"
        },
        {
          "content": "水产苗种",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "被许可人以欺骗、贿赂等不正当手段取得行政许可的，应当（      ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "予以撤销",
          "label": "A"
        },
        {
          "content": "予以注销",
          "label": "B"
        },
        {
          "content": "可以撤销",
          "label": "C"
        },
        {
          "content": "可以注销",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "县级以上人民政府农业农村主管部门应当建立健全（ ）机制，按照监督抽查计划，组织开展农产品质量安全监督抽查。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "随机抽查",
          "label": "A"
        },
        {
          "content": "任意抽查",
          "label": "B"
        },
        {
          "content": "判断抽查",
          "label": "C"
        },
        {
          "content": "以上都不是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《动物防疫法》规定，禁止屠宰、经营、运输的动物有（    ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "封锁疫区内与所发生的动物疫病有关的",
          "label": "A"
        },
        {
          "content": "疫区内易感染的",
          "label": "B"
        },
        {
          "content": "依法应当检疫而未经检疫或者检疫不合格的",
          "label": "C"
        },
        {
          "content": "以上全都是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "为了维护国家安全，防范和应对生物安全风险，保障人民生命健康，保护生物资源和生态环境，促进生物技术健康发展，推动构建（ ），实现人与自然和谐共生，制定《生物安全法》。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "人类命运共同体",
          "label": "A"
        },
        {
          "content": "人类健康环境",
          "label": "B"
        },
        {
          "content": "动物福利保障机制",
          "label": "C"
        },
        {
          "content": "生物安全体制",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号），就加强跨省份道路运输动物指定通道（     ）等工作进行了通知。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "公布",
          "label": "A"
        },
        {
          "content": "监管",
          "label": "B"
        },
        {
          "content": "建设",
          "label": "C"
        },
        {
          "content": "管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《三类动物疫病防治规范》，治疗畜禽寄生虫病后，应及时收集（     ），并进行无害化处理。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "掉落的毛、羽毛",
          "label": "A"
        },
        {
          "content": "排出的虫体",
          "label": "B"
        },
        {
          "content": "排出的粪便",
          "label": "C"
        },
        {
          "content": "脱落的皮肤",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列情况应当应当提前三十日向原发证机关报告，提交审查的是：（ ）。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "变更单位名称",
          "label": "A"
        },
        {
          "content": "变更法定代表人（负责人）",
          "label": "B"
        },
        {
          "content": "变更布局、设施设备和制度",
          "label": "C"
        },
        {
          "content": "其他可能引起动物防疫条件发生变化的情况",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，已经确定指定通道的，要定期开展本省份及周边区域（   ）等情况的分析研判。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "动物疫病",
          "label": "A"
        },
        {
          "content": "路网布局",
          "label": "B"
        },
        {
          "content": "畜禽流通",
          "label": "C"
        },
        {
          "content": "无害化处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "（ ）建立生物安全工作协调机制，组织协调、督促推进本行政区域内生物安全相关工作。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "中央",
          "label": "A"
        },
        {
          "content": "省",
          "label": "B"
        },
        {
          "content": "自治区",
          "label": "C"
        },
        {
          "content": "直辖市",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下是我国畜间人兽共患病防治工作面临的困难挑战的有（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": " 畜间人兽共患病种类多，病原复杂，流行范围广",
          "label": "A"
        },
        {
          "content": "基层动物防疫体系职能淡化、力量弱化、支持虚化等问题比较突出",
          "label": "B"
        },
        {
          "content": "畜禽养殖总量大，规模化程度总体不高，生物安全水平较低",
          "label": "C"
        },
        {
          "content": "周边及主要贸易国家和地区动物疫情频发，多种外部风险因素相互交织，防治任务繁重艰巨",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "从事动物运输的单位、个人应当妥善保存（     ）等信息。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "行程路线",
          "label": "A"
        },
        {
          "content": "运输的动物名称",
          "label": "B"
        },
        {
          "content": "运输的动物数量",
          "label": "C"
        },
        {
          "content": "检疫证明编号",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，县级人民政府农业农村主管部门可以根据动物检疫工作需要，向乡、镇或者特定区域派驻（         ）。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "协检员",
          "label": "B"
        },
        {
          "content": "执业兽医",
          "label": "C"
        },
        {
          "content": "官方兽医",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列说法错误的是（      ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "行政机关实施行政许可，可以向申请人提出购买指定商品",
          "label": "A"
        },
        {
          "content": "行政机关工作人员办理行政许可，不得收受申请人的财务",
          "label": "B"
        },
        {
          "content": "行政机关工作人员办理行政许可，不得向申请人收取费用，但法律法规另有规定的除外",
          "label": "C"
        },
        {
          "content": "行政机关实施行政许可，不得接受有偿服务",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，监察机关发现公职人员任免机关、单位应当给予处分而未给予，或者给予的处分违法、不当的，应当及时提出（   ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "抗诉",
          "label": "A"
        },
        {
          "content": "管理建议",
          "label": "B"
        },
        {
          "content": "处分建议",
          "label": "C"
        },
        {
          "content": "监察建议",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国畜牧法》规定，（       ）农业农村主管部门应当组织制定畜禽生产规范，指导畜禽的安全生产。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "省级以上人民政府",
          "label": "A"
        },
        {
          "content": "市级以上人民政府",
          "label": "B"
        },
        {
          "content": "县级以上人民政府",
          "label": "C"
        },
        {
          "content": "国务院",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要完善(    )系统，及时采集、更新指定通道及有关检查站名称、位置坐标等基础信息，并实现信息共享。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "动物检疫出证",
          "label": "A"
        },
        {
          "content": "动物检疫通知单出证",
          "label": "B"
        },
        {
          "content": "动物检疫证明电子出证",
          "label": "C"
        },
        {
          "content": "动物检疫证明出证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "病死畜禽和病害畜禽产品收集、无害化处理、资源化利用应当符合农业农村部相关技术规范, 并采取（     ），防止传播动物疫病。 ",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "专用处理设施设备",
          "label": "A"
        },
        {
          "content": "专用材料包装",
          "label": "B"
        },
        {
          "content": "专用车辆运输",
          "label": "C"
        },
        {
          "content": "必要的防疫措施",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "为加强畜间布病防控，降低（），促进畜牧业高质量发展，维护人民群众身体健康，制定本方案。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "发病率",
          "label": "A"
        },
        {
          "content": "死亡率",
          "label": "B"
        },
        {
          "content": "流行率 ",
          "label": "C"
        },
        {
          "content": " 传播风险",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国畜牧法》规定，下列属于畜禽养殖场应当具备的条件有（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "有与其饲养规模相适应的生产场所和配套的生产设施",
          "label": "A"
        },
        {
          "content": "有为其服务的畜牧兽医技术人员",
          "label": "B"
        },
        {
          "content": "\r\n具备法律、行政法规和国务院农业农村主管部门规定的防疫条件",
          "label": "C"
        },
        {
          "content": "\r\n有与畜禽粪污无害化处理和资源化利用相适应的设施设备",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "规范检疫出证，强化检疫监管，监督养殖主体和贩运人严格落实动物（）制度，实现动物调运启运地、途经地、目的地全程可追溯。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "养殖备案",
          "label": "A"
        },
        {
          "content": "检疫申报",
          "label": "B"
        },
        {
          "content": "强制免疫",
          "label": "C"
        },
        {
          "content": "落地报告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，下列有权发布封锁令的机关是（　　）。",
      "standard_answer": "B,D,E",
      "option_list": [
        {
          "content": "××省农业厅",
          "label": "A"
        },
        {
          "content": "××县人民政府",
          "label": "B"
        },
        {
          "content": "农业农村部",
          "label": "C"
        },
        {
          "content": "××省人民政府",
          "label": "D"
        },
        {
          "content": "××市人民政府",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员有下列（   ）情形的，可以从轻或者减轻给与政务处分。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "主动交代本人应当受到政务处分的违法行为的",
          "label": "A"
        },
        {
          "content": "配合调查，如实说明本人违法事实的",
          "label": "B"
        },
        {
          "content": "检举他人违纪违法行为，经查证属实的",
          "label": "C"
        },
        {
          "content": "主动采取措施，有效避免、挽回损失或者消除不良影响的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农办牧〔2021〕38号》通知要求，发现违法违规行为的，要依法处理或者按照职责分工移送有关部门。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "布病强制免疫工作有效开展，免疫地区免疫密度常年保持在90%以上，免疫建档率100%，免疫奶牛场备案率()%。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "70",
          "label": "A"
        },
        {
          "content": "80",
          "label": "B"
        },
        {
          "content": "90",
          "label": "C"
        },
        {
          "content": "100",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，宣讲指定通道制度内容及相关法律责任，提高（   ）守法意识，营造良好工作氛围。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "动物检疫主体",
          "label": "A"
        },
        {
          "content": "货主",
          "label": "B"
        },
        {
          "content": "承运人",
          "label": "C"
        },
        {
          "content": "生产经营主体",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列关于动物防疫条件审查监督管理说法错误的是：（ ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "变更布局、设施设备和制度，可能引起动物防疫条件发生变化的，应当提前三十日向原发证机关报告。",
          "label": "A"
        },
        {
          "content": "变更布局、设施设备和制度，可能引起动物防疫条件发生变化的，向原发证机关报告后，发证机关应当在十五日内完成审查，并将审查结果通知申请人。",
          "label": "B"
        },
        {
          "content": "变更单位名称或者法定代表人（负责人）的，应当在变更后三十日内持有效证明申请变更动物防疫条件合格证。",
          "label": "C"
        },
        {
          "content": "禁止转让、伪造或者变造动物防疫条件合格证。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员公开发表反对反对社会主义制度，反对改革开放的文章、演说、宣言、声明等的，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《生猪屠宰管理条例》规定，生猪定点屠宰厂（场）应当有（       ）的兽医卫生检验人员。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "经考核合格",
          "label": "A"
        },
        {
          "content": "具有畜牧兽医相关专业大专以上学历",
          "label": "B"
        },
        {
          "content": "取得执业兽医资格",
          "label": "C"
        },
        {
          "content": "通过职业技能鉴定",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，病死动物，是指（    ）的死亡动物。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "染疫死亡",
          "label": "A"
        },
        {
          "content": "因病死亡",
          "label": "B"
        },
        {
          "content": "死因不明",
          "label": "C"
        },
        {
          "content": "经检验检疫可能危害人体健康",
          "label": "D"
        },
        {
          "content": "经检验检疫可能危害动物健康",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列属于证据的是（        ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "鉴定意见",
          "label": "A"
        },
        {
          "content": "物证",
          "label": "B"
        },
        {
          "content": "勘验笔录、现场笔录",
          "label": "C"
        },
        {
          "content": "证人证言",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "开展农产品质量安全监督检查，有权进行（ ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "进入生产经营场所进行现场检查，调查了解农产品质量安全的有关情况",
          "label": "A"
        },
        {
          "content": "查阅、复制农产品生产记录、购销台账等与农产品质量安全有关的资料",
          "label": "B"
        },
        {
          "content": "查封、扣押用于违法生产经营农产品的设施、设备、场所以及运输工具",
          "label": "C"
        },
        {
          "content": "收缴伪造的农产品质量标志",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各地农业农村部门要根据工作实际，采取（）方式，进一步完善官方兽医培训方式，丰富培训内容，加快补齐官方兽医业务知识技能短板，强化政风行风建设，教育引导官方兽医严格遵守法律法规、职业道德和纪律规矩，保持良好作风和工作状态。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "集中培训",
          "label": "A"
        },
        {
          "content": "日常自学",
          "label": "B"
        },
        {
          "content": "线上培训",
          "label": "C"
        },
        {
          "content": " 线下培训 ",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列属于证据的是（        ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "书证",
          "label": "A"
        },
        {
          "content": "视听资料",
          "label": "B"
        },
        {
          "content": "举报线索",
          "label": "C"
        },
        {
          "content": "电子数据",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国畜牧法》要求县级以上人民政府促进种养结合和农牧循环、绿色发展，推进畜牧产业化经营，提高畜牧业综合生产能力，发展（     ）的畜牧业。",
      "standard_answer": "A,B,C,E",
      "option_list": [
        {
          "content": "安全",
          "label": "A"
        },
        {
          "content": "优质",
          "label": "B"
        },
        {
          "content": "高效",
          "label": "C"
        },
        {
          "content": "绿色",
          "label": "D"
        },
        {
          "content": "生态",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对在岗官方兽医进行()等方面的继续教育培训，持续提升官方兽医依法履职能力和专业技术水平。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "动物检疫监督",
          "label": "A"
        },
        {
          "content": "动物卫生法学理论",
          "label": "B"
        },
        {
          "content": "重大动物疫情应急处置",
          "label": "C"
        },
        {
          "content": "官方兽医职业道德规范和行风建设",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，任何单位和个人不得（    ）已被依法隔离、封存、处理的动物和动物产品。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "藏匿",
          "label": "A"
        },
        {
          "content": "转移",
          "label": "B"
        },
        {
          "content": "盗掘",
          "label": "C"
        },
        {
          "content": "登记",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，从事动物饲养、屠宰、经营、隔离以及动物产品生产、经营、加工、贮藏等活动的单位和个人，应当按照国家有关规定做好病死动物、病害动物产品的无害化处理，不能委托进行处理。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物防疫条件审查发证评估办法由（ ）农业农村主管部门依据《中华人民共和国畜牧法》、《中华人民共和国动物防疫法》等法律法规和动物防疫条件审查办法制定。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "国务院",
          "label": "A"
        },
        {
          "content": "省级人民政府",
          "label": "B"
        },
        {
          "content": "市级人民政府",
          "label": "C"
        },
        {
          "content": "县级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国畜牧法》规定，（       ）应当按照规定对经检验、检疫不合格的畜禽产品的无害化处理的费用和损失给予补助。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "各级人民政府",
          "label": "A"
        },
        {
          "content": "各级人民政府财政主管部门",
          "label": "B"
        },
        {
          "content": "地方各级人民政府",
          "label": "C"
        },
        {
          "content": "地方各级人民政府财政主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员组织赌博的，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级",
          "label": "B"
        },
        {
          "content": "撤职或者开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列关于行政许可法律责任的说法中，错误的是\r\n（        ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "违反法定程序实施许可要承担相应的法律责任",
          "label": "A"
        },
        {
          "content": "监督不力造成严重后果的要承担相应的法律责任",
          "label": "B"
        },
        {
          "content": "申请人、被许可人违法不需要承担相应的法律责任",
          "label": "C"
        },
        {
          "content": "违法设定行政许可的要承担法律责任",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "县级以上人民政府卫生健康主管部门和本级人民政府(     )应当建立人畜共患传染病防治的协作机制。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "农业农村、野生动物保护等主管部门",
          "label": "A"
        },
        {
          "content": "农业农村主管部门",
          "label": "B"
        },
        {
          "content": "野生动物保护等主管部门",
          "label": "C"
        },
        {
          "content": "农业农村、林业草原等主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "任何单位和个人不得冒用或者使用（       ）的生猪定点屠宰证书和生猪定点屠宰标志牌。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "复制",
          "label": "A"
        },
        {
          "content": "仿造",
          "label": "B"
        },
        {
          "content": "伪造",
          "label": "C"
        },
        {
          "content": "变造",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（）是免疫主体，承担免疫责任。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "兽医站",
          "label": "A"
        },
        {
          "content": "农业农村主管部门",
          "label": "B"
        },
        {
          "content": "饲养动物的单位和个人",
          "label": "C"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（    ）组织本辖区饲养动物的单位和个人做好强制免疫，协助做好监督检查；村民委员会、居民委员会协助做好相关工作。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "乡级人民政府、街道办事处",
          "label": "A"
        },
        {
          "content": "县级人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "县级人民政府",
          "label": "C"
        },
        {
          "content": "乡级人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》所称动物防疫，是指动物疫病的（    ）和动物、动物产品的检疫，以及病死动物、病害动物产品的无害化处理。",
      "standard_answer": "A,B,D,E",
      "option_list": [
        {
          "content": "预防、控制",
          "label": "A"
        },
        {
          "content": "净化",
          "label": "B"
        },
        {
          "content": "防控",
          "label": "C"
        },
        {
          "content": "消灭",
          "label": "D"
        },
        {
          "content": "诊疗",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对新任命的官方兽医进行()等方面培训，经考试合格的方可上岗。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "动物检疫监督",
          "label": "A"
        },
        {
          "content": "畜禽屠宰管理",
          "label": "B"
        },
        {
          "content": "动物产品质量安全",
          "label": "C"
        },
        {
          "content": "官方兽医职业道德规范和行风建设",
          "label": "D"
        },
        {
          "content": "动物卫生法学理论",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，以下属于复审、复核机关应当变更原政务处分决定的情形有（）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "适用法律错误",
          "label": "A"
        },
        {
          "content": "违法行为情节认定错误",
          "label": "B"
        },
        {
          "content": "违反法定程序",
          "label": "C"
        },
        {
          "content": "政务处分不当",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物防疫法》规定，动物疫情的法定报告人有(      )",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "养殖场",
          "label": "A"
        },
        {
          "content": "动物疫病预防控制机构的工作人员",
          "label": "B"
        },
        {
          "content": "官方兽医",
          "label": "C"
        },
        {
          "content": "承运人",
          "label": "D"
        },
        {
          "content": "屠宰场",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，国务院农业农村主管部门根据行政区划、养殖屠宰产业布局、风险评估情况等对动物疫病实施分区防控，可以采取（     ）跨区域调运等措施。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "禁止特定动物",
          "label": "A"
        },
        {
          "content": "限制特定动物",
          "label": "B"
        },
        {
          "content": "禁止特定动物产品",
          "label": "C"
        },
        {
          "content": "限制特定动物产品",
          "label": "D"
        },
        {
          "content": "禁止动物、动物产品",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》明确重点防治病种高致病性禽流感、布病、牛结核病、狂犬病、炭疽、包虫病、沙门氏菌病、马鼻疽。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "以种畜场、奶畜场和规模牛羊场为重点，全面开展布病净化场和无疫小区建设。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "动物防疫实行预防为主，预防与控制、净化、消灭相结合的方针。（   ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物检疫中需要的实验室疫病检测报告应当由动物疫病预防控制机构、取得相关资质认定、国家认可机构认可或者符合（    ）规定条件的实验室出具。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "县级农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "省级动物卫生监督机构",
          "label": "C"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，要建立指定通道监管信息实时采集、上传功能，并将数据共享至（   ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "动物卫生综合信息平台",
          "label": "A"
        },
        {
          "content": "动物检疫综合信息平台",
          "label": "B"
        },
        {
          "content": "兽医卫生信息平台",
          "label": "C"
        },
        {
          "content": "兽医卫生综合信息平台",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员受到开除以外的政务处分，在政务处分期内有悔改表现，并且没有再发生应当给予政务处分的违法行为的，政务处分期满后（）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "一周内解除",
          "label": "A"
        },
        {
          "content": "由原作出政务处分决定的机管解除",
          "label": "B"
        },
        {
          "content": "由当事人申请批准后解除",
          "label": "C"
        },
        {
          "content": "自动解除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《中华人民共和国畜牧法》的规定，运输畜禽，应当符合法律、行政法规和国务院农业农村主管部门规定的（       ），采取措施保护畜禽安全。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物防疫条件",
          "label": "A"
        },
        {
          "content": "空间条件",
          "label": "B"
        },
        {
          "content": "饲喂条件",
          "label": "C"
        },
        {
          "content": "饮水条件",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，动物诊疗许可证载明事项变更的，应当（      ）动物诊疗许可证。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "申请变更",
          "label": "A"
        },
        {
          "content": "申请换发",
          "label": "B"
        },
        {
          "content": "申请注销",
          "label": "C"
        },
        {
          "content": "重新申请取得",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "违反《生猪屠宰管理条例》规定，生猪定点屠宰厂（场）有下列（       ）情形之一的，由农业农村主管部门责令改正，给予警告；拒不改正的，责令停业整顿，处5000元以上5万元以下的罚款，对其直接负责的主管人员和其他直接责任人员处2万元以上5万元以下的罚款；情节严重的，由设区的市级人民政府吊销生猪定点屠宰证书，收回生猪定点屠宰标志牌。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "未按照规定建立并遵守生猪进厂（场）查验登记制度、生猪产品出厂（场）记录制度的",
          "label": "A"
        },
        {
          "content": "未按照规定签订、保存委托屠宰协议的",
          "label": "B"
        },
        {
          "content": "屠宰生猪不遵守国家规定的操作规程、技术要求和生猪屠宰质量管理规范以及消毒技术规范的",
          "label": "C"
        },
        {
          "content": "未按照规定建立并遵守肉品品质检验制度的",
          "label": "D"
        },
        {
          "content": "对经肉品品质检验不合格的生猪产品未按照国家有关规定处理并如实记录处理情况的",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，官方兽医按照（    ）等规定实施检疫。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "《中华人民共和国动物防疫法》",
          "label": "A"
        },
        {
          "content": "《中华人民共和国畜牧法》",
          "label": "B"
        },
        {
          "content": "《动物检疫管理办法》",
          "label": "C"
        },
        {
          "content": "检疫规程",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员有（）情形的，应当从重给予政务处分。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "阻止他人检举、提供证据的",
          "label": "A"
        },
        {
          "content": "串供或者伪造、隐匿、毁灭证据的",
          "label": "B"
        },
        {
          "content": "胁迫、唆使他人实施违法行为的",
          "label": "C"
        },
        {
          "content": "主动上交或者退赔违法所得的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "跨省际流入的病死畜禽，由（     ）会同有关地方和部门组织调查。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "生态环境部",
          "label": "A"
        },
        {
          "content": "流入地省级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "国务院办公厅",
          "label": "C"
        },
        {
          "content": "农业农村部",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对当事人的违法行为依法不予行政处罚的，行政机关应当对当事人进行（      ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "责任约谈",
          "label": "A"
        },
        {
          "content": "责令改正",
          "label": "B"
        },
        {
          "content": "警告",
          "label": "C"
        },
        {
          "content": "教育",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "被许可人以欺骗、贿赂等不正当手段取得行政许可的，行政机关应当作出的正确处理是（    ）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "撤销行政许可",
          "label": "A"
        },
        {
          "content": "要求被许可人补办手续",
          "label": "B"
        },
        {
          "content": "不能撤销行政许可",
          "label": "C"
        },
        {
          "content": "撤销行政许可可能对公共利益造成重大损害的，行政许可不予撤销。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关违法实施行政许可，给当事人的合法权益造成损害的，应当依照（    ）的规定给予赔偿。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "行政许可法",
          "label": "A"
        },
        {
          "content": "行政诉讼法",
          "label": "B"
        },
        {
          "content": "行政处罚法",
          "label": "C"
        },
        {
          "content": "国家赔偿法",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列属于动物和动物产品无害化处理场所应当符合的防疫条件的是：（ ）。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "无害化处理区内设置无害化处理间、冷库。",
          "label": "A"
        },
        {
          "content": "动物卸载区域有固定的车辆消毒场地，并配备车辆清洗消毒设备。",
          "label": "B"
        },
        {
          "content": "配备与其处理规模相适应的病死动物和病害动物产品的无害化处理设施设备，符合农业农村部规定条件的专用运输车辆，以及相关病原检测设备，或者委托有资质的单位开展检测。",
          "label": "C"
        },
        {
          "content": "建立病死动物和病害动物产品入场登记、无害化处理记录、病原检测、处理产物流向登记、人员防护等动物防疫制度。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，县级以上地方人民政府农业农村主管部门执行监督检查任务，可以对动物、动物产品按照规定采取的措施有（　　）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "采样",
          "label": "A"
        },
        {
          "content": "留验",
          "label": "B"
        },
        {
          "content": "抽检",
          "label": "C"
        },
        {
          "content": "处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对全国所有鸡、鸭、鹅、鹌鹑等人工饲养的禽类，根据当地实际情况，在科学评估的基础上选择适宜疫苗，进行()型高致病性禽流感免疫。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "H5 ",
          "label": "A"
        },
        {
          "content": "H7 ",
          "label": "B"
        },
        {
          "content": "H9",
          "label": "C"
        },
        {
          "content": "H8",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "农业农村主管部门在监督检查中发现生猪定点屠宰厂（场）不再具备《生猪屠宰管理条例》规定条件的，应当采取的处理、处罚是（       ）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "给予警告",
          "label": "A"
        },
        {
          "content": "责令停业整顿，并限期整改",
          "label": "B"
        },
        {
          "content": "限期整改的期限届满后，仍达不到本《生猪屠宰管理条例》规定条件的，报请发证的设区的市级人民政府吊销生猪定点屠宰证书，收回生猪定点屠宰标志牌",
          "label": "C"
        },
        {
          "content": "并处5万元以上10万以下的罚款",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于畜间人兽共患病防治的指导思想的有（     ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "坚持人民至上、生命至上",
          "label": "A"
        },
        {
          "content": "实行积极防御、系统治理",
          "label": "B"
        },
        {
          "content": "全面夯实基层基础",
          "label": "C"
        },
        {
          "content": "提升风险防范和综合防治能力",
          "label": "D"
        },
        {
          "content": "有计划地控制、净化和消灭若干种严重危害畜牧业生产和人民群众健康安全的畜间人兽共患病",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，动物检疫应遵循（    ）相结合的原则。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "过程监管",
          "label": "A"
        },
        {
          "content": "风险控制",
          "label": "B"
        },
        {
          "content": "区域化",
          "label": "C"
        },
        {
          "content": "可追溯管理",
          "label": "D"
        },
        {
          "content": "网格化",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "县级以上人民政府（ ）主管部门应当加强对农业生产中合理用药的指导和监督，采取措施防止抗微生物药物的不合理使用，降低在农业生产环境中的残留。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "卫生健康",
          "label": "A"
        },
        {
          "content": "农业农村",
          "label": "B"
        },
        {
          "content": "林业草原",
          "label": "C"
        },
        {
          "content": "生态环境",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下面依法应当先经下级行政机关审查后报上级行政机关决定的行政许可的表述，错误的是（      ）",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "下级行政机关应当将初步审查意见直接报送上级行政机关",
          "label": "A"
        },
        {
          "content": "下级行政机关应当将全部申请材料直接报送上级行政机关",
          "label": "B"
        },
        {
          "content": "上级行政机关可以要求申请人重复提供申请材料",
          "label": "C"
        },
        {
          "content": "下级行政机关应当在法定期限内审查完，并告知申请人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "关于行政处罚的立法目的，下列那说法是正确的\r\n（       ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "规范行政处罚的规定",
          "label": "A"
        },
        {
          "content": "保障行政处罚的实施",
          "label": "B"
        },
        {
          "content": "规范行政处罚的设定",
          "label": "C"
        },
        {
          "content": "维护行政机关权威",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，跨省运输用于饲养的畜禽到达目的后，目的地饲养场（户）应当向（   ）报告。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "所在地县级人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "所在地乡镇人民政府",
          "label": "B"
        },
        {
          "content": "所在地省级动物卫生监督机构",
          "label": "C"
        },
        {
          "content": "所在地县级动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列不属于《行政许可法》调整范围的是（    ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "某县级动物卫生监督机构对某屠宰企业检疫合格的动物产品出具动物检疫合格证明",
          "label": "A"
        },
        {
          "content": "某学校希望上级教育主管部门拨款，对学校进行扩建， 上级教育主管部门对齐申请予以审批",
          "label": "B"
        },
        {
          "content": "行政机关根据某食品加工企业的申请，发给其生产许可证",
          "label": "C"
        },
        {
          "content": "某企业因为更名和变更法人，向市场监督管理部门提出申请",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，给与记过处分的，政务处分期为（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "六个月",
          "label": "A"
        },
        {
          "content": "十二个月",
          "label": "B"
        },
        {
          "content": "十八个月",
          "label": "C"
        },
        {
          "content": "二十四个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "电子技术监控设备的（     ）应当向社会公布。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "设置地点",
          "label": "A"
        },
        {
          "content": "设置标志",
          "label": "B"
        },
        {
          "content": "设置标准",
          "label": "C"
        },
        {
          "content": "设置单位",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（ ）对本行政区域的农产品质量安全工作负责，统一领导、组织、协调本行政区域的农产品质量安全工作，建立健全农产品质量安全工作机制，提高农产品质量安全水平。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "地方各级人民政府",
          "label": "A"
        },
        {
          "content": "省级人民政府",
          "label": "B"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "C"
        },
        {
          "content": "乡镇人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "尚未完全丧失辨认或者控制自己行为能力的精神病人、智力残疾人有违法行为的，可以（    ）行政处罚。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "从轻",
          "label": "A"
        },
        {
          "content": "减轻",
          "label": "B"
        },
        {
          "content": "从轻或者减轻",
          "label": "C"
        },
        {
          "content": "不予",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "XX县农业农村局监督检查时发现，该县胡某养猪场生猪佩戴的耳标为假耳标，经调查该批耳标为本县赵某伪造，共10000枚，已售出2500枚，经营该批耳标共获违法收入500元。县农业农村局依法对赵某立案查处，下列处罚（    ）是符合法律规定的。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "责令当事人立即改正违法行为",
          "label": "A"
        },
        {
          "content": "没收违法所得500元",
          "label": "B"
        },
        {
          "content": "没收耳标",
          "label": "C"
        },
        {
          "content": "罚款20000元",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，有关单位拒不执行政务处分决定的，由（）责令改正。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "上级机关",
          "label": "A"
        },
        {
          "content": "主管部门",
          "label": "B"
        },
        {
          "content": "任免机关",
          "label": "C"
        },
        {
          "content": "监察机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员参与赌博，情节较重的，予以（）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "记大过",
          "label": "A"
        },
        {
          "content": "降级",
          "label": "B"
        },
        {
          "content": "撤职",
          "label": "C"
        },
        {
          "content": "开除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政许可所依据的法律、法规、规章修改或者废止的，为了公共利益的需要，行政机关可以依法（      ）已经生效的行政许可。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "注销",
          "label": "A"
        },
        {
          "content": "变更",
          "label": "B"
        },
        {
          "content": "撤回",
          "label": "C"
        },
        {
          "content": "撤销",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关作出（     ）行政处罚决定之前，应当告知当事人有要求举行听证的权力。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "责令停产停业",
          "label": "A"
        },
        {
          "content": "行政拘留",
          "label": "B"
        },
        {
          "content": "法人或组织罚款1万元的",
          "label": "C"
        },
        {
          "content": "没收个人违法所得1千元",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》适用于（    ）的检疫及其监督管理活动。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "本行政区域内以及进出境动物、动物产品",
          "label": "A"
        },
        {
          "content": "本行政区域内",
          "label": "B"
        },
        {
          "content": "中华人民共和国领域以及进出境动物、动物产品",
          "label": "C"
        },
        {
          "content": "中华人民共和国领域内的动物、动物产品",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，国务院农业农村主管部门根据行政区划、养殖屠宰产业布局、风险评估情况等对动物疫病实施（    ），可以采取禁止或者限制特定动物、动物产品跨区域调运等措施。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "大区防控",
          "label": "A"
        },
        {
          "content": "分区防控",
          "label": "B"
        },
        {
          "content": "监测",
          "label": "C"
        },
        {
          "content": "检测",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，经检疫不合格的动物、动物产品，由（　　）出具检疫处理通知单。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "检疫人员",
          "label": "A"
        },
        {
          "content": "官方兽医",
          "label": "B"
        },
        {
          "content": "动物防疫人员",
          "label": "C"
        },
        {
          "content": "协检人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（）具体承担官方兽医培训教材课件制作、考试题库完善、培训考试平台运维等工作。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "农业农村部",
          "label": "A"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "动物卫生监督",
          "label": "C"
        },
        {
          "content": "中国动物疫病预防控制中心",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，（     ）应当按照规定定期免疫接种狂犬病疫苗，凭动物诊疗机构出具的免疫证明向所在地养犬登记机关申请登记。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "单位和个人饲养犬猫",
          "label": "A"
        },
        {
          "content": "单位饲养犬只",
          "label": "B"
        },
        {
          "content": "单位和个人饲养犬只",
          "label": "C"
        },
        {
          "content": "单位饲养犬猫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "布病强制免疫工作有效开展，免疫地区免疫密度常年保持在90%以上，免疫建档率（）%。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "70",
          "label": "A"
        },
        {
          "content": "80",
          "label": "B"
        },
        {
          "content": "90",
          "label": "C"
        },
        {
          "content": "100",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定,动物检疫遵循过程监管、风险控制、（   ）和可追溯管理相结合的原则。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "区域化",
          "label": "A"
        },
        {
          "content": "网格化",
          "label": "B"
        },
        {
          "content": "运输备案",
          "label": "C"
        },
        {
          "content": "落地报告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": " 病死畜禽和病害畜禽产品无害化处理场所未建立管理制度的，由县级以上地方人民政府农业农村主管部门责令改正；拒不改正或者情节严重的，处（     ）罚款。 ",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "一千元以上五千元以下",
          "label": "A"
        },
        {
          "content": "二千元以上一万元以下",
          "label": "B"
        },
        {
          "content": "二千元以上二万元以下",
          "label": "C"
        },
        {
          "content": "五千元以上二万元以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员组织赌博，组织、支持、参与卖淫、嫖娼、色情淫乱活动的，予以（）。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "记大过",
          "label": "A"
        },
        {
          "content": "降级",
          "label": "B"
        },
        {
          "content": "撤职",
          "label": "C"
        },
        {
          "content": "开除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，依法应当检疫而未经检疫的（   ）等动物产品，不予补检，予以收缴销毁。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "胴体、肉",
          "label": "A"
        },
        {
          "content": "脏器、脂、血液",
          "label": "B"
        },
        {
          "content": "精液、卵、胚胎",
          "label": "C"
        },
        {
          "content": "骨、蹄、头、筋",
          "label": "D"
        },
        {
          "content": "种蛋",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所建设竣工后，应当向所在地县级人民政府农业农村主管部门提出申请，并提交（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "《动物防疫条件审查申请表》",
          "label": "A"
        },
        {
          "content": "场所地理位置图、各功能区布局平面图",
          "label": "B"
        },
        {
          "content": "设施设备清单和管理制度文本",
          "label": "C"
        },
        {
          "content": "人员信息和动物种类数量",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "开展小反刍兽疫非免疫无疫区建设的区域，经国务院农业农村部门同意后，可不实施免疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "违反《动物防疫法》规定，患有人畜共患传染病的人员，直接从事动物疫病监测、检测、检验检疫、动物诊疗等活动，情节严重的，由县级以上地方人民政府农业农村责令改正。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "官方兽医在岗培训，每人每年参加培训时间不得少于40学时（含线上培训）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "法律、法规授权的具有管理公共事务职能的组织，在法定授权范围内，以（      ）的名义实施行政许可。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "自己",
          "label": "A"
        },
        {
          "content": "授权机关",
          "label": "B"
        },
        {
          "content": "委托行政机关",
          "label": "C"
        },
        {
          "content": "特定行政机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "病死畜禽和病害畜禽产品无害化处理场所应当安装视频监控设备，对病死畜禽和病害畜禽产品进（出）场、交接、处理和处理产物存放等进行全程监控，相关监控影像资料保存期不少于（     ）。 ",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "六十天",
          "label": "A"
        },
        {
          "content": "三十天",
          "label": "B"
        },
        {
          "content": "九十天",
          "label": "C"
        },
        {
          "content": "一百八十天",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国务院有关部门组织建立相关领域、行业的生物安全技术咨询专家委员会，为生物安全工作提供（ ）技术支撑。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "咨询",
          "label": "A"
        },
        {
          "content": "指导",
          "label": "B"
        },
        {
          "content": "评估",
          "label": "C"
        },
        {
          "content": "论证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列有关国家建立农产品质量安全风险评估制度说法正确的是：（ ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "农业农村主管部门应当设立农产品质量安全风险评估专家委员会。",
          "label": "A"
        },
        {
          "content": "农产品质量安全风险评估专家委员会对可能影响农产品质量安全的潜在危害进行风险分析和评估。",
          "label": "B"
        },
        {
          "content": "农产品质量安全风险评估专家委员会由农业农村主管部门工作人员组成。",
          "label": "C"
        },
        {
          "content": "卫生健康、市场监督管理等部门发现需要对农产品进行质量安全风险评估的，应当向农业农村主管部门提出风险评估建议。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关及其工作人员对实施行政处罚过程中知悉的（      ），应当依法予以保密。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "国家秘密",
          "label": "A"
        },
        {
          "content": "商业秘密",
          "label": "B"
        },
        {
          "content": "工作秘密",
          "label": "C"
        },
        {
          "content": "个人隐私",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国畜牧法》的规定，县级以上地方人民政府农业农村主管部门发现畜禽屠宰企业不再具备规定条件，以下处理、处罚正确的是（       ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "应当责令停业整顿",
          "label": "B"
        },
        {
          "content": "限期整改",
          "label": "C"
        },
        {
          "content": "限期整改的期限届满后，仍未达到《中华人民共和国畜牧法》规定条件的，责令关闭",
          "label": "D"
        },
        {
          "content": "限期整改的期限届满后，仍未达到《中华人民共和国畜牧法》规定条件的，对实行定点屠宰管理的，吊销定点屠宰证书",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "按照农业农村部公告第2号要求，除（）外，跨省调运活畜时，禁止布病易感动物从高风险区域（免疫区）向低风险区域（非免疫区）调运。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "布病无疫区",
          "label": "A"
        },
        {
          "content": "布病无疫小区",
          "label": "B"
        },
        {
          "content": "布病净化场",
          "label": "C"
        },
        {
          "content": "用于屠宰和种用乳用",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，补检的（    ）等动物产品符合补检合格条件的，可出具动物检疫证明。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "生皮",
          "label": "A"
        },
        {
          "content": "原毛",
          "label": "B"
        },
        {
          "content": "绒",
          "label": "C"
        },
        {
          "content": "角",
          "label": "D"
        },
        {
          "content": "种蛋",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，（    ）的动物产品，应当附有检疫证明。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "分销",
          "label": "A"
        },
        {
          "content": "运输",
          "label": "B"
        },
        {
          "content": "经营",
          "label": "C"
        },
        {
          "content": "购买",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "关于行政许可的申请与受理程序，下列说法不正确的是（         ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "申请材料存在可以当场更正的错误的，应当允许申请人当场更正",
          "label": "A"
        },
        {
          "content": "申请人必须亲自到行政机关提交申请",
          "label": "B"
        },
        {
          "content": "行政机关不予受理行政许可申请，应当出具加盖本行政机关专用印章和注明日期的书面凭证",
          "label": "C"
        },
        {
          "content": "申请事项依法不需要取得行政许可的，应当即时告知申请人不受理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，出售或者运输动物、动物产品的，货主应提前（　　）天所在地动物卫生监督机构申报检疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "一",
          "label": "A"
        },
        {
          "content": "三",
          "label": "B"
        },
        {
          "content": "五",
          "label": "C"
        },
        {
          "content": "七",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（    ）应当严格执行动物入场查验登记、待宰巡查等制度，查验进场待宰动物的动物检疫证明和畜禽标识",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "官方兽医",
          "label": "A"
        },
        {
          "content": "屠宰加工场所",
          "label": "B"
        },
        {
          "content": "动物卫生监督机构",
          "label": "C"
        },
        {
          "content": "官方兽医或协检人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列哪些情形，在行政机关负责人作出行政处罚的决定之前，应当由从事行政处罚决定法制审核的人员进行法制审核。（      ）",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "涉及重大公共利益的",
          "label": "A"
        },
        {
          "content": "直接关系当事人或者第三人重大权益，经过听证程序的",
          "label": "B"
        },
        {
          "content": "案件情况疑难复杂、涉及多个法律关系的",
          "label": "C"
        },
        {
          "content": "法律、法规规定应当进行法制审核的其他情形。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，进入屠宰加工场所的待宰动物应当(       )。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "附有动物检疫证明",
          "label": "A"
        },
        {
          "content": "附有无疫区证明",
          "label": "B"
        },
        {
          "content": "附有非封锁区证明",
          "label": "C"
        },
        {
          "content": "加施有符合规定的畜禽标识",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "开展病死猪无害化处理与保险联动机制建设试点工作，是要探索形成（     ）的病死猪无害化处理市场化运行机制。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "政府监管",
          "label": "A"
        },
        {
          "content": "财政支持",
          "label": "B"
        },
        {
          "content": "企业运作",
          "label": "C"
        },
        {
          "content": "保险联动",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "为加强动物检疫活动管理，（         ）动物疫病，防控人畜共患传染病，保障公卫生安全和人体健康，根据《中华人民共和国动物防疫法》,制定《动物检疫管理办法》。",
      "standard_answer": "A,B,C,E",
      "option_list": [
        {
          "content": "净化",
          "label": "A"
        },
        {
          "content": "预防",
          "label": "B"
        },
        {
          "content": "控制",
          "label": "C"
        },
        {
          "content": "扑灭",
          "label": "D"
        },
        {
          "content": "消灭",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《行政许可法》中关于行政许可的设立，下列说法正确的是（       ）",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "法律可以任意设定行政许可",
          "label": "A"
        },
        {
          "content": "尚未制定法律、行政法规和地方性法规的，因行政管理的需要，确需立即实施行政许可的，国务院各部门的规章可以设定临时性的行政许可。",
          "label": "B"
        },
        {
          "content": "某国对我国出口的动物产品采取歧视性措施时，国务院采取发布决定的方式，设定进出口许可和配额管理等临时性行政许可，符合《行政许可法》的规定",
          "label": "C"
        },
        {
          "content": "为了保护本地的企业，省人民政府发布规章设定行政许可，可以不允许外地企业在本地市场上销售同类的动物产品",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "精神病人、智力残疾人在不能辨认或者不能控制自己行为时有违法行为的，（    ）行政处罚。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "不予",
          "label": "A"
        },
        {
          "content": "从轻",
          "label": "B"
        },
        {
          "content": "减轻",
          "label": "C"
        },
        {
          "content": "从轻或者减轻",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "完善官方兽医师资轮训制度，官方兽医师资每（）年轮训一次。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "1",
          "label": "A"
        },
        {
          "content": "2",
          "label": "B"
        },
        {
          "content": "1-2 ",
          "label": "C"
        },
        {
          "content": "2-3",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "申请材料不齐全或者不符合法定形式的，应当当场或者在（      ）内一次告知申请人需要补正的全部内容，",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "三日",
          "label": "A"
        },
        {
          "content": "五日",
          "label": "B"
        },
        {
          "content": "七日",
          "label": "C"
        },
        {
          "content": "十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物检疫管理办法》规定，官方兽医应当符合以下（    ）条件。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "动物卫生监督机构的在编人员",
          "label": "A"
        },
        {
          "content": "从事动物检疫工作",
          "label": "B"
        },
        {
          "content": "具有畜牧兽医水产初级以上职称或者相关专业大专以上学历或者从事动物防疫等相关工作满三年以上",
          "label": "C"
        },
        {
          "content": "接受岗前培训，并经考核合格",
          "label": "D"
        },
        {
          "content": "或者接受动物卫生监督机构业务指导的其他机构在编人员",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各地通过兽医卫生综合信息平台“官方兽医”模块开展动物卫生监督机构、动物检疫申报点、官方兽医信息的（）等工作。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "公开",
          "label": "A"
        },
        {
          "content": "新增",
          "label": "B"
        },
        {
          "content": "注销",
          "label": "C"
        },
        {
          "content": "更新完善",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员犯罪，有（   ）情形的，予以开除。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "因故意犯罪被判处管制、拘役以上刑罚（含宣告缓刑）的",
          "label": "A"
        },
        {
          "content": "因故意犯罪被判处有期徒刑以上刑罚（含宣告缓刑）的",
          "label": "B"
        },
        {
          "content": "因过失犯罪被判处有期徒刑，刑期超过三年的",
          "label": "C"
        },
        {
          "content": "因犯罪被单处或者并处剥夺政治权利的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《生猪屠宰管理条例》规定，生猪定点屠宰厂（场）会被责令停业整顿的情形有（       ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "农业农村主管部门在监督检查中发现生猪定点屠宰厂（场）不再具备规定条件的",
          "label": "A"
        },
        {
          "content": "未按照规定签订、保存委托屠宰协议且拒不改正的",
          "label": "B"
        },
        {
          "content": "生猪定点屠宰厂（场）出厂（场）未经肉品品质检验或者经肉品品质检验不合格的生猪产品的",
          "label": "C"
        },
        {
          "content": "生猪定点屠宰厂（场）对生猪、生猪产品注水或者注入其他物质的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，有（　　）行为之一的单位或个人，由县级以上地方人民政府农业农村主管部门责令改正，处三千元以上三万元以下罚款；情节严重的，责令停业整顿，并处三万元以上十万元以下的罚款。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "经营动物、动物产品的集贸市场不具备国务院农业农村主管部门规定的防疫条件的；",
          "label": "A"
        },
        {
          "content": "开办动物屠宰场未取得动物防疫条件合格证的",
          "label": "B"
        },
        {
          "content": "跨省、自治区、直辖市引进种用、乳用动物到达输入地后未按照规定进行隔离观察的；",
          "label": "C"
        },
        {
          "content": "未按照规定处理或者随意弃置病死动物、病害动物产品的；",
          "label": "D"
        },
        {
          "content": "饲养种用、乳用动物的单位和个人，未按照国务院农业农村主管部门的要求定期开展动物疫病检测的。",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，国有企业管理人员在政务处分期内，不得晋升（）",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "职务",
          "label": "A"
        },
        {
          "content": "岗位等级",
          "label": "B"
        },
        {
          "content": "衔级",
          "label": "C"
        },
        {
          "content": "职称",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对全国所有鸡、鸭、鹅、鹌鹑等人工饲养的禽类，根据当地实际情况，在科学评估的基础上选择适宜疫苗，进行（）免疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "新城疫",
          "label": "A"
        },
        {
          "content": "高致病性禽流感",
          "label": "B"
        },
        {
          "content": "禽传染性支气管炎",
          "label": "C"
        },
        {
          "content": "禽传染性喉气管炎",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，运输的畜禽到达屠宰场后，屠宰场应当在接收畜禽后(   )内向所在地县级动物卫生监督机构报告。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "二十四小时",
          "label": "A"
        },
        {
          "content": "十二小时",
          "label": "B"
        },
        {
          "content": "三日",
          "label": "C"
        },
        {
          "content": "二日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "为了加强对动物防疫活动的管理，（　　）动物疫病，促进养殖业发展，防控人畜共患传染病，保障公共卫生安全和人体健康，制定《中华人民共和国动物防疫法》。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "预防",
          "label": "A"
        },
        {
          "content": "控制",
          "label": "B"
        },
        {
          "content": "净化",
          "label": "C"
        },
        {
          "content": "消灭",
          "label": "D"
        },
        {
          "content": "扑灭",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "农业农村部负责（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "组织制定官方兽医培训大纲",
          "label": "A"
        },
        {
          "content": "审定培训教材",
          "label": "B"
        },
        {
          "content": "审定考试题库",
          "label": "C"
        },
        {
          "content": "制作培训教材课件",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，屠宰检疫合格后，可对动物的胴体及（    ）出具动物检疫证明，加盖验讫印章或者其他检疫标志。",
      "standard_answer": "B,C,E",
      "option_list": [
        {
          "content": "骨、筋",
          "label": "A"
        },
        {
          "content": "脏器、血液",
          "label": "B"
        },
        {
          "content": "头、蹄、角",
          "label": "C"
        },
        {
          "content": "肉、脂、",
          "label": "D"
        },
        {
          "content": "生皮、原毛、绒",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《生猪屠宰管理条例》规定，农业农村主管部门依法进行监督检查，可以采取下列哪些措施（       ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "进入生猪屠宰等有关场所实施现场检查",
          "label": "A"
        },
        {
          "content": "向有关单位和个人了解情况",
          "label": "B"
        },
        {
          "content": "查阅、复制有关记录、票据以及其他资料",
          "label": "C"
        },
        {
          "content": "查封与违法生猪屠宰活动有关的场所、设施，扣押与违法生猪屠宰活动有关的生猪、生猪产品以及屠宰工具和设备",
          "label": "D"
        },
        {
          "content": "对生猪及其产品进行采样、留验、抽检",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "货主申报检疫时，应当提交检疫申报单以及（    ）其他材料，并对申报材料的真实性负责。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "农业农村部规定的",
          "label": "A"
        },
        {
          "content": "动物疫病检测报告等",
          "label": "B"
        },
        {
          "content": "省级农业农村主管部门规定的",
          "label": "C"
        },
        {
          "content": "养殖档案等",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪肉品品质检验检验结果记录保存期限不得少于（       ）年。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "1",
          "label": "A"
        },
        {
          "content": "3",
          "label": "B"
        },
        {
          "content": "5",
          "label": "C"
        },
        {
          "content": "2",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对全国所有（）进行O型和A型口蹄疫免疫",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "牛",
          "label": "A"
        },
        {
          "content": "羊",
          "label": "B"
        },
        {
          "content": "鹿",
          "label": "C"
        },
        {
          "content": "骆驼",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》所称动物，是指（    ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "家畜家禽 ",
          "label": "A"
        },
        {
          "content": "人工饲养",
          "label": "B"
        },
        {
          "content": "合法捕获的其他动物",
          "label": "C"
        },
        {
          "content": "捕获的其他动物",
          "label": "D"
        },
        {
          "content": "合法捕获的野生动物",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "陆生野生动物检疫办法，由（     ）会同（    ）另行制定。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "农业农村部",
          "label": "A"
        },
        {
          "content": "野生动物保护部门",
          "label": "B"
        },
        {
          "content": "国家林业和草原局",
          "label": "C"
        },
        {
          "content": "农业农村部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "饲养动物的有关单位和个人应（），确保可追溯。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "自行开展免疫",
          "label": "A"
        },
        {
          "content": "建立免疫档案",
          "label": "B"
        },
        {
          "content": "向第三方服务主体购买免疫服务",
          "label": "C"
        },
        {
          "content": "加施畜禽标识",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "关于行政处罚，两个以上行政机关都有管辖权的，由（    ）行政机关管辖。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "上级行政机关指定的",
          "label": "A"
        },
        {
          "content": "最先立案的",
          "label": "B"
        },
        {
          "content": "同级人民政府指定的",
          "label": "C"
        },
        {
          "content": "任一",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所，应当在每年（ ）将上一年的动物防疫条件情况和防疫制度执行情况向县级人民政府农业农村主管部门报告。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "一月底前",
          "label": "A"
        },
        {
          "content": "二月底前",
          "label": "B"
        },
        {
          "content": "三月底前",
          "label": "C"
        },
        {
          "content": "六月底前",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "公民法人或者其他组织因违法行为受到行政处罚，其违法行为对他人造成损害的，应当依法承担（     ）责任。违法行为构成犯罪，应当依法追究（      ）责任的，不得以（     ）处罚代替（      ）处罚。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "民事、刑事、行政、刑事",
          "label": "A"
        },
        {
          "content": "行政、刑事、刑事、行政",
          "label": "B"
        },
        {
          "content": "刑事、行政、民事、行政",
          "label": "C"
        },
        {
          "content": "民事、行政、刑事、行政",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "被许可人以欺骗、贿赂等不正当手段取得行政许可的，行政机关应当依法给予行政处罚；取得的行政许可属于直接关系公共安全、人身健康、生命财产安全事项的，申请人在（     ）不得再次申请该行政许可。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "一年内",
          "label": "A"
        },
        {
          "content": "二年内",
          "label": "B"
        },
        {
          "content": "三年内",
          "label": "C"
        },
        {
          "content": "五年内",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违反民主集中制原则，个人或者少数人决定重大事项的，予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，以下属于复审、复核机关应当撤销原政务处分决定，重新作出决定的情形有（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "处分依据的事实不清",
          "label": "A"
        },
        {
          "content": "处分依据的证据不足",
          "label": "B"
        },
        {
          "content": "违反法定程序",
          "label": "C"
        },
        {
          "content": "超越职权作出决定",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "到2025年，（）四级官方兽医培训体系进一步健全完善，官方兽医知识结构持续更新优化，专业能力和综合素质明显提升，省、市、县三级动物卫生监督机构官方兽医完训率达到100%，考试合格率达到90％以上。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "国家",
          "label": "A"
        },
        {
          "content": "省",
          "label": "B"
        },
        {
          "content": "市",
          "label": "C"
        },
        {
          "content": "县 ",
          "label": "D"
        },
        {
          "content": "乡",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国家在（      ）农业等领域推行建立综合行政执法制度。",
      "standard_answer": "A,B,C,D,E,F",
      "option_list": [
        {
          "content": "城市管理",
          "label": "A"
        },
        {
          "content": "市场监管",
          "label": "B"
        },
        {
          "content": "生态环境",
          "label": "C"
        },
        {
          "content": "文化市场",
          "label": "D"
        },
        {
          "content": "交通运输",
          "label": "E"
        },
        {
          "content": "应急管理",
          "label": "F"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "为了规范动物防疫条件审查，（），根据《中华人民共和国动物防疫法》，制定动物防疫条件审查办法。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "有效预防、控制、净化、消灭动物疫病",
          "label": "A"
        },
        {
          "content": "迅速控制、扑灭重大动物疫情",
          "label": "B"
        },
        {
          "content": "防控人畜共患传染病",
          "label": "C"
        },
        {
          "content": "保障公共卫生安全和人体健康",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政许可法》，行政许可二十日内不能作出决定的，经（     ）批准，可以延长十日。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "办公室主任",
          "label": "A"
        },
        {
          "content": "具体业务分管领导",
          "label": "B"
        },
        {
          "content": "本行政机关负责人",
          "label": "C"
        },
        {
          "content": "以上都不是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "公安机关在生猪屠宰相关犯罪案件侦查过程中，需要农业农村主管部门给予检验、认定等协助的，农业农村主管部门应当给予（       ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "协助",
          "label": "A"
        },
        {
          "content": "帮助",
          "label": "B"
        },
        {
          "content": "配合",
          "label": "C"
        },
        {
          "content": "支持",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，法律、法规授权的组织中从事公务的人员，在受到（）政务处分期内，不得晋升薪酬待遇等级。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "记过",
          "label": "A"
        },
        {
          "content": "记大过",
          "label": "B"
        },
        {
          "content": "降级",
          "label": "C"
        },
        {
          "content": "撤职",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《全国畜间人兽共患病防治规划2022—2030年）》提出的狂犬病的重点防治措施的是（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "严格实施犬只免疫",
          "label": "A"
        },
        {
          "content": "开展监测流调",
          "label": "B"
        },
        {
          "content": "做好应急处置",
          "label": "C"
        },
        {
          "content": "加强流浪犬和农村犬只防疫管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）屠宰注水或者注入其他物质的生猪的，农业农村主管部门可给予的处罚有（       ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "责令停业整顿，没收注水或者注入其他物质的生猪、生猪产品和违法所得",
          "label": "A"
        },
        {
          "content": "货值金额不足1万元的，并处5万元以上10万元以下的罚款",
          "label": "B"
        },
        {
          "content": "货值金额1万元以上的，并处货值金额10倍以上20倍以下的罚款",
          "label": "C"
        },
        {
          "content": "对其直接负责的主管人员和其他直接责任人员处5万元以上10万元以下的罚款",
          "label": "D"
        },
        {
          "content": "吊销生猪定点屠宰证书，收回生猪定点屠宰标志牌",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "（ ）应当符合动物防疫条件审查办法规定的动物防疫条件，并取得动物防疫条件合格证。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "动物饲养场",
          "label": "A"
        },
        {
          "content": "动物隔离场所",
          "label": "B"
        },
        {
          "content": "动物屠宰加工场所",
          "label": "C"
        },
        {
          "content": "动物和动物产品无害化处理场所",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对()引发动物疫情的单位和个人，要依法处理并追究责任。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "实施强制免疫",
          "label": "A"
        },
        {
          "content": "免疫不到位",
          "label": "B"
        },
        {
          "content": "拒不履行强制免疫义务",
          "label": "C"
        },
        {
          "content": "报告疫情",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪屠宰行业发展规划应当包括（     ）等内容。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "发展目标",
          "label": "A"
        },
        {
          "content": "屠宰厂（场）设置",
          "label": "B"
        },
        {
          "content": "政策措施",
          "label": "C"
        },
        {
          "content": "销售范围",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《行政许可法》规定可以设定行政许可的事项，尚未制定法律、行政法规的，（     ）可以设定行政许可。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "地方性法规",
          "label": "A"
        },
        {
          "content": "地方政府规章",
          "label": "B"
        },
        {
          "content": "部门规章",
          "label": "C"
        },
        {
          "content": "国务院文件",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列关于动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所审查发证说法错误的是：（ ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "场所建设竣工后，应当向所在地县级人民政府农业农村主管部门提出申请，并提交相应材料。",
          "label": "A"
        },
        {
          "content": "提交材料包括《动物防疫条件审查申请表》、场所地理位置图、各功能区布局平面图、设施设备清单、管理制度文本、人员信息。",
          "label": "B"
        },
        {
          "content": "申请材料不齐全或者不符合规定条件的，县级人民政府农业农村主管部门应当自收到申请材料之日起十个工作日内，一次性告知申请人需补正的内容。",
          "label": "C"
        },
        {
          "content": "县级人民政府农业农村主管部门应当自受理申请之日起十五个工作日内完成材料审核，并结合选址综合评估结果完成现场核查，审查合格的，颁发动物防疫条件合格证。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "张某违反《生猪屠宰管理条例》规定，未经定点从事生猪屠宰活动，农业农村主管部门应对张某给予的处罚有（       ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "责令关闭",
          "label": "A"
        },
        {
          "content": "没收生猪、生猪产品、屠宰工具和设备以及违法所得",
          "label": "B"
        },
        {
          "content": "货值金额不足1万元的，并处5万元以上10万元以下的罚款",
          "label": "C"
        },
        {
          "content": "货值金额1万元以上的，并处货值金额10倍以上20倍以下的罚款",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "公民、法人或者其他组织可以向行政许可的\r\n（     ）就行政许可的设定和实施提出意见和建议。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "设定机关",
          "label": "A"
        },
        {
          "content": "受委托机关",
          "label": "B"
        },
        {
          "content": "实施机关",
          "label": "C"
        },
        {
          "content": "监督机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜间布鲁氏菌病防控五年行动方案》指导思想：贯彻习近平总书记关于加强国家生物安全风险防控和治理体系建设指示精神，坚持人民至上、生命至上，实行积极防御、系统治理，(),切实做好布病源头防控工作，维护畜牧业生产安全、公共卫生安全和生物安全。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "有效控制传染源",
          "label": "A"
        },
        {
          "content": "阻断传播途径",
          "label": "B"
        },
        {
          "content": "提高抗病能力",
          "label": "C"
        },
        {
          "content": "强化检疫监管",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "强制免疫动物疫病的应免畜禽免疫密度应达到100%。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对阳性场群，有针对性地持续开展全群（），确保覆盖区域内所有存在阳性个体和造成人感染情况的场群。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "流调",
          "label": "A"
        },
        {
          "content": "跟踪监测",
          "label": "B"
        },
        {
          "content": "扑杀",
          "label": "C"
        },
        {
          "content": "免疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》提出旋毛虫病、囊尾蚴病的重点防治措施是以屠宰场为重点，严格（     ），做好污染肉品的无害化处理。 ",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "宰后检疫检验",
          "label": "A"
        },
        {
          "content": "入场查验登记",
          "label": "B"
        },
        {
          "content": "待宰采样检测",
          "label": "C"
        },
        {
          "content": "待宰检验和送宰检验",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，因过失犯罪被判处管制、拘役或者三年以下有期徒刑的，一般应当予以开除；案件情况特殊，予以撤职更为适当的，可以不予开除，但是应当报请（）批准。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "上级党委",
          "label": "A"
        },
        {
          "content": "同级人民政府",
          "label": "B"
        },
        {
          "content": "监察机关",
          "label": "C"
        },
        {
          "content": "上一级机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员工作中有形式主义、官僚主义行为，造成不良后果或者影响，情节严重的，予以（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列农产品不得进行销售的是：（ ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "含有国家禁止使用的农药、兽药或者其他化合物",
          "label": "A"
        },
        {
          "content": "含有的重金属等有毒有害物质不符合农产品质量安全标准",
          "label": "B"
        },
        {
          "content": "含有的致病性寄生虫、微生物或者生物毒素不符合农产品质量安全标准",
          "label": "C"
        },
        {
          "content": "病死、毒死或者死因不明的动物及其产品",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，国务院农业农村主管部门根据国内外动物疫情和保护养殖业生产及人体健康的需要，及时会同国务院卫生健康等有关部门对动物疫病进行风险评估，并制定、公布动物疫病的（　　）措施和技术规范。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "免疫、净化",
          "label": "A"
        },
        {
          "content": "监测、检测",
          "label": "B"
        },
        {
          "content": "净化、消灭",
          "label": "C"
        },
        {
          "content": "预防、控制",
          "label": "D"
        },
        {
          "content": "免疫、检疫",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违法行为情节轻微，且具有从轻或者减轻情形的，可以对其进行（    ），免予或者不予政务处分。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "谈话提醒",
          "label": "A"
        },
        {
          "content": "批评教育",
          "label": "B"
        },
        {
          "content": "责令检查",
          "label": "C"
        },
        {
          "content": "予以诫勉",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，对于（      ）的，要在动物检疫证明上加盖专用印章。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "证物相符",
          "label": "A"
        },
        {
          "content": "无违法违规情况",
          "label": "B"
        },
        {
          "content": "动物临床健康",
          "label": "C"
        },
        {
          "content": "动物检查合格",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "王某从外省购买种鸡3000只，该批种鸡应当在隔离场隔离（　　）天。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "十五",
          "label": "A"
        },
        {
          "content": "二十一",
          "label": "B"
        },
        {
          "content": "三十",
          "label": "C"
        },
        {
          "content": "四十五",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，可以安排官方兽医、特聘动物防疫专员等参与相关工作，满足（   ）小时值守需求。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "6",
          "label": "A"
        },
        {
          "content": "8",
          "label": "B"
        },
        {
          "content": "12",
          "label": "C"
        },
        {
          "content": "24",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "对于生物安全风险防控体制，县级以上地方人民政府及其有关部门应当制定并组织、指导和督促相关企业事业单位制定生物安全事件应急预案，加强应急准备、人员培训和应急演练，开展生物安全事件（ ）等工作。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "应急部署",
          "label": "A"
        },
        {
          "content": "应急处置",
          "label": "B"
        },
        {
          "content": "应急救援",
          "label": "C"
        },
        {
          "content": "事后恢复",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，申报动物检疫（    ）的，依照《中华人民共和国行政许可法》有关规定予以处罚。",
      "standard_answer": "B,D,E",
      "option_list": [
        {
          "content": "填写错误信息",
          "label": "A"
        },
        {
          "content": "隐瞒有关情况",
          "label": "B"
        },
        {
          "content": "随意更改材料信息",
          "label": "C"
        },
        {
          "content": "提供虚假材料",
          "label": "D"
        },
        {
          "content": "以欺骗、贿赂等不正当手段取得动物检疫证明",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，以下哪些场所（     ），应当具备国务院农业农村主管部门规定的动物防疫条件，并接受农业农村主管部门的监督检查。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "经营动物产品的集贸市场",
          "label": "A"
        },
        {
          "content": "车辆洗消中心",
          "label": "B"
        },
        {
          "content": "养殖场内的隔离场所",
          "label": "C"
        },
        {
          "content": "经营动物的集贸市场",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员因不明真相被裹挟或者被胁迫参与违法活动，经批评教育后确有悔改表现的，可以（  ）政务处分。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "减轻",
          "label": "A"
        },
        {
          "content": "免予",
          "label": "B"
        },
        {
          "content": "不予",
          "label": "C"
        },
        {
          "content": "处记过以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《全国畜间人兽共患病防治规划2022—2030年）》提出的对牛海绵状脑病、尼帕病毒性脑炎等外来病种的重点防治措施的是（     ）。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "加强国际疫情监视,做好传入风险分析和预警",
          "label": "A"
        },
        {
          "content": "禁止用穿刺脑部使动物失去知觉的办法屠宰牛",
          "label": "B"
        },
        {
          "content": "加强联防联控，健全跨部门协作机制，强化入境检疫和边境监管措施",
          "label": "C"
        },
        {
          "content": "在边境、口岸等高风险区域开展应急演练，提高应急处置能力",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，申报材料不齐全的，动物卫生监督机构当场或在（    ）日内已经一次性告知申报人需要补正的内容，但申报人拒不补正，不予受理。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "一",
          "label": "A"
        },
        {
          "content": "二",
          "label": "B"
        },
        {
          "content": "三",
          "label": "C"
        },
        {
          "content": "五",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "间歇性精神病人在精神正常时有违法行为的，应当（     ）行政处罚。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "不予",
          "label": "A"
        },
        {
          "content": "从轻",
          "label": "B"
        },
        {
          "content": "减轻",
          "label": "C"
        },
        {
          "content": "给予",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "开展病死猪无害化处理与保险联动，依照“（     ）”原则，将病死猪无害化处理作为保险理赔的前提条件，对不能确认已经无害化处理的病死猪，不予进行保险理赔。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "先检测再理赔",
          "label": "A"
        },
        {
          "content": "先理赔后处理",
          "label": "B"
        },
        {
          "content": "先垫付再理赔",
          "label": "C"
        },
        {
          "content": "先处理后理赔",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违反规定出境或者办理因私出境证件的，予以（）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（       ）农业农村主管部门应当建立生猪定点屠宰厂（场）信用档案，记录日常监督检查结果、违法行为查处等情况，并依法向社会公示。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "县级以上人民政府",
          "label": "A"
        },
        {
          "content": "市级以上人民政府",
          "label": "B"
        },
        {
          "content": "省级以上人民政府",
          "label": "C"
        },
        {
          "content": "生猪定点屠宰厂（场）所在地县级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，应当检疫出证的（   ）包括畜禽经宰杀、放血后除去毛、内脏、头、尾及四肢（腕及关节以下）后的躯体部分，也包括家畜躯体部分的二分体、四分体。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "畜禽肉品",
          "label": "A"
        },
        {
          "content": "畜禽产品",
          "label": "B"
        },
        {
          "content": "畜禽胴体",
          "label": "C"
        },
        {
          "content": "畜禽肉体",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）会被设区的市级人民政府吊销生猪定点屠宰证书，收回生猪定点屠宰标志牌的情形有（       ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "生猪定点屠宰厂（场）出借、转让生猪定点屠宰证书或者生猪定点屠宰标志牌的",
          "label": "A"
        },
        {
          "content": "屠宰生猪不遵守国家规定的操作规程、技术要求和生猪屠宰质量管理规范以及消毒技术规范，情节严重的",
          "label": "B"
        },
        {
          "content": "生猪定点屠宰厂（场）出厂（场）未经肉品品质检验或者经肉品品质检验不合格的生猪产品，情节严重的",
          "label": "C"
        },
        {
          "content": "生猪定点屠宰厂（场）屠宰注水或者注入其他物质的生猪，情节严重的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "病死畜禽和病害畜禽产品无害化处理场所应当于每年一月底前向所在地县级人民政府农业农村主管部门报告上一年度（     ）等情况。 ",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "利润",
          "label": "A"
        },
        {
          "content": "病死畜禽和病害畜禽产品无害化处理",
          "label": "B"
        },
        {
          "content": "运输车辆",
          "label": "C"
        },
        {
          "content": "环境清洗消毒",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《生猪屠宰管理条例》，县级以上人民政府农业农村主管部门应当建立生猪定点屠宰厂（场）信用档案。以下属于信用档案记录内容的有（     ）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "同步检疫结果",
          "label": "A"
        },
        {
          "content": "日常监督检查结果",
          "label": "B"
        },
        {
          "content": "违法行为查处情况",
          "label": "C"
        },
        {
          "content": "品质检验结果",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "为了加强动物检疫活动管理，预防、控制、净化、消灭动物疫病，（     ）保障公共卫生安全和人体健康，根据《中华人民共和国动物防疫法》，制定本办法。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "促进养殖业发展",
          "label": "A"
        },
        {
          "content": "防控人畜共患传染病",
          "label": "B"
        },
        {
          "content": "防控动物疫病",
          "label": "C"
        },
        {
          "content": "促进养殖业健康发展",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "张三从A县运输用于继续饲养的畜禽到达目的地后，未向A县动物卫生监督机构报告，A县农业农村局可以处以张三（   ）罚款。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "一千元以上五千元以下",
          "label": "A"
        },
        {
          "content": "一千元以上三千元以下",
          "label": "B"
        },
        {
          "content": "两千元以上五千元以下",
          "label": "C"
        },
        {
          "content": "两千元以上三千元以下",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "严格按照《家畜布鲁氏菌病防治技术规范》要求处置疫情，对发病和监测阳性动物进行（）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "治疗",
          "label": "A"
        },
        {
          "content": "隔离",
          "label": "B"
        },
        {
          "content": "转场",
          "label": "C"
        },
        {
          "content": "扑杀",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要完善动物检疫证明电子出证系统，建立指定通道监管信息（    ），并将数据共享至兽医卫生综合信息平台。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "定期采集",
          "label": "A"
        },
        {
          "content": "实时采集",
          "label": "B"
        },
        {
          "content": "及时更新",
          "label": "C"
        },
        {
          "content": "上传功能",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "县级以上人民政府有关部门实施生物安全监督检查，可以依法采取（ ）措施。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "向有关单位和个人了解情况",
          "label": "A"
        },
        {
          "content": "查阅、复制有关文件、资料、档案、记录、凭证等",
          "label": "B"
        },
        {
          "content": "查封涉嫌实施生物安全违法行为的场所、设施",
          "label": "C"
        },
        {
          "content": "扣押涉嫌实施生物安全违法行为的工具、设备以及相关物品",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》所称动物疫病，是指动物传染病、寄生虫病。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国畜牧法》的规定，销售、收购国务院农业农村主管部门规定应当加施标识而没有标识的畜禽，或者重复使用畜禽标识的，由县级以上地方人民政府农业农村主管部门和市场监督管理部门按照职责分工（       ）。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "责令改正",
          "label": "B"
        },
        {
          "content": "收缴重复使用的畜禽标识",
          "label": "C"
        },
        {
          "content": "可以处二千元以下罚款",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "县级以上人民政府农业农村主管部门依照《动物防疫法》的规定，可以对（　　）等活动中的动物防疫实施监督管理。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "动物饲养、屠宰、经营",
          "label": "A"
        },
        {
          "content": "动物隔离、运输",
          "label": "B"
        },
        {
          "content": "动物产品生产、经营",
          "label": "C"
        },
        {
          "content": "动物产品加工、贮藏",
          "label": "D"
        },
        {
          "content": "动物产品的运输",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "2021年修订的《生猪屠宰管理条例》自（       ）起施行。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "2021年6月25日",
          "label": "A"
        },
        {
          "content": "2021年8月1日",
          "label": "B"
        },
        {
          "content": "2021年10月1日",
          "label": "C"
        },
        {
          "content": "2021年9月1日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对散养动物，采取（）的方式进行强制免疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "春秋冬三季集中免疫",
          "label": "A"
        },
        {
          "content": "春秋两季集中免疫与定期补免相结合",
          "label": "B"
        },
        {
          "content": "自主免疫与定期补免相结合",
          "label": "C"
        },
        {
          "content": "程序化免疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "行政机关根据检验、检测、检疫结果，作出不予行政许可决定的，应当（      ）不予行政许可所依据的技术标准、技术规范。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "当面口头说明或书面说明",
          "label": "A"
        },
        {
          "content": "书面说明",
          "label": "B"
        },
        {
          "content": "当面口头说明",
          "label": "C"
        },
        {
          "content": "电话告知",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政许可申请可以通过（      ）等方式提出。",
      "standard_answer": "A,B,C,D,E,F",
      "option_list": [
        {
          "content": "信函",
          "label": "A"
        },
        {
          "content": "电子邮件",
          "label": "B"
        },
        {
          "content": "电报",
          "label": "C"
        },
        {
          "content": "电传",
          "label": "D"
        },
        {
          "content": "传真",
          "label": "E"
        },
        {
          "content": "电子数据交换",
          "label": "F"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "从事动物产品（    ）等活动的单位和个人，应当按照国家有关规定做好病害动物产品的无害化处理，或者委托无害化处理场所处理。",
      "standard_answer": "B,C,D,E",
      "option_list": [
        {
          "content": "运输",
          "label": "A"
        },
        {
          "content": "生产",
          "label": "B"
        },
        {
          "content": "经营",
          "label": "C"
        },
        {
          "content": "加工",
          "label": "D"
        },
        {
          "content": "贮藏",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下生猪产品中，必须是生猪定点屠宰厂（场）经检疫和肉品品质检验合格的生猪产品的是（       ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "从事生猪产品销售的单位和个人生产经营的生猪产品",
          "label": "A"
        },
        {
          "content": "从事肉食品生产加工的单位和个人生产经营的生猪产品",
          "label": "B"
        },
        {
          "content": "餐饮服务经营者生产经营的生猪产品",
          "label": "C"
        },
        {
          "content": "集中用餐单位生产经营的生猪产品",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "严格按照检疫规程规定实施检疫，严禁（）等行为，对相关违法违规行为依法严肃查处。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "不检疫即出证",
          "label": "A"
        },
        {
          "content": "违规出证",
          "label": "B"
        },
        {
          "content": "检疫收费",
          "label": "C"
        },
        {
          "content": "倒卖动物检疫证章标志",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "发生动物疫情时，生猪定点屠宰厂（场）应当按照国务院农业农村主管部门的规定，开展（       ），做好动物疫情排查和报告。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "全厂（场）清洗消毒",
          "label": "A"
        },
        {
          "content": "动物疫病检测",
          "label": "B"
        },
        {
          "content": "动物疫病监测",
          "label": "C"
        },
        {
          "content": "生猪来源追溯",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "在岗培训由省、市两级农业农村部门负责组织实施。每人每年参加培训时间不得少于（）学时（含线上培训）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "20",
          "label": "A"
        },
        {
          "content": "30",
          "label": "B"
        },
        {
          "content": "40",
          "label": "C"
        },
        {
          "content": "50",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "同一个违法行为违反多个法律规范应当给予罚款处罚的，按照（     ）的规定处罚。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "罚款数额之和",
          "label": "A"
        },
        {
          "content": "罚款数额高",
          "label": "B"
        },
        {
          "content": "罚款数额低",
          "label": "C"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员弄虚作假，骗取职务、职级、衔级、级别、岗位和职员等级、职称、待遇、资格、学历、学位、荣誉、奖励或者其他利益情节较重的，予以（）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "记大过",
          "label": "A"
        },
        {
          "content": "降级",
          "label": "B"
        },
        {
          "content": "撤职",
          "label": "C"
        },
        {
          "content": "开除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物检疫管理办法》规定，张三是县监督所的人员，经省级农业农村厅确认为官方兽医后，应当由他所在的县级农业农村局（     ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "考核",
          "label": "A"
        },
        {
          "content": "任命",
          "label": "B"
        },
        {
          "content": "颁发官方兽医证",
          "label": "C"
        },
        {
          "content": "公布",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，县级人民政府农业农村主管部门可以根据动物防疫工作需要，向（    ）派驻兽医机构或者工作人员。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "村",
          "label": "A"
        },
        {
          "content": "乡",
          "label": "B"
        },
        {
          "content": "镇",
          "label": "C"
        },
        {
          "content": "特定区域",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "运载工具在装载前和卸载后应当及时（     ）。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "报告",
          "label": "A"
        },
        {
          "content": "清洗",
          "label": "B"
        },
        {
          "content": "烘干",
          "label": "C"
        },
        {
          "content": "消毒",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "养殖场(户)要详细记录（）等信息。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "疫苗种类",
          "label": "A"
        },
        {
          "content": "生产厂家",
          "label": "B"
        },
        {
          "content": "品牌",
          "label": "C"
        },
        {
          "content": "生产批号",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，有关机关或者人员有下列（）情形的，由管辖机关责令改正，依法给予处理。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "拒不配合或者阻碍调查的",
          "label": "A"
        },
        {
          "content": "拒不执行政务处分决定的",
          "label": "B"
        },
        {
          "content": "对调查人员进行打击报复的",
          "label": "C"
        },
        {
          "content": "诬告陷害公职人员的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "A省的张三从B省调入一车鸡苗，应当经省级人民政府设立的（    ）进入省境。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "指定通道",
          "label": "A"
        },
        {
          "content": "监督检查站",
          "label": "B"
        },
        {
          "content": "防疫检查站",
          "label": "C"
        },
        {
          "content": "指定道路",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "县级以上地方人民政府农业农村主管部门应当根据（       ）和国务院农业农村主管部门的规定，加强对生猪定点屠宰厂（场）质量安全管理状况的监督检查。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "日常监督检查情况",
          "label": "A"
        },
        {
          "content": "生猪肉品品质监测结果",
          "label": "B"
        },
        {
          "content": "生猪屠宰质量安全风险监测结果",
          "label": "C"
        },
        {
          "content": "生猪肉品抽样检查结果",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，在（    ）年底前全面实施无纸化出证，推动实现动物检疫证明电子证照全国互通互认。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "2024",
          "label": "A"
        },
        {
          "content": "2025",
          "label": "B"
        },
        {
          "content": "2026",
          "label": "C"
        },
        {
          "content": "2027",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《畜禽标识和养殖档案管理办法》规定，羊的畜禽种类代码是（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "1",
          "label": "A"
        },
        {
          "content": "2",
          "label": "B"
        },
        {
          "content": "3",
          "label": "C"
        },
        {
          "content": "4",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）依照本条例规定应当召回生猪产品而不召回的，由农业农村主管部门责令召回，停止屠宰；拒不召回或者拒不停止屠宰的，下列处罚正确的是（       ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "责令停业整顿，没收生猪产品和违法所得",
          "label": "A"
        },
        {
          "content": "货值金额不足1万元的，并处5万元以上10万元以下的罚款",
          "label": "B"
        },
        {
          "content": "货值金额1万元以上的，并处货值金额10倍以上20倍以下的罚款",
          "label": "C"
        },
        {
          "content": "对其直接负责的主管人员和其他直接责任人员处5万元以上10万元以下的罚款",
          "label": "D"
        },
        {
          "content": "情节严重的，由设区的市级人民政府吊销生猪定点屠宰证书，收回生猪定点屠宰标志牌",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关依法作出不予行政许可的书面决定的，应当说明理由，并告知申请人享有依法申请（   ）或者提起（     ）的权利。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "仲裁",
          "label": "A"
        },
        {
          "content": "行政复议",
          "label": "B"
        },
        {
          "content": "调解",
          "label": "C"
        },
        {
          "content": "行政诉讼",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物检疫证章标志的内容、各市、规格、编码和制作等要求，由（   ）统一规定。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "县级以上地方人民政府",
          "label": "A"
        },
        {
          "content": "农业农村部",
          "label": "B"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "C"
        },
        {
          "content": "省级动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "农业农村部规定，生猪屠宰检疫验讫印章印模展开圆周长度为（）毫米。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "128",
          "label": "A"
        },
        {
          "content": "165",
          "label": "B"
        },
        {
          "content": "68",
          "label": "C"
        },
        {
          "content": "42",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员工作中有弄虚作假，误导、欺骗行为，造成不良后果或者影响的，予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国畜牧法》规定，畜禽养殖场应当建立养殖档案。以下属于养殖档案载明的内容有（     ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "畜禽的品种、数量、繁殖记录、标识情况、来源和进出场日期",
          "label": "A"
        },
        {
          "content": "饲料、饲料添加剂、兽药等投入品的来源、名称、使用对象、时间和用量",
          "label": "B"
        },
        {
          "content": "检疫、免疫、消毒情况",
          "label": "C"
        },
        {
          "content": "畜禽发病、死亡和无害化处理情况",
          "label": "D"
        },
        {
          "content": "畜禽粪污收集、储存、无害化处理和资源化利用情况",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "当事人有（     ）情形的，应当从轻或者减轻行政处罚：",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "主动消除或者减轻违法行为危害后果的",
          "label": "A"
        },
        {
          "content": "受他人胁迫或者诱骗实施违法行为的",
          "label": "B"
        },
        {
          "content": "主动供述行政机关尚未掌握的违法行为的",
          "label": "C"
        },
        {
          "content": "配合行政机关查处违法行为有立功表现的",
          "label": "D"
        },
        {
          "content": "违法行为轻微并及时改正，没有造成危害后果的",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "不满十四周岁的未成年人有违法行为的，（   ）行政处罚，责令监护人加以管教。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "减轻",
          "label": "A"
        },
        {
          "content": "不予",
          "label": "B"
        },
        {
          "content": "免除",
          "label": "C"
        },
        {
          "content": "从轻",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政许可法》，下列说法正确的是（      ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "地方性法规和省、自治区、直辖市人民政府规章，可以设定企业或者其他组织的设立登记及其前置性行政许可",
          "label": "A"
        },
        {
          "content": "地方性法规和省、自治区、直辖市人民政府规章设定的行政许可，不得限制其他地区的个人或者企业到本地区从事生产经营和提供服务，不得限制其他地区的商品进入本地区市场",
          "label": "B"
        },
        {
          "content": "地方性法规和省、自治区、直辖市人民政府规章，可以设定应当由国家统一确定的公民、法人或者其他组织的资格、资质的行政许可",
          "label": "C"
        },
        {
          "content": "省、自治区、直辖市人民政府规章不可以设定临时性的行政许可",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，县级以上（  ）应当做好本行政区域内的动物检疫信息数据管理工作。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "农业农村主管部门",
          "label": "B"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "畜牧兽医主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要完善动物检疫证明电子出证系统，及时采集、更新指定通道及有关检查站（   ）等基础信息。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "名称",
          "label": "A"
        },
        {
          "content": "数量",
          "label": "B"
        },
        {
          "content": "位置坐标",
          "label": "C"
        },
        {
          "content": "地址",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，担任领导职务的公职人员有违法行为，被（）领导职务的，监察机关可以同时给予政务处分。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "罢免",
          "label": "A"
        },
        {
          "content": "撤销",
          "label": "B"
        },
        {
          "content": "免去",
          "label": "C"
        },
        {
          "content": "辞去",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，动物和动物产品无害化处理场所除符合基础动物防疫条件外，还应当具有（    ）。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "病原检测设备",
          "label": "A"
        },
        {
          "content": "病原消毒设施",
          "label": "B"
        },
        {
          "content": "病原检测能力",
          "label": "C"
        },
        {
          "content": "符合动物防疫要求的专用运输车辆",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《病死畜禽和病害畜禽产品无害化处理管理办法》，以下死亡畜禽依法由所在地县级人民政府组织收集、处理并溯源的有（     ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "在江河发现的死亡畜禽",
          "label": "A"
        },
        {
          "content": "在湖泊发现的死亡畜禽",
          "label": "B"
        },
        {
          "content": "在城市公共场所现的死亡畜禽",
          "label": "C"
        },
        {
          "content": "在水库发现的死亡畜禽",
          "label": "D"
        },
        {
          "content": "在乡村发现的死亡畜禽",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，县级以上动物卫生监督机构提出官方兽医任命建议，报（    ）审核。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "农业农村部",
          "label": "A"
        },
        {
          "content": "上级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "同级农业农村主管部门",
          "label": "C"
        },
        {
          "content": "上级动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，跨省、自治区、直辖市引进的乳用、种用动物到达输入地后，应当在（　　）进行隔离观察。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "隔离场",
          "label": "A"
        },
        {
          "content": "饲养场内的隔离舍",
          "label": "B"
        },
        {
          "content": "动物卫生监督机构指定的隔离场",
          "label": "C"
        },
        {
          "content": "隔离场或者饲养场内的隔离舍",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违反规定从事或者参与营利性活动，或者违反规定兼任职务、领取报酬，情节较重的，予以（）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "记大过",
          "label": "A"
        },
        {
          "content": "降级",
          "label": "B"
        },
        {
          "content": "撤职",
          "label": "C"
        },
        {
          "content": "开除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "某养殖场向某无规定疫病区输入相关易感动物（牛），根据《动物检疫管理办法》规定，应当在（    ）隔离，隔离检疫期为（   ）天。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "45",
          "label": "A"
        },
        {
          "content": "输入地县级动物卫生监督机构指定的隔离场所",
          "label": "B"
        },
        {
          "content": "30",
          "label": "C"
        },
        {
          "content": "在输入地省级动物卫生监督机构指定隔离场所进行隔离",
          "label": "D"
        },
        {
          "content": "隔离场或养殖场的隔离舍",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "运输病死畜禽和病害畜禽产品的单位和个人，应当遵守下列哪些规定（     ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "及时对车辆、相关工具及作业环境进行消毒",
          "label": "A"
        },
        {
          "content": "作业过程中如发生渗漏，应当妥善处理后再继续运输",
          "label": "B"
        },
        {
          "content": "做好人员防护和消毒",
          "label": "C"
        },
        {
          "content": "定期进行健康体检",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，有关单位无正当理由拒不采纳监察建议的，由（）责令改正。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "上级机关",
          "label": "A"
        },
        {
          "content": "主管部门",
          "label": "B"
        },
        {
          "content": "任免机关",
          "label": "C"
        },
        {
          "content": "监察机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》所称动物，是指家畜家禽和人工饲养、（    ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "合法捕获的其他动物",
          "label": "A"
        },
        {
          "content": "合法捕获的野生动物",
          "label": "B"
        },
        {
          "content": "捕获的其他动物",
          "label": "C"
        },
        {
          "content": "捕获的野生动物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，（　　）主管本行政区域内的动物检疫工作，负责动物检疫监督管理工作。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "县级以上动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "动物卫生监督机构",
          "label": "B"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "C"
        },
        {
          "content": "动物疫病预防控制中心",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "个人和组织发现违法从事行政许可事项的活动，有权向行政机关举报，行政机关应当（       ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "对举报人实施奖励",
          "label": "A"
        },
        {
          "content": "及时核实、处理",
          "label": "B"
        },
        {
          "content": "将审查结果告知举报人",
          "label": "C"
        },
        {
          "content": "将审查结果向社会公示",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国畜牧法》规定，国家加快建立（     ）的畜禽交易市场体系。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "防疫规范",
          "label": "A"
        },
        {
          "content": "统一开放",
          "label": "B"
        },
        {
          "content": "竞争有序",
          "label": "C"
        },
        {
          "content": "安全便捷",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "（ ）应当对传染病、动植物疫病和列入监测范围的不明原因疾病开展主动监测，收集、分析、报告监测信息，预测新发突发传染病、动植物疫病的发生、流行趋势。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "医疗机构",
          "label": "A"
        },
        {
          "content": "疾病预防控制机构",
          "label": "B"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "植物病虫害预防控制机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，下级监察机关根据上级监察机关的指定管辖决定进行调查的案件，调查终结后，对不属于本监察机关管辖范围内的监察对象，应当交（）依法作出政务处分决定。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "上级监察机关",
          "label": "A"
        },
        {
          "content": "同级人民政府",
          "label": "B"
        },
        {
          "content": "有管理权限的监察机关",
          "label": "C"
        },
        {
          "content": "被调查单位",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "动物防疫条件合格证丢失或者损毁的，应当在（ ）向原发证机关申请补发。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "三日内",
          "label": "A"
        },
        {
          "content": "七日内",
          "label": "B"
        },
        {
          "content": "十五日内",
          "label": "C"
        },
        {
          "content": "一个月内",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列哪些情形，行政处罚无效。（     ）",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "行政处罚没有依据",
          "label": "A"
        },
        {
          "content": "实施主体不具有行政主体资格",
          "label": "B"
        },
        {
          "content": "违反法定程序构成重大且明显违法",
          "label": "C"
        },
        {
          "content": "违法事实不清",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "为深入贯彻落实（        ）有关要求，农业部办公厅发布《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "《中华人民共和国动物防疫法》",
          "label": "A"
        },
        {
          "content": "《国务院办公厅关于加强非洲猪瘟防控工作的意见》",
          "label": "B"
        },
        {
          "content": "《国务院办公厅关于促进畜牧业高质量发展的意见》",
          "label": "C"
        },
        {
          "content": "《动物检疫管理办法》",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《动物防疫法》规定，因科研、药用、展示等特殊情形需要非食用性利用的野生动物，应当按照国家有关规定报野生动物保护部门检疫，检疫合格的，方可利用。（　　）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，动物卫生监督机构受理申报后，可以安排协检人员（    ）到现场或指定地点核实信息，开展临床健康检查。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "独立",
          "label": "A"
        },
        {
          "content": "帮助官方兽医",
          "label": "B"
        },
        {
          "content": "协助官方兽医",
          "label": "C"
        },
        {
          "content": "代替官方兽医",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，国家对动物疫病实行（   ）方针。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "净化为主",
          "label": "A"
        },
        {
          "content": "预防与控制、净化、消灭相结合",
          "label": "B"
        },
        {
          "content": "预防为主",
          "label": "C"
        },
        {
          "content": "净化与控制、诊疗、消灭相结合",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫证明填写中，以下针对动物种类填写方式正确的有（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "猪",
          "label": "A"
        },
        {
          "content": "奶牛",
          "label": "B"
        },
        {
          "content": "雏鸡",
          "label": "C"
        },
        {
          "content": "商品蛋",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，要规范设置指定通道引导标志，在显要位置公示（   ）和监督电话等信息。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "工作职责",
          "label": "A"
        },
        {
          "content": "工作程序",
          "label": "B"
        },
        {
          "content": "动物运输管理要求",
          "label": "C"
        },
        {
          "content": "监督检查人员",
          "label": "D"
        },
        {
          "content": "设立依据",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国家动物疫病强制免疫坚持（）的原则。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "人病兽防",
          "label": "A"
        },
        {
          "content": "关口前移",
          "label": "B"
        },
        {
          "content": "预防为主",
          "label": "C"
        },
        {
          "content": "应免尽免",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国行政许可法》行政机关可以对被许可人生产经营的产品依法进行（     ），对其生产经营场所依法进行实地检查。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "查封",
          "label": "A"
        },
        {
          "content": "抽样检查",
          "label": "B"
        },
        {
          "content": "检验",
          "label": "C"
        },
        {
          "content": "检测",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物检疫管理办法》规定，运输用于的（    ）畜禽到达目的地后，货主或者承运人应当在三日内向启运地县级动物卫生监督机构报告。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "继续饲养",
          "label": "A"
        },
        {
          "content": "展示",
          "label": "B"
        },
        {
          "content": "屠宰",
          "label": "C"
        },
        {
          "content": "实验",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "有下列哪些情形，行政机关及其工作人员由其上级行政机关或者监察机关责令改正；情节严重的，对直接负责的主管人员和其他直接责任人员依法给予行政处分。 （      ）",
      "standard_answer": "A,B,C,D,E,F",
      "option_list": [
        {
          "content": "对符合法定条件的行政许可申请不予受理的",
          "label": "A"
        },
        {
          "content": "不在办公场所公示依法应当公示的材料的",
          "label": "B"
        },
        {
          "content": "在受理、审查、决定行政许可过程中，未向申请人、利害关系人履行法定告知义务的 ",
          "label": "C"
        },
        {
          "content": "申请人提交的申请材料不齐全、不符合法定形式，不一次告知申请人必须补正的全部内容的",
          "label": "D"
        },
        {
          "content": "未依法说明不受理行政许可申请或者不予行政许可的理由的",
          "label": "E"
        },
        {
          "content": "依法应当举行听证而不举行听证的",
          "label": "F"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，给予公职人员政务处分，应当（    )。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "事实清楚、证据确凿",
          "label": "A"
        },
        {
          "content": "定性准确、处理恰当",
          "label": "B"
        },
        {
          "content": "程序合法、手续完备",
          "label": "C"
        },
        {
          "content": "有法必究、依法严惩",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，动物卫生监督证章标志的填写可以采取（）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "黑色钢笔",
          "label": "A"
        },
        {
          "content": "签字笔",
          "label": "B"
        },
        {
          "content": "圆珠笔",
          "label": "C"
        },
        {
          "content": "打印",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《三类动物疫病防治规范》，对于需使用抗菌药、抗病毒药、驱虫和杀虫剂、消毒剂等进行治疗的，应当符合国家兽药管理规定。药物使用应确保精准，严格执行用药（     ）等规定，建立用药记录。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "时间",
          "label": "A"
        },
        {
          "content": "剂量",
          "label": "B"
        },
        {
          "content": "疗程",
          "label": "C"
        },
        {
          "content": "休药期",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "屠宰加工场所查验进场待宰动物，发现动物染疫或者疑似染疫的，应当立即向所在地（      ）报告。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "农业综合执法机构",
          "label": "A"
        },
        {
          "content": "动物卫生监督机构",
          "label": "B"
        },
        {
          "content": "农业农村主管部门",
          "label": "C"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，输入到无规定动物疫病区的相关易感动物产品，应当在输入地省级动物卫生监督机构指定的地点检疫。检疫合格的，由（    ）动物卫生监督机构的官方兽医出具动物检疫证明。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "省级",
          "label": "A"
        },
        {
          "content": "当地县级",
          "label": "B"
        },
        {
          "content": "省级指定",
          "label": "C"
        },
        {
          "content": "输入地省级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《行政处罚法》，不得限制或者变相限制当事人享有的（       ）。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "自主选择权",
          "label": "A"
        },
        {
          "content": "陈述权",
          "label": "B"
        },
        {
          "content": "知情权",
          "label": "C"
        },
        {
          "content": "申辩权",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，向无规定动物疫病区输入相关易感易感动物产品的，货主在输入地（     ）地点申报。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "无疫区动物卫生监督机构指定的",
          "label": "A"
        },
        {
          "content": "省级农业农村主管部门指定的",
          "label": "B"
        },
        {
          "content": "县级动物卫生监督机构指定的",
          "label": "C"
        },
        {
          "content": "省级动物卫生监督机构指定的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "被许可人需要延续依法取得的行政许可的有效期的，行政机关应当根据被许可人的申请，在该行政许可有效期届满前作出是否准予延续的决定；逾期未作决定的，（      ）延续。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "视为不准予",
          "label": "A"
        },
        {
          "content": "不得",
          "label": "B"
        },
        {
          "content": "视为准予",
          "label": "C"
        },
        {
          "content": "由该行政机关零星解释是否准予",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，依法履行公职的人员有违法行为的，监察机关可以予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列属于动物屠宰加工场所应当符合的防疫条件的是：（ ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "具有相对独立的动物隔离舍。",
          "label": "A"
        },
        {
          "content": "屠宰间配备检疫操作台。",
          "label": "B"
        },
        {
          "content": "有符合国家规定的病死动物和病害动物产品无害化处理设施设备或者冷藏冷冻等暂存设施设备。",
          "label": "C"
        },
        {
          "content": "建立动物进场查验登记、动物产品出场登记、检疫申报、疫情报告、无害化处理等动物防疫制度。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "坚持一地一策，根据各地布病流行形势，以（）为基本单位连片推进布病防控，免疫区以实施持续免疫为主，非免疫区以实施持续监测剔除为主，有效落实各项基础性、综合性防控措施。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "省 ",
          "label": "A"
        },
        {
          "content": "市",
          "label": "B"
        },
        {
          "content": "县",
          "label": "C"
        },
        {
          "content": "区",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，发生重大动物疫情时，（    ）负责划定动物疫病风险区，禁止或者限制特定动物、动物产品由高风险区向低风险区调运。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "省级人民政府",
          "label": "B"
        },
        {
          "content": "国务院农业农村主管部门",
          "label": "C"
        },
        {
          "content": "县级以上地方人民政府",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，补检的动物如果畜禽标识不符合规定，应该具备下列（   ）条件，补检合格，出具动物检疫证明。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "货主于五日内提供检疫规程规定的实验室疫病检测报告，检测结果合格",
          "label": "A"
        },
        {
          "content": "检疫申报需要提供的材料齐全、符合要求",
          "label": "B"
        },
        {
          "content": "临床检查健康",
          "label": "C"
        },
        {
          "content": "货主于七日内提供检疫规程规定的实验室疫病检测报告，检测结果合格",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物防疫法》规定，跨省、自治区、直辖市引进的（     ）动物到达输入地后，货主应当按照国务院农业农村主管部门的规定对其进行隔离观察。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "继续饲养的",
          "label": "A"
        },
        {
          "content": "非食用野生动物",
          "label": "B"
        },
        {
          "content": "种用",
          "label": "C"
        },
        {
          "content": "乳用",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "进出口动物和动物产品，承运人凭（     ）运递。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "进口报关单证",
          "label": "A"
        },
        {
          "content": "动物检疫证明",
          "label": "B"
        },
        {
          "content": "进口报税单证",
          "label": "C"
        },
        {
          "content": "海关签发的检疫单证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "病死畜禽和病害畜禽产品无害化处理要坚持的原则是（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "统筹规划与属地负责相结合",
          "label": "A"
        },
        {
          "content": "政府监管与市场运作相结合",
          "label": "B"
        },
        {
          "content": "财政补助与保险联动相结合",
          "label": "C"
        },
        {
          "content": "集中处理与自行处理相结合",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下属于《全国畜间人兽共患病防治规划2022—2030年）》提出的牛结核病的重点防治措施的是（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "加强监测",
          "label": "A"
        },
        {
          "content": "加快推进净化",
          "label": "B"
        },
        {
          "content": "加强生物安全管理",
          "label": "C"
        },
        {
          "content": "加强奶牛群体风险监测",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": " 《动物检疫管理办法》规定，禁止（     ）依法应当检疫而未经检疫或者检疫不合格的动物产品。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "加工",
          "label": "A"
        },
        {
          "content": "经营",
          "label": "B"
        },
        {
          "content": "运输",
          "label": "C"
        },
        {
          "content": "贮藏",
          "label": "D"
        },
        {
          "content": "屠宰",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员受到解除人事关系或者劳动关系处理的，不得录用为（）。",
      "standard_answer": "B,D",
      "option_list": [
        {
          "content": "企业员工",
          "label": "A"
        },
        {
          "content": "公务员",
          "label": "B"
        },
        {
          "content": "国有企业管理人员",
          "label": "C"
        },
        {
          "content": "参照《中华人民共和国公务员法》管理的人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "动物饲养场、屠宰企业的（     ），应当协助官方兽医实施检疫。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "肉品品质检验人员",
          "label": "A"
        },
        {
          "content": "兽医卫生人员",
          "label": "B"
        },
        {
          "content": "执业兽医",
          "label": "C"
        },
        {
          "content": "动物防疫技术人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国家动物疫病强制免疫的病种有（）",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "高致病性禽流感",
          "label": "A"
        },
        {
          "content": "口蹄疫",
          "label": "B"
        },
        {
          "content": "小反刍兽疫",
          "label": "C"
        },
        {
          "content": "布鲁氏菌病",
          "label": "D"
        },
        {
          "content": "包虫病",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物防疫法》规定，县级以上地方人民政府农业农村主管部门根据动物疫病预防、控制需要，经所在地县级以上地方人民政府批准，可以在车站、港口、机场等相关场所派驻(      )。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "官方兽医",
          "label": "A"
        },
        {
          "content": "官方兽医或者工作人员",
          "label": "B"
        },
        {
          "content": "工作人员",
          "label": "C"
        },
        {
          "content": "官方兽医和工作人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": " 以下属于病死畜禽和病害畜禽产品无害化处理场所应当建立并严格执行的制度的有（     ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "设施设备运行管理制度",
          "label": "A"
        },
        {
          "content": "清洗消毒制度",
          "label": "B"
        },
        {
          "content": "人员防护制度",
          "label": "C"
        },
        {
          "content": "生物安全制度",
          "label": "D"
        },
        {
          "content": "安全生产和应急处理制度",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员不按照规定请示、报告重大事项，情节较重的，予以（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "警告",
          "label": "A"
        },
        {
          "content": "记过",
          "label": "B"
        },
        {
          "content": "记大过",
          "label": "C"
        },
        {
          "content": "降级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员因违法行为获得的职务、职级、衔级、级别、岗位和职员等级、职称、待遇、资格、学历、学位、荣誉、奖励等其他利益，（）应当建议有关机关、单位、组织按规定予以纠正。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "检查机关",
          "label": "A"
        },
        {
          "content": "监察机关",
          "label": "B"
        },
        {
          "content": "机关党委",
          "label": "C"
        },
        {
          "content": "机管纪委",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫证明填写中，以下针对用途填写方式正确的有（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "演出",
          "label": "A"
        },
        {
          "content": "屠宰",
          "label": "B"
        },
        {
          "content": "饲养",
          "label": "C"
        },
        {
          "content": "销售",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "在中华人民共和国境内从事畜禽的遗传资源保护利用以及（     ）等活动，适用《中华人民共和国畜牧法》。",
      "standard_answer": "A,B,D,E",
      "option_list": [
        {
          "content": "畜禽的繁育、饲养",
          "label": "A"
        },
        {
          "content": "畜禽的经营",
          "label": "B"
        },
        {
          "content": "以畜禽产品为原料的食品生产",
          "label": "C"
        },
        {
          "content": "畜禽的运输",
          "label": "D"
        },
        {
          "content": "畜禽的屠宰",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "病死畜禽和病害畜禽产品集中暂存点应当具备下列哪些条件（     ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "有独立封闭的贮存区域，并且防渗、防漏、防鼠、防盗，易于清洗消毒",
          "label": "A"
        },
        {
          "content": "有冷藏冷冻、清洗消毒等设施设备",
          "label": "B"
        },
        {
          "content": "设置显著警示标识",
          "label": "C"
        },
        {
          "content": "有符合动物防疫需要的其他设施设备",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，我国流通的检疫证明包括（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "《动物检疫合格证明》（动物A）",
          "label": "A"
        },
        {
          "content": "《动物检疫合格证明》（动物B）",
          "label": "B"
        },
        {
          "content": "《动物检疫合格证明》（产品A）",
          "label": "C"
        },
        {
          "content": "《动物检疫合格证明》（产品B）",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "县级以上人民政府农业农村主管部门应当建立生猪定点屠宰厂（场）（       ）档案，记录日常监督检查结果、违法行为查处等情况，并依法向社会公示。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "信誉",
          "label": "A"
        },
        {
          "content": "信用",
          "label": "B"
        },
        {
          "content": "诚信",
          "label": "C"
        },
        {
          "content": "评级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员利用宗族或者黑恶势力等欺压群众，或者纵容、包庇黑恶势力活动的，予以（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "开除",
          "label": "A"
        },
        {
          "content": "撤职",
          "label": "B"
        },
        {
          "content": "留党察看",
          "label": "C"
        },
        {
          "content": "党内警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "农产品生产企业、农民专业合作社、农业社会化服务组织应当建立农产品生产记录，农产品生产记录应如实记载（ ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "使用农业投入品的名称、来源、用法、用量",
          "label": "A"
        },
        {
          "content": "使用农业投入品的使用、停用日期",
          "label": "B"
        },
        {
          "content": "动物疫病、农作物病虫害的发生和防治情况",
          "label": "C"
        },
        {
          "content": "收获、屠宰或者捕捞的日期",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "病死畜禽和病害畜禽产品专用运输车辆备案时，应当通过农业农村部指定的信息系统提交下列哪些资料（     ）。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "车辆所有权人的营业执照",
          "label": "A"
        },
        {
          "content": "车辆所有权人的健康证明",
          "label": "B"
        },
        {
          "content": "运输车辆行驶证",
          "label": "C"
        },
        {
          "content": "运输车辆照片",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员拒不执行、擅自改变集体作出的重大决定情节严重的，予以（）。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "记过",
          "label": "A"
        },
        {
          "content": "记大过",
          "label": "B"
        },
        {
          "content": "降级",
          "label": "C"
        },
        {
          "content": "撤职",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，基层群众性自治组织中从事管理的人员受到政务处分的，应当由县级或者乡镇人民政府根据具体情况减发或者扣发（）。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "工资",
          "label": "A"
        },
        {
          "content": "补贴",
          "label": "B"
        },
        {
          "content": "奖金",
          "label": "C"
        },
        {
          "content": "医疗报销",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "农业农村部规定，生猪屠宰检疫验讫印章印模宽度为（）毫米。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "128",
          "label": "A"
        },
        {
          "content": "165",
          "label": "B"
        },
        {
          "content": "68",
          "label": "C"
        },
        {
          "content": "42",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员违反规定取得外国国籍或者获取境外永久居留资格、长期居留许可的，予以（）。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "记大过",
          "label": "A"
        },
        {
          "content": "降级",
          "label": "B"
        },
        {
          "content": "撤职",
          "label": "C"
        },
        {
          "content": "开除",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下面关于行政机关对行政许可申请处理的表述，正确的是（       ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "申请事项依法不需要取得行政许可的，应当即时告知申请人不受理",
          "label": "A"
        },
        {
          "content": "申请事项依法不属于本行政机关职权范围的，应当即时作出不予受理的决定，并告知申请人向有关行政机关申请",
          "label": "B"
        },
        {
          "content": "申请材料存在可以当场更正的错误的，应当允许申请人当场更正",
          "label": "C"
        },
        {
          "content": "申请材料不齐全或者不符合法定形式的，应当当场或者在十日内一次告知申请人需要补正的全部内容",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各省份可采用()等多种形式，全面推进“先打后补”工作。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "养殖场(户)自行免疫",
          "label": "A"
        },
        {
          "content": "第三方服务主体免疫",
          "label": "B"
        },
        {
          "content": "政府购买服务",
          "label": "C"
        },
        {
          "content": "养殖场(户)自愿免疫",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生物安全是（ ）的重要组成部分。维护生物安全应当贯彻总体国家安全观，统筹发展和安全，坚持以人为本、风险预防、分类管理、协同配合的原则。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "生命安全",
          "label": "A"
        },
        {
          "content": "国家安全",
          "label": "B"
        },
        {
          "content": "种质资源保护",
          "label": "C"
        },
        {
          "content": "生态安全",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定,(    )主管全国动物检疫工作。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "农业农村部",
          "label": "A"
        },
        {
          "content": "动物疫病预防控制中心",
          "label": "B"
        },
        {
          "content": "农业农村部畜牧兽医局",
          "label": "C"
        },
        {
          "content": "国务院",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "冒用或者使用伪造的生猪定点屠宰证书或者生猪定点屠宰标志牌的，应给予的处罚有（       ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "责令关闭",
          "label": "A"
        },
        {
          "content": "没收生猪、生猪产品、屠宰工具和设备以及违法所得",
          "label": "B"
        },
        {
          "content": "货值金额不足1万元的，并处5万元以上10万元以下的罚款",
          "label": "C"
        },
        {
          "content": "货值金额1万元以上的，并处货值金额10倍以上20倍以下的罚款",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "制定农产品质量安全标准应（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "依照法律、行政法规的规定进行制定和发布",
          "label": "A"
        },
        {
          "content": "充分考虑农产品质量安全风险评估结果",
          "label": "B"
        },
        {
          "content": "听取农产品生产经营者、消费者、有关部门、行业协会等的意见",
          "label": "C"
        },
        {
          "content": "由农业农村主管部门负责制定",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "防治日本血吸虫病可以在有钉螺分布的低洼沼泽地带（非基本农田） 开挖池塘、实施标准化池塘改造，发展优质水产养殖业，实行（     ）灭螺。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "放水",
          "label": "A"
        },
        {
          "content": "净水",
          "label": "B"
        },
        {
          "content": "蓄水",
          "label": "C"
        },
        {
          "content": "生物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "刘某欲从B省C县购进用于饲养的非种用动物到A省D县，到达目的地后，刘某应当在规定时间内向 （　　）动物卫生监督机构报告。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "A省",
          "label": "A"
        },
        {
          "content": "B省",
          "label": "B"
        },
        {
          "content": "C县",
          "label": "C"
        },
        {
          "content": "D县",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）出借、转让生猪定点屠宰证书或者生猪定点屠宰标志牌的，应给予的处罚有（       ）。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "给予警告",
          "label": "A"
        },
        {
          "content": "责令改正",
          "label": "B"
        },
        {
          "content": "由设区的市级人民政府吊销生猪定点屠宰证书，收回生猪定点屠宰标志牌",
          "label": "C"
        },
        {
          "content": "有违法所得的，由农业农村主管部门没收违法所得，并处5万元以上10万元以下的罚款",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "农业农村部办公厅关于进一步加强官方兽医管理工作的通知（农办牧〔2023〕10号）要求各地要按规定做好官方兽医(),定期更新完善相关信息。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "资格确认",
          "label": "A"
        },
        {
          "content": "任命",
          "label": "B"
        },
        {
          "content": "注销",
          "label": "C"
        },
        {
          "content": "备案",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，补检的动物如果检疫申报需要提供的材料不齐全，应该具备下列（   ）条件，补检合格，出具动物检疫证明。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "货主于七日内提供检疫规程规定的实验室疫病检测报告，检测结果合格",
          "label": "A"
        },
        {
          "content": "畜禽标识符合规定",
          "label": "B"
        },
        {
          "content": "临床检查健康",
          "label": "C"
        },
        {
          "content": "货主于五日内提供检疫规程规定的实验室疫病检测报告，检测结果合格",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《三类动物疫病防治规范》，从事动物饲养、屠宰、经营、隔离等活动的单位和个人应控制（     ）等进出，并严格消毒。",
      "standard_answer": "A,C,D",
      "option_list": [
        {
          "content": "车辆",
          "label": "A"
        },
        {
          "content": "昆虫",
          "label": "B"
        },
        {
          "content": "人员",
          "label": "C"
        },
        {
          "content": "物品",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，县级人民政府（   ）可以根据动物检疫工作需要，向乡、镇或者特定区域派驻动物卫生监督机构或者官方兽医。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "动物卫生监督机构",
          "label": "A"
        },
        {
          "content": "农业农村主管部门",
          "label": "B"
        },
        {
          "content": "动物疫病预防控制机构",
          "label": "C"
        },
        {
          "content": "农业综合执法机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《行政处罚法》，当事人要求听证的，应当在行政机关告知后（     ）内提出",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "三日",
          "label": "A"
        },
        {
          "content": "五日",
          "label": "B"
        },
        {
          "content": "七日",
          "label": "C"
        },
        {
          "content": "十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，监察机关在调查中发现公职人员受到不实检举、控告或者诬告陷害，造成不良影响的，应当按照规定及时（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "澄清事实",
          "label": "A"
        },
        {
          "content": "恢复名誉",
          "label": "B"
        },
        {
          "content": "消除不良影响",
          "label": "C"
        },
        {
          "content": "做出经济补偿",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "为了公共利益的需要，行政机关可以依法变更或者撤回已生效的行政许可的情形是（      ）。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "行政许可所依据的法律、法规、规章修改或者废止的",
          "label": "A"
        },
        {
          "content": "因不可抗拒力导致许可事项无法实施的",
          "label": "B"
        },
        {
          "content": "准予行政许可所依据的客观情况发生重大变化的",
          "label": "C"
        },
        {
          "content": "行政许可的有效期限届满的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《全国畜间人兽共患病防治规划2022—2030年）》提出马鼻疽的重点防治措施是继续实施（     ），严格落实监测、扑杀、无害化处理、移动控制等关键措施。 ",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "净化",
          "label": "A"
        },
        {
          "content": "预防",
          "label": "B"
        },
        {
          "content": "控制",
          "label": "C"
        },
        {
          "content": "消灭计划",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "某县畜产品贸易公司拟向某无规定动物疫病区输入相关易感动物产品，根据《动物检疫管理办法》规定，运输时需要附有（    ），还应当在（    ），按照无规定疫病区有关检疫要求进行检疫，否则不得运输或进入输入地。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "输出地动物卫生监督机构出具的动物检疫证明",
          "label": "A"
        },
        {
          "content": "在输入地省级动物卫生监督机构指定的地点",
          "label": "B"
        },
        {
          "content": "向输入地县级动物卫生监督机构的所在地点",
          "label": "C"
        },
        {
          "content": "输入地动物卫生监督机构出具的动物检疫证明",
          "label": "D"
        },
        {
          "content": "输出地省级动物卫生监督机构出具的动物检疫证明",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "承运人张三从A省B县拉一车肉鸡到B省D屠宰场，到达目的地后，张三应当向（    ）动物卫生监督机构报告。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "D县",
          "label": "A"
        },
        {
          "content": "B县",
          "label": "B"
        },
        {
          "content": "A省",
          "label": "C"
        },
        {
          "content": "B省",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家实行动物检疫（  ）制度。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "报检",
          "label": "A"
        },
        {
          "content": "报告",
          "label": "B"
        },
        {
          "content": "申报",
          "label": "C"
        },
        {
          "content": "申请",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "未经批准，采集、保藏我国人类遗传资源或者利用我国人类遗传资源开展国际科学研究合作，情节严重的，对（）依法给予处分，五年内禁止从事相应活动。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "法定代表人",
          "label": "A"
        },
        {
          "content": "主要负责人",
          "label": "B"
        },
        {
          "content": "直接负责的主管人员",
          "label": "C"
        },
        {
          "content": "其他直接责任人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "官方兽医去A养殖场进行检疫，养殖场的(    )，应当协助官方兽医实施动物检疫。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "执业兽医",
          "label": "A"
        },
        {
          "content": "动物防疫技术人员",
          "label": "B"
        },
        {
          "content": "执业兽医或者动物防疫技术人员",
          "label": "C"
        },
        {
          "content": "兽医人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）被吊销生猪定点屠宰证书的，其（       ）自处罚决定作出之日起5年内不得申请生猪定点屠宰证书或者从事生猪屠宰管理活动；因食品安全犯罪被判处有期徒刑以上刑罚的，终身不得从事生猪屠宰管理活动。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "法定代表人（负责人）",
          "label": "A"
        },
        {
          "content": "直接负责的主管人员",
          "label": "B"
        },
        {
          "content": "兽医卫生检验人员",
          "label": "C"
        },
        {
          "content": "其他直接责任人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "各级人民政府及其有关部门应当加强生物安全法律法规和生物安全知识宣传普及工作，引导（ ）开展生物安全法律法规和生物安全知识宣传，促进全社会生物安全意识的提升。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "社会团体",
          "label": "A"
        },
        {
          "content": "群众组织",
          "label": "B"
        },
        {
          "content": "基层群众性自治组织",
          "label": "C"
        },
        {
          "content": "社会组织",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "为未经定点违法从事生猪屠宰活动的单位和个人提供生猪屠宰场所或者生猪产品储存设施，或者为对生猪、生猪产品注水或者注入其他物质的单位和个人提供场所的，农业农村主管部门实施的处理、处罚正确的有（       ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "责令改正",
          "label": "A"
        },
        {
          "content": "没收违法所得",
          "label": "B"
        },
        {
          "content": "并处5万元以上10万以下的罚款",
          "label": "C"
        },
        {
          "content": "吊销营业执照",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对全国所有猪进行（）口蹄疫免疫。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "O型和(或)A型口蹄疫",
          "label": "A"
        },
        {
          "content": "C型",
          "label": "B"
        },
        {
          "content": "0型",
          "label": "C"
        },
        {
          "content": "A型",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "根据《生猪屠宰管理条例》规定，（       ）应当按照规定足额配备农业农村主管部门任命的兽医，由其监督生猪定点屠宰厂（场）依法查验检疫证明等文件。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "省级人民政府农业农村主管部门",
          "label": "A"
        },
        {
          "content": "县级以上地方人民政府农业农村主管部门",
          "label": "B"
        },
        {
          "content": "省级动物卫生监督机构",
          "label": "C"
        },
        {
          "content": "县级以上动物卫生监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关依法对被许可人从事行政许可事项的活动进行监督检查时，应当记录（    ），由监督检查人员签字后归档。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "有关责任人员的行政处分",
          "label": "A"
        },
        {
          "content": "监督检查的情况",
          "label": "B"
        },
        {
          "content": "监督检查的处理结果",
          "label": "C"
        },
        {
          "content": "违法实施行政许可的行政机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员不履行或者不正确履行职责，玩忽职守，贻误工作，造成不良后果或者影响，情节较重的，予以（）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "记过或者记大过",
          "label": "A"
        },
        {
          "content": "降级或者撤职",
          "label": "B"
        },
        {
          "content": "开除",
          "label": "C"
        },
        {
          "content": "警告或者严重警告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "国家实行动物疫病（   ）和疫情（   ）制度。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "检测",
          "label": "A"
        },
        {
          "content": "监测",
          "label": "B"
        },
        {
          "content": "预警",
          "label": "C"
        },
        {
          "content": "报告",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》中，实验室疫病检测报告应当由（   ）的实验室出具。",
      "standard_answer": "A,C,D,E",
      "option_list": [
        {
          "content": "动物疫病预防控制机构",
          "label": "A"
        },
        {
          "content": "省级动物卫生监督机构指定",
          "label": "B"
        },
        {
          "content": "取得相关资质认定",
          "label": "C"
        },
        {
          "content": "符合省级农业农村主管部门规定条件",
          "label": "D"
        },
        {
          "content": "国家认可机构认可",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "赵某欲将收购的20头生猪准备第二天进行屠宰，在待宰中发现2头猪因天热等应激原因需要急宰，他可以（　　）向所在地动物卫生监督机构申报检疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "提前1小时",
          "label": "A"
        },
        {
          "content": "随时",
          "label": "B"
        },
        {
          "content": "提前2小时",
          "label": "C"
        },
        {
          "content": "提前6小时",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，动物卫生监督机构向依法设立的屠宰加工场所派驻（出）（    ）实施检疫。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "官方兽医",
          "label": "A"
        },
        {
          "content": "官方兽医和协检人员",
          "label": "B"
        },
        {
          "content": "检疫人员",
          "label": "C"
        },
        {
          "content": "官方兽医或协检人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，执业兽医、乡村兽医应当按照所在地人民政府和农业农村主管部门的要求，参加动物疫病（    ）等活动。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "监测",
          "label": "A"
        },
        {
          "content": "预防",
          "label": "B"
        },
        {
          "content": "控制",
          "label": "C"
        },
        {
          "content": "动物疫情扑灭",
          "label": "D"
        },
        {
          "content": "净化",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政许可的实施和结果应当公开，涉及（    ）的除外。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "商业秘密",
          "label": "A"
        },
        {
          "content": "国家秘密",
          "label": "B"
        },
        {
          "content": "机关工作秘密",
          "label": "C"
        },
        {
          "content": "个人隐私",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪定点屠宰厂（场）对未能及时出厂（场）的生猪产品，应当采取（       ）等必要措施予以储存。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "调理",
          "label": "A"
        },
        {
          "content": "干燥",
          "label": "B"
        },
        {
          "content": "灭菌",
          "label": "C"
        },
        {
          "content": "冷冻或者冷藏",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "变更布局、设施设备和制度，可能引起动物防疫条件发生变化的，应当提前（ ）向原发证机关报告。发证机关应当在（ ）内完成审查，并将审查结果通知申请人。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "十五日；十五日",
          "label": "A"
        },
        {
          "content": "三十日；十五日",
          "label": "B"
        },
        {
          "content": "十五日；三十日",
          "label": "C"
        },
        {
          "content": "三十日；三十日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农业农村部办公厅关于做好跨省份道路运输动物指定通道有关工作的通知》（农办牧〔2021〕38号）要求，各省份要按照（      ）的原则，尽快确定并公布指定通道。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "科学规划",
          "label": "A"
        },
        {
          "content": "合理布局",
          "label": "B"
        },
        {
          "content": "安全便民",
          "label": "C"
        },
        {
          "content": "管理可行",
          "label": "D"
        },
        {
          "content": "管理方便",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关实施行政许可或行政机关工作人员办理行政许可，不得向申请人提出（       ）等不正当要求。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "补齐申请资料",
          "label": "A"
        },
        {
          "content": "购买指定商品",
          "label": "B"
        },
        {
          "content": "接受有偿服务",
          "label": "C"
        },
        {
          "content": "索取或者收受申请人的财物",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列哪种情形，应当从轻或者减轻行政处罚。\r\n（     ）",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "当事人有证据足以证明没有主观过错的",
          "label": "A"
        },
        {
          "content": "初次违法且危害后果轻微并及时改正的",
          "label": "B"
        },
        {
          "content": "受他人胁迫或者诱骗实施违法行为的",
          "label": "C"
        },
        {
          "content": "违法行为轻微并及时改正，没有造成危害后果的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《中华人民共和国畜牧法》的规定，畜禽屠宰企业应当具备的条件有（       ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "有与屠宰规模相适应、水质符合国家规定标准的用水供应条件",
          "label": "A"
        },
        {
          "content": "有符合国家规定的设施设备和运载工具",
          "label": "B"
        },
        {
          "content": "有依法取得健康证明的屠宰技术人员",
          "label": "C"
        },
        {
          "content": "有经考核合格的兽医卫生检验人员",
          "label": "D"
        },
        {
          "content": "依法取得动物防疫条件合格证和其他法律法规规定的证明文件",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》所称动物防疫，是指动物疫病的预防、控制、（    ）和动物、动物产品的检疫，以及病死动物、病害动物产品的无害化处理。",
      "standard_answer": "A,B,E",
      "option_list": [
        {
          "content": "诊疗",
          "label": "A"
        },
        {
          "content": "净化",
          "label": "B"
        },
        {
          "content": "扑灭",
          "label": "C"
        },
        {
          "content": "诊断",
          "label": "D"
        },
        {
          "content": "消灭",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，依法应当检疫而未经检疫的胴体、肉、脏器、脂、血液、（    ）等动物产品，不予补检，予以收缴销毁。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "生皮、原毛、绒",
          "label": "A"
        },
        {
          "content": "精液、卵、胚胎",
          "label": "B"
        },
        {
          "content": "骨、蹄、头、筋",
          "label": "C"
        },
        {
          "content": "种蛋",
          "label": "D"
        },
        {
          "content": "角",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "病死畜禽和病害畜禽产品专用运输车辆应当符合以下哪些要求（     ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "不得运输病死畜禽和病害畜禽产品以外的其他物品",
          "label": "A"
        },
        {
          "content": "车厢密闭、防水、防渗、耐腐蚀，易于清洗和消毒",
          "label": "B"
        },
        {
          "content": "配备能够接入国家监管监控平台的车辆定位跟踪系统、车载终端",
          "label": "C"
        },
        {
          "content": "配备人员防护、清洗消毒等应急防疫用品",
          "label": "D"
        },
        {
          "content": "有符合动物防疫需要的其他设施设备",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "A县综合执法大队查货一批生皮，需要补检，补检合格的条件不包括（    ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "经外观检查无腐败变质",
          "label": "A"
        },
        {
          "content": "来自非封锁区证明",
          "label": "B"
        },
        {
          "content": "按照规定进行消毒",
          "label": "C"
        },
        {
          "content": "货主于七日内提供检疫规程规定的实验室疫病检测报告，检测结果合格",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公务员以及参照《中华人民共和国公务员法》管理的人员，在受到（  ）政务处分期内，不得晋升工资档次。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "记过",
          "label": "A"
        },
        {
          "content": "记大过",
          "label": "B"
        },
        {
          "content": "降级",
          "label": "C"
        },
        {
          "content": "撤职",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《畜间布鲁氏菌病防控五年行动方案》基本原则是()。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "源头防控，突出重点",
          "label": "A"
        },
        {
          "content": "因地制宜，综合施策",
          "label": "B"
        },
        {
          "content": "技术创新，强化支撑",
          "label": "C"
        },
        {
          "content": "健全机制，持续推进",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "生猪定点屠宰厂（场）对病害生猪及生猪产品进行无害化处理的费用和损失，由（       ）予以适当补贴。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "县级以上人民政府",
          "label": "A"
        },
        {
          "content": "县级以上人民政府财政主管部门",
          "label": "B"
        },
        {
          "content": "地方各级人民政府",
          "label": "C"
        },
        {
          "content": "地方各级人民政府财政主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所未经审查变更布局、设施设备和制度，不再符合规定的动物防疫条件继续从事相关活动的，应（ ）。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "由县级以上地方人民政府农业农村主管部门给予警告，责令限期改正",
          "label": "A"
        },
        {
          "content": "情节严重的，责令停业整顿",
          "label": "B"
        },
        {
          "content": "处三万元以上十万元以下罚款",
          "label": "C"
        },
        {
          "content": "逾期仍达不到规定条件的，吊销动物防疫条件合格证，并通报市场监督管理部门依法处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《国家畜禽遗传资源品种名录》规定，以下不属于规定的传统畜禽的有（）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "非洲黑鸵鸟",
          "label": "A"
        },
        {
          "content": "鸸鹋",
          "label": "B"
        },
        {
          "content": "尼古拉斯火鸡",
          "label": "C"
        },
        {
          "content": "哥伦比亚洛克鸡",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": " 《动物检疫管理办法》规定，经检疫不合格的动物、动物产品，由官方兽医出具检疫处理通知单，（     ）应当在农业农村主管部门的监督下按照国家有关规定处理。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "承运人",
          "label": "A"
        },
        {
          "content": "货主",
          "label": "B"
        },
        {
          "content": "屠宰加工场所",
          "label": "C"
        },
        {
          "content": "申报人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "行政机关在作出行政处罚决定之前，应当告知当事人依法享有的（        ）等权利。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "拒绝",
          "label": "A"
        },
        {
          "content": "陈述",
          "label": "B"
        },
        {
          "content": "申辩",
          "label": "C"
        },
        {
          "content": "要求听证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，以下不属于可以从轻或者减轻政务处分情形的是（）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "配合调查，如实说明本人违法事实的",
          "label": "A"
        },
        {
          "content": "检举他人违纪违法行为，经查证属实的",
          "label": "B"
        },
        {
          "content": "在共同违法行为中起主要或者谋划作用的",
          "label": "C"
        },
        {
          "content": "主动上交或者退赔违法所得的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "动物饲养场、动物隔离场所、动物屠宰加工场所以及动物和动物产品无害化处理场所应当符合（ ）等动物防疫条件。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "各场所之间，各场所与动物诊疗场所、居民生活区、生活饮用水水源地、学校、医院等公共场所之间保持必要的距离。",
          "label": "A"
        },
        {
          "content": "场区周围建有围墙等隔离设施；场区出入口处设置运输车辆消毒通道或者消毒池，并单独设置人员消毒通道；生产经营区与生活办公区分开，并有隔离设施；生产经营区入口处设置人员更衣消毒室。",
          "label": "B"
        },
        {
          "content": "配备与其生产经营规模相适应的执业兽医或者动物防疫技术人员；建立隔离消毒、购销台账、日常巡查等动物防疫制度。",
          "label": "C"
        },
        {
          "content": "配备与其生产经营规模相适应的污水、污物处理设施，清洗消毒设施设备，以及必要的防鼠、防鸟、防虫设施设备。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "经确认的官方兽医，由（     ）任命，颁发官方兽医证，公布人员名单。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "市级农业农村主管部门",
          "label": "A"
        },
        {
          "content": "上级农业农村主管部门",
          "label": "B"
        },
        {
          "content": "省级农业农村主管部门",
          "label": "C"
        },
        {
          "content": "其所在的农业农村主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）的直接负责的主管人员和其他直接责任人员将会被处以罚款的情形有（       ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "未按照规定建立并遵守生猪进厂（场）查验登记制度、生猪产品出厂（场）记录制度，且拒不改正的",
          "label": "A"
        },
        {
          "content": "发生动物疫情时，生猪定点屠宰厂（场）未按照规定开展动物疫病检测的",
          "label": "B"
        },
        {
          "content": "生猪定点屠宰厂（场）出厂（场）未经肉品品质检验或者经肉品品质检验不合格的生猪产品的",
          "label": "C"
        },
        {
          "content": "生猪定点屠宰厂（场）依照规定应当召回生猪产品而不召回，农业农村主管部门责令召回、停止屠宰，但是生猪定点屠宰厂（场）拒不召回或者拒不停止屠宰的",
          "label": "D"
        },
        {
          "content": "生猪定点屠宰厂（场）对生猪、生猪产品注水或者注入其他物质的",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "关于发现违法行为的期限说法正确的是（    ）。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "违法行为在二年内未被发现的，不再给予行政处罚；涉及公民生命健康安全、金融安全且有危害后果的，上述期限延长至五年",
          "label": "A"
        },
        {
          "content": "违法行为在三年内未被发现的，不再给予行政处罚；涉及公民生命健康安全、金融安全且有危害后果的，上述期限延长至五年",
          "label": "B"
        },
        {
          "content": "违法行为在二年内未被发现的，不再给予行政处罚；涉及公民生命健康安全、金融安全且有危害后果的，上述期限延长至十年",
          "label": "C"
        },
        {
          "content": "违法行为有连续或者继续状态的，从行为终了之日起计算期限",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，动物、动物产品的（　　）等应当符合国务院农业农村主管部门规定的动物防疫要求。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "运载工具",
          "label": "A"
        },
        {
          "content": "垫料",
          "label": "B"
        },
        {
          "content": "包装物",
          "label": "C"
        },
        {
          "content": "容器",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定,动物检疫遵循过程监管、(   )、区域化和可追溯管理相结合的原则。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "风险管理",
          "label": "A"
        },
        {
          "content": "运输控制",
          "label": "B"
        },
        {
          "content": "现场核查",
          "label": "C"
        },
        {
          "content": "风险控制",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，申报检疫采取（   ）等方式进行申报。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "电话",
          "label": "A"
        },
        {
          "content": "申报点填报",
          "label": "B"
        },
        {
          "content": "传真",
          "label": "C"
        },
        {
          "content": "电子数据交换",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "下列畜禽和畜禽产品不属于《病死畜禽和病害畜禽产品无害化处理管理办法》规定应当进行无害化处理的是（     ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "屠宰过程中经肉品品质检验确认为不可食用的",
          "label": "A"
        },
        {
          "content": "死胎、木乃伊胎",
          "label": "B"
        },
        {
          "content": "营养不良导致的“僵猪”",
          "label": "C"
        },
        {
          "content": "因自然灾害、应激反应、物理挤压等因素死亡的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "张三从外省购买奶牛10头，到达目的后应当隔离观察（　　）天。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "二十一",
          "label": "A"
        },
        {
          "content": "三十",
          "label": "B"
        },
        {
          "content": "四十五",
          "label": "C"
        },
        {
          "content": "四十二",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "动物园的大象在展示后需要继续运输，检疫证明在调运有效期内，需要符合下列（   ）条件。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "有原始动物检疫证明",
          "label": "A"
        },
        {
          "content": "完整的进出场记录",
          "label": "B"
        },
        {
          "content": "临床检查健康",
          "label": "C"
        },
        {
          "content": "畜禽标识符合规定",
          "label": "D"
        },
        {
          "content": "实验室疫病检测结果合格",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "当事人提出回避申请的，行政机关应当依法审查，由（      ）决定。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "执法人员",
          "label": "A"
        },
        {
          "content": "办公室主任",
          "label": "B"
        },
        {
          "content": "具体业务分管领导",
          "label": "C"
        },
        {
          "content": "行政机关负责人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，有下列（   ）情形之一的，撤销动物检疫证明，并及时通告有关单位和个人。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "官方兽医滥用职权、玩忽职守出具动物检疫证明的",
          "label": "A"
        },
        {
          "content": "超出动物检疫范围实施检疫，出具动物检疫证明的",
          "label": "B"
        },
        {
          "content": "以欺骗、贿赂等不正当手段取得动物检疫证明的",
          "label": "C"
        },
        {
          "content": "对不符合检疫申报条件或者不符合检疫合格标准的动物、动物产品，出具动物检疫证明的",
          "label": "D"
        },
        {
          "content": "其他未按照《中华人民共和国动物防疫法》、本办法和检疫规程的规定实施检疫，出具动物检疫证明的。",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物卫生监督证章标志填写及应用规范》规定，检疫证明填写中，以下针对数量及单位填写方式正确的有（）。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "壹头",
          "label": "A"
        },
        {
          "content": "两只",
          "label": "B"
        },
        {
          "content": "三匹",
          "label": "C"
        },
        {
          "content": "壹佰羽",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）对其生产的生猪产品质量安全负责，发现其生产的生猪产品不符合食品安全标准、有证据证明可能危害人体健康、染疫或者疑似染疫的，应当采取的措施有（     ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "立即停止屠宰",
          "label": "A"
        },
        {
          "content": "报告农业农村主管部门",
          "label": "B"
        },
        {
          "content": "通知销售者或者委托人",
          "label": "C"
        },
        {
          "content": "召回已经销售的生猪产品",
          "label": "D"
        },
        {
          "content": "记录通知和召回情况",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽（  ）及生皮、原毛、绒、脏器、血液、蹄、头、角等直接从屠宰线生产的畜禽产品出证。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "胴体",
          "label": "A"
        },
        {
          "content": "肉",
          "label": "B"
        },
        {
          "content": "分割肉",
          "label": "C"
        },
        {
          "content": "躯体",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物防疫法》规定，县级人民政府农业农村主管部门可以根据动物防疫工作需要，向乡、镇或者特定区域派驻（    ）。",
      "standard_answer": "A,D",
      "option_list": [
        {
          "content": "兽医机构",
          "label": "A"
        },
        {
          "content": "防疫机构",
          "label": "B"
        },
        {
          "content": "防疫工作人员",
          "label": "C"
        },
        {
          "content": "兽医工作人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列属于活禽交易市场应当符合的防疫条件的是：（ ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "动物卸载区域有固定的车辆消毒场地，并配备车辆清洗消毒设备。",
          "label": "A"
        },
        {
          "content": "活禽销售应单独分区，有独立出入口；市场内水禽与其他家禽应相对隔离；活禽宰杀间应相对封闭，宰杀间、销售区域、消费者之间应实施物理隔离。",
          "label": "B"
        },
        {
          "content": "配备通风、无害化处理等设施设备，设置排污通道。",
          "label": "C"
        },
        {
          "content": "建立日常监测、从业人员卫生防护、突发事件应急处置等动物防疫制度。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农业农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，尚未实施畜禽产品无纸化出证并实现电子证照互通互认的地区，暂时对在本屠宰企业内分割加工的畜禽产品（   )。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "不再继续出证",
          "label": "A"
        },
        {
          "content": "不再重复出证",
          "label": "B"
        },
        {
          "content": "重复出证",
          "label": "C"
        },
        {
          "content": "继续出证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，有关机关、单位、组织集体作出的决定违法或者实施违法行为的，对负有责任的（    ）中的公职人员依法给予政务处分。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "领导人员",
          "label": "A"
        },
        {
          "content": "该部门",
          "label": "B"
        },
        {
          "content": "直接责任人员",
          "label": "C"
        },
        {
          "content": "工作人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "动物防疫条件合格证应当载明（ ）等事项，具体格式由农业农村部规定。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "申请人的名称（姓名）",
          "label": "A"
        },
        {
          "content": "场（厂）址",
          "label": "B"
        },
        {
          "content": "动物（动物产品）种类",
          "label": "C"
        },
        {
          "content": "动物（动物产品）数量",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，屠宰加工场所应当为官方兽医提供（        ）等设施。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "驻场检疫室",
          "label": "A"
        },
        {
          "content": "工作室",
          "label": "B"
        },
        {
          "content": "实验室",
          "label": "C"
        },
        {
          "content": "检疫操作台",
          "label": "D"
        },
        {
          "content": "厨房",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "执法人员当场作出的行政处罚决定，应当报所属行政机关（       ）。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "审批",
          "label": "A"
        },
        {
          "content": "批准",
          "label": "B"
        },
        {
          "content": "备案",
          "label": "C"
        },
        {
          "content": "审核",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列属于动物饲养场应当符合的防疫条件的是：（ ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "设置配备疫苗冷藏冷冻设备、消毒和诊疗等防疫设备的兽医室；生产区清洁道、污染道分设；具有相对独立的动物隔离舍。",
          "label": "A"
        },
        {
          "content": "配备符合国家规定的病死动物和病害动物产品无害化处理设施设备或者冷藏冷冻等暂存设施设备。",
          "label": "B"
        },
        {
          "content": "设置待宰圈、急宰间，封闭式熏蒸消毒间。",
          "label": "C"
        },
        {
          "content": "建立免疫、用药、检疫申报、疫情报告、无害化处理、畜禽标识及养殖档案管理等动物防疫制度。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "当事人对行政处罚决定不服，申请行政复议或者提起行政诉讼的，行政处罚（    ）执行，法律另有规定的除外。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "暂停",
          "label": "A"
        },
        {
          "content": "不停止",
          "label": "B"
        },
        {
          "content": "中止",
          "label": "C"
        },
        {
          "content": "不再",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，向无规定动物疫病区输入相关易感动物、易感动物产品的，货主应当在前（　　）天向输入地动物卫生监督机构申报检疫。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "一",
          "label": "A"
        },
        {
          "content": "三",
          "label": "B"
        },
        {
          "content": "七",
          "label": "C"
        },
        {
          "content": "十五",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "官方兽医实施动物检疫的，可以由（      ）进行协助。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "执业兽医",
          "label": "A"
        },
        {
          "content": "协检人员",
          "label": "B"
        },
        {
          "content": "检疫人员",
          "label": "C"
        },
        {
          "content": "防疫人员",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "畜禽养殖场、屠宰厂（场）、隔离场委托病死畜禽无害化处理场处理的，应当符合以下哪些要求（     ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "安装视频监控设备",
          "label": "A"
        },
        {
          "content": "采取必要的冷藏冷冻、清洗消毒等措施",
          "label": "B"
        },
        {
          "content": "具有病死畜禽和病害畜禽产品输出通道",
          "label": "C"
        },
        {
          "content": "及时通知病死畜禽无害化处理场进行收集，或自行送至指定地点",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，监察机关在调查终结后，可以作出的处理决定包括（）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "政务处分决定",
          "label": "A"
        },
        {
          "content": "撤销案件",
          "label": "B"
        },
        {
          "content": "免予处分决定",
          "label": "C"
        },
        {
          "content": "涉嫌其他违法行为移送主管机关处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员应当给予撤职以下多个相同政务处分的，可以在一个政务处分期以上、多个政务处分期之和以下确定政务处分期，但是最长不得超过（）。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "十二个月",
          "label": "A"
        },
        {
          "content": "十八个月",
          "label": "B"
        },
        {
          "content": "二十四个月",
          "label": "C"
        },
        {
          "content": "四十八个月",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，公职人员有下列（）行为情节严重的，予以开除。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "变相不执行中国共产党和国家的路线方针政策的",
          "label": "A"
        },
        {
          "content": "挑拨、破坏民族关系的",
          "label": "B"
        },
        {
          "content": "利用宗教活动破坏民族团结和社会稳定的",
          "label": "C"
        },
        {
          "content": "在对外交往中损害国家荣誉和利益的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，依法履行公职的人员违法行为情节严重的，由所在单位直接给予或者监察机关建议有关机关、单位给予（）等处理。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "降低薪酬待遇",
          "label": "A"
        },
        {
          "content": "调离岗位",
          "label": "B"
        },
        {
          "content": "解除人事关系",
          "label": "C"
        },
        {
          "content": "解除劳动关系",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《中华人民共和国公职人员政务处分法》规定，为了规范政务处分，加强对所有行使公权力的公职人员的监督，促进公职人员（    ），根据《中华人民共和国监察法》，制定本法。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "依法履职",
          "label": "A"
        },
        {
          "content": "秉公用权",
          "label": "B"
        },
        {
          "content": "廉洁从政从业",
          "label": "C"
        },
        {
          "content": "坚持道德操守",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "指导牛羊养殖场、屠宰场等场所建立健全()制度，强化人员、物流隔离和消毒措施，及时对污染的场所、用具、物品进行彻底清洗，不断提高生物安全水平。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "检疫申报",
          "label": "A"
        },
        {
          "content": "无害化处理",
          "label": "B"
        },
        {
          "content": "清洗消毒",
          "label": "C"
        },
        {
          "content": "生物安全管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列属于经营动物和动物产品的集贸市场应当符合的防疫条件的是：（ ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "场内设管理区、交易区和废弃物处理区，且各区相对独立。",
          "label": "A"
        },
        {
          "content": "动物交易区与动物产品交易区相对隔离，动物交易区内不同种类动物交易场所相对独立。",
          "label": "B"
        },
        {
          "content": "配备与其经营规模相适应的污水、污物处理设施和清洗消毒设施设备。",
          "label": "C"
        },
        {
          "content": "建立定期休市、清洗消毒等动物防疫制度。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，依法应当检疫而未经检疫的胴体、肉、脏器、脂、血液、精液、卵、胚胎、骨、蹄、头、筋、种蛋等动物产品，不予补检，予以（    ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "收缴销毁",
          "label": "A"
        },
        {
          "content": "没收销毁",
          "label": "B"
        },
        {
          "content": "收缴无害化处理",
          "label": "C"
        },
        {
          "content": "没收无害化处理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "有下列哪些情形，行政机关应当依法办理有关行政许可的注销手续。（      ）",
      "standard_answer": "A,C,E",
      "option_list": [
        {
          "content": "行政许可有效期届满未延续的",
          "label": "A"
        },
        {
          "content": "违反法定程序作出准予行政许可决定的",
          "label": "B"
        },
        {
          "content": "因不可抗力导致行政许可事项无法实施的",
          "label": "C"
        },
        {
          "content": "超越法定职权作出准予行政许可决定的",
          "label": "D"
        },
        {
          "content": "法人或者其他组织依法终止的",
          "label": "E"
        },
        {
          "content": "行政机关工作人员滥用职权、玩忽职守作出准予行政许可决定的",
          "label": "F"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "以下哪些情形，可以或应当从轻或减轻行政处罚。（      ）",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "满十四周岁的未成年人有违法行为的",
          "label": "A"
        },
        {
          "content": "尚未完全丧失辨认或者控制自己行为能力的精神病人、智力残疾人有违法行为的",
          "label": "B"
        },
        {
          "content": "已满十四周岁不满十八周岁的未成年人有违法行为的",
          "label": "C"
        },
        {
          "content": "精神病人、智力残疾人在不能辨认或者不能控制自己行为时有违法行为的",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《动物检疫管理办法》规定，输入到无规定动物疫病区的相关易感动物，隔离检疫合格的，由（    ）动物卫生监督机构的官方兽医出具动物检疫证明。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "隔离场所在地",
          "label": "A"
        },
        {
          "content": "当地县级",
          "label": "B"
        },
        {
          "content": "省级",
          "label": "C"
        },
        {
          "content": "输入地省级",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《病死畜禽和病害畜禽产品无害化处理管理办法》，以下依法由所在地街道办事处、乡级人民政府组织收集、处理并溯源的死亡畜禽有（     ）。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "在城市公共场所现的死亡畜禽",
          "label": "A"
        },
        {
          "content": "在湖泊发现的死亡畜禽",
          "label": "B"
        },
        {
          "content": "在乡村发现的死亡畜禽",
          "label": "C"
        },
        {
          "content": "在水库发现的死亡畜禽",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "设定行政许可，应当遵循经济和社会发展规律，有利于发挥（     ）的积极性、主动性。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "公民",
          "label": "A"
        },
        {
          "content": "法人",
          "label": "B"
        },
        {
          "content": "行政机关",
          "label": "C"
        },
        {
          "content": "其他组织",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "生猪定点屠宰厂（场）应当建立生猪进厂（场）查验登记制度。以下属于发生动物疫情时，生猪定点屠宰厂（场）应当查验、记录的信息的有（     ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "屠宰生猪的来源、数量、检疫证明号",
          "label": "A"
        },
        {
          "content": "屠宰生猪的供货者名称、地址、联系方式",
          "label": "B"
        },
        {
          "content": "屠宰生猪的品种",
          "label": "C"
        },
        {
          "content": "运输车辆基本情况",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物防疫法》规定，县级以上人民政府农业农村主管部门对官方兽医的职责有以下（      ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "制定培训计划",
          "label": "A"
        },
        {
          "content": "提供培训条件",
          "label": "B"
        },
        {
          "content": "定期培训",
          "label": "C"
        },
        {
          "content": "定期考核",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "根据《动物检疫管理办法》规定，县级以上地方人们政府农业农村主管部门对官方兽医的管理职责有（     ）。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "制定辖区内官方兽医培训计划",
          "label": "A"
        },
        {
          "content": "提供必要的培训条件",
          "label": "B"
        },
        {
          "content": "定期对官方兽医培训",
          "label": "C"
        },
        {
          "content": "设立考核指标",
          "label": "D"
        },
        {
          "content": "定期对官方兽医考核",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "经屠宰检疫符合条件的，对动物的（   ）及生皮、原毛、绒、脏器、血液、蹄、头、角出具动物检疫证明，加盖检疫验讫印章或者加施其他检疫标志。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "肉",
          "label": "A"
        },
        {
          "content": "胴体",
          "label": "B"
        },
        {
          "content": "肉品",
          "label": "C"
        },
        {
          "content": "白条肉",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": " 违反《动物检疫管理办法》规定运输畜禽，有下列（   ）行为之一的，由县级以上地方人民政府农业农村主管部门按照《动物检疫管理办法》处以罚款。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "未按照动物检疫证明载明的目的地运输的",
          "label": "A"
        },
        {
          "content": "未按照动物检疫证明规定时间运达且无正当理由的",
          "label": "B"
        },
        {
          "content": "实际运输的数量少于动物检疫证明载明数量且无正当理由的",
          "label": "C"
        },
        {
          "content": "运输用于继续饲养或者屠宰的畜禽到达目的地后，未向启运地动物卫生监督机构报告的",
          "label": "D"
        },
        {
          "content": "实际运输的数量多于动物检疫证明载明数量且无正当理由的",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "公民、法人或者其他组织依法享有陈述权、申辩权，对行政处罚不服的，有权依法申请（    ）。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "行政复议",
          "label": "A"
        },
        {
          "content": "行政诉讼",
          "label": "B"
        },
        {
          "content": "听证",
          "label": "C"
        },
        {
          "content": "国家赔偿",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《动物检疫管理办法》规定，从事动物（      ）等活动的单位和个人，应当按照要求在动物检疫管理信息化系统填报动物检疫相关信息。",
      "standard_answer": "A,B,C,D,E",
      "option_list": [
        {
          "content": "经营",
          "label": "A"
        },
        {
          "content": "饲养",
          "label": "B"
        },
        {
          "content": "屠宰",
          "label": "C"
        },
        {
          "content": "运输",
          "label": "D"
        },
        {
          "content": "隔离",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "重大动物疫情应急工作按照（ ）的原则，实行政府统一领导、部门分工负责，逐级建立责任制。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "分级管理",
          "label": "A"
        },
        {
          "content": "垂直管理",
          "label": "B"
        },
        {
          "content": "属地管理",
          "label": "C"
        },
        {
          "content": "属人管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（ ）应当及时收集境外重大动物疫情信息，加强进出境动物及其产品的检验检疫工作，防止动物疫病传入和传出。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "农业农村主管部门",
          "label": "A"
        },
        {
          "content": "兽医主管部门",
          "label": "B"
        },
        {
          "content": "动物疫病防控机关",
          "label": "C"
        },
        {
          "content": "出入境检验检疫机关",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列有关重大动物疫情报告时限说法正确的是：（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "县(市)动物防疫监督机构初步认为属于重大动物疫情的，应当在2小时内将情况逐级报省、自治区、直辖市动物防疫监督机构，并同时报所在地人民政府兽医主管部门。",
          "label": "A"
        },
        {
          "content": "省、自治区、直辖市动物防疫监督机构应当在接到报告后1小时内，向省、自治区、直辖市人民政府兽医主管部门和国务院兽医主管部门所属的动物防疫监督机构报告。",
          "label": "B"
        },
        {
          "content": "省、自治区、直辖市人民政府兽医主管部门应当在接到报告后1小时内报本级人民政府和国务院兽医主管部门。",
          "label": "C"
        },
        {
          "content": "重大动物疫情发生后，省、自治区、直辖市人民政府和国务院兽医主管部门应当在2小时内向国务院报告。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "重大动物疫情发生后，对受威胁区应当采取下列措施：（ ）。",
      "standard_answer": "A,B",
      "option_list": [
        {
          "content": "对易感染的动物进行监测",
          "label": "A"
        },
        {
          "content": "对易感染的动物根据需要实施紧急免疫接种",
          "label": "B"
        },
        {
          "content": "必要时对易感染的动物进行扑杀",
          "label": "C"
        },
        {
          "content": "关闭动物及动物产品交易市场，禁止动物进出疫区和动物产品运出疫区",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "重大动物疫情报告应包括（ ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "疫情发生的时间、地点",
          "label": "A"
        },
        {
          "content": "染疫、疑似染疫动物种类和数量、同群动物数量、免疫情况、死亡数量、临床症状、病理变化、诊断情况",
          "label": "B"
        },
        {
          "content": "流行病学和疫源追踪情况及已采取的控制措施",
          "label": "C"
        },
        {
          "content": "疫情报告的单位、负责人、报告人及联系方式",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "重大动物疫情，是指高致病性禽流感等发病率或者死亡率高的动物疫病突然发生，迅速传播，给养殖业生产安全造成严重威胁、危害，以及可能对公众身体健康与生命安全造成危害的情形，包括特别重大动物疫情。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对到达目的地后分销的畜禽产品，可以结合本地实际，确定是否出证。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽躯体及生皮、原毛、绒、脏器、血液、蹄、头、角等直接从屠宰线生产的畜禽产品出证。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "重大动物疫病应当由动物防疫监督机构采集病料。其他单位和个人采集病料的，应当（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "重大动物疫病病料采集目的、病原微生物的用途应当符合国务院兽医主管部门的规定",
          "label": "A"
        },
        {
          "content": "具有与采集病料相适应的动物病原微生物实验室条件",
          "label": "B"
        },
        {
          "content": "具有与采集病料所需要的生物安全防护水平相适应的设备，以及防止病原感染和扩散的有效措施",
          "label": "C"
        },
        {
          "content": "相关工作人员应当具有相应从业资质",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，已经实施畜禽产品（   ）并实现（   ）的地区，对取得动物检疫证明的畜禽产品，继续在本屠宰企业内分割加工的，不再重复出证。",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "双轨并行出证",
          "label": "A"
        },
        {
          "content": "无纸化出证",
          "label": "B"
        },
        {
          "content": "电子证照互通互认",
          "label": "C"
        },
        {
          "content": "检疫检验信息互通互认",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "重大动物疫情发生后，县级以上地方人民政府兽医主管部门应当立即划定（ ），调查疫源，向本级人民政府提出启动重大动物疫情应急指挥系统、应急预案和对疫区实行封锁的建议，有关人民政府应当立即作出决定。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "健康区",
          "label": "A"
        },
        {
          "content": "疫点",
          "label": "B"
        },
        {
          "content": "疫区",
          "label": "C"
        },
        {
          "content": "受威胁区",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "自疫区内最后一头(只)发病动物及其同群动物处理完毕起，经过一个生产周期以上的监测，未出现新的病例的，彻底消毒后，经上一级动物防疫监督机构验收合格，由原发布封锁令的人民政府宣布解除封锁，撤销疫区。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，应当检疫出证的（   ）包括畜禽经宰杀、放血后除去毛、内脏、头、尾及四肢（腕及关节以下）后的躯体部分，也包括家畜躯体部分的二分体、四分体。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "畜禽肉品",
          "label": "A"
        },
        {
          "content": "畜禽产品",
          "label": "B"
        },
        {
          "content": "畜禽胴体",
          "label": "C"
        },
        {
          "content": "畜禽肉体",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，在（    ）年底前全面实施无纸化出证，推动实现动物检疫证明电子证照全国互通互认。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "2024",
          "label": "A"
        },
        {
          "content": "2025",
          "label": "B"
        },
        {
          "content": "2026",
          "label": "C"
        },
        {
          "content": "2027",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列关于重大动物疫情应急工作说法正确的是：（ ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "重大动物疫情应急工作按照分级管理的原则，实行政府统一领导、部门分工负责，逐级建立责任制。",
          "label": "A"
        },
        {
          "content": "县级以上人民政府兽医主管部门具体负责组织重大动物疫情的监测、调查、控制、扑灭等应急工作。",
          "label": "B"
        },
        {
          "content": "县级以上人民政府林业主管部门、兽医主管部门按照职责分工，加强对陆生野生动物疫源疫病的监测。",
          "label": "C"
        },
        {
          "content": "县级以上人民政府其他有关部门在各自的职责范围内，做好重大动物疫情的应急工作。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "县级以上人民政府（ ）按照职责分工，加强对陆生野生动物疫源疫病的监测。",
      "standard_answer": "C,D",
      "option_list": [
        {
          "content": "农业农村主管部门",
          "label": "A"
        },
        {
          "content": "卫生健康主管部门",
          "label": "B"
        },
        {
          "content": "林业主管部门",
          "label": "C"
        },
        {
          "content": "兽医主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，已经实施畜禽产品无纸化出证的地区，对取得动物检疫证明的畜禽产品，继续在本屠宰企业内分割加工的，不再重复出证。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》要求，在农业农村部指导下加快动物检疫信息化进程，在2027年底前全面实施无纸化出证，推动实现动物检疫证明电子证照全国互通互认。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "县级以上人民政府林业主管部门、兽医主管部门按照职责分工，加强对陆生野生动物疫源疫病的监测。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "在重大动物疫情报告期间，有关动物防疫监督机构应当立即采取临时隔离控制措施；必要时，当地县级以上地方人民政府可以作出封锁决定并采取扑杀、销毁等措施。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "县(市)动物防疫监督机构初步认为属于重大动物疫情的，应当在一日内将情况逐级报省、自治区、直辖市动物防疫监督机构，并同时报所在地人民政府兽医主管部门。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "县级以上人民政府应当建立和完善重大动物疫情监测网络和预防控制体系，加强（ ）建设，并保证其正常运行，提高对重大动物疫情的应急处理能力。",
      "standard_answer": "A,C",
      "option_list": [
        {
          "content": "动物防疫基础设施",
          "label": "A"
        },
        {
          "content": "应急处理设施",
          "label": "B"
        },
        {
          "content": "乡镇动物防疫组织",
          "label": "C"
        },
        {
          "content": "防疫专业队伍",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "重大动物疫情发生后，应对疫点内易感染的动物根据需要实施紧急免疫接种。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "在重大动物疫情报告期间，有关动物防疫监督机构应当立即采取临时隔离控制措施；必要时，当地（ ）可以作出封锁决定并采取扑杀、销毁等措施。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "县级以上地方人民政府",
          "label": "A"
        },
        {
          "content": "卫生健康主管部门",
          "label": "B"
        },
        {
          "content": "兽医主管部门",
          "label": "C"
        },
        {
          "content": "动物防疫监督机构",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "动物疫病防控机关负责重大动物疫情的监测，饲养、经营动物和生产、经营动物产品的单位和个人应当配合，不得拒绝和阻碍。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "重大动物疫情应急指挥部根据应急处理需要，有权紧急调集人员、物资、运输工具以及相关设施、设备。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽胴体及生皮、原毛、绒、脏器、血液、蹄、头、角等直接从（    ）的畜禽产品出证。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "屠宰场生产",
          "label": "A"
        },
        {
          "content": "屠宰场销售",
          "label": "B"
        },
        {
          "content": "屠宰线生产",
          "label": "C"
        },
        {
          "content": "屠宰线分割",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，尚未实施畜禽产品无纸化出证并实现电子证照互通互认的地区，对在本屠宰企业内分割加工的畜禽产品不再继续出证。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽（  ）及生皮、原毛、绒、脏器、血液、蹄、头、角等直接从屠宰线生产的畜禽产品出证。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "胴体",
          "label": "A"
        },
        {
          "content": "肉",
          "label": "B"
        },
        {
          "content": "分割肉",
          "label": "C"
        },
        {
          "content": "躯体",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "县级以上人民政府应当建立和完善重大动物疫情（ ）体系，加强动物防疫基础设施和乡镇动物防疫组织建设，并保证其正常运行，提高对重大动物疫情的应急处理能力",
      "standard_answer": "B,C",
      "option_list": [
        {
          "content": "信息收集",
          "label": "A"
        },
        {
          "content": "监测网络",
          "label": "B"
        },
        {
          "content": "预防控制",
          "label": "C"
        },
        {
          "content": "应急管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽（    ）等直接从屠宰线生产的畜禽产品出证。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "胴体",
          "label": "A"
        },
        {
          "content": "生皮、原毛、绒",
          "label": "B"
        },
        {
          "content": "脏器、血液",
          "label": "C"
        },
        {
          "content": "蹄、头、角",
          "label": "D"
        },
        {
          "content": "肉",
          "label": "E"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "重大动物疫情发生后，县级以上地方人民政府兽医主管部门应当立即划定疫点、疫区和受威胁区，调查疫源，向本级人民政府提出（ ）建议，有关人民政府应当立即作出决定。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "启动重大动物疫情应急指挥系统",
          "label": "A"
        },
        {
          "content": "启动应急预案",
          "label": "B"
        },
        {
          "content": "对疫区实行封锁",
          "label": "C"
        },
        {
          "content": "采取隔离、扑杀、销毁、消毒、紧急免疫接种等控制、扑灭措施",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "对不履行或者不按照规定履行重大动物疫情应急处理职责的行为，任何单位和个人有权检举控告。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，已经实施畜禽产品无纸化出证并实现电子证照互通互认的地区，对取得动物检疫证明的畜禽产品，继续在本屠宰企业内分割加工的，不再重复出证，附具动物检疫证明电子证照加注件。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "县级以上人民政府（ ）主管部门具体负责组织重大动物疫情的监测、调查、控制、扑灭等应急工作。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "农业农村",
          "label": "A"
        },
        {
          "content": "兽医",
          "label": "B"
        },
        {
          "content": "卫生健康",
          "label": "C"
        },
        {
          "content": "应急管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "重大动物疫情应急预案应当包括重大动物疫情的（ ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "监测",
          "label": "A"
        },
        {
          "content": "信息收集",
          "label": "B"
        },
        {
          "content": "报告",
          "label": "C"
        },
        {
          "content": "通报",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "兽医主管部门应当及时收集境外重大动物疫情信息，加强进出境动物及其产品的检验检疫工作，防止动物疫病传入和传出。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对到达目的地后分销的畜禽产品，不再重复出证。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "县(市)动物防疫监督机构初步认为属于重大动物疫情的，应当在（ ）内将情况逐级报省、自治区、直辖市动物防疫监督机构，并同时报所在地人民政府兽医主管部门。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "1小时",
          "label": "A"
        },
        {
          "content": "2小时",
          "label": "B"
        },
        {
          "content": "4小时",
          "label": "C"
        },
        {
          "content": "1日",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，尚未实施畜禽产品无纸化出证并实现电子证照互通互认的地区，暂时对在本屠宰企业内分割加工的畜禽产品继续出证。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "县级以上人民政府及其兽医主管部门应当加强对重大动物疫情应急知识和重大动物疫病科普知识的宣传，增强全社会的重大动物疫情防范意识。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "重大动物疫情应急处理中，（ ）应当组织力量，向村民、居民宣传动物疫病防治的相关知识，协助做好疫情信息的收集、报告和各项应急处理措施的落实工作。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "乡镇人民政府",
          "label": "A"
        },
        {
          "content": "村民委员会",
          "label": "B"
        },
        {
          "content": "居民委员会",
          "label": "C"
        },
        {
          "content": "以上都是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，符合不再重复出证条件的畜禽产品，需要附具（     ）。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "动物检疫证明电子证照加注件",
          "label": "A"
        },
        {
          "content": "动物检疫证明电子证照",
          "label": "B"
        },
        {
          "content": "无纸化动物检疫证明",
          "label": "C"
        },
        {
          "content": "动物检疫证明电子证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "重大动物疫情发生后，下列属于应当对疫区采取的措施的是：（ ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "在疫区周围设置警示标志，在出入疫区的交通路口设置临时动物检疫消毒站，对出入的人员和车辆进行消毒。",
          "label": "A"
        },
        {
          "content": "对易感染的动物进行监测，并按规定实施紧急免疫接种，必要时对易感染的动物进行扑杀。",
          "label": "B"
        },
        {
          "content": "关闭动物及动物产品交易市场，禁止动物进出疫区和动物产品运出疫区。",
          "label": "C"
        },
        {
          "content": "对动物圈舍、动物排泄物、垫料、污水和其他可能受污染的物品、场地，进行消毒或者无害化处理。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列属于重大动物疫情应急预案中主要包括的内容的是：（ ）。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "重大动物疫情的监测、信息收集、报告和通报",
          "label": "A"
        },
        {
          "content": "动物疫病的确认、重大动物疫情的分级和相应的应急处理工作方案",
          "label": "B"
        },
        {
          "content": "重大动物疫情疫源的追踪和流行病学调查分析",
          "label": "C"
        },
        {
          "content": "重大动物疫情应急处理设施和专业队伍建设",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽肉及生皮、原毛、绒、脏器、脂、血液、骨、蹄、头、角、筋等直接从屠宰场生产的畜禽产品出证。（    ）",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "重大疫情应急处理过程中，紧急免疫接种所需费用由（ ）承担。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "中央财政",
          "label": "A"
        },
        {
          "content": "地方财政",
          "label": "B"
        },
        {
          "content": "中央财政和地方财政",
          "label": "C"
        },
        {
          "content": "单位和个人",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对经检疫合格的畜禽胴体及生皮、原毛、绒、脏器、血液、蹄、头、角等直接从屠宰线生产的畜禽产品出证。（    ）",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "情况紧急时，可以由省级政府兽医主管部门公布重大动物疫情。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "自疫区内最后一头(只)发病动物及其同群动物处理完毕起，经过（ ）监测，未出现新的病例的，彻底消毒后，经上一级动物防疫监督机构验收合格，由原发布封锁令的人民政府宣布解除封锁，撤销疫区。",
      "standard_answer": "C",
      "option_list": [
        {
          "content": "一个月以上",
          "label": "A"
        },
        {
          "content": "一年以上",
          "label": "B"
        },
        {
          "content": "一个潜伏期以上",
          "label": "C"
        },
        {
          "content": "一个生产周期以上",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "疫点、疫区和受威胁区的范围应当按照不同动物疫病病种及其流行特点和危害程度划定，具体划定标准由（ ）制定。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "国务院农业农村主管部门",
          "label": "A"
        },
        {
          "content": "国务院兽医主管部门",
          "label": "B"
        },
        {
          "content": "省级人民政府兽医主管部门",
          "label": "C"
        },
        {
          "content": "县级人民政府兽医主管部门",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "应急预备队由（ ）组成；必要时，可以组织动员社会上有一定专业知识的人员参加。",
      "standard_answer": "A,B,C,D",
      "option_list": [
        {
          "content": "当地兽医行政管理人员",
          "label": "A"
        },
        {
          "content": "动物防疫工作人员",
          "label": "B"
        },
        {
          "content": "有关专家",
          "label": "C"
        },
        {
          "content": "执业兽医等",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "判断题",
      "question": "重大动物疫情应急工作按照分级管理的原则，实行政府统一领导、部门分工负责，逐级建立责任制。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "正确",
          "label": "A"
        },
        {
          "content": "错误",
          "label": "B"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "重大动物疫情发生后，对疫点应当采取下列措施：（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "扑杀并销毁染疫动物和易感染的动物及其产品",
          "label": "A"
        },
        {
          "content": "对病死的动物、动物排泄物、被污染饲料、垫料、污水进行无害化处理",
          "label": "B"
        },
        {
          "content": "对被污染的物品、用具、动物圈舍、场地进行严格消毒",
          "label": "C"
        },
        {
          "content": "对易感染的动物根据需要实施紧急免疫接种",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列关于重大动物疫情应急处理说法正确的是：（ ）。",
      "standard_answer": "A,B,C",
      "option_list": [
        {
          "content": "由重大动物疫情应急指挥部决定的采取隔离、扑杀、销毁、消毒、紧急免疫接种等控制、扑灭措施，有关单位和个人必须服从。",
          "label": "A"
        },
        {
          "content": "重大动物疫情应急处理中拒不服从应急指挥部决定的，由公安机关协助执行。",
          "label": "B"
        },
        {
          "content": "重大动物疫情应急指挥部根据应急处理需要，有权紧急调集人员、物资、运输工具以及相关设施、设备。",
          "label": "C"
        },
        {
          "content": "重大动物疫情应急指挥部可无偿征集使用单位和个人的物资、运输工具以及相关设施、设备。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，尚未实施畜禽产品无纸化出证并实现电子证照互通互认的地区，暂时对在本屠宰企业内分割加工的畜禽产品（   )。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "不再继续出证",
          "label": "A"
        },
        {
          "content": "不再重复出证",
          "label": "B"
        },
        {
          "content": "重复出证",
          "label": "C"
        },
        {
          "content": "继续出证",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "下列关于发生重大动物疫情说法正确的是：（ ）。",
      "standard_answer": "A,B,D",
      "option_list": [
        {
          "content": "发生重大动物疫情可能感染人群时，卫生主管部门应当对疫区内易受感染的人群进行监测，并采取相应的预防、控制措施。",
          "label": "A"
        },
        {
          "content": "卫生主管部门和兽医主管部门应当及时相互通报情况。",
          "label": "B"
        },
        {
          "content": "必要时，当地兽医主管部门可以作出封锁决定并采取扑杀、销毁等措施。",
          "label": "C"
        },
        {
          "content": "有关单位和个人对重大动物疫情不得瞒报、谎报、迟报，不得授意他人瞒报、谎报、迟报，不得阻碍他人报告。",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "国家对重大动物疫情应急处理实行（ ），按照应急预案确定的疫情等级，由有关人民政府采取相应的应急控制措施。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "分级管理",
          "label": "A"
        },
        {
          "content": "垂直管理",
          "label": "B"
        },
        {
          "content": "属地管理",
          "label": "C"
        },
        {
          "content": "属人管理",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "重大动物疫情，是指高致病性禽流感等（ ）高的动物疫病突然发生，迅速传播，给养殖业生产安全造成严重威胁、危害，以及可能对公众身体健康与生命安全造成危害的情形，包括特别重大动物疫情。",
      "standard_answer": "A",
      "option_list": [
        {
          "content": "发病率或者死亡率",
          "label": "A"
        },
        {
          "content": "发病率或者病死率",
          "label": "B"
        },
        {
          "content": "病死率或者死亡率",
          "label": "C"
        },
        {
          "content": "发病率或致死率",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，对(    )的畜禽产品，不再重复出证。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "到达目的地后贮存",
          "label": "A"
        },
        {
          "content": "到达目的地后分销",
          "label": "B"
        },
        {
          "content": "到达目的地后屠宰",
          "label": "C"
        },
        {
          "content": "已取得动物检疫证明",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "多选题",
      "question": "《农村农村部办公厅关于进一步规范畜禽屠宰检疫有关工作的通知》（农办牧〔2022〕31号》规定，应当检疫出证的畜禽胴体包括畜禽经宰杀、放血后除去（   ）的躯体部分，也包括家畜躯体部分的（   ）。",
      "standard_answer": "B,C,D",
      "option_list": [
        {
          "content": "毛、头、尾及四肢后",
          "label": "A"
        },
        {
          "content": "二分体",
          "label": "B"
        },
        {
          "content": "毛、内脏、头、尾及四肢（腕及关节以下）后",
          "label": "C"
        },
        {
          "content": "四分体",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "对不履行或者不按照规定履行重大动物疫情应急处理职责的行为，任何单位和个人有权（ ）。",
      "standard_answer": "B",
      "option_list": [
        {
          "content": "谴责抗议",
          "label": "A"
        },
        {
          "content": "检举控告",
          "label": "B"
        },
        {
          "content": "举报揭发",
          "label": "C"
        },
        {
          "content": "以上都不是",
          "label": "D"
        }
      ]
    },
    {
      "section_name": "单选题",
      "question": "（ ）负责重大动物疫情的监测，饲养、经营动物和生产、经营动物产品的单位和个人应当配合，不得拒绝和阻碍。",
      "standard_answer": "D",
      "option_list": [
        {
          "content": "农业农村主管部门",
          "label": "A"
        },
        {
          "content": "兽医主管部门",
          "label": "B"
        },
        {
          "content": "动物疫病防控机关",
          "label": "C"
        },
        {
          "content": "动物防疫监督机构",
          "label": "D"
        }
      ]
    }
  ]
}